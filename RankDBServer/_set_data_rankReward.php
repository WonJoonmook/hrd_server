<?php
    function openMyRedis() {
        $redis = new Redis();
        try {
            $redis->connect('127.0.0.1', 6379, 2.5);//2.5 sec timeout
            //Auth Password(redis.conf requirepass)
            //$redis->auth('foobared');
            return $redis;
        } catch (Exception $e) {
         //   exit( "Cannot connect to redis server : ".$e->getMessage() );
            return 0;
        }
    }

    $redis = openMyRedis();
    if ( $redis !== 0 ) {
     $redis->del('climbRankRewards');
     
      $redis->rpush('climbRankRewards', 
        "10,5004,400,5000,30000,5005,100,5006,300", 
        "100,5004,300,5000,20000,5005,80,5006,200", 
        "500,5004,250,5000,10000,5005,60,5006,100", 
        "1000,5004,200,5000,8000,5005,40,5006,50", 
        "2000,5004,150,5000,6000,5005,20,5002,5",
        "3000,5004,100,5000,4000,5005,10,5002,5",
        "5000,5004,50,5000,2000,5002,3",
        "9999,5000,1000"
        );

      $redis->close();
  }

?>