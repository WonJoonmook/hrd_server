<?php
include_once('./common/Config.php');
include_once './class/game_service_sender.php';
$game_service_request = new game_service_request();
$userId = $_GET['userId'];
$send_array = null;
$send_array['Protocol'] = 'ReqUserWebView';
$send_array['Sess'] = 'server';
$send_array['Ver'] = '1.0.0';
$send_array['Type'] = 'A';
$send_array['Data']['userId'] = $userId;
$result_send_mail = $game_service_request -> request($send_array);
$Data = $result_send_mail['Data'];
$no190003Cnt = $Data['no190003Cnt'];
$no190004Cnt = $Data['no190004Cnt'];
$no190005Cnt = $Data['no190005Cnt'];
?>


<html lang="ko">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="content-language" content="ja">
  <meta http-equiv="content-style-type" content="text/css">
  <meta http-equiv="content-script-type" content="text/javascript">
  <meta http-equiv="pragma" content="no-cache"/>
  <meta http-equiv="pragma" content="no-store"/>
  <meta http-equiv="cache-control" content="no-cache"/>
  <meta http-equiv="Expires" content="-1"/>
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no,initial-scale=1">

  <title>Event</title>
  <link rel='stylesheet' href='./css/base.css' />
  <link rel='stylesheet' href='./css/detail.css' />
</head>

<body id="home">
  <div class="wrapper">
    <a name="top" id="top"></a>
    <div>
      <img src="./image/top.png" alt="head" width="100%">
    </div>
    <div class="liststyle01">
        <ul> 
		<div style = "position:relative">
		<img width="100%" src="./banners/event_xmas/banner_xmas.png" >

		<strong>	
		<div style = "position:absolute;left:54%;top:47.6%;width:10%"  >
		<font size="2.5" color="white"> <?php echo $no190003Cnt?>	</font>
		</div>

		<div style = "position:absolute;left:68%;top:47.6%;width:10%"  >
		<font size="2.5" color="white"> <?php echo $no190004Cnt?>	</font>
		</div>

		<div style = "position:absolute;left:82%;top:47.6%;width:10%"  >
		<font size="2.5" color="white"> <?php echo $no190005Cnt?>	</font>
		</div>
		</strong>

		<!-- socks -->
		<div style = "position:absolute;left:24%;top:71%;width:8%">
		<?php 
		if ($no190003Cnt >= 10) {
		?>
		<img id= 'e1' width='100%' src='./image/available.png' onclick="location.href='./class/game_send.php?type=1&userId=<? print "$userId"; ?>';" >
		<?php
		} else {
		echo " <img id= 'e1' width='100%' src='./image/disable.png' >";
		} ?>
        </div>

        <div style = 'position:absolute;left:24%;top:79%;width:8%'>
		<?php 
		if ($no190003Cnt >= 30) {
		?>
		<img id= 'e1' width='100%' src='./image/available.png' onclick="location.href='./class/game_send.php?type=2&userId=<? print "$userId"; ?>';" >
		<?php
		} else {
        echo "<img id= 'e1' width='100%' src='./image/disable.png' >";
		} ?>
        </div>

        <div style = 'position:absolute;left:24%;top:87%;width:8%'>
		<?php 
		if ($no190003Cnt >= 50) {
		?>
		<img id= 'e1' width='100%' src='./image/available.png' onclick="location.href='./class/game_send.php?type=3&userId=<? print "$userId"; ?>';" >
		<?php
		} else {
        echo "<img id= 'e1' width='100%' src='./image/disable.png' >";
		} ?>
        </div>

        <!-- hats -->
        <div style = 'position:absolute;left:55%;top:71%;width:8%'>
		<?php 
		if ($no190004Cnt >= 5) {
		?>
		<img id= 'e1' width='100%' src='./image/available.png' onclick="location.href='./class/game_send.php?type=4&userId=<? print "$userId"; ?>';" >
		<?php
		} else {
        echo "<img id= 'e1' width='100%' src='./image/disable.png' >";
		} ?>
        </div>

        <div style = 'position:absolute;left:55%;top:79%;width:8%'>
		<?php 
		if ($no190004Cnt >= 10) {
		?>
		<img id= 'e1' width='100%' src='./image/available.png' onclick="location.href='./class/game_send.php?type=5&userId=<? print "$userId"; ?>';" >
		<?php
		} else {
        echo "<img id= 'e1' width='100%' src='./image/disable.png' >";
		} ?>
        </div>

        <div style = 'position:absolute;left:55%;top:87%;width:8%'>
		<?php 
		if ($no190004Cnt >= 30) {
		?>
		<img id= 'e1' width='100%' src='./image/available.png' onclick="location.href='./class/game_send.php?type=6&userId=<? print "$userId"; ?>';" >
		<?php
		} else {
        echo "<img id= 'e1' width='100%' src='./image/disable.png' >";
		} ?>
        </div>

        <!-- tree -->
        <div style = 'position:absolute;left:86%;top:71%;width:8%'>
		<?php 
		if ($no190005Cnt >= 2) {
		?>
		<img id= 'e1' width='100%' src='./image/available.png' onclick="location.href='./class/game_send.php?type=7&userId=<? print "$userId"; ?>';" >
		<?php
		} else {
        echo "<img id= 'e1' width='100%' src='./image/disable.png' >";
		} ?>
        </div>

        <div style = 'position:absolute;left:86%;top:79%;width:8%'>
		<?php 
		if ($no190005Cnt >= 5) {
		?>
		<img id= 'e1' width='100%' src='./image/available.png' onclick="location.href='./class/game_send.php?type=8&userId=<? print "$userId"; ?>';" >
		<?php
		} else {
        echo "<img id= 'e1' width='100%' src='./image/disable.png' >";
		} ?>
        </div>

        <div style = 'position:absolute;left:86%;top:87%;width:8%'>
		<?php 
		if ($no190005Cnt >= 10) {
		?>
		<img id= 'e1' width='100%' src='./image/available.png' onclick="location.href='./class/game_send.php?type=9&userId=<? print "$userId"; ?>';" >
		<?php
		} else {
        echo "<img id= 'e1' width='100%' src='./image/disable.png' >";
		} ?>
        </div>
		</ul>
    </div>
	  <a href="javascript:void(0)" onclick="Unity.call('Close');">
      <img src="./image/back.png" alt="end" width="20%" style="margin: 0 auto;">
	  </a>
    <div id="Foot_home">
      <div id="Foot_home_inner">
        <p>@ 2016 HoneyJam Games. Heroes Random Defence.</p>
      </div>
    </div>
  </div>
  </body>
</html>


