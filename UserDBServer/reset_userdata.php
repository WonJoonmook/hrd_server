<?php
  include_once("./myAes.php");
  $id = $_REQUEST["id"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  else {
    $db->query("set autocommit=0");

    $query = sprintf("delete from frdUserPost where recvUserId = '%s'", $id);
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    $query = sprintf("delete from frdCharacEvolveExps where userId='%s'",$id);
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    $query = sprintf("delete from frdHavingWeapons where userId='%s'", $id);
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    $query = sprintf("delete from frdHavingArtifacts where userId='%s'", $id);
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    $query = sprintf("delete from frdUserData where privateId='%s'", $id);
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    $query = sprintf("delete from frdSkillLevels where userId='%s'", $id);
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
    $db->query("commit");
    $db->close();
  }
  echo json_encode($data);
?>