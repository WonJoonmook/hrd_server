<?php
	include_once("../myAes.php");

	$redis = openRedis();
  	if ( $redis == false )
  		return;

    $redis->del('effectRewardAB');
    $redis->hset('effectRewardAB', 2, "1002,15000");  //Get More Medal      + 15%
    $redis->hset('effectRewardAB', 6, "1006,20000");  //Get More Gold       + 20%
    $redis->hset('effectRewardAB', 15, "1015,20000");  //Get More Rune       + 20%
    $redis->hset('effectRewardAB', 26, "1006,30000");  //Get More Gold       + 30%
    $redis->hset('effectRewardAB', 36, "1036,15000");  //Get Medal Percent   + 15% 
    $redis->hset('effectRewardAB', 38, "1038,30000");  //Get Rune Percent   + 30%
    $redis->hset('effectRewardAB', 43, "1,25000");  //Get exp Percent   + 25% 
    $redis->hset('effectRewardAB', 46, "1046,20000");  //Get QuestItem Percent   + 20%
    $redis->hset('effectRewardAB', 47, "1,30000");  //Get exp Percent   + 25% 
    
    $redis->del('effectEtcAB');
    $redis->hset('effectEtcAB', 0, "1000,130000");  //Increase_TempleOutput + 1.3배
    $redis->hset('effectEtcAB', 4, "1004,391");     //Get_LegendWeapon
    $redis->hset('effectEtcAB', 8, "1008,30000");   //BonusScore_ClimbTop   + 30%
    $redis->hset('effectEtcAB', 12, "1012,7500");   //Sale_BlackMarket      + 7.5%
    $redis->hset('effectEtcAB', 14, "1004,394");     //Get_LegendWeapon
    $redis->hset('effectEtcAB', 17, "1017,30000");  //Get More Powder when extraction       + 30%
    $redis->hset('effectEtcAB', 18, "1004,390");     //Get_LegendWeapon
    $redis->hset('effectEtcAB', 19, "1019,690");     //Get_LegendMagic
    $redis->hset('effectEtcAB', 28, "1004,393");     //Get_LegendWeapon
    $redis->hset('effectEtcAB', 31, "1031,10000");     //Reduce_WeaponLvUpPrice
    $redis->hset('effectEtcAB', 32, "1032,15000");     //Increase_MaxHeart
    $redis->hset('effectEtcAB', 34, "1004,396");     //Get_LegendWeapon
    $redis->hset('effectEtcAB', 41, "1041,15000");   //Increase_MagicLvUpPercent
    $redis->hset('effectEtcAB', 49, "1004,395");     //Get_LegendWeapon
    
    //Only Local
    //$redis->hset('only local', 1, "1001,5");
    //$redis->hset('only local', 3, "1003,10");
    //$redis->hset('only local', 5, "1005,10000");
   	//$redis->hset('only local', 7, "1005,20000");
    //$redis->hset('only local', 9, "1009,386");
    //$redis->hset('only local', 11, "1010,15000");
    //$redis->hset('only local', 10, "1011,0");
    //$redis->hset('only local', 13, "1013,385");
    //$redis->hset('only local', 16, "1016,0");
    echo "Comp!";
	$redis->close();
?>