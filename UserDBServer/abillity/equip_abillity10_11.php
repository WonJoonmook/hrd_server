<?php
	include_once("../myAes.php");
	include_once("./_calculate.php");
	include_once("./applyAbillity8_4.php");

	$maxSlotCount = 25;

	$id = $_REQUEST["id"];
	$session = (int)$_REQUEST["session"];
	$abillityId = (int)$_REQUEST["abillityId"];
	
	$data = array();
	$data["error"] = 0;
	$db = getDB();
	if (mysqli_connect_errno()) {
	  echo 0;
	  return;
	}

	$redis = openRedis();
	if ( $redis == false ) {
		echo 0;
		$db->close();
		return;
	}

	$db->query("set autocommit=0");
	$query = "select session from frdUserData where privateId = $id";
	$res = $db->query($query);
	if ($res == false) {
		echo 0;$db->close();return;
	}
  	if ($res->num_rows <= 0) {
  		echo 0;$db->close();return;
  	}
    $row = $res->fetch_assoc();
    if ( (int)$row["session"] !== $session ) {
    	echo 1;$db->close();return;
    }

	if ( $abillityId < 63 ) {
		$bundleIdx = 0;
		$mask = 1 << $abillityId;
		$bundleName = "bundle0";
		$usingBundleName = "usingBundle0";
	}
	else {
		$bundleIdx = 1;
		$mask = 1 << ($abillityId-64);
		$bundleName = "bundle1";
		$usingBundleName = "usingBundle1";
	}

	$query = sprintf("select openedSlot, %s, usingBundle0, usingBundle1 from frdAbillity where userId=%d", $bundleName, $id);
	$res = $db->query($query);
	if ($res == false) {
		echo 0;
		$db->query("rollback");
		$db->close();
		return;
	}

	if ( $res->num_rows <= 0 ) {
		echo 1;
		addBlacklist($id, "hack_EquipAbillity");
      	$db->close();
      	return;
	}
	
	$row = $res->fetch_assoc();									//보유중인 어빌리티 아닐시 핵
	$resultMask = $row[$bundleName] & $mask;
	if ( $resultMask <= 0 ) {
		echo 1;
		addBlacklist($id, "hack_EquipAbillity2");
      	$db->close();
      	return;
	}
	
	$openSlotVal = $row["openedSlot"];
	$openedSlotCount=0;											// 오픈된 슬롯 카운트 측정
	for ( $i=0; $i<$maxSlotCount; $i++ ) {
		if ( ($openSlotVal & 1) == 1 )
			$openedSlotCount++;

		$openSlotVal >>= 1;
	}

	$val0 = $row["usingBundle0"];
	$val1 = $row["usingBundle1"];
	$usingSlotCount=0;
	for ( $i=0; $i<64; $i++ ) {									// 사용중인 슬롯 카운트 측정
		if ( ($val0 & 1) == 1 )
			$usingSlotCount += GetAbillityNeedSlot($i);
		$val0 >>= 1;

		if ( ($val1 & 1) == 1 )
			$usingSlotCount += GetAbillityNeedSlot($i+64);
		$val1 >>= 1;
	}

	$needSlotCount = GetAbillityNeedSlot($abillityId);
	if ( $needSlotCount + $usingSlotCount > $openedSlotCount ) {
		echo 1;
		addBlacklist($id, "EquipAbillity_LackOfStorage");
      	$db->close();
      	return;
	}

	$resultUsingMask = $row[$usingBundleName] | $mask;			//장착
	$query = sprintf("update frdAbillity set %s='%s' where userId=%d", $usingBundleName, $resultUsingMask, $id);
	$isGood = $db->query($query);
	if ($isGood == false) {
		echo 0;
		$db->query("rollback");
		$db->close();
		return;
	}

	$sp = $redis->hget('effectRewardAB', $abillityId);
    if ( $sp !== false ) {
    	$pieces = explode (",", $sp);
    	$type = (int)$pieces[0];
    	$val = (int)$pieces[1];

    	$query = sprintf("select * from frdEffectForRewards where userId=%d and type=%d",$id, $type);
	    $sres = $db->query($query);
	    if ($sres == false) {
	      echo 0;
	      $db->query("rollback");
	      $db->close();
	      return;
	    }

	    if ( $sres->num_rows <= 0 ) {
	    	$query = sprintf("insert into frdEffectForRewards values (%d, %d, %d)", $id, $type, $val);
	    }
	    else {
	    	$query = sprintf("update frdEffectForRewards set val=val+%d where userId=%d and type=%d", $val, $id, $type);
	    }
	    $isGood = $db->query($query);
		if ($isGood == false) {
	      echo 0;
	      $db->query("rollback");
	      $db->close();
	      return;
	    }
    }

    $sp = $redis->hget('effectEtcAB', $abillityId);
    if ( $sp !== false ) {
    	$pieces = explode (",", $sp);
    	$type = (int)$pieces[0];
    	$val = (int)$pieces[1];

    	if ( false == ApplyAbillity($db, $id, $data, $type, $val, true) ) {
    		echo 0;
	    	$db->query("rollback");
	    	$db->close();
	    	return;
    	}

    	$query = sprintf("select * from frdEffectForEtc where userId=%d and type=%d",$id, $type);
	    $sres = $db->query($query);
	    if ($sres == false) {
	      echo 0;
	      $db->query("rollback");
	      $db->close();
	      return;
	    }

	    if ( $sres->num_rows <= 0 ) {
	    	$query = sprintf("insert into frdEffectForEtc values (%d, %d, %d)", $id, $type, $val);
	    }
	    else {
	    	$query = sprintf("update frdEffectForEtc set val=val+%d where userId=%d and type=%d", $val, $id, $type);
	    }
	    $isGood = $db->query($query);
		if ($isGood == false) {
	      echo 0;
	      $db->query("rollback");
	      $db->close();
	      return;
	    }
    }

    if ( $bundleIdx == 0) {
    	$data["usingBundle0"] = $resultUsingMask;
    	$data["usingBundle1"] = $row["usingBundle1"];
    }
    else {
    	$data["usingBundle0"] = $row["usingBundle0"];
    	$data["usingBundle1"] = $resultUsingMask;
    }

    $newSession = mt_rand();
    $data["session"] = $newSession;
    $query = "update frdUserData set session=$newSession where privateId=$id";
	$isGood = $db->query($query);
	if ($isGood == false) {
		echo 0;$db->query("rollback");$db->close();return;
	}

	$db->query("commit");
	$db->close(); 

  	$keyAndIv = formatTo16String($session);
  	echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
