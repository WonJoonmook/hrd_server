<?php
	include_once("../temple/_calculate10_5.php");

	function ApplyAbillity(&$db, $id, &$data, $abillityType, $val, $isEquip) {
		switch($abillityType) {
		case 1000:		//Increase_TempleOutput
			$query = sprintf("select templeId, templeLevel, lastRecvTime, keepAmount, keepFlowTime from frdTemple where userId = %d", $id);
			$res = $db->query($query);
			if ($res == false) 
				return false;
			
			$flowTime = array();
			$amount = array();
			$templeId = array();
			for ($idx = 0; $idx < $res->num_rows; $idx++) {
				$res->data_seek($idx);
				$row = $res->fetch_assoc();

				$tId = (int)$row["templeId"];
				$ft = (time() - (int)$row["lastRecvTime"]);
				$am = GetTempleReward($db, $id, $tId, (int)$row["templeLevel"], $ft, (int)$row["keepAmount"], (int)$row["keepFlowTime"]);
				if ( $am < 0 ) 
					return false;
				$templeId[] = $tId;
				$flowTime[] = $ft;
				$amount[] = $am;
			}
			$query = "update frdTemple set keepAmount = case ";
			for ( $i=0; $i<count($templeId); $i++ )
				$query .= sprintf("when templeId=%d then %d ", $templeId[$i], $amount[$i]);
			$query .= "else keepAmount end, keepFlowTime = case ";
			for ( $i=0; $i<count($templeId); $i++ )
				$query .= sprintf("when templeId=%d then %d ", $templeId[$i], $flowTime[$i]);
			$query .= sprintf("else keepFlowTime end where userId in (%d)", $id);

			$isGood = $db->query($query);
			if ($isGood == false) 
				return false;
			return true;
		

		case 1004:		//legendWeapon
			if ( $isEquip ) {
		        $query = sprintf("select weaponIdPointer from frdUserData where privateId = %d", $id);
		        $res = $db->query($query);
		        if ($res == false)
		        	return false;
		          
		        $row = $res->fetch_assoc();
		        $weaponIdPointer = $row["weaponIdPointer"];

		        $stat = 9999;
		        $query = sprintf("insert into frdHavingWeapons values (%d, %d, %d, 9999999, %d)", $weaponIdPointer, $id, $val, $stat);
		        $isGood = $db->query($query);
		        if ($isGood == false) 
		          return false;
		        
		        $data["weaponIdPointer"] = $weaponIdPointer;
		        $data["weaponId"] = $val;
		        $data["stat"] = $stat;

		        $weaponIdPointer++;
		        $query = sprintf("update frdUserData set weaponIdPointer=%d where privateId=%d", $weaponIdPointer, $id);
		        $isGood = $db->query($query);
		        if ($isGood == false)
		        	return false;
		    }
		    else {
		    	$query = sprintf("delete from frdHavingWeapons where userId=%d and weaponId=%d", $id, $val);
				$isGood = $db->query($query);
				if ($isGood == false) 
					return false;
		    }
			return true;

		case 1019:		//legendMagic
			if ( $isEquip ) {
		        $query = sprintf("select artifactIdPointer from frdUserData where privateId = %d", $id);
		        $res = $db->query($query);
		        if ($res == false)
		        	return false;
		          
		        $row = $res->fetch_assoc();
		        $artifactIdPointer = $row["artifactIdPointer"];

		        $query = sprintf("insert into frdHavingArtifacts values (%d, %d, %d, 5, 0)", $artifactIdPointer, $id, $val);
		        $isGood = $db->query($query);
		        if ($isGood == false) 
		          return false;
		        
		        $data["artifactIdPointer"] = $artifactIdPointer;
		        $data["artifactId"] = $val;

		        $artifactIdPointer++;
		        $query = sprintf("update frdUserData set artifactIdPointer=%d where privateId=%d", $artifactIdPointer, $id);
		        $isGood = $db->query($query);
		        if ($isGood == false)
		        	return false;
		    }
		    else {
		    	$query = sprintf("delete from frdHavingArtifacts where userId=%d and itemId=%d", $id, $val);
				$isGood = $db->query($query);
				if ($isGood == false) 
					return false;
		    }
			return true;

		default:		//powder
			return true;
		}
	}

	

?>