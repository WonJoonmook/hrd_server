<?php
	function GetCantUnlockSlotValue() {
		$val= 9223372036854775807;	// 2^63 -1
		return $val;
	}

	function GetAbillityNeedSlot($id) {
		switch($id) {
		case 0: return 1;
		case 1: return 3;
		case 2: return 2;
		case 3: return 1;
		case 4: return 3;

		case 5: return 2;
		case 6: return 2;
		case 7: return 2;
		case 8: return 4;
		case 9: return 2;

		case 10: return 1;
		case 11: return 2;
		case 12: return 3;
		case 13: return 2;
		case 14: return 3;

		case 15: return 3;
		case 16: return 1;
		case 17: return 2;
		case 18: return 3;
		case 19: return 5;

		case 20: return 2;
		case 21: return 3;
		case 22: return 2;
		case 23: return 4;
		case 24: return 2;

		case 25: return 2;
		case 26: return 3;
		case 27: return 2;
		case 28: return 3;
		case 29: return 2;

		case 30: return 2;
		case 31: return 4;
		case 32: return 3;
		case 33: return 2;
		case 34: return 3;

		case 35: return 2;
		case 36: return 3;
		case 37: return 2;
		case 38: return 4;
		case 39: return 2;

		case 40: return 2;
		case 41: return 4;
		case 42: return 2;
		case 43: return 3;
		case 44: return 2;

		case 45: return 2;
		case 46: return 4;
		case 47: return 5;
		case 48: return 2;
		case 49: return 3;
		default:		//powder
			return 1;
		}
	}

?>