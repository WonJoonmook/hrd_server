<?php
	include_once("../myAes.php");
	include_once("../consumeResource.php");
	include_once("./_calculate.php");

	$id = $_REQUEST["id"];
	$slotIdx = (int)$_REQUEST["slotIdx"];

	function GetUnLockMats($slotIdx) {
		switch($slotIdx) {
		case 0:
			return array(5000,500);
		case 4:
			return array(5004,100);
		case 5:
			return array(5004,300);
		case 6:
			return array(115001,200);
		case 7:
			return array(115003,100);
		case 8:
			return array(5000,100000);
		case 9:
			return array(5000,300000);
		case 10:
			return array(5005,5000);
		case 11:
			return array(5006,10000);
		case 12:
			return array(110000,10);
		case 13:
			return array(110000,30);
		case 14:
			return array(5000,50000);
		case 15:
			return array(5000,100000);
		case 17:
			return array(106000,1,106001,1,106002,1,106003,1,106004,1,106005,1,106006,1,106007,1,106008,1,106009,1);
		case 18:
			return array(5002,50);
		case 19:
			return array(110000,100);
		case 20:
			return array(5004,500);
		case 22:
			return array(5004,1000);
		case 23:
			return array(5005,10000);
		case 24:
			return array(5004,150);
		default:
			return null;
		}
	}

	$data = array();
	$data["error"] = 0;
	$db = getDB();
	if (mysqli_connect_errno()) {
	  echo 0;
	  return;
	}

	$db->query("set autocommit=0");


	$mats = GetUnLockMats($slotIdx);
  	$matCount = (int)(count($mats)/2);
  	for ( $i=0; $i<$matCount; $i++ ) {
	    $rewardType = $mats[$i*2];
	    $rewardCount = $mats[$i*2+1];

	    $resultVal = ConsumeResource($db, $id, $i, $rewardType, $rewardCount);
	    $data["consumeType"][] = $rewardType;
	    $data["consumeCount"][] = $rewardCount;
	    if ( $resultVal == 0) {
	      echo 0;
	      $db->query("rollback");
	      $db->close();
	      return;
	    }
	    else if ( $resultVal == -1 ) {
	      addBlacklist($id, "cantTempleLvUp_Lack");
	      echo 1;
	      $db->query("rollback");
	      $db->close();
	      return;
	    }
  	}

  	$data["consumeType"][] = 5000;
	$data["consumeCount"][] = 1;
	$data["consumeType"][] = 5000;
	$data["consumeCount"][] = -1;

	$query = sprintf("select openedSlot from frdAbillity where userId=%d", $id);
	$res = $db->query($query);
	if ($res == false) {
		echo 0;
		$db->query("rollback");
		$db->close();
		return;
	}

	if ( $res->num_rows <= 0 ) {
		$query = sprintf("insert into frdAbillity values (%d, 0, %d, 0, 0, 0, 0)", $id, GetCantUnlockSlotValue());
		$isGood = $db->query($query);
		if ($isGood == false) {
			echo 0;
			$db->query("rollback");
			$db->close();
			return;
		}
		$openedSlot = 0;
	}
	else {
		$row = $res->fetch_assoc();
		$openedSlot = $row["openedSlot"];
	}

	$openedSlot |= (1<<$slotIdx);
	$query = sprintf("update frdAbillity set openedSlot=%d where userId=%d", $openedSlot, $id);
	$isGood = $db->query($query);
	if ($isGood == false) {
		echo 0;
		$db->query("rollback");
		$db->close();
		return;
	}

	$data["openedSlot"] = $openedSlot;
	$db->query("commit");
	$db->close();
  	echo json_encode($data);


?>
