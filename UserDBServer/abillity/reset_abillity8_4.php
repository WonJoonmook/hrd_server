<?php
	include_once("../myAes.php");
	include_once("./_calculate.php");
	include_once("./applyAbillity8_4.php");

	$id = $_REQUEST["id"];
	$price = 10;

	function RemEffectAB(&$redis, &$db, $id, &$data, $abillityId) {
	//	echo $abillityId."!!\n";
		$sp = $redis->hget('effectRewardAB', $abillityId);
	    if ( $sp !== false ) {
	    	$pieces = explode (",", $sp);
	    	$type = (int)$pieces[0];
	    	$val = (int)$pieces[1];

	    	$query = sprintf("select val from frdEffectForRewards where userId=%d and type=%d",$id, $type);
		    $sres = $db->query($query);
		    if ($sres == false) {
		      echo 0;
		      $db->query("rollback");
		      $db->close();
		      return;
		    }
		//    echo $sres->num_rows.", ".$type.", ".$val."\n";
		    if ( $sres->num_rows <= 0 ) {
		    	return;
		    }

		    $srow = $sres->fetch_assoc();
		    $resultVal = $srow["val"] - $val;
		    if ( $resultVal > 0 )
		    	$query = sprintf("update frdEffectForRewards set val=val-%d where userId=%d and type=%d", $val, $id, $type);
		    else
		    	$query = sprintf("delete from frdEffectForRewards where userId=%d and type=%d", $id, $type);

		    $isGood = $db->query($query);
			if ($isGood == false) {
		      echo 0;
		      $db->query("rollback");
		      $db->close();
		      return;
		    }
    	}

    	$sp = $redis->hget('effectEtcAB', $abillityId);
	    if ( $sp !== false ) {
	    	$pieces = explode (",", $sp);
	    	$type = (int)$pieces[0];
	    	$val = (int)$pieces[1];

	    	if ( false == ApplyAbillity($db, $id, $data, $type, $val, false) ) {
	    		echo 0;
		    	$db->query("rollback");
		    	$db->close();
		    	return;
	    	}

	    	$query = sprintf("select val from frdEffectForEtc where userId=%d and type=%d",$id, $type);
		    $sres = $db->query($query);
		    if ($sres == false) {
		      echo 0;
		      $db->query("rollback");
		      $db->close();
		      return;
		    }
		//  echo $sres->num_rows.", ".$type.", ".$val."\n";
		    if ( $sres->num_rows <= 0 ) {
		    	return;
		    }

		    $srow = $sres->fetch_assoc();
		    $resultVal = $srow["val"] - $val;
		    if ( $resultVal > 0 )
		    	$query = sprintf("update frdEffectForEtc set val=val-%d where userId=%d and type=%d", $val, $id, $type);
		    else
		    	$query = sprintf("delete from frdEffectForEtc where userId=%d and type=%d", $id, $type);

		    $isGood = $db->query($query);
			if ($isGood == false) {
		      echo 0;
		      $db->query("rollback");
		      $db->close();
		      return;
		    }
    	}
	}



	$data = array();
	$data["error"] = 0;
	$db = getDB();
	if (mysqli_connect_errno()) {
	  echo 0;
	  return;
	}

	$db->query("set autocommit=0");



	$query = sprintf("select usingBundle0, usingBundle1 from frdAbillity where userId=%d", $id);
	$res = $db->query($query);
	if ($res == false) {
		echo 0;
		$db->query("rollback");
		$db->close();
		return;
	}

	$row = $res->fetch_assoc();
	$val0 = $row["usingBundle0"];
	$val1 = $row["usingBundle1"];
	$usingSlotCount=0;

	$redis = openRedis();
	if ( !$redis ) {
		echo 0;
		$db->close();
		return;
	}
	for ( $i=0; $i<64; $i++ ) {									// 사용중인 슬롯 카운트 측정
		if ( ($val0 & 1) > 0 )
			RemEffectAB($redis, $db, $id, $data, $i);
		$val0 >>= 1;

		if ( ($val1 & 1) > 0 )
			RemEffectAB($redis, $db, $id, $data, $i+64);
		$val1 >>= 1;
	}
	$redis->close();

	$query = sprintf("update frdAbillity set usingBundle0=0, usingBundle1=0 where userId=%d", $id);
	$isGood = $db->query($query);
	if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }



    $query = sprintf("select itemCount from frdHavingItems where userId = %d and itemId=%d", $id, 100002);// 100002 = pass_ResetABSlot
    $sres = $db->query($query);
    if ($sres == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
    if ( $sres->num_rows > 0 ) {

		$srow = $sres->fetch_assoc();
		$data["restItem"] = (int)$srow["itemCount"] -1;
		if ( $data["restItem"] <= 0 )
			$query = sprintf("delete from frdHavingItems where userId=%d and itemId=%d", $id, 100002);
		else
			$query = sprintf("update frdHavingItems set itemCount=%d where userId=%d and itemId=%d", $data["restItem"], $id, 100002);

		$isGood = $db->query($query);
		if ($isGood == false) {
			echo 0;
			$db->query("rollback");
			$db->close();
			return;
		}

	}
  else {

    $query = sprintf("select jewel from frdUserData where privateId = %d", $id);
    $res = $db->query($query);
    if ($res == false) {
    	echo 0;
	    $db->query("rollback");
	    $db->close();
	    return;
    }

    $row = $res->fetch_assoc();
    $restJewel = $row["jewel"] - $price;
    if ( $restJewel < 0 ) {
      addBlacklist($id, "reset_abillity_NoJewel");
      echo 1;
      $db->query("rollback");
      $db->close();
    }

		include_once("../EventAccuJewel.php");
		$resultCode = AccuUseJewel($db, $id, $price);
		if ($resultCode == 0) {
			echo 0;
			$db->query("rollback");
			$db->close();
			return;
		}

		$query = sprintf("update frdUserData set jewel=%d where privateId=%d", $restJewel, $id);
		$isGood = $db->query($query);
		if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
		}

		$data["jewel"] = $restJewel;
  }

	$db->query("commit");
	$db->close();
	echo json_encode($data);

?>
