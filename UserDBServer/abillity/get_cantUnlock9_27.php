<?php
	include_once("../myAes.php");
	include_once("./_calculate.php");

	$id = $_REQUEST["id"];

	$db = getDB();
	$data["error"] = 0;
	if (mysqli_connect_errno()) {
	  echo 0;
	  return;
	}

	$db->query("set autocommit=0");

	$query = sprintf("select cantUnlockSlot from frdAbillity where userId=%d", $id);
   	$res = $db->query($query);
   	if ($res == false) {
   		echo 0;$db->close();
		return;
	}

	if ( $res->num_rows <= 0 ) {
		$query = sprintf("insert into frdAbillity values (%d, 0, %d, 0, 0, 0, 0)", $id, GetCantUnlockSlotValue());
		$isGood = $db->query($query);
	   	if ($isGood == false) {
	   		echo 0;$db->query("rollback");$db->close();
			return;
		}
		$cantUnlockSlotVal = GetCantUnlockSlotValue();
	}
	else {
		$row = $res->fetch_assoc();
		$cantUnlockSlotVal = $row["cantUnlockSlot"];
	}

	$resultVal = $cantUnlockSlotVal;
	for ( $i=0; $i<25; $i++ ) {

		if ( ($cantUnlockSlotVal & (1<<$i)) > 0 ) {
		//	echo "FF!!\n";
			$isUpdate=false;
			switch((int)$i) {
			case 1:case 2:case 3:
				$query = sprintf("select exp from frdUserData where privateId=%d", $id);
				$res = $db->query($query);
				if ($res == false) {
					echo 0;$db->query("rollback");$db->close();
					return;
				}

				if ( $i == 1 ) $needLevel = 13-1;
				else if ( $i == 2 ) $needLevel = 33-1;
				else $needLevel = 53-1;

				if ( $redis == null ) {
					$redis = openRedis();
					if ( $redis == false ) {
						echo 0;$db->query("rollback");$db->close();
						return;
					}
				}
				$row = $res->fetch_assoc();
				$needExp = $redis->lindex('userNeedExpAccu1', $needLevel);
				if ( $row["exp"] >= $needExp )
					$isUpdate = true;
				break;

			case 8:
				$characId = 134;
				$bundleIdx = ($characId >> 3);
				$query = sprintf("select exps from frdCharacEvolveExps where userId=%d and bundleIdx=%d", $id, $bundleIdx);
				$res = $db->query($query);
				if ($res == false) {
					echo 0;$db->query("rollback");$db->close();
					return;
				}
				if ( $res->num_rows > 0 ) {
					$row = $res->fetch_assoc();
					$longData = $row["exps"];
					$bitIdx = 8*($characId&7);
            		$preExp = ( ($longData >> $bitIdx) & 0xff);
            		if ( $preExp >= 10 )
            			$isUpdate = true;
				}
				break;

			case 9:
				$goddessId = 10;
				$query = sprintf("select level from frdGoddess where userId=%d and goddessId=%d", $id, $goddessId);
				$res = $db->query($query);
				if ($res == false) {
					echo 0;$db->query("rollback");$db->close();
					return;
				}
				if ( $res->num_rows > 0 )
            		$isUpdate = true;
				break;

			case 14:
				$query = sprintf("select bestRank, bestTotalUserCount from frdClimbTopData where userId=%d", $id);
				$res = $db->query($query);
				if ($res == false) {
					echo 0;$db->query("rollback");$db->close();
					return;
				}
				if ( $res->num_rows > 0) {
					$row = $res->fetch_assoc();
					if ( ($row["bestRank"] / $row["bestTotalUserCount"]) < 0.05 )
						$isUpdate = true;
				}
				break;

			case 15:
				$query = sprintf("select level from frdAbyss where userId=%d", $id);
				$res = $db->query($query);
				if ($res == false) {
					echo 0;$db->query("rollback");$db->close();
					return;
				}
				$row = $res->fetch_assoc();
				if ( $res->num_rows > 0 && $row["level"] >= 1030 )
					$isUpdate = true;
				break;

			case 16:
				$query = sprintf("select id from frdLogBuy where id=%d and productId='package_abyss'", $id);
				$res = $db->query($query);
				if ($res == false) {
					echo 0;$db->query("rollback");$db->close();
					return;
				}
				if ( $res->num_rows > 0 )
					$isUpdate = true;
				break;

			case 18:
				$query = sprintf("select clearCount from frdClearCount where userId=%d and stageId>=2100 and stageId<5000", $id);
				$res = $db->query($query);
				if ($res == false) {
					echo 0;$db->query("rollback");$db->close();
					return;
				}

				$playCount = 0;
				for ( $j=0; $j<$res->num_rows; $j++ ) {
					$res->data_seek($j);
					$row = $res->fetch_assoc();
					$playCount += $row["clearCount"];
				}
				if ( $playCount >= 100 )
					$isUpdate = true;
				break;

			case 21:
				$query = sprintf("select id from frdLogBuy where id=%d and productId='package_event_abillity'", $id);
				$res = $db->query($query);
				if ($res == false) {
					echo 0;$db->query("rollback");$db->close();
					return;
				}
				if ( $res->num_rows > 0 )
					$isUpdate = true;
				break;

			case 23:
				$query = sprintf("select accuSkillPoint from frdSkillPoints where userId=%d", $id);
				$res = $db->query($query);
				if ($res == false) {
					echo 0;$db->query("rollback");$db->close();
					return;
				}
				$row = $res->fetch_assoc();
				if ( $res->num_rows > 0 && $row["accuSkillPoint"] >= 50000 )
					$isUpdate = true;
				break;

			case 24:
				$query = sprintf("select clearCount from frdClearCount where userId=%d and stageId>=1 and stageId<500", $id);
				$res = $db->query($query);
				if ($res == false) {
					echo 0;$db->query("rollback");$db->close();
					return;
				}

				$playCount = 0;
				for ( $j=0; $j<$res->num_rows; $j++ ) {
					$res->data_seek($j);
					$row = $res->fetch_assoc();
					$playCount += $row["clearCount"];
				}
				if ( $playCount >= 100 )
					$isUpdate = true;
				break;

			default:
				$isUpdate = true;
				break;
			}


			if ( $isUpdate ) {
				$resultVal -= (1<<$i);
			}
			else {
				$isCantUnlock[] = $i;
			}
		}

	}


	if ( $resultVal !== $cantUnlockSlotVal ) {
		$query = "update frdAbillity set cantUnlockSlot=$resultVal where userId=$id";
		$isGood = $db->query($query);
		if ($isGood == false) {
			echo 0;
			$db->query("rollback");
			$db->close();
			return;
		}
	}


	$data["isCantUnlock"] = $isCantUnlock;
	$db->query("commit");
	$db->close();
	if ( $redis !== null )
		$redis->close();

  	echo json_encode($data);

?>
