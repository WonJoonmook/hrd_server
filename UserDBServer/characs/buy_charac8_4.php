<?php
  // 캐릭 정보창에서 캐릭 구매 요청 ( $idx는 버튼 번호인데 구매 버튼은 최대 2개까지 있습니다. )
  include_once("../myAes.php");
  $session = $_REQUEST["session"];
  $id = (int)$_REQUEST["id"];
  $idx = (int)$_REQUEST["idx"];
  $characId = (int)$_REQUEST["type"];
  $price = (int)$_REQUEST["price"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }

  $db->query("set autocommit=0");
  if ( $idx > 1) {
    echo 0;
    $db->close();
    return;
  }
  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    $db->close();
    return;
  }
  $prices = json_decode( $redis->get("characPrice".$characId) );
  $redis->close();

  $realPrice = (int)($prices[$idx]);

  if ( (int)$price !== $realPrice ) {
    addBlacklist($id, "buy_charac_noMatchPrice");
    echo 1;
    $db->close();
    return;
  }

  if ( $realPrice > 200000000 ) {
    $realPrice -= 200000000;
    $resourceName = "powder";
  }
  else if ( $realPrice > 100000000 ) {
    $realPrice -= 100000000;
    $resourceName = "jewel";

		include_once("../EventAccuJewel.php");
		$resultCode = AccuUseJewel($db, $id, $realPrice);
		if ($resultCode == 0) {
			echo 0;
	    $db->query("rollback");
	    $db->close();
	    return;
		}
  }
  else
    $resourceName = "gold";

  $query = sprintf("select %s, session from frdUserData where privateId = '%s'",$resourceName, $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;

      $resultGold = $row[$resourceName]-$realPrice;
      if ( $resultGold < 0) {
        addBlacklist($id, "buy_charac_notEnoughMoney");
        echo 1;
        $db->close();
        return;
      }

      $query = sprintf("update frdUserData set %s=%d, session=%d where privateId='%s'", $resourceName, $resultGold, $newSession, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      $data["resultId"] = $characId;
      $data[$resourceName] = $resultGold;

      $bundleIdx = (int)($characId>>3);
      $query = sprintf("select exps from frdCharacEvolveExps where userId='%s' and bundleIdx=%d",$id, $bundleIdx);
      $sres = $db->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      if ($sres->num_rows > 0) {
        $row = $sres->fetch_assoc();
        $longData = $row["exps"];
        $bitIdx = 8*($characId%8);
        $preExp = ( ($longData >> $bitIdx) & 255);

        $mask = $preExp;
        $mask <<= $bitIdx;
        $longData -= $mask;

        $mask = ($preExp + 10);
        if ( $mask > 255 )
          $mask = 255;
        $data["resultCount"] = $mask;
        $mask <<= $bitIdx;
        $resultData = ($longData | $mask);

        $query = sprintf("update frdCharacEvolveExps set exps='%s' where userId=%d and bundleIdx=%d", $resultData, $id, $bundleIdx);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

      }
      else {
        $resultData = 10 << (8*($characId%8));
        $query = sprintf("insert into frdCharacEvolveExps values ('%s', %d, '%s')", $id, $bundleIdx, $resultData);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $data["resultCount"] = 10;

      }
      $sres->close();
    }
    else {
      addBlacklist($id, "buy_charac_session");
      echo 1;
      $db->close();
      return;
    }
  }
  $res->close();
  $db->query("commit");
  $db->close();

  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
