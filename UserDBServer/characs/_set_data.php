<?php
//	include_once("../myAes.php");

  	function openRedis() {
        $redis = new Redis();
        try {
            $redis->connect('127.0.0.1', 6379, 2.5);//2.5 sec timeout
            //Auth Password(redis.conf requirepass)
            //$redis->auth('foobared');
            return $redis;
        } catch (Exception $e) {
         //   exit( "Cannot connect to redis server : ".$e->getMessage() );
            return 0;
        }
    }

	$redis = openRedis();
  	if ( $redis !== 0 ) {
  		$prePrice = 0;
  		$redis->set('testGit', "1000");
  	//	$redis->set('isSaleCharac', "50,50,0");
        $redis->set('isSaleCharac', "");

  		$redis->del('blockCharacInShop');
	    $redis->rpush('blockCharacInShop',
	      "75",
	      "193",

	      "284",
	      "285",

	      "286",
	      "386",
	      "387"
	     );


  		$redis->del('dailyRewardType');
	    $redis->rpush('dailyRewardType',
	      "5003",
	      "5000,1501",
	      "5000,5005",
	      "5005,1501",
	      "5004",
	      "5004",
	      "5000"
	      );

	    $redis->del('dailyRewardAmount');
	    $redis->rpush('dailyRewardAmount',
	      "15",
	      "600,1",
	      "500,10",
	      "10,1",
	      "5",
	      "5",
	      "400"
	      );

	    $redis->del('characMinIdx');			//7_1
	    $redis->rpush('characMinIdx',
	      0,
	      0,
	      10,
	      50,
	      100,
	      200,
	      300,
	      390
	      );

	    $redis->del('characMaxIdx');
	    $redis->rpush('characMaxIdx',
	      0,
	      7,
	      23,
	      75,
	      141,
	      234,
	      308
	      );


	    $redis->del('characHiddenStartIdx');
	    $redis->rpush('characHiddenStartIdx',
	      0,
	      9,
	      49,
	      98,
	      192,
	      283,
	      378
	      );

	    $redis->del('chargingCharacs');
	    $redis->rpush('chargingCharacs',
	      75,
	      136,
	      137,
	      138,
	      139,
	      140,
	      141,
	      224,
	      225,
	      226,
	      227,
	      229,
	      230,
	      231,
	      232,
	      233,
	      234,
	      308
	      );

	 	$p = 0;
  		for ( $i=0; $i<400; $i++ ) {
  			$price = GetCharacBuyPrice($i);
  			if ( $price !== $prePrice ) {
  				$prePrice = $price;
  				$data[0] = $i;
  				$data[1] = $price;
  				$resultData[$p++] = $data;
  			}
  			$redis->set('characPrice'.$i, json_encode($price));
  		}

  		echo json_encode($resultData);
	    $redis->set('characPrices', json_encode($resultData));

	    $redis->close();
	}


	function GetCharacBuyPrice($characId) {
		switch($characId) {
		case 50:
			$data[0] = 10;
			break;
		case 75:
			$data = null;
			break;

		case 98:
			$data[0] = 100000140;
			break;

		case 100:case 101:case 102:case 103:case 104:case 105:case 106:case 107:case 108:case 109:
		case 110:case 111:case 112:case 113:case 114:case 115:case 116:case 117:case 118:case 119:
		case 120:case 121:case 122:case 123:case 124:case 125:case 126:case 127:case 128:case 129:
		case 130:case 131:case 132:case 133:case 135:
			$data[0] = 1000;
			$data[1] = 200000150;
			break;

		case 136:case 137:case 138:case 139:case 140:case 141:
			$data[0] = 100000140;
			$data[1] = 200000750;
			break;

		case 192:case 194:case 195:case 196:case 197:case 198:case 199:
			$data[0] = 200000750;
			$data[1] = 100000140;
			break;

		case 200:case 201:case 202:case 203:case 204:case 205:case 206:case 207:case 208:case 209:
		case 210:case 211:case 212:case 213:case 214:case 215:case 216:case 217:case 218:case 219:
		case 220:case 221:case 222:case 223:case 228:
			$data[0] = 3000;
			$data[1] = 200000300;
			break;

		case 224:case 225:case 226:case 227:case 229:case 230:case 231:case 232:case 233:case 234:
			$data[0] = 100000260;
			$data[1] = 200001500;
			break;


		case 284:case 285:case 287:case 288:case 289:case 290:case 291:case 292:case 293:case 294:case 295:case 296:case 297:case 298:case 299:
			$data[0] = 200001500;
			$data[1] = 100000260;
			break;


		case 300:case 301:case 302:case 303:case 304:case 305:case 306:case 307:
			$data[0] = 10000;
			$data[1] = 200000350;
			break;

		case 308:case 309:
			$data[0] = 100000380;
			$data[1] = 200003000;
			break;

		case 388:case 389:
			$data[0] = 200003000;
			$data[1] = 100000380;
			break;

		default:
			$data = null;//$data[0] = 200001500;
			break;
		}
		return $data;
	}
?>
