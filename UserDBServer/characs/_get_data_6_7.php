<?php
include_once("../myAes.php");

  $data = array();
  $data["error"] = 0;
  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    return;
  }
  else {
    $data["isSaleCharac"] = $redis->get('isSaleCharac');
    $data["dailyRewardType"] = $redis->lrange('dailyRewardType',0, -1);
    $data["dailyRewardAmount"] = $redis->lrange('dailyRewardAmount', 0, -1);
    $data["characHiddenStartIdx"] = $redis->lrange('characHiddenStartIdx', 0, -1);
    $data["characMinIdx"] = $redis->lrange('characMinIdx', 0, -1);
    $data["characMaxIdx"] = $redis->lrange('characMaxIdx', 0, -1);
    $data["characPrices"] = json_decode( $redis->get('characPrices') );
    $data["chargingCharacs"] = $redis->lrange('chargingCharacs', 0, -1);
    $redis->close();
  }

  echo json_encode($data);
?>
