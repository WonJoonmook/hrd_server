<?php
  include_once("../myAes.php");
  include_once("./_calculate10_5.php");

  $id = $_REQUEST["id"];
  $templeId = $_REQUEST["templeId"];


  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");

  $query = sprintf("select templeLevel, lastRecvTime, endLvUpTime, keepAmount, keepFlowTime from frdTemple where userId = %d and templeId=%d", $id, $templeId);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }
  
  $row = $res->fetch_assoc();
  if ( ((int)$row["endLvUpTime"] > time()+5) || ((int)$row["endLvUpTime"] == 0) ) {
    echo 1;
    $db->query("rollback");
    $db->close();
    return;
  }

  $flowTime = (time() - (int)$row["lastRecvTime"]);
  $amount = GetTempleReward($db, $id, $templeId, (int)$row["templeLevel"], $flowTime, (int)$row["keepAmount"], (int)$row["keepFlowTime"]);
  if ( $amount < 0 ) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }
  $row["templeLevel"] = (int)$row["templeLevel"] + 1;
  $query = sprintf("update frdTemple set templeLevel=%d, startLvUpTime=0, endLvUpTime=0, keepAmount=%d, keepFlowTime=%d where userId=%d and templeId=%d"
                ,$row["templeLevel"], $amount, $flowTime, $id, $templeId);
 
  $isGood = $db->query($query);
  if ($isGood == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $db->query("commit");
  $db->close();
  
  $data["templeLevel"] = $row["templeLevel"];
  $data["keepAmount"] = $row["keepAmount"];
  $data["keepFlowTime"] = $row["keepFlowTime"];

  echo json_encode($data);
?>
