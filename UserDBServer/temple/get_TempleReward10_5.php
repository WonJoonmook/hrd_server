<?php
  include_once("../myAes.php");
  include_once("./_calculate10_5.php");


  $id = $_REQUEST["id"];
  $templeId = $_REQUEST["templeId"];


  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");

  $query = sprintf("select templeLevel, lastRecvTime, keepAmount, keepFlowTime from frdTemple where userId = %d and templeId=%d", $id, $templeId);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }
  
  $row = $res->fetch_assoc();

  $flowTime = (time() - (int)$row["lastRecvTime"]);
  $rewardType = GetTempleResourceType($templeId);
  $amount = GetTempleReward($db, $id, $templeId, (int)$row["templeLevel"], $flowTime, (int)$row["keepAmount"], (int)$row["keepFlowTime"]);
  if ( $amount < 0 ) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }
  $maxAmount = GetTempleMaxStorage($templeId, (int)$row["templeLevel"]);
  if ( $amount > $maxAmount )
    $amount = $maxAmount;

  switch($rewardType) {
    case 5000:
    $resourceName = "gold";
    break;
    case 5001:
    $resourceName = "exp";
    break;
    case 5002:
    $resourceName = "ticket";
    break;
    case 5003:
    $resourceName = "heart";
    break;
    case 5004:
    $resourceName = "jewel";
    break;
    case 5005:
    $resourceName = "skillPoint";
    break;
    case 5006:
    $resourceName = "powder";
    break;
    default:
    $resourceName = $rewardType;
    break;
  }

  if ( $rewardType >= 5000 && $rewardType < 5010 ) {
    $query = sprintf("select %s from frdUserData where privateId = %d", $resourceName, $id);
    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    $row = $res->fetch_assoc();
    $preHavingResourceAmount = $row[$resourceName];
    $resultResourceAmount = $preHavingResourceAmount + (int)$amount;

    $query = sprintf("update frdUserData set %s=%d where privateId=%d", $resourceName, $resultResourceAmount, $id);
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }
  else {

    $query = "select itemCount from frdHavingItems where userId = $id and itemId = $resourceName";
    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    if ( $res->num_rows > 0 ) {
      $row = $res->fetch_assoc();
      $preHavingResourceAmount = $row["itemCount"];
      $resultResourceAmount = $preHavingResourceAmount + (int)$amount;

      $query = "update frdHavingItems set itemCount=$resultResourceAmount where userId = $id and itemId = $resourceName";
    }
    else {
      $resultResourceAmount = (int)$amount;
      $query = "insert into frdHavingItems values ($id, $resourceName, $resultResourceAmount)";
    }
    
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
    
  }

  $query = sprintf("update frdTemple set lastRecvTime=%d, keepAmount=0 where userId=%d and templeId=%d", time(), $id, $templeId);
  $isGood = $db->query($query);
  if ($isGood == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ( $rewardType == 5001 ) {
    $redis = openRedis();
    if ( $redis == false ) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    $userLevel = FindLevel(50, $redis, 0, $redis->llen('userNeedExp1')-1, $preHavingResourceAmount);
    $curUserLevel = FindLevel(50, $redis, 0, $redis->llen('userNeedExp1')-1, $resultResourceAmount);
    if ( $curUserLevel > $userLevel ) {
      
      $query = sprintf("select heart from frdUserData where privateId = %d", $id);
      $sres = $db->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        $redis->close();
        return;
      }
      $srow = $sres->fetch_assoc();
      $data["preHeart"] = $srow["heart"];

      $maxHeart = $redis->lindex('userMaxHeart1', $curUserLevel);
      $query = sprintf("select val from frdEffectForEtc where userId = %d and type=%d", $id, $GLOBALS['$ABILL_Bonus_MaxHeart']);
      $ssres = $db->query($query);
      if ($ssres == false) {
        echo 0;$db->query("rollback");$db->close();
        return;
      }
      if ( $ssres->num_rows > 0 ) {
        $ssrow = $ssres->fetch_assoc();
        $bonusMaxHeart = round($maxHeart * $ssrow["val"]*0.00001);
        $maxHeart += $bonusMaxHeart;
      }

      if ( $srow["heart"] < $maxHeart ) {
        $query = sprintf("update frdUserData set heart=%d where privateId=%d", $maxHeart, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return;
        }
        $data["curHeart"] = $maxHeart;
      }
      else {
        $data["curHeart"] = $srow["heart"];
      }
      $data["resultLevel"] = $curUserLevel;
      $data["preLevel"] = $userLevel;
    }
    $redis->close();
  }
  else if ( $rewardType == 5005 ) {

    $query = "update frdSkillPoints set accuSkillPoint=accuSkillPoint+$amount where userId=$id";
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;$db->query("rollback");$db->close();return;
    }
    
  }

  $db->query("commit");
  $db->close();
  
  $data["getAmount"] = (int)$amount;
  $data["resultAmount"] = $resultResourceAmount;
  $data["lastRecvTime"] = time();
  echo json_encode($data);
?>
