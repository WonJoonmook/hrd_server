<?php
  include_once("../myAes.php");
  include_once("../consumeResource.php");
  include_once("./_calculate10_5.php");

  $id = $_REQUEST["id"];
  $templeId = $_REQUEST["templeId"];


  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");

  $query = sprintf("select templeLevel from frdTemple where userId = %d and templeId=%d", $id, $templeId);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $row = $res->fetch_assoc();
  $templeLevel = $row["templeLevel"];

  $mats = GetTempleNeedLevelUpMats($templeId, $templeLevel);
  $matCount = (int)(count($mats)/2);

  for ( $i=0; $i<$matCount; $i++ ) {
    $rewardType = $mats[$i*2];
    $rewardCount = $mats[$i*2+1];

  //  echo "Type = ".$rewardType." ".$rewardCount."\n";

    $resultVal = ConsumeResource($db, $id, $i, $rewardType, $rewardCount);
    $data["consumeType"][] = $rewardType;
    $data["consumeCount"][] = $rewardCount;
    if ( $resultVal == 0) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
    else if ( $resultVal == -1 ) {
      addBlacklist($id, "cantTempleLvUp_Lack");
      echo 1;
      $db->query("rollback");
      $db->close();
      return;
    }
  }

  $startTime = time();
  $endTime = $startTime + GetTempleNeedLevelUpMinute($templeId, $templeLevel)*60;

  $query = sprintf("update frdTemple set startLvUpTime=%d, endLvUpTime=%d where userId=%d and templeId=%d"
                ,$startTime, $endTime, $id, $templeId);

  $isGood = $db->query($query);
  if ($isGood == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $db->query("commit");
  $db->close();

  $data["startLvUpTime"] = $startTime;
  $data["endLvUpTime"] = $endTime;
  echo json_encode($data);
?>
