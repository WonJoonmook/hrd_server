<?php
	include_once("../myAes.php");
    
	$redis = openRedis();
  	if ( $redis !== 0 ) {

	    $redis->del('templeExp');
	    $redis->rpush('templeExp', 
	      0,
	      100,
	      200,
	      300,
	      500,
	      800,
	      1300,
	      2100,
	      3400,
	      5500
	      );

	    $length = $redis->llen('templeExp');
	    $redis->del('templeExpAccu');
	    $sum=0;
	    for ( $idx=0; $idx<$length; $idx++ ) {
	      $sum += $redis->lindex('templeExp', $idx);
	      $redis->rpush('templeExpAccu', $sum);
	    }

	    $redis->close();
	    echo "Comp!";
	}

?>