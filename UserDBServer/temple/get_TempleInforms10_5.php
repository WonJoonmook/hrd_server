<?php
  include_once("../myAes.php");
  include_once("./_calculate10_5.php");

  $id = $_REQUEST["id"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");

  $query = sprintf("select * from frdTemple where userId = %d", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }
  
  for ($idx = 0; $idx < $res->num_rows; $idx++) {
    $res->data_seek($idx);
    $row = $res->fetch_assoc();
    
    $endLvUpTime = (int)$row["endLvUpTime"];

    if ( $endLvUpTime !== 0 && $endLvUpTime < time() ) {
      $templeId = (int)$row["templeId"];
      $flowTime = ($endLvUpTime - (int)$row["lastRecvTime"]);
      if ( $flowTime < 0 )
        $flowTime = 0;

      $amount = GetTempleReward($db, $id, $templeId, (int)$row["templeLevel"], $flowTime, (int)$row["keepAmount"], (int)$row["keepFlowTime"]);
      if ( $amount < 0 ) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      $row["templeLevel"] = (int)$row["templeLevel"] + 1;
      $query = sprintf("update frdTemple set templeLevel=%d, startLvUpTime=0, endLvUpTime=0, keepAmount=%d, keepFlowTime=%d where userId=%d and templeId=%d"
                ,$row["templeLevel"], $amount, $flowTime, $id, $templeId);
 
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      $endLvUpTime = 0;
      $row["startLvUpTime"] = 0;
      
    }

    $char = array();
    $char["templeId"] = $row["templeId"];
    $char["templeLevel"] = $row["templeLevel"];
    $char["clearLevel"] = $row["clearLevel"];
    $char["lastRecvTime"] = $row["lastRecvTime"];
    $char["startLvUpTime"] = $row["startLvUpTime"];
    $char["endLvUpTime"] = $endLvUpTime;
    $char["keepAmount"] = $row["keepAmount"];
    $char["keepFlowTime"] = $row["keepFlowTime"];

    $data["datas"][] = $char;
  }
  

  $db->query("commit");
  $db->close();
  
  echo json_encode($data);
?>
