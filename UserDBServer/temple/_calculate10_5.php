<?php
	include_once("../globalDefine.php");

	function GetTempleResourceType($templeId) {
		switch($templeId) {
		case 1:		//gold
			return 5000;	
		case 2:		//exp
			return 5001;
		case 3:
			return 5005;
		case 4:
			return 5006;	
		case 5:	
			return 5003;
		case 6:		//TR2
			return 112002;
		case 7:		//FR2
			return 112001;	
		case 8:		//medal
			return 112004;
		case 9:
			return 112000;
		case 10:
			return 112003;
		default:	//powder
			return 5006;
		}
	}

	function GetTempleReward(&$db, $userId, $templeId, $templeLevel, $flowTime, $keepAmount, $keepFlowTime) {
		if ( $keepAmount > 0 )
			$flowTime -= $keepFlowTime;
		if ( $flowTime < 0 )
			$flowTime = 0;

		$resultAmount = GetTempleRewardAmount($templeId, $templeLevel);
		
		$query = sprintf("select val from frdEffectForEtc where userId = %d and type=%d", $userId, $GLOBALS['$ABILL_TempleOutput']);
		$res = $db->query($query);
		if ($res == false)
			return -1;
		
		if ( $res->num_rows > 0 ) {
			$row = $res->fetch_assoc();
			$resultAmount = $row["val"]/ 100000 *  $resultAmount;
		}

		$resultAmount = $resultAmount/60*$flowTime;
		if ( $keepAmount > 0 )
			$resultAmount += $keepAmount;

		return (int)$resultAmount;
	}

	function GetTempleRewardAmount($templeId, $templeLevel) {
		switch($templeId) {
		case 1:
			return 3.3333333*$templeLevel;
		case 2:
			return 3.3333333*$templeLevel;
		case 3:
			switch($templeLevel) {
			case 1:
				return 0.01666667;
			default:
				return 0.03333334*($templeLevel-1);
			}
		case 4:
			return 0.01666667*($templeLevel+1);
		case 5:
			switch($templeLevel) {
			case 1:case 2:case 3:
				return 0.01666667*$templeLevel;
			case 4:
				return 0.05833333;
			case 5:
				return 0.0625;
			case 6:
				return 0.01666667*4;
			case 7:
				return 0.07083333;
			case 8:
				return 0.075;
			case 9:
				return 0.07916667;
			case 10:
				return 0.01666667*5;
			default:
				return 0.01666667*5;
			}
		case 6:case 7:case 8:case 9:case 10:
			return 0.01666667*$templeLevel;
		default:return 0.5;
		}
	}


	function GetTempleMaxStorage($templeId, $templeLevel) {
		$result;
		switch($templeId) {
			case 1:case 2:
			switch($templeLevel) {
			case 1:$result = 1000;break;
			case 2:$result = 2400;break;
			case 3:$result = 3900;break;
			case 4:$result = 5600;break;
			case 5:$result = 7500;break;
			case 6:$result = 9600;break;
			case 7:$result = 11900;break;
			case 8:$result = 14400;break;
			case 9:$result = 17100;break;
			case 10:$result = 20000;break;
			default:$result = 20000;break;
			}
			break;
		case 3:
			switch($templeLevel) {
			case 1:$result = 5;break;
			case 2:$result = 12;break;
			case 3:$result = 26;break;
			case 4:$result = 42;break;
			case 5:$result = 60;break;
			case 6:$result = 80;break;
			case 7:$result = 102;break;
			case 8:$result = 126;break;
			case 9:$result = 152;break;
			case 10:$result = 180;break;
			default:$result = 180;break;
			}
			break;
		case 4:
			switch($templeLevel) {
			case 1:$result = 10;break;
			case 2:$result = 18;break;
			case 3:$result = 26;break;
			case 4:$result = 35;break;
			case 5:$result = 45;break;
			case 6:$result = 56;break;
			case 7:$result = 68;break;
			case 8:$result = 81;break;
			case 9:$result = 95;break;
			case 10:$result = 110;break;
			default:$result = 110;break;
			}
			break;
		case 5:
			switch($templeLevel) {
			case 1:$result = 5;break;
			case 2:$result = 12;break;
			case 3:$result = 16;break;
			case 4:$result = 19;break;
			case 5:$result = 22;break;
			case 6:$result = 24;break;
			case 7:$result = 28;break;
			case 8:$result = 31;break;
			case 9:$result = 34;break;
			case 10:$result = 40;break;
			default:$result = 40;break;
			}
			break;
		case 6:case 7:case 8:case 9:case 10:
			switch($templeLevel) {
			case 1:$result = 5;break;
			case 2:$result = 11;break;
			case 3:$result = 18;break;
			case 4:$result = 26;break;
			case 5:$result = 35;break;
			case 6:$result = 45;break;
			case 7:$result = 56;break;
			case 8:$result = 68;break;
			case 9:$result = 81;break;
			case 10:$result = 100;break;
			default:$result = 100;break;
			}
			break;

		default:$result = 25+ $templeLevel*2;break;
		}
		return $result;
	}

	function GetTempleNeedLevelUpMinute($templeId, $templeLevel) {
		switch($templeId) {
		case 1:
			switch($templeLevel) {
			case 1:return 30;
			case 2:return 60;
			case 3:return 120;
			case 4:return 240;
			case 5:return 480;
			case 6:return 960;
			case 7:return 1920;
			case 8:return 3840;
			case 9:return 7680;
			default: return 7680;
			}
		case 2:case 3:case 4:case 5:case 6:case 7:case 8:case 9:case 10:
			switch($templeLevel) {
			case 1:return 30;
			case 2:return 120;
			case 3:return 240;
			case 4:return 360;
			case 5:return 480;
			case 6:return 600;
			case 7:return 1200;
			case 8:return 2400;
			case 9:return 4800;
			default: return 4800;
			}
		default:
			return 1 + $templeLevel*1;
		}
	}


	function GetTempleNeedLevelUpMats($templeId, $templeLevel) {
		switch($templeId) {
		case 1:
			switch($templeLevel) {
			case 1: return array(5000, 6000,  111004, 50, 101000, 20);
			case 2: return array(5000, 24000,  111004, 40, 101000, 30, 102000, 10);
			case 3: return array(5000, 54000,  112004, 20, 102000, 20);
			case 4: return array(5000, 96000, 112004, 30, 102000, 20, 103000, 10);
			case 5: return array(5000, 150000, 113004, 10, 103000, 10);
			case 6: return array(5000, 216000, 113004, 15, 103000, 20, 104000, 10);
			case 7: return array(5000, 294000, 114004, 8, 104000, 20, 5004, 10, 110000, 1);
			case 8: return array(5000, 384000, 114004, 10, 105000, 10, 5004, 50, 110000, 3);
			case 9: return array(5000, 486000, 115004, 5, 106000, 5,  5004, 100,110000, 5);
			default:
				return array(5002, 99999999);
			}
		case 2:
			switch($templeLevel) {
			case 1: return array(5000, 3000,  111002, 50, 111000, 30,  101001, 15);
			case 2: return array(5000, 12000,  111002, 50, 111000, 40, 101001, 20);
			case 3: return array(5000, 27000,  112002, 30, 112000, 20, 102001, 24);
			case 4: return array(5000, 48000,  112002, 40, 112000, 30, 102001, 40);
			case 5: return array(5000, 75000, 113002, 15, 112001, 30, 103001, 10);
			case 6: return array(5000, 108000, 113002, 15, 113001, 10, 103001, 30);
			case 7: return array(5000, 147000, 114002, 10, 113001, 10, 104001, 20, 5004, 10, 5005, 300);
			case 8: return array(5000, 192000, 114002, 15, 114001, 10, 105001, 15, 5004, 50, 5005, 500);
			case 9: return array(5000, 243000, 115002, 5, 115001, 5, 106001, 5,  5004, 100,5005, 1000);
			default:
				return array(5002, 99999999);
			}
		case 3:
			switch($templeLevel) {
			case 1: return array(5000, 3000,  111003, 80, 101002, 15);
			case 2: return array(5000, 12000,  112003, 60, 101002, 20);
			case 3: return array(5000, 27000,  113003, 40, 102002, 15);
			case 4: return array(5000, 48000,  114003, 20, 102002, 20);
			case 5: return array(5000, 75000,  115003, 10, 103002, 10);
			case 6: return array(5000, 108000, 114003, 30, 114001, 30, 103002, 15);
			case 7: return array(5000, 147000, 115003, 10, 115000, 10, 104002, 10, 5004, 10, 110000, 1);
			case 8: return array(5000, 192000, 115003, 10, 115004, 10, 105002, 10, 5004, 50, 110000, 3);
			case 9: return array(5000, 243000, 115003, 15, 115002, 15, 106002, 5,  5004, 100,110000, 5);
			default:
				return array(5002, 99999999);
			}
		case 4:
			switch($templeLevel) {
			case 1: return array(5000, 100,  111001, 20, 101003, 10, 5006, 200);
			case 2: return array(5000, 500,  112001, 50, 102003, 9, 5006, 500);
			case 3: return array(5000, 1000,  111001, 50, 112001, 30, 103003, 8, 5006, 800);
			case 4: return array(5000, 5000,  113001, 50, 104003, 7, 5006, 1000);
			case 5: return array(5000, 10000,  113001, 30, 113000, 20, 105003, 6, 5006, 2000);
			case 6: return array(5000, 20000, 114001, 30, 114004, 20, 106003, 5, 5006, 3000);
			case 7: return array(5000, 50000, 115001, 20, 115002, 10, 5004, 10, 110000, 1);
			case 8: return array(5000, 80000, 115001, 20, 115003, 10, 5004, 50, 110000, 3);
			case 9: return array(5000, 120000, 115001, 30, 5004, 100, 110000, 5);
			default:
				return array();
			}
		case 5:
			switch($templeLevel) {
			case 1: return array(5000, 25000,  111000, 50, 112000, 20, 101004, 30);
			case 2: return array(5000, 50000,  111000, 60, 113000, 20, 101004, 50);
			case 3: return array(5000, 90000,  112000, 30, 113000, 30, 102004, 30);
			case 4: return array(5000, 140000, 112000, 40, 114000, 15, 102004, 40);
			case 5: return array(5000, 200000, 113000, 30, 114000, 15, 103004, 20);
			case 6: return array(5000, 280000, 114000, 15, 115000, 10, 103004, 30, 5004, 10);
			case 7: return array(5000, 370000, 115000, 20, 115002, 10, 104004, 25, 5004, 50, 110000, 1);
			case 8: return array(5000, 500000, 115000, 20, 115001, 15, 105004, 20, 5004, 100, 110000, 3);
			case 9: return array(5000, 700000, 115000, 30, 115003, 15, 106004, 15, 5004, 150, 110000, 5);
			default:
				return array();
			}
		case 6:
			switch($templeLevel) {
			case 1: return array(5000, 20000,  111002, 50, 112000, 30, 101005, 30, 5006, 500);
			case 2: return array(5000, 40000,  112002, 30, 112004, 35, 101005, 40, 5006, 1000);
			case 3: return array(5000, 65000,  112002, 45, 113001, 20, 102005, 25, 5006, 2000);
			case 4: return array(5000, 95000,  113002, 25, 113003, 30, 102005, 35, 5006, 3000);
			case 5: return array(5000, 130000, 113002, 40, 114000, 15, 103005, 25, 5006, 4000);
			case 6: return array(5000, 190000, 114002, 20, 114003, 20, 103005, 30, 5004, 10, 5006, 5000);
			case 7: return array(5000, 280000, 114002, 35, 115004, 10, 104005, 30, 5004, 50, 110000, 1);
			case 8: return array(5000, 380000, 115002, 15, 115001, 10, 105005, 20, 5004, 100, 110000, 3);
			case 9: return array(5000, 500000, 115002, 25, 115003, 15, 106005, 15, 5004, 150, 110000, 5);
			default:
				return array();
			}
		case 7:
			switch($templeLevel) {
			case 1: return array(5000, 20000,  111001, 50, 112002, 30, 101006, 30, 5006, 500);
			case 2: return array(5000, 40000,  112001, 30, 112000, 35, 101006, 40, 5006, 1000);
			case 3: return array(5000, 65000,  112001, 45, 113003, 20, 102006, 25, 5006, 2000);
			case 4: return array(5000, 95000,  113001, 25, 113004, 30, 102006, 35, 5006, 3000);
			case 5: return array(5000, 130000, 113001, 40, 114000, 15, 103006, 25, 5006, 4000);
			case 6: return array(5000, 190000, 114001, 20, 114002, 20, 103006, 30, 5004, 10, 5006, 5000);
			case 7: return array(5000, 280000, 114001, 35, 115002, 10, 104006, 30, 5004, 50, 110000, 1);
			case 8: return array(5000, 380000, 115001, 15, 115004, 10, 105006, 20, 5004, 100, 110000, 3);
			case 9: return array(5000, 500000, 115001, 25, 115000, 15, 106006, 15, 5004, 150, 110000, 5);
			default:
				return array();
			}
		case 8:
			switch($templeLevel) {
			case 1: return array(5000, 20000,  111004, 50, 112002, 30, 101007, 30, 5006, 500);
			case 2: return array(5000, 40000,  112004, 30, 112003, 35, 101007, 40, 5006, 1000);
			case 3: return array(5000, 65000,  112004, 45, 113001, 20, 102007, 25, 5006, 2000);
			case 4: return array(5000, 95000,  113004, 25, 113000, 30, 102007, 35, 5006, 3000);
			case 5: return array(5000, 130000, 113004, 40, 114002, 15, 103007, 25, 5006, 4000);
			case 6: return array(5000, 190000, 114004, 20, 114001, 20, 103007, 30, 5004, 10, 5006, 5000);
			case 7: return array(5000, 280000, 114004, 35, 115001, 10, 104007, 30, 5004, 50, 110000, 1);
			case 8: return array(5000, 380000, 115004, 15, 115000, 10, 105007, 20, 5004, 100, 110000, 3);
			case 9: return array(5000, 500000, 115004, 25, 115002, 15, 106007, 15, 5004, 150, 110000, 5);
			default:
				return array();
			}
		case 9:
			switch($templeLevel) {
			case 1: return array(5000, 20000,  111000, 50, 112003, 30, 101008, 30, 5006, 500);
			case 2: return array(5000, 40000,  112000, 30, 112001, 35, 101008, 40, 5006, 1000);
			case 3: return array(5000, 65000,  112000, 45, 113000, 20, 102008, 25, 5006, 2000);
			case 4: return array(5000, 95000,  113000, 25, 113001, 30, 102008, 35, 5006, 3000);
			case 5: return array(5000, 130000, 113000, 40, 114000, 15, 103008, 25, 5006, 4000);
			case 6: return array(5000, 190000, 114000, 20, 114002, 20, 103008, 30, 5004, 10, 5006, 5000);
			case 7: return array(5000, 280000, 114000, 35, 115002, 10, 104008, 30, 5004, 50, 110000, 1);
			case 8: return array(5000, 380000, 115000, 15, 115004, 10, 105008, 20, 5004, 100, 110000, 3);
			case 9: return array(5000, 500000, 115000, 25, 115001, 15, 106008, 15, 5004, 150, 110000, 5);
			default:
				return array();
			}
		case 10:
			switch($templeLevel) {
			case 1: return array(5000, 20000,  111003, 50, 112004, 30, 101009, 30, 5006, 500);
			case 2: return array(5000, 40000,  112003, 30, 112001, 35, 101009, 40, 5006, 1000);
			case 3: return array(5000, 65000,  112003, 45, 113000, 20, 102009, 25, 5006, 2000);
			case 4: return array(5000, 95000,  113003, 25, 113001, 30, 102009, 35, 5006, 3000);
			case 5: return array(5000, 130000, 113003, 40, 114004, 15, 103009, 25, 5006, 4000);
			case 6: return array(5000, 190000, 114003, 20, 114002, 20, 103009, 30, 5004, 10, 5006, 5000);
			case 7: return array(5000, 280000, 114003, 35, 115000, 10, 104009, 30, 5004, 50, 110000, 1);
			case 8: return array(5000, 380000, 115003, 15, 115004, 10, 105009, 20, 5004, 100, 110000, 3);
			case 9: return array(5000, 500000, 115003, 25, 115002, 15, 106009, 15, 5004, 150, 110000, 5);
			default:
				return array();
			}
		default:
			switch($templeLevel) {
			case 1:
				return array(5000, 1000);
			default:
				return array(101001, 1, 5002, 100); 
			}
		}
	}

?>