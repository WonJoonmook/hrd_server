<?php
    function ConsumeResource(&$db, $userId, $idx, $type, &$count) {
        if ( $type < 500 ) {                                //charac soulStone

            $bundleIdx = ($type>>3);
            $query = sprintf("select exps from frdCharacEvolveExps where userId=%d and bundleIdx=%d",$userId, $bundleIdx);
            $sres = $db->query($query);
            if ( $sres == false )
              return 0;

            if ($sres->num_rows <= 0)
              return -1;

            $row = $sres->fetch_assoc();
            $longData = $row["exps"];
            $bitIdx = 8*($type&7);
            $preExp = ( ($longData >> $bitIdx) & 0xff);

            $mask = $preExp;
            $mask <<= $bitIdx;
            $longData -= $mask;

            $mask = ($preExp - $count);
            if ( $mask < 0 )
                return -1;

            $mask <<= $bitIdx;
            $resultData = ($longData | $mask);

            $query = sprintf("update frdCharacEvolveExps set exps='%s' where userId=%d and bundleIdx=%d"
                , $resultData, $userId, $bundleIdx);
            $isGood = $db->query($query);
            if ($isGood == false)
              return 0;

        }
        else if ( $type < 2000 ) {                          //weapon

            if ( $count <= 1)
                $needExp = 0;
            else
                $needExp = $redis->lindex('weaponNeedExpAccu', ($count-2));

            $query = sprintf("select privateId from frdHavingWeapons where userId=%d and weaponId=%d and exp>=%d order by exp ASC"
                , $userId, ($type-1000), $needExp);
            $sres = $db->query($query);
            if ($sres == false)
                return 0;

            if ($sres->num_rows <= 0)
                return -1;

            $row = $sres->fetch_assoc();
            $count = $row["privateId"];
            $query = sprintf("delete from frdHavingWeapons where privateId=%d and userId=%d", $count, $userId);
            $isGood = $db->query($query);
            if ($isGood == false)
                return 0;

        }
        else if ( $type >=5000 && $type < 5100 ) {

            switch($type) {
            case 5000:
                $resourceName = "gold";
                break;
            case 5001:
                $resourceName = "exp";
                break;
            case 5002:
                $resourceName = "ticket";
                break;
            case 5003:
                $resourceName = "heart";
            break;
            case 5004:
                $resourceName = "jewel";
                include_once("../EventAccuJewel.php");
                $resultCode = AccuUseJewel($db, $userId, $count);
          			if ($resultCode == 0) {
          				echo 0;
          		    $db->query("rollback");
          		    $db->close();
          		    return;
          			}
                break;
            case 5005:
                $resourceName = "skillPoint";
                $query = sprintf("select accuSkillPoint from frdSkillPoints where userId=%d", $userId);
                $sres = $db->query($query);
                if ($sres == false)
                    return 0;

                $row = $sres->fetch_assoc();
                $restAmount = $row["accuSkillPoint"] - $count;
                if ( $restAmount < 0 )
                    return -1;

                $query = sprintf("update frdSkillPoints set accuSkillPoint=%d where userId=%d", $restAmount, $userId);
                $isGood = $db->query($query);
                if ($isGood == false)
                    return 0;
                break;
            case 5006:
                $resourceName = "powder";
                break;
            default:
                $resourceName = "gold";
                break;
            }

            $query = sprintf("select %s from frdUserData where privateId=%d", $resourceName, $userId);
            $sres = $db->query($query);
            if ($sres == false)
                return 0;

            $row = $sres->fetch_assoc();
            $restAmount = $row[$resourceName] - $count;
            if ( $restAmount < 0 ) {
                return -1;
            }

            $query = sprintf("update frdUserData set %s=%d where privateId=%d", $resourceName, $restAmount, $userId);
            $isGood = $db->query($query);
            if ($isGood == false)
                return 0;

        }
        else if ( $type >= 10000 && $type < 11000 ) {       //magic

            $query = sprintf("select privateId from frdHavingArtifacts where userId=%d and itemId=%d order by itemLevel ASC", $userId, ($type-10000));
            $sres = $db->query($query);
            if ($sres == false)
                return 0;

            if ($sres->num_rows <= 0)
                return -1;

            $row = $sres->fetch_assoc();
            $count = $row["privateId"];
            $query = sprintf("delete from frdHavingArtifacts where privateId=%d and userId=%d", $count, $userId);
            $isGood = $db->query($query);
            if ($isGood == false)
                return 0;


        }
        else if ( $type >= 100000 && $type < 200000 ) {       //inventory item

            $query = sprintf("select itemCount from frdHavingItems where userId=%d and itemId=%d", $userId, $type);
            $sres = $db->query($query);
            if ($sres == false)
                return 0;

            if ($sres->num_rows <= 0) {
                echo "Cant Find ".$userId.", ".$type;
                return -1;
            }

            $row = $sres->fetch_assoc();
            $resultCount = (int)$row["itemCount"] - $count;
            if ( $resultCount < 0 ) {
                echo $resultCount." Result";
                return -1;
            }

            if ( $resultCount > 0)
                $query = sprintf("update frdHavingItems set itemCount=%d where userId=%d and itemId=%d", $resultCount, $userId, $type);
            else
                $query = sprintf("delete from frdHavingItems where userId=%d and itemId=%d", $userId, $type);
            $isGood = $db->query($query);
            if ($isGood == false)
                return 0;

        }
        return 1;
    }

?>
