<?php
	function GetNeedQuestMats($goddessId, $level) {
		switch($goddessId) {
		case 1:
			switch($level) {
			case 1:
				return array(103000, 5, 101006, 10, 102000, 10, 102007, 5, 113004, 20);
			case 2:
				return array(105000, 10, 106000, 5,  115004, 5, 115002, 5, 1118, 1);
			case 3:
				return array(106000, 5,  103009, 8, 115004, 10, 1129, 1, 1117, 1, 1215, 1);
			case 4:
				return array(1200, 1, 1225, 1, 1202, 1, 1201, 1, 1208, 1, 110000, 10);
			default:
				return array(100000, 2, 5002, 100, 110000, 10);
			}
		case 2:
			switch($level) {
			case 1:
				return array(101001, 30, 102001, 20, 103001, 15, 104001, 10, 114002, 20);
			case 2:
				return array(5000, 50000, 115002, 10,  105001, 10, 5006, 3000, 104008, 2);
			case 3:
				return array(5000, 80000,  106001, 5, 102005, 6, 103003, 3, 105008, 1, 110000, 5);
			case 4:
				return array(5000, 120000, 5006, 5000, 1204, 1, 1204, 1, 1204, 1, 101001, 100);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		case 3:
			switch($level) {
			case 1:
				return array(101002, 12, 102002, 10, 103002, 8, 104002, 5, 115002, 3);
			case 2:
				return array(105002, 10, 106002, 5,  111003, 50, 112003, 30, 113003, 20);
			case 3:
				return array(114003, 35, 115003, 10, 103007, 15, 103006, 15, 103005, 15, 110000, 5);
			case 4:
				return array(115003, 30, 103002, 20, 103003, 20, 103004, 20, 106002, 10, 110000, 10);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		case 4:
			switch($level) {
			case 1:
				return array(111001, 100, 102003, 20, 103003, 15, 112001, 20, 10205, 1);
			case 2:
				return array(101003, 50, 113001, 20,  113003, 20, 113000, 20, 10207, 1);
			case 3:
				return array(114001, 40, 115001, 15, 104003, 20, 105003, 10, 10304, 1, 5000, 200000);
			case 4:
				return array(115001, 10, 106003, 5, 106002, 5, 106001, 5, 10400, 1, 110000, 10);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		case 5:
			switch($level) {
			case 1:
				return array(101000, 50, 106003, 3, 102004, 30, 112000, 50, 5006, 3000);
			case 2:
				return array(101004, 50, 104004, 10, 104007, 10, 104009, 10, 114000, 30);
			case 3:
				return array(105004, 10, 105005, 5, 105006, 10, 105007, 10, 105009, 10, 110000, 10);
			case 4:
				return array(115000, 30, 1211, 1, 1217, 1, 1218, 1, 106004, 20, 110000, 15);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		case 6:
			switch($level) {
			case 1:
				return array(103005, 5, 106000, 5, 111002, 50, 113000, 40, 113004, 20);
			case 2:
				return array(101005, 30, 102005, 20, 103008, 10, 104006, 8, 105002, 5, 106005, 5);
			case 3:
				return array(104005, 20, 106002, 10, 113002, 50, 114002, 25, 115004, 15, 5005, 3000);
			case 4:
				return array(106008, 10, 104001, 20, 115004, 15, 115002, 30, 114000, 40, 110000, 15);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		case 7:
			switch($level) {
			case 1:
				return array(101006, 40, 102008, 30, 106007, 5, 113004, 30, 114000, 20);
			case 2:
				return array(102006, 40, 101007, 30, 103008, 30, 106005, 10, 104006, 40, 101008, 40);
			case 3:
				return array(105006, 25, 105007, 20, 105008, 20, 114003, 35, 115001, 20, 110000, 5);
			case 4:
				return array(106006, 15, 106009, 10, 115002, 15, 115001, 35, 5000, 300000, 110000, 15);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		case 8:
			switch($level) {
			case 1:
				return array(101007, 45, 104007, 20, 106008, 5, 102002, 35, 111004, 70);
			case 2:
				return array(103007, 30, 106001, 10, 104001, 30, 114002, 40, 5005, 1000, 110000, 5);
			case 3:
				return array(103002, 35, 102003, 40, 104009, 30, 115004, 25, 115000, 15, 110000, 10);
			case 4:
				return array(106007, 10, 106003, 10, 102009, 40, 115001, 20, 115004, 30, 110000, 15);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		case 9:
			switch($level) {
			case 1:
				return array(104000, 40, 104004, 30, 105001, 20, 106001, 5, 5005, 2000);
			case 2:
				return array(103001, 30, 103007, 30, 103009, 25, 106005, 10, 113000, 50, 5006, 5000);
			case 3:
				return array(101008, 30, 102008, 30, 103008, 20, 106007, 10, 115000, 20, 110000, 5);
			case 4:
				return array(115001, 15, 115003, 15, 115002, 15, 115004, 15, 106008, 10, 110000, 15);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		case 10:
			switch($level) {
			case 1:
				return array(104009, 25, 104008, 25, 104004, 25, 104001, 25, 5000, 200000);
			case 2:
				return array(101009, 50, 102009, 40, 104006, 25, 112003, 50, 113002, 30, 5005, 3000);
			case 3:
				return array(103009, 20, 105005, 10, 106004, 10, 114003, 30, 115004, 20, 110000, 5);
			case 4:
				return array(105009, 20, 103005, 20, 106009, 10, 115003, 25, 115001, 15, 110000, 15);
			default:
				return array(100000, 2, 5002, 100, 110000, 5);
			}
		default:
			return array(5000,50);
		}


	}

	function GetGoddessAttribute($id) {
		switch($id) {
		case 1:return 4;
		case 2:return 2;
		case 3:return 3;
		case 4:return 1;
		case 5:return 0;
		case 6:return 2;
		case 7:return 1;
		case 8:return 4;
		case 9:return 0;
		case 10:return 3;
		default:return 0;
		}
	}

	function GetGoddessNeedExp($goddessId, $level) {
		switch($goddessId) {
		case 1:
			switch($level) {
			case 1:return 20000;
			case 2:return 50000;
			case 3:return 100000;
			case 4:return 280000;
			default:return 1;
			}
		case 2:
			switch($level) {
			case 1:return 13333;
			case 2:return 33333;
			case 3:return 66667;
			case 4:return 186667;
			default:return 1;
			}
		case 3:
			switch($level) {
			case 1:return 30000;
			case 2:return 60000;
			case 3:return 120000;
			case 4:return 290000;
			default:return 1;
			}
		case 4:
			switch($level) {
			case 1:return 25000;
			case 2:return 60000;
			case 3:return 150000;
			case 4:return 260000;
			default:return 1;
			}
		case 5:
			switch($level) {
			case 1:return 30000;
			case 2:return 70000;
			case 3:return 140000;
			case 4:return 300000;
			default:return 1;
			}
		case 6:
			switch($level) {
			case 1:return 25000;
			case 2:return 60000;
			case 3:return 150000;
			case 4:return 300000;
			default:return 1;
			}
		case 7:
			switch($level) {
			case 1:return 35000;
			case 2:return 70000;
			case 3:return 160000;
			case 4:return 285000;
			default:return 1;
			}
		case 8:
			switch($level) {
			case 1:return 30000;
			case 2:return 75000;
			case 3:return 155000;
			case 4:return 290000;
			default:return 1;
			}
		case 9:
			switch($level) {
			case 1:return 32000;
			case 2:return 65000;
			case 3:return 170000;
			case 4:return 285000;
			default:return 1;
			}
		case 10:
			switch($level) {
			case 1:return 25000;
			case 2:return 65000;
			case 3:return 180000;
			case 4:return 280000;
			default:return 1;
			}
		default:
			return 0;
		}
	}

	function GetRuneAttribute($runeId) {
		if ( $runeId == 110000 )
			return 10;
		$val = $runeId % 1000;
		return $val;
	}

	function GetRuneLevel( $runeId) {
		$val = (int)((int)($runeId - 110000)/1000);
		if ( $val < 0 )
			return 9;	//rainbow
		return $val;
	}

	function GetRuneExp($level) {
		switch($level) {
		case 1:return 160;
		case 2:return 290;
		case 3:return 720;
		case 4:return 1200;
		case 5:return 2800;
		default:return 9999;
		}
	}

	function GetRuneExpForGoddess($goddessId, $runeId) {
		$level = GetRuneLevel($runeId);
		$runeExp = GetRuneExp($level);
		$goddessAtt = GetGoddessAttribute($goddessId);
		$runeAtt = GetRuneAttribute($runeId);
		if ( $goddessAtt == $runeAtt || $runeAtt == 10 )
			return (int)round($runeExp * 1.5);
		return $runeExp;
	}


?>
