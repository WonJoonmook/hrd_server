<?php
	include_once("../myAes.php");
	include_once("./_calculate8_4.php");


	$id = $_REQUEST["userId"];
	$goddessId = $_REQUEST["goddessId"];

	$data = array();
	$data["error"] = 0;
	$db = getDB();
	if (mysqli_connect_errno()) {
	  echo 0;
	  return;
	}

	$db->query("set autocommit=0");

	$query = sprintf("select exp from frdGoddess where userId=%d and goddessId=%d", $id, $goddessId);
	$res = $db->query($query);
	if ($res == false) {
		echo 0;
		$db->close();
		return;
	}
	if ( $res->num_rows <= 0 ) {
		echo 1;
		$db->close();
		return;
	}

	$row = $res->fetch_assoc();
	$preExp = (int)$row["exp"];
	for ( $matCount=0; $matCount<8; $matCount++ ) {
		if ( is_null( $_REQUEST["runeId".$matCount] ) )
			break;

		$runeIds[] = $_REQUEST["runeId".$matCount];
		$runeAmounts[] = $_REQUEST["runeAmount".$matCount];
	}

	$query = "select itemId, itemCount from frdHavingItems where userId=";
	$query .= $id;
	$query .= " and (";
	for ( $i=0; $i<$matCount-1; $i++ ) {
		$query .= "itemId = ";
		$query .= $runeIds[$i];
		$query .= " or ";
	}
	$query .= "itemId = ";
	$query .= $runeIds[$i];
	$query .= ") ORDER BY userId ASC";

	$res = $db->query($query);
	if ($res == false) {
		echo 0;
		$db->close();
		return;
	}

	if ( $res->num_rows !== $matCount ) {
		echo 1;
		addBlacklist($id, "cantPresentGod_UnMatchCount");
	    $db->query("rollback");
		$db->close();
		return;
	}

	$totalExp = 0;
	for ( $i=0; $i<$matCount; $i++ ) {
		$res->data_seek($i);
    	$row = $res->fetch_assoc();
    	$itemId = $row["itemId"];
		$itemCount = $row["itemCount"];
    	$resultItemCount = $itemCount - $runeAmounts[$i];
	    if ( $resultItemCount <= 0 ) {
	    	$deleteItemId[] = $itemId;
	  	}
	    else {
	    	$updateItemId[] = $itemId;
	    	$updateItemCount[] = $resultItemCount;
	    }
	  	$runeExp = GetRuneExpForGoddess($goddessId, $runeIds[$i]);
	  	
		$totalExp += (int)($runeExp * $runeAmounts[$i]);
	//	echo "runeExp = ".$runeExp.", totalExp = ".$totalExp."\n";
		$data["itemCount"][] = $resultItemCount;
	}

	if ( !is_null($deleteItemId) ) {
		$query = sprintf("delete from frdHavingItems where userId=%d and (", $id);
		for ( $i=0; $i<count($deleteItemId)-1; $i++ )
			$query .= sprintf("itemId=%d or ", $deleteItemId[$i]);
		$query .= sprintf("itemId=%d)", $deleteItemId[count($deleteItemId)-1]);
	//	echo $query;
		$isGood = $db->query($query);
		if ($isGood == false) {
			echo mysqli_error($db);
			$db->query("rollback");
			$db->close();
			return;
		}
	}
	if ( !is_null($updateItemId) ) {
		$query = "update frdHavingItems set itemCount = case ";
		for ( $i=0; $i<count($updateItemId); $i++ )
			$query .= sprintf("when itemId=%d then %d ", $updateItemId[$i], $updateItemCount[$i]);
		$query .= sprintf("else itemCount end where userId in (%d)", $id);
	//	echo $query;
		$isGood = $db->query($query);
		if ($isGood == false) {
			echo mysqli_error($db);
			$db->query("rollback");
			$db->close();
			return;
		}
	}


	

	$resultExp = $preExp+$totalExp;
//	echo "preExp = ".$preExp.", totalExp = ".$totalExp."\nresultExp = ".$resultExp;

	$query = sprintf("update frdGoddess set exp=%d where userId=%d and goddessId=%d", $resultExp, $id, $goddessId);
	$isGood = $db->query($query);
	if ($isGood == false) {
		echo mysqli_error($db);
		$db->query("rollback");
		$db->close();
		return;
	}

	$data["exp"] = $resultExp;

	$db->query("commit");
	$db->close(); 

  	echo json_encode($data);

?>
