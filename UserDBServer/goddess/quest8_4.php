<?php
	include_once("../myAes.php");
	include_once("../consumeResource.php");
	include_once("./_calculate8_4.php");
	include_once("./getAbillity.php");

	$id = $_REQUEST["userId"];
	$goddessId = (int)$_REQUEST["goddessId"];

	$data = array();
	$data["error"] = 0;
	$db = getDB();
	if (mysqli_connect_errno()) {
	  echo 0;
	  return;
	}
	$db->query("set autocommit=0");

	$query = sprintf("select level, exp from frdGoddess where userId=%d and goddessId=%d", $id, $goddessId);
	$res = $db->query($query);
	if ($res == false) {
		echo 0;
		$db->query("rollback");
		$db->close();
		return;
	}
	if ( $res->num_rows <= 0 ) {
		echo 1;
		$db->query("rollback");
		$db->close();
		return;
	}
	$row = $res->fetch_assoc();
	if ( $row["exp"] < GetGoddessNeedExp($goddessId, $row["level"]) ) {
		echo 1;
		$db->query("rollback");
		$db->close();
		return;
	}

	$mats = GetNeedQuestMats($goddessId, (int)$row["level"]);
	$matCount = (int)(count($mats)/2);
	for ( $i=0; $i<$matCount; $i++ ) {
		$rewardType = $mats[$i*2];
		$rewardCount = $mats[$i*2+1];
	//	echo $rewardType.", ".$rewardCount."\n";
		$resultVal = ConsumeResource($db, $id, $i, $rewardType, $rewardCount);
		$data["consumeType"][] = $rewardType;
	    $data["consumeCount"][] = $rewardCount;
		if ( $resultVal == 0) {
		  echo 0;
		  $db->query("rollback");
		  $db->close();
		  return;
		}
		else if ( $resultVal == -1 ) {
		  echo 1;
		  $db->query("rollback");
		  $db->close();
		  return;
		}
	}

	$questIdx = (int)$row["level"];
	$resultLevel = $questIdx+1;
	if ( !GetAbillity( $db, $id, $questIdx, $goddessId, $data ) ) {
		echo 1;
		$db->query("rollback");
		$db->close();
		return;
	}

	$query = sprintf("update frdGoddess set level=%d, exp=0 where userId=%d and goddessId=%d", $resultLevel, $id, $goddessId);
	$isGood = $db->query($query);
	if ($isGood == false) {
		echo 0;
		$db->query("rollback");
		$db->close();
		return;
	}
	$data["resultLevel"] = $resultLevel;

	$db->query("commit");
	$db->close();

  	echo json_encode($data);

?>
