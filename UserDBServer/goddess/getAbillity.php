<?php
	function GetAbillity( &$db, $id, $questIdx, $goddessId, &$data ) {

		$privateIdx = ($goddessId-1)*5+$questIdx;

		if ( $privateIdx < 63 ) {
			$bundleIdx = 0;
			$mask = 1 << $privateIdx;
			$bundleName = "bundle0";
		}
		else {
			$bundleIdx = 1;
			$mask = 1 << ($privateIdx-64);
			$bundleName = "bundle1";
		}

		$query = sprintf("select bundle0, bundle1 from frdAbillity where userId=%d", $id);
		$res = $db->query($query);
		if ($res == false)
			return false;

		if ( $res->num_rows <= 0 ) {
			if ( $bundleIdx == 0 ) {
				$bundle0 = $mask;
				$bundle1 = 0;
				$query = sprintf("insert into frdAbillity values (%d, 0, %d, '%s', 0, 0, 0)", $id, GetCantUnlockSlotValue(), $mask);

			}
			else {
				$bundle0 = 0;
				$bundle1 = $mask;
				$query = sprintf("insert into frdAbillity values (%d, 0, %d, 0, '%s', 0, 0)", $id, GetCantUnlockSlotValue(), $mask);
			}
		}

		else {
			$row = $res->fetch_assoc();
			$bundle0 = $row["bundle0"];
			$bundle1 = $row["bundle1"];
			$resultMask = $row[$bundleName] | $mask;
			$query = sprintf("update frdAbillity set %s='%s' where userId=%d", $bundleName, $resultMask, $id);
		}
		$isGood = $db->query($query);
		if ($isGood == false) {
			return false;
		}

		if ( $bundleIdx == 0) {
	    	$data["bundle0"] = $resultMask;
	    	$data["bundle1"] = $bundle1;
	    }
	    else {
	    	$data["bundle0"] = $bundle0;
	    	$data["bundle1"] = $resultMask;
	    }
	    return true;
	}
?>