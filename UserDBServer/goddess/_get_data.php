<?php
  include_once("../myAes.php");

  $id = $_REQUEST["userId"];
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }

  $data = array();
  $data["error"] = 0;

  $query = sprintf("select goddessId, level, exp from frdGoddess where userId = %d", $id);
//  echo $query."\n";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  for ( $i=0; $i<$res->num_rows; $i++ ) {
 //   echo $res->num_rows."\n";
    $res->data_seek($i);
    $row = $res->fetch_assoc();

    $unit = array();
    $unit["goddessId"] = $row["goddessId"];
    $unit["level"] = $row["level"];
    $unit["exp"] = $row["exp"];
    $data["goddess"][] = $unit;

  }

  echo json_encode($data);

?>
