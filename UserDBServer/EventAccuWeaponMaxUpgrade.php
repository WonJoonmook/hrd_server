<?php

  function AccuWeaponMaxUpgrade($db, $id) {
    $now = time();
    $date = intval(date("ymd", $now));
    if ($date < 161222 || $date > 170101) {
      if ($id != 800002186 && $id != 800026064)
        return 100;
    }

    // event accu buy jewel
    $query = "select * from Event_Accu_Action where userId = $id";
    $res = $db->query($query);
    if ($res == false) {
      return 0;
    }

    if ($res->num_rows <= 0) {
      $query = "insert into Event_Accu_Action value ($id, 0, 0, 0, 0)";
      $res = $db->query($query);
      if ($res == false) {
        return 0;
      }
      $accuWeaponMaxUpgrade = 1;
    }
    else {
      $row = $res->fetch_assoc();
      $accuWeaponMaxUpgrade = $row["accuWeaponMaxUpgrade"] + 1;
    }

    if ( $accuWeaponMaxUpgrade > 7) {
      return 400;
    }

    $now = time();  // 황상뽑1, 어초권2,  특초권1,  유옵권1, 랜4마1, 랜5유1,  황상뽑3,   무룬1,    랜덤 찬란한 룬 3,     티켓5
    $itemIds = array( 100003, 100002, 100001, 100000, 10504,  1505, 100003, 110000, rand(115000, 115004), 5002 );
    $itemCounts = array(   1,      2,      1,      1,     1,     1,      3,      1,                    3,    5 );
    $rand = rand(0, 9);
    if ($rand == 9) {
      $query = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) values";
      $query .= " ($id, 5, 115000, 1, $now),";
      $query .= " ($id, 5, 115001, 1, $now),";
      $query .= " ($id, 5, 115002, 1, $now),";
      $query .= " ($id, 5, 115003, 1, $now),";
      $query .= " ($id, 5, 115004, 1, $now)";
    }
    else {
      $itemId = $itemIds[$rand];
      $itemCount = $itemCounts[$rand];
      $query = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) value ($id, 5, $itemId, $itemCount, $now)";
    }

    $isGood = $db->query($query);
    if ($isGood == false) {
      return 0;
    }

    $query = "update Event_Accu_Action set accuWeaponMaxUpgrade = $accuWeaponMaxUpgrade where userId = $id";
    $isGood = $db->query($query);
    if ($isGood == false) {
      return 0;
    }

    return 400;
  }

?>
