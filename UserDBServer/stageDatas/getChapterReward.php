<?php
  // 로비에서 스테이지 정보 화면 진입시 스테이지 정보를 요청.
  include_once("../myAes.php");

  $data = array();
  $data["error"] = 0;
  
  $chapter = $_REQUEST["chapter"];
  $userId = $_REQUEST["userId"];
  switch($chapter) {
  case 1:
    $rewardType = 1502;
    $rewardCount = 1;
    break;
  case 2:
    $rewardType = 10501;
    $rewardCount = 1;
    break;
  case 3:
    $rewardType = 100003;
    $rewardCount = 1;
    break;
  case 4:
    $rewardType = 10502;
    $rewardCount = 1;
    break;
  case 5:
    $rewardType = 1503;
    $rewardCount = 1;
    break;
  case 6:
    $rewardType = 100003;
    $rewardCount = 2;
    break;
  case 7:
    $rewardType = 10503;
    $rewardCount = 1;
    break;
  case 8:
    $rewardType = 100001;
    $rewardCount = 1;
    break;
  case 9:
    $rewardType = 100003;
    $rewardCount = 3;
    break;
  case 10:
    $rewardType = 110000;
    $rewardCount = 1;
    break;
  default:
    $rewardType = 1502;
    $rewardCount = 1;
    break;
  }

  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }
  $db->query("set autocommit=0");

  $query = "select chapterReward from frdUserData where privateId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->close();
    return;
  }

  if ($res->num_rows <= 0) {
    echo 1;
    $db->close();
    return;
  }

  $row = $res->fetch_assoc();
  $val = $row["chapterReward"];

  if ( (($val >> $chapter) & 1) == 1 ) {
    echo 1;
    $db->close();
    return;
  }

  $bitVal = (1 << $chapter);
  $val |= $bitVal;

  $query = "update frdUserData set chapterReward=$val where privateId = $userId";
  $isGood = $db->query($query);
  if ( $isGood == false ) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $time = time();
  $query = "insert into frdUserPost values (0, $userId, 0, $rewardType, $rewardCount, $time)";
  $isGood = $db->query($query);
  if ( $isGood == false ) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $data["chapterReward"] = $val;
  $db->query("commit");
  $db->close();
  
  echo json_encode($data);
?>
