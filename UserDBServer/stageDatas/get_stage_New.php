<?php
  // 로비에서 스테이지 정보 화면 진입시 스테이지 정보를 요청.
  include_once("../myAes.php");

  $stageId = $_REQUEST["stageId"];
  $data = array();

  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    return;
  }

  $json =json_decode($redis->hget('stageReward_New', $stageId), true);
  $data["heart_bundleId"] = $json["heart_bundleId"];
  $data["tw_ml_wmc_if"] = $json["tw_ml_wmc_if"];
  $data["clearConditionType"] = $json["clearConditionType"];
  $data["clearConditionVal"] = $json["clearConditionVal"];
  $data["rewardId"] = $json["rewardId"];
  $data["rewardCount"] = $json["rewardCount"];
  $data["rewardPercent"] = $json["rewardPercent"];
  $data["clearConditionIdx"] = $json["clearConditionIdx"];
  $data["specialDatas"] = $json["specialDatas"];

  if ( $stageId > 1000 ) {
    $firstRewardDatas = $redis->hget('firstClearBonus_New', $stageId);
    if ( $firstRewardDatas != null ) {
      $firstRewards = explode (",", $firstRewardDatas);
      $length = count($firstRewards);
      for ( $i=0; $i<$length; $i+=2 ) {
        $data["rewardId"][] = (int)$firstRewards[$i];
        $data["rewardCount"][] = (int)$firstRewards[$i+1];
        $data["rewardPercent"][] = 1000;
        $data["clearConditionIdx"][] = 0;
      }
      $data["firstClearBonusCount"] = (int)($length/2);
    }
  }
  

  if ( $stageId >= 500 && !($stageId>=600 && $stageId<700) ) {
    $db = getDB();
    if (mysqli_connect_errno()) {
      echo 0;
      return;
    }
    $db->query("set autocommit=0");
    $query = sprintf("select clearCount from frdClearCount where userId = '%s' and stageId=%d", $_REQUEST["userId"], $stageId);
    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    if ($res->num_rows > 0) {
      $row = $res->fetch_assoc();
      $data["clearCount"] = $row["clearCount"];
    }
    else {
      $data["clearCount"] = 0;
    }
    $db->query("commit");
    $db->close();
  }
  $redis->close();
  echo json_encode($data);
?>
