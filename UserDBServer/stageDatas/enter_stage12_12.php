<?php
  // 스테이지 진입 시 현 스테이지가 유효한지와 AP 처리 요청
  include_once("../myAes.php");

  $needVers = 1.215;
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];
  $stageId = $_REQUEST["stageId"];
  $selectedDifficult = $_REQUEST["selectedDifficult"];
  $curPId = (int)$_REQUEST["curPId"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }

  $db->query("set autocommit=0");
  $query = "select curPId from frdUserData where privateId = $id";
  $res = $db->query($query);
  if ($res == false || $res->num_rows <= 0) {
    echo 0;$db->close(); return;
  }

  $row = $res->fetch_assoc();
  if ( ($curPId+1) < (int)$row["curPId"] ) {    //curPid = 1     db = 1
    echo 4;$db->close(); return;
  }

  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    return;
  }

  if ( $stageId >= 500 && $stageId < 20000 ) {
    
    if ( $stageId >= 10000) {     // 강림
      $st = $_REQUEST["startDate"];
      $descends = $redis->zrangebyscore('descendTT', $st, $st);
      $length = count($descends);
      if ( $length <= 0 ) {
        echo 2;
        $db->query("rollback");
        $db->close();
        $redis->close();
        return;
      }

      $isFind = false;
      $bigStageId = (int)($stageId/10)*10;

      for ( $i=0; $i<$length; $i++ ) {
        $datas = explode (",", $descends[$i]);

        if ( $datas[2] == $bigStageId ) {
          $isFind = true;
          $st = $datas[0];
          $y = $st/1000000;
          $m = ($st%1000000)/10000;
          $d = ($st%10000)/100;
          $h = $st%100;
          $endTime = mktime($h,0,0,$m,$d,$y) + $datas[1]*3600 + 3600;
          
          if ( date('YmdH', $endTime) < date('YmdH') ) {
         //   echo date('YmdH', $endTime). "<". date('YmdH');
            echo 2;
            $db->query("rollback");

            return;
          }
          
          break;
        }
      }
      
      if ( !$isFind ) {
        echo 2;
        $db->query("rollback");
        $db->close();
        $redis->close();
        return;
      }
      
    }

    else if ( $stageId < 600 ){  // 요일던전
      $dayOfWeek = ($stageId-500)%7;
      if ( $dayOfWeek == 0 )
        $dayOfWeek = 7;

      if ( (int)$dayOfWeek !== (int)date("N") ) {
        echo 2;
        $db->query("rollback");
        $db->close();
        $redis->close();
        return;
      }
    }



    else if ( $stageId > 2000 && $stageId < 4000 ) {  //신전 
      $templeId = (int)(($stageId-2000)/100);
      
      $query = "select dailyCount from frdTempleClearCount where userId = $id";
      $res = $db->query($query);
      if ($res == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      
      if ( $res->num_rows > 0 ) {
        $row = $res->fetch_assoc();
        $dailyCount = (int)$row["dailyCount"];
        switch($dailyCount) {
        case 0:case 1:break;
        case 2: $needTicket = 3;break;
        case 3: $needTicket = 6;break;
        case 4: $needTicket = 9;break;
        default:$needTicket = 9999999;break;
        }
          
      }
      
    }
  
  }
  
  if ( ($stageId>=600 && $stageId<700) || ($stageId>1000&&$stageId<2000) ) {
    if ( (float)$_REQUEST["cliVers"] < (float)$needVers ) {
      echo $needVers;
      $db->query("rollback");
      return;
    }

    if ( ($stageId>=600 && $stageId<700) ) {  //climbTop
      $time = time() + 9*3600 - 24*3600*4-5*3600;
      $totalWeeks = (int)($time / 604800);
      $weekVal = (int)600 + ((int)($totalWeeks%5));
      // if ( $weekVal == 602 )
      //  $weekVal = 600;
      // if ( $weekVal == 603 ) {
      //   echo 5;
      //   $db->query("rollback");
      //   $db->close();
      //   $redis->close();
      //   return;
      // }
      if ( (int)$weekVal !== (int)$stageId ) {
        echo 2;
        $db->query("rollback");
        $db->close();
        $redis->close();
        return;
      }
      
    }
  }
  
  if ( !is_null($needTicket) )
    $query = "select exp, ticket, heart, startTime, givedHeartCount, session from frdUserData where privateId = $id";
  else if ( !is_null($needJewel) )
    $query = "select exp, jewel, heart, startTime, givedHeartCount, session from frdUserData where privateId = $id";
  else
    $query = "select exp, heart, startTime, givedHeartCount, session from frdUserData where privateId = $id";

  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;

      if ( $selectedDifficult >= 0 && $selectedDifficult < 100 ) {
        $addHeartCount=0;
        $addTotalHeartCount = getAddHeartCount($db, $redis, $id, $row["exp"], $row["startTime"], $row["givedHeartCount"], $row["heart"], $addHeartCount);
        if ( $addTotalHeartCount < 0 ) {
          echo 0;$db->query("rollback");$db->close();
          return;
        }
  
        $needHeart = $redis->hget('stageHeart_New', $stageId);
        $resultHeart = $row["heart"] + (int)$addHeartCount - $needHeart;
        if ( $resultHeart < -1) {
          addBlacklist($id, "get_stageData_noHeart");
          echo 3;
          $db->query("rollback");
          $db->close();
          $redis->close();

          return;
        }

        if ( !is_null($needTicket) ) {
          $resultTicket = (int)$row["ticket"] - $needTicket;
          if ( $resultTicket < 0) {
            addBlacklist($id, "get_stageData_noTicket");
            echo 3;
            $db->query("rollback");
            $db->close();
            $redis->close();
            return;
          }
          $query = sprintf("update frdUserData set ticket=%d, heart=%d, givedHeartCount=%d, session=%d where privateId=%d", 
            $resultTicket, $resultHeart, $addTotalHeartCount, $newSession, $id);
          $data["ticket"] = $resultTicket;
        }
        else if ( !is_null($needJewel) ) {
          $resultJewel = (int)$row["jewel"] - $needJewel;
          if ( $resultJewel < 0) {
            addBlacklist($id, "get_stageData_noJewel");
            echo 3;
            $db->query("rollback");
            $db->close();
            $redis->close();
            return;
          }
          $query = sprintf("update frdUserData set jewel=%d, heart=%d, givedHeartCount=%d, session=%d where privateId=%d", 
            $resultJewel, $resultHeart, $addTotalHeartCount, $newSession, $id);
          $data["jewel"] = $resultJewel;
        }
        else {
          $query = "update frdUserData set heart=$resultHeart, givedHeartCount=$addTotalHeartCount, session=$newSession where privateId=$id";
        }
  
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 1;
          $db->query("rollback");
          $db->close();
          return;
        }

        $data["heart"] = $resultHeart;
        $data["givedHeartCount"] = $addTotalHeartCount;
        $data["time"] = time();
      }
      else {    //load
        $query = sprintf("select length from saveDatas where userId = %d", $id);
        $sres = $db->query($query);
        if ($sres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $saveLength = (int)$_REQUEST["saveLength"];
        if ( $sres->num_rows > 0) {
          $srow = $sres->fetch_assoc(); 
          /*
          if ( (int)$srow["length"] == $saveLength ) {
            addBlacklist($id, "hack_SaveFile");
            echo 1;
            $db->close();
            $redis->close();
            return;
          }
          */
          $query = sprintf("update saveDatas set length=%d where userId=%d", $saveLength, $id);
        }
        else {
          $query = sprintf("insert into saveDatas values (%d, %d)", $id, $saveLength);
        }
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        
        $query = sprintf("update frdUserData set session=%d where privateId=%d", $newSession, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
      }
    }
    else {
      addBlacklist($id, "get_stageRewards_session");
      echo 1;
      $db->query("rollback");
      $db->close();
      $redis->close();
      return;
    }
  }
  $res->close();
  $db->query("commit");
  $db->close();

  $json =json_decode($redis->hget('stageData_New', $stageId), true);
  $data["heart_bundleId"] = $json["heart_bundleId"];
  $data["waveTimer"] = $json["waveTimer"];
  $data["monInform"] = $json["monInform"];
  $data["monGenCount"] = $json["monGenCount"];
  $data["monGenIntervalFrame"] = $json["monGenIntervalFrame"];
  $data["waveRewardCharacs"] = $json["waveRewardCharacs"];
  $data["monPath"] = $json["monPath"];
  $data["tw_ml_wmc_if"] = $json["tw_ml_wmc_if"];
  $data["bossWave"] = $json["bossWave"];
  $data["startGenCharacs"] = $json["startGenCharacs"];
  $data["characPos"] = $json["characPos"];
  $data["specialDatas"] = $json["specialDatas"];
  

  $redis->close();
  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
