<?php
  // 스테이지 진입 시 현 스테이지가 유효한지와 AP 처리 요청
  include_once("../myAes.php");

  $needVers = 1.118;
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];
  $stageId = $_REQUEST["stageId"];
  $selectedDifficult = $_REQUEST["selectedDifficult"];
  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }

  $db->query("set autocommit=0");
  function FindLevel($count, $redis, $min, $max, $exp) {
    $count--;
    if ( $count <= 0 )
      return 0;

    if ( $min == $max )
      return $min;

  //  echo($min." ".$max." \n");

    $center = (int)(($min+$max)>>1);

    $lowExp = $redis->lindex('userNeedExpAccu1', $center);
    $highExp = $redis->lindex('userNeedExpAccu1', $center+1);
    if ( $exp < $lowExp ) {
      return FindLevel($count, $redis, $min, $center, $exp);
    } 
    else if ( $exp >= $highExp ) {
      return FindLevel($count, $redis, $center+1, $max, $exp);
    }
    else {  //find
      return $center+1;
    }
  }


  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    return;
  }

  if ( $stageId >= 500 && $stageId < 20000 ) {
    
    if ( $stageId >= 10000) {     // 강림
      $st = $_REQUEST["startDate"];
      $descends = $redis->zrangebyscore('descendTT', $st, $st);
      $length = count($descends);
      if ( $length <= 0 ) {
        echo 2;
        $db->close();
        $redis->close();
        return;
      }

      $isFind = false;
      $bigStageId = (int)($stageId/10)*10;

      for ( $i=0; $i<$length; $i++ ) {
        $datas = explode (",", $descends[$i]);

        if ( $datas[2] == $bigStageId ) {
          $isFind = true;
          $st = $datas[0];
          $y = $st/1000000;
          $m = ($st%1000000)/10000;
          $d = ($st%10000)/100;
          $h = $st%100;
          $endTime = mktime($h,0,0,$m,$d,$y) + $datas[1]*3600 + 3600;
          
          if ( date('YmdH', $endTime) < date('YmdH') ) {
         //   echo date('YmdH', $endTime). "<". date('YmdH');
            echo 2;
            return;
          }
          
          break;
        }
      }
      
      if ( !$isFind ) {
        echo 2;
        $db->close();
        $redis->close();
        return;
      }
      
    }

    else if ( $stageId < 600 ){  // 요일던전
      $dayOfWeek = ($stageId-500)%7;
      if ( $dayOfWeek == 0 )
        $dayOfWeek = 7;

      if ( (int)$dayOfWeek !== (int)date("N") ) {
        echo 2;
        $db->close();
        $redis->close();
        return;
      }
    }

    //Event ~

    else if ( $stageId >= 600 && $stageId < 610 ) { //고탑
      $query = "select dailySeconds from Event_7_5_5 where userId = $id";
      $eres = $db->query($query);
      if ($eres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      $day = ((int)date('d'));
      $isRecvEvent = false;
      $query = "";
      if ( $eres->num_rows <= 0 ) {
        $isRecvEvent = true;
        $query = "insert into Event_7_5_5 values ($id, $day, 0)";
      }
      else {
        $erow = $eres->fetch_assoc();
        if ( (int)$erow["dailySeconds"] !== $day ) {
          $isRecvEvent = true;
          $query = "update Event_7_5_5 set dailySeconds=$day where userId=$id"; 
        }
      }
      
      if ( $isRecvEvent && $query !== "" ) {
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $onlyDayTime = mktime(0,0,0, date('m'), date('d')-6, date('y'));
        switch( $day ) {
          case 24:  //test day
          $query = "insert into frdUserPost values (0, $id, 0, 5000, 10000, $onlyDayTime)";
          break;

          case 26:  //day 1
          $query = "insert into frdUserPost values (0, $id, 0, 5000, 10000, $onlyDayTime)";
          break;

          case 27:  //day 2
          $query = "insert into frdUserPost values (0, $id, 0, 5006, 200, $onlyDayTime)";
          break;

          case 28:  //day 3
          $query = "insert into frdUserPost values (0, $id, 0, 5004, 30, $onlyDayTime)";
          break;

          case 29:  //day 4
          $query = "insert into frdUserPost values (0, $id, 0, 5002, 5, $onlyDayTime)";
          break;

          case 30:  //day 5
          $query = "insert into frdUserPost values (0, $id, 0, 100003, 1, $onlyDayTime)";
          break;

          case 1:  //day 6
          $query = "insert into frdUserPost values (0, $id, 0, 100000, 1, $onlyDayTime)";
          break;

          case 2:  //day 7
          $query = "insert into frdUserPost values (0, $id, 0, 100001, 1, $onlyDayTime)";
          break;

          default:
          $query = "";
          break;
        }
        if ( $query !== "" ) {
          $isGood = $db->query($query);
          if ($isGood == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }
        }
      }

    }

    // ~Event


    else if ( $stageId > 2000 && $stageId < 4000 ) {  //신전 
      $templeId = (int)(($stageId-2000)/100);
      
      $query = "select dailyCount from frdTempleClearCount where userId = $id";
      $res = $db->query($query);
      if ($res == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      
      if ( $res->num_rows > 0 ) {
        $row = $res->fetch_assoc();
        $dailyCount = (int)$row["dailyCount"];
        switch($dailyCount) {
        case 0:case 1:break;
        case 2: $needTicket = 3;break;
        case 3: $needTicket = 6;break;
        case 4: $needTicket = 9;break;
        default:$needTicket = 9999999;break;
        }
          
      }
      
    }
  
  }
  
  if ( ($stageId == 0) || ($stageId>=600 && $stageId<700) || ($stageId>1000&&$stageId<2000) ) {
    if ( (float)$_REQUEST["cliVers"] < (float)$needVers ) {
      echo $needVers;
      return;
    }

    if ( ($stageId>=600 && $stageId<700) ) {  //climbTop
      $time = time() + 9*3600 - 24*3600*4-5*3600;
      $totalWeeks = (int)($time / 604800);
      $weekVal = (int)600 + ((int)($totalWeeks%5));
      
      if ( (int)$weekVal !== (int)$stageId ) {
        echo 2;
        $db->close();
        $redis->close();
        return;
      }

    }
  }
  
  if ( !is_null($needTicket) )
    $query = "select exp, ticket, heart, startTime, givedHeartCount, session from frdUserData where privateId = $id";
  else if ( !is_null($needJewel) )
    $query = "select exp, jewel, heart, startTime, givedHeartCount, session from frdUserData where privateId = $id";
  else
    $query = "select exp, heart, startTime, givedHeartCount, session from frdUserData where privateId = $id";

  $res = $db->query($query);
  if ($res == false) {
    echo mysqli_error($db);
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;

      if ( $selectedDifficult >= 0 && $selectedDifficult < 100 ) {

        $addTotalHeartCount = (int)((time() - $row["startTime"])/180);
        if ( (int)$row["givedHeartCount"] < $addTotalHeartCount ) { 
          $addHeartCount = $addTotalHeartCount - $row["givedHeartCount"];

          $userLevel = FindLevel(50, $redis, 0, $redis->llen('userNeedExp1')-1, $row["exp"]);
          $maxHeart = $redis->lindex('userMaxHeart1', $userLevel);
          $needMaxHeart = $maxHeart - $row["heart"];
          if ( $needMaxHeart < 0)
            $needMaxHeart = 0;
          if ( $addHeartCount > $needMaxHeart ) {
         //   $addTotalHeartCount += $addHeartCount;
            $addHeartCount = $needMaxHeart;
          }
        }

        $needHeart = $redis->hget('stageHeart_New', $stageId);
        $resultHeart = $row["heart"] + (int)$addHeartCount - $needHeart;
        if ( $resultHeart < -1) {
          addBlacklist($id, "get_stageData_noHeart");
          echo 3;
          $db->close();
          $redis->close();
          return;
        }

        if ( !is_null($needTicket) ) {
          $resultTicket = (int)$row["ticket"] - $needTicket;
          if ( $resultTicket < 0) {
            addBlacklist($id, "get_stageData_noTicket");
            echo 3;
            $db->close();
            $redis->close();
            return;
          }
          $query = sprintf("update frdUserData set ticket=%d, heart=%d, givedHeartCount=%d, session=%d where privateId=%d", 
            $resultTicket, $resultHeart, $addTotalHeartCount, $newSession, $id);
          $data["ticket"] = $resultTicket;
        }
        else if ( !is_null($needJewel) ) {
          $resultJewel = (int)$row["jewel"] - $needJewel;
          if ( $resultJewel < 0) {
            addBlacklist($id, "get_stageData_noJewel");
            echo 3;
            $db->close();
            $redis->close();
            return;
          }
          $query = sprintf("update frdUserData set jewel=%d, heart=%d, givedHeartCount=%d, session=%d where privateId=%d", 
            $resultJewel, $resultHeart, $addTotalHeartCount, $newSession, $id);
          $data["jewel"] = $resultJewel;
        }
        else {
          $query = "update frdUserData set heart=$resultHeart, givedHeartCount=$addTotalHeartCount, session=$newSession where privateId=$id";
        }
  
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo mysqli_error($db);
          $db->query("rollback");
          $db->close();
          return;
        }

        $data["heart"] = $resultHeart;
        $data["givedHeartCount"] = $addTotalHeartCount;
        $data["time"] = time();
      }
      else {    //load
        $query = sprintf("select length from saveDatas where userId = %d", $id);
        $sres = $db->query($query);
        if ($sres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $saveLength = (int)$_REQUEST["saveLength"];
        if ( $sres->num_rows > 0) {
          $srow = $sres->fetch_assoc(); 
          /*
          if ( (int)$srow["length"] == $saveLength ) {
            addBlacklist($id, "hack_SaveFile");
            echo 1;
            $db->close();
            $redis->close();
            return;
          }
          */
          $query = sprintf("update saveDatas set length=%d where userId=%d", $saveLength, $id);
        }
        else {
          $query = sprintf("insert into saveDatas values (%d, %d)", $id, $saveLength);
        }
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        
        $query = sprintf("update frdUserData set session=%d where privateId=%d", $newSession, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
      }
    }
    else {
      addBlacklist($id, "get_stageRewards_session");
      echo 1;
      $db->close();
      $redis->close();
      return;
    }
  }
  $res->close();
  $db->query("commit");
  $db->close();

  $json =json_decode($redis->hget('stageData_New', $stageId), true);
  $data["heart_bundleId"] = $json["heart_bundleId"];
  $data["waveTimer"] = $json["waveTimer"];
  $data["monInform"] = $json["monInform"];
  $data["monGenCount"] = $json["monGenCount"];
  $data["monGenIntervalFrame"] = $json["monGenIntervalFrame"];
  $data["waveRewardCharacs"] = $json["waveRewardCharacs"];
  $data["monPath"] = $json["monPath"];
  $data["tw_ml_wmc_if"] = $json["tw_ml_wmc_if"];
  $data["bossWave"] = $json["bossWave"];
  $data["startGenCharacs"] = $json["startGenCharacs"];
  $data["characPos"] = $json["characPos"];
  $data["specialDatas"] = $json["specialDatas"];
  

  $redis->close();
  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
