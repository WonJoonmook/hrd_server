<?php
  // 스테이지 클리어/게임오버 후 요청.
  // 보상, 경험치 지급등 처리후 결과를 뿌려줍니다.
  include_once("../globalDefine.php");
  include_once("../myAes.php");
  include_once("../getReward2.php");
  include_once("../getBonus1.php");
  include_once("../temple/_calculate10_5.php");
  include_once("../goddess/getAbillity.php");
  include_once("../goddess/_calculate8_4.php");

  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];
  $isClear = $_REQUEST["isClear"];
  $stageId = (int)$_REQUEST["stageId"];
  $lastWave = (int)$_REQUEST["lastWave"];
  $seconds = (int)$_REQUEST["seconds"];
  $difficulty = (int)$_REQUEST["difficulty"];
//  $_REQUEST["bonus0"] ~ $_REQUEST["bonus3"]
//  $_REQUEST["weaponIdx".$idxJ]
//  $_REQUEST["curRewardId0"]
//  echo $session." ".$id." ".$isClear." ".$stageId." ".$lastWave." ".$seconds." ".$difficulty."<br>";
  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");


  function IsSuccess($conditionType, $lastWave, $std, $seconds, $difficulty, $isClear) {

    switch($conditionType) {
    case 0:
      $value = (int)$seconds;
      if ( $std < $value || !$isClear )
        return false;
      else
        return true;

    case 4:
      if ( $lastWave >= $std )
        return true;
      else
        return false;

    case 5:
      if ( $isClear > 0 )
        return true;
      else
        return false;

    case 6:
      return true;

    case 10:      // Life difficulty 0
      return true;

    case 11:      // Life difficulty 1
      if ( $difficulty >= 1 && $isClear > 0 )
        return true;
      else
        return false;

    case 12:      // Life difficulty 2
      if ( $difficulty >= 2 && $isClear > 0 )
        return true;
      else
        return false;

    case 13:      // Life difficulty 3
      if ( $difficulty >= 3 && $isClear > 0 )
        return true;
      else
        return false;

    default:
      return false;
    }
  }

  function getFirstClearBonus($redis, $stageId, &$resultRewardType, &$resultRewardCount) {
    $firstRewardDatas = $redis->hget('firstClearBonus', $stageId);
    if ( !is_null($firstRewardDatas) ) {
      $firstRewards = explode (",", $firstRewardDatas);
      for ( $i=0; $i<count($firstRewards); $i+=2 ) {
        $resultRewardType[] = (int)$firstRewards[$i];
        $resultRewardCount[] = (int)$firstRewards[$i+1];
      }
    }
  }

  if ( $isClear > 0 && $difficulty >= 0 && $stageId>80 ) {   //check save hack
    $query = sprintf("select * from frdLastPlayTime where userId = %d", $id);
    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
    if ( $res->num_rows <= 0 ) {
      $query = sprintf("insert into frdLastPlayTime values (%d, %d, %d, %d, %d)", $id, time(), 0, $stageId, 0);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
    }
    else {
      $row = $res->fetch_assoc();
      $curTime = time();
      $checkTimer = 240;
      if ( $stageId>1000 && $stageId<2000 )
        $checkTimer = 180;

      if ( $curTime - (int)$row["lastTime2"] < $checkTimer ) { // 10분
        $one = $curTime - (int)$row["lastTime1"];
        $two = (int)$row["lastTime1"] - (int)$row["lastTime2"];
        addBlacklist($id, "hackSave_".$one."_".$two);
        echo 1;
        $db->close();
        return;
      }
      $query = sprintf("update frdLastPlayTime set lastTime1=%d, lastTime2=%d, lastStage1=%d, lastStage2=%d where userId=%d"
                      ,$curTime, (int)$row["lastTime1"], $stageId, (int)$row["lastStage1"], $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
    }
  }




  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    $db->close();
    return;
  }

  $needHeart = $redis->hget('stageHeart_New', $stageId);
  $query = sprintf("select * from frdUserData where privateId = %d", $id);
  $res = $db->query($query);
  if ($res == false || $res->num_rows <= 0) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $row = $res->fetch_assoc();
  if ( (int)$row["session"] !== (int)$session ) {
    addBlacklist($id, "get_stageRewards_session");
    echo $row["session"].", ".$session;
    $db->close();
    $redis->close();
    return;
  }

  $newSession = mt_rand();
  $data["session"] = $newSession;

  $addHeartCount=0;
  $addTotalHeartCount = getAddHeartCount($db, $redis, $id, $row["exp"], $row["startTime"], $row["givedHeartCount"], $row["heart"], $addHeartCount);
  if ( $addTotalHeartCount < 0 ) {
    echo 0;$db->query("rollback");$db->close();
    return;
  }

  $resultHeart = $row["heart"] + $addHeartCount;
  $query = sprintf("update frdUserData set heart=%d, givedHeartCount=%d, session=%d where privateId='%s'",
      $resultHeart, $addTotalHeartCount, $newSession, $id);

  $isGood = $db->query($query);
  if ($isGood == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ( $difficulty < 0) {
    if ( $stageId > 20000 )
      $needTicket = (int)floor($needHeart/4);
    else
      $needTicket = (int)floor($needHeart/6);

    $resultTicket = $row["ticket"]-$needTicket;
    if ( $resultTicket < 0) {
      addBlacklist($id, "get_stageReward_noTicket");
      echo 1;
      $db->close();
      $redis->close();
      return;
    }

    $resultHeart = $resultHeart - $needHeart;
    if ( $resultHeart < 0) {
      addBlacklist($id, "get_stageReward_noHeart");
      echo 1;
      $db->close();
      $redis->close();
      return;
    }

    // Log ~
    $query = sprintf("select id from frdLogPlay where id = '%s' and stageId=%d", $id, $stageId);
    $sres = $db->query($query);
    if ($sres == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    $query = sprintf("update frdLogPlay set ticketCount=ticketCount+1 where id='%s' and stageId=%d", $id, $stageId);
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
    // ~ Log

    $data["ticket"] = $resultTicket;
    $query = sprintf("update frdUserData set heart=%d, ticket=%d where privateId='%s'", $resultHeart, $resultTicket, $id);
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }
  else {
    $isHack = $_REQUEST["isHack"];
    if ( $isHack > 0 ) {
      addBlacklist($id, "hack");
      echo 1;
      $db->close();
      $redis->close();
      return;
    }

    $data["needHeart"] = $needHeart;

    // Log ~
    $query = sprintf("select id from frdLogPlay where id = '%s' and stageId=%d", $id, $stageId);
    $sres = $db->query($query);
    if ($sres == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
    if ($sres->num_rows <= 0) {
      $query = sprintf("insert into frdLogPlay values ('%s', %d, %d, '%s', 1, 0)", $id, $stageId, $lastWave, $seconds);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
    }
    else {
      $query = sprintf("update frdLogPlay set totalWave=totalWave+%d, totalSecs=totalSecs+%d, playCount=playCount+1 where id='%s' and stageId=%d", $lastWave, $seconds, $id, $stageId);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
    }
    $sres->close();
    // ~ Log


    if ( ($lastWave > 50) && (($seconds/$lastWave) <= 3) ) {
      addBlacklist($id, "hack_TimeAndWave");
      echo 1;
      $db->close();
      $redis->close();
      return;
    }

  }


  $data["heart"] = $resultHeart;
  $data["givedHeartCount"] = $addTotalHeartCount;
  $data["time"] = time();

  $idx = 0;
  $weaponIdPointer = $row["weaponIdPointer"];
  $artifactIdPointer = $row["artifactIdPointer"];
  $curMaxStageLevel = ((int)($row["stageLevel"] / 10000000));
  $curMaxWave = (int)( ((int)($row["stageLevel"] % 10000000)) / 10000);

  if ( $isClear ) {
    $query = sprintf("select clearCount from frdClearCount where userId=%d and stageId=%d", $id, $stageId);
    $sres = $db->query($query);
    if ($sres == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
    if ($sres->num_rows <= 0)
      $query = sprintf("insert into frdClearCount values (%d, %d, 1)", $id, $stageId);
    else
      $query = sprintf("update frdClearCount set clearCount=clearCount+1 where userId=%d and stageId=%d", $id, $stageId);

    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }
  $resultRewardType = array();
  $resultRewardCount = array();
  if ( $stageId < 210 ) { //normal and hidden
    if ( ($curMaxStageLevel <= $stageId-1) ) {
      $resultStageLevel;
      if ( ($isClear > 0) ) {//&& ($lastWave >= $curMaxWave-6) ) {
        $resultStageLevel = ($stageId)*10000000;

        $query = sprintf("update frdUserData set stageLevel=%d where privateId='%s'", $resultStageLevel, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $data["stageLevel"] = $resultStageLevel;
        $data["isFinalStageClear"] = 1;

        getFirstClearBonus($redis, $stageId, $resultRewardType, $resultRewardCount);

        $addVal = (((int)$row["tutoLevel"]) & 0xf0000000);
        $tutoLevel = $stageId;
        switch($stageId) {
        case 1:
          $tutoLevel = 3;break;
        case 2:case 3:case 4:case 5:
          $tutoLevel = 5;break;
        default:
          $tutoLevel = $stageId;break;
        }
        $query = sprintf("update frdUserData set tutoLevel=%d where privateId=%d", $addVal+$tutoLevel, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $data["tutoLevel"] = $addVal+$tutoLevel;

      }
      else if ($lastWave >= $curMaxWave) {
        $resultStageLevel = ($stageId-1)*10000000 + $lastWave*10000;

        $query = sprintf("update frdUserData set stageLevel=%d where privateId='%s'", $resultStageLevel, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $data["stageLevel"] = $resultStageLevel;
      }
    }
  }
  else if ( $stageId > 2000 && $stageId < 4000 ) {    // 신전 스테이지
    $diffLevel = $stageId%100;
    $templeId = (int)round(($stageId-2000) / 100);

    if ( $isClear > 0 ) {

      $query = "select dailyCount, totalCount from frdTempleClearCount where userId = $id";
      $sres = $db->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      if ( $sres->num_rows <= 0) {
        $resultTempleDCC = 1;
        $query = "insert into frdTempleClearCount values ($id, 1, 1)";
      }
      else {
        $srow = $sres->fetch_assoc();
        $resultTempleDCC = (int)$srow["dailyCount"]+1;
        if ( $resultTempleDCC >= 6) {
          echo 1;
          $db->query("rollback");
          $db->close();
          return;
        }

        $totalCount = (int)$srow["totalCount"]+1;
        $query = "update frdTempleClearCount set dailyCount=$resultTempleDCC, totalCount=$totalCount where userId=$id";
      }
      $data["templeDCC"] = $resultTempleDCC;
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }


      $query = sprintf("select clearLevel from frdTemple where userId=%d and templeId=%d", $id, $templeId);
      $sres = $db->query($query);
      if ($sres == false) {
        echo 1;
        $db->query("rollback");
        $db->close();
        return;
      }

      if ( $sres->num_rows <= 0 ) {
        $query = sprintf("insert into frdTemple values (%d, %d, 1, 1, %d, 0, 0, 0, 0)", $id, $templeId, time());
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 1;
          $db->query("rollback");
          $db->close();
          return;
        }

        if ( !GetAbillity( $db, $id, 0, $templeId, $data ) ) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $data["templeClearLevel"] = 1;
        $query = sprintf("insert into frdGoddess values (%d, %d, 1, 0)", $id, $templeId);
      }
      else {
        $srow = $sres->fetch_assoc();
        if ( $diffLevel >= $srow["clearLevel"] ) {
          $query = "update frdTemple set clearLevel=($diffLevel+1) where userId=$id and templeId=$templeId";
          $data["templeClearLevel"] = $diffLevel+1;
        }
        else
          $query = null;
      }
      if ( $query !== null ) {
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
      }
    }

  }
  else {    // 요일 강림 히든 심연 스테이지
    if ( $isClear > 0 ) {

      if ( $stageId > 1000 && $stageId < 2000 ) {   // abyss

        $getScore = (int)$redis->lindex('abyssScore', $stageId-1000);
        $query = sprintf("select level, score from frdAbyss where userId = %d", $id);
        $sres = $db->query($query);
        if ($sres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        if ( $sres->num_rows > 0 ) {
          $arow = $sres->fetch_assoc();
          $getScore += $arow["score"];
          if ( $stageId > $arow["level"] ) {
            $query = sprintf("update frdAbyss set level=%d, score=%d where userId = %d", $stageId, $getScore, $id);
            $dres = $db->query($query);

            getFirstClearBonus($redis, $stageId, $resultRewardType, $resultRewardCount);
          }
          else {
            $query = sprintf("update frdAbyss set score=%d where userId = %d", $getScore, $id);
            $dres = $db->query($query);
          }
        }
        else {
          $query = sprintf("insert into frdAbyss values (%d, %d, 0, %d)", $id, $stageId, $getScore);
          $dres = $db->query($query);

          getFirstClearBonus($redis, $stageId, $resultRewardType, $resultRewardCount);
        }
        if ($dres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

      }
    }
  }





  // ----------------- reward set start ----------------------
  $json = json_decode($redis->hget('stageReward_New', $stageId), true);

  $query = sprintf("select type, val from frdEffectForRewards where userId=%d",$id);
  $eres = $db->query($query);
  if ($eres == false) {
    echo 0;$db->query("rollback");$db->close();
    return 0;
  }

  $effectType = array();
  $effectVal = array();
  for ( $i=0; $i< $eres->num_rows; $i++ ) {
    $erow = $eres->fetch_assoc();
    $effectType[] = (int)$erow["type"];
    $effectVal[] = (int)$erow["val"];
  }
  
  $length = count($json["clearConditionType"]);
  for ( $i=0; $i<$length; $i++ ) {
    $isSuccess[$i] = IsSuccess($json["clearConditionType"][$i], $lastWave, $json["clearConditionVal"][$i], $seconds, $difficulty, $isClear);
  }

  $length = count($json["rewardId"]);
  for ( $i=0; $i<$length; $i++ ) {
    $type = $json["rewardId"][$i];
    if ( $type == 5005 ) {   //medal
      $addPercent = (int)($_REQUEST["bonus3"]);
      $idx = array_search((int)$GLOBALS['$ABILL_PercentUp_Medal'], $effectType);
      if ( $idx !== false )
        $addPercent += round($effectVal[$idx]/100);

      if ( $addPercent > 0 )
        $json["rewardPercent"][$i] += $addPercent;
    }
  }


  $rewardItemIdxPointer=0;
  $lastRIPointer= $json["clearConditionIdx"][0];

  // event
  $eventD = (int)date('d');
  $eventH = (int)date('H');
  //

  for ( $i=0; $i<$length; $i++ ) {
    $conditionIdx = $json["clearConditionIdx"][$i];
    $rewardType = $json["rewardId"][$i];
    $rewardPercent = (int)($json["rewardPercent"][$i]);

    // event
    // if ( $eventD == 10 &&
    //     ($eventH >= 20 && $eventH < 22) &&
    //      $rewardType>=1000 && $rewardType<1508  )
    //   $rewardPercent = (int)($rewardPercent*1.5);
    //

    if ( $isSuccess[$conditionIdx] && (rand(0, 999) < ($rewardPercent * (1 + $difficulty*0.10))) ) {  //receive
      $rewardCount = $json["rewardCount"][$i];
      if ( $rewardType > 500 && $rewardType < 507 ) { //random soulstone
        $rewardType = $_REQUEST["curRewardId".$i];
        if ( is_null($rewardType) ) {
          $rewardType = $json["rewardId"][$i];
          $min = $redis->lindex('characMinIdx', $rewardType-500);
          $max = $redis->lindex('characMaxIdx', $rewardType-500);
          $rewardType = rand($min, $max);
        }
      }
      else if ( $rewardType > 1500 && $rewardType < 1508 ) {  //random weapon
        $rewardType =$_REQUEST["curRewardId".$i];
  //      echo "FU ".$i." = ".$rewardType."\n";
        if ( is_null($rewardType) ) {
          $rewardType = $json["rewardId"][$i];
          $min = $redis->lindex('weaponMinIdx', $rewardType-1500)+1000;
          $max = $redis->lindex('weaponMaxIdx', $rewardType-1500)+1000;
          while ( $rewardCount > 1 ) {
            $rewardType = rand($min, $max);
            $resultRewardType[] = $rewardType;
            $resultRewardCount[] = 1;
            $rewardCount--;
          }
          $rewardType = rand($min, $max);
        }
      }
      else if ( $rewardType > 10500 && $rewardType < 10508 ) {  //random magic
        $rewardType =$_REQUEST["curRewardId".$i];
        if ( is_null($rewardType) ) {
          $rewardType = $json["rewardId"][$i];
          $min = $redis->lindex('magicMinIdx1', $rewardType-10500)+10000;
          $max = $redis->lindex('magicMaxIdx1', $rewardType-10500)+10000;
          while ( $rewardCount > 1 ) {
            $rewardType = rand($min, $max);
            $resultRewardType[] = $rewardType;
            $resultRewardCount[] = 1;
            $rewardCount--;
          }
          $rewardType = rand($min, $max);
        }
      }
      else if ( $rewardType == 5000 ) {    // gold
        if ( $conditionIdx == 0 ) {
          if ( $difficulty < 0 )
            $rewardCount *= ($lastWave>>1);
          else {
            if ( $stageId>1000 && $stageId<1500 ) //abyss
              $rewardCount *= 100;
            else
              $rewardCount *= $lastWave;
          }
        }

        $rewardCount += (int)($rewardCount * $_REQUEST["bonus0"] / 1000);

      }
      else if ( $rewardType == 5001 ) {    // exp
        if ( $conditionIdx == 0 ) {
          if ( $difficulty < 0 )
            $rewardCount *= ($lastWave>>1);
          else{
            if ( $stageId>1000 && $stageId<1500 ) //abyss
              $rewardCount *= 100;
            else
              $rewardCount *= $lastWave;
          }
        }
        $rewardCount += (int)($rewardCount * $_REQUEST["bonus1"] / 1000);
      }
      //  5002 ticket   5003 heart    5004 jewel
      else if ( $rewardType == 5005 ) {    // medal
        if ( rand(0,999) < (int)($_REQUEST["bonus2"]) )
          $rewardCount *= 2;
      }

      if ( $lastRIPointer < $conditionIdx ) {
        $lastRIPointer = $conditionIdx;
        $rewardItemIdxPointer++;
      }


      if ( in_array($rewardType, $resultRewardType) && !($rewardType>=1000&&$rewardType<2000) && !($rewardType>=10000&&$rewardType<11000)) {
        $tempIdx = array_search($rewardType, $resultRewardType);
        $resultRewardCount[$tempIdx] += $rewardCount;
      }
      else {
        $resultRewardType[] = $rewardType;
        $resultRewardCount[] = $rewardCount;
      }
    }
  }


  if ( $isClear > 0 ) {
    
    $questGetPer = 1;
    $questCountVal = 1;
    $runeGetPer = 1;
    $runeCountVal = 1;
    //event
    // if ( $eventD == 7 || $eventD == 12 ) {
    //   if ( $eventH >= 12 && $eventH < 14 )
    //     $questGetPer = 1.2;

    //   if ( $eventH >= 20 && $eventH < 22 )
    //     $questCountVal = 2;
    // }

    // if ( $eventD == 9 ) {
    //   if ( $eventH >= 12 && $eventH < 14 )
    //     $runeGetPer = 1.2;

    //   if ( $eventH >= 20 && $eventH < 22 )
    //     $runeCountVal = 1.4;
    // }

    // if ( $eventD == 11 ) {
    //   if ( $eventH >= 20 && $eventH < 22 )
    //     $runeCountVal = 1.4;
    // }

    // if ( $eventD == 13 ) {
    //   if ( $eventH >= 20 && $eventH < 22 )
    //     $runeGetPer = 1.2;
    // }
    //~
    $idx = array_search((int)$GLOBALS['$ABILL_Up_Per_Rune'], $effectType);
    if ( $idx !== false )
      $runeGetPer += ($runeGetPer * ($effectVal[$idx]/100000));

    if ( ($stageId > 0 && $stageId < 210) || ($stageId > 20000) ) {   //normal, hidden
      if ($stageId > 20000) {
        if ( $diffLevel < 0 )
          $mulVal = 1.75;
        else
          $mulVal = 2;
        $chapter = (int)($stageId-20000-1);
      }
      else {
        $mulVal = 1;
        $chapter = (int)(($stageId-1)/10);
      }
      if ( rand(0,99) < $needHeart*0.1*$mulVal*$questGetPer) { $resultRewardType[] = 106000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
      if ( rand(0,99) < $needHeart*0.3*$mulVal*$questGetPer) { $resultRewardType[] = 105000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
      if ( rand(0,99) < $needHeart*0.6*$mulVal*$questGetPer) { $resultRewardType[] = 104000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
      if ( rand(0,99) < $needHeart*1.25*$mulVal*$questGetPer) { $resultRewardType[] = 103000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
      if ( rand(0,99) < $needHeart*2*$mulVal*$questGetPer) { $resultRewardType[] = 102000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
      if ( rand(0,99) < $needHeart*2.5*$mulVal*$questGetPer) { $resultRewardType[] = 101000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }

      if ( rand(0,99) < $needHeart*0.2*$mulVal*$runeGetPer) { $resultRewardType[] = 115000 + rand(0,4);   $resultRewardCount[] = 1*$runeCountVal; }
      if ( rand(0,99) < $needHeart*0.6*$mulVal*$runeGetPer) { $resultRewardType[] = 114000 + rand(0,4);   $resultRewardCount[] = floor(rand(1,3)*$runeCountVal); }
      if ( rand(0,99) < $needHeart*1*$mulVal*$runeGetPer) { $resultRewardType[] = 113000 + rand(0,4);   $resultRewardCount[] = floor(rand(2,4)*$runeCountVal); }
      if ( rand(0,99) < $needHeart*1.25*$mulVal*$runeGetPer) { $resultRewardType[] = 112000 + rand(0,4);   $resultRewardCount[] = floor(rand(2,6)*$runeCountVal); }
      if ( rand(0,99) < $needHeart*1.65*$mulVal*$runeGetPer) { $resultRewardType[] = 111000 + rand(0,4);   $resultRewardCount[] = floor(rand(3,7)*$runeCountVal); }
    }
    else if ( $stageId > 2000 && $stageId < 4000 ) {
      $chapter = $templeId-1;

      switch($diffLevel) {
      case 0:
        if ( rand(0,99) < 1*$questGetPer) { $resultRewardType[] = 106000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 5*$questGetPer) { $resultRewardType[] = 105000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 10*$questGetPer) { $resultRewardType[] = 104000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 25*$questGetPer) { $resultRewardType[] = 103000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 50*$questGetPer) { $resultRewardType[] = 102000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 100*$questGetPer) { $resultRewardType[] = 101000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }

        if ( rand(0,99) < 5*$runeGetPer) { $resultRewardType[] = 115000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(1,3)*$runeCountVal); }
        if ( rand(0,99) < 10*$runeGetPer) { $resultRewardType[] = 114000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(2,6)*$runeCountVal); }
        if ( rand(0,99) < 25*$runeGetPer) { $resultRewardType[] = 113000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(4,8)*$runeCountVal); }
        if ( rand(0,99) < 33*$runeGetPer) { $resultRewardType[] = 112000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(6,10)*$runeCountVal); }
        if ( rand(0,99) < 50*$runeGetPer) { $resultRewardType[] = 111000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(7,13)*$runeCountVal); }
        break;

      case 1:
        if ( rand(0,99) < 5*$questGetPer) { $resultRewardType[] = 106000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 10*$questGetPer) { $resultRewardType[] = 105000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 25*$questGetPer) { $resultRewardType[] = 104000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 50*$questGetPer) { $resultRewardType[] = 103000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 100*$questGetPer) { $resultRewardType[] = 102000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 70*$questGetPer) { $resultRewardType[] = 101000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }

        if ( rand(0,99) < 10*$runeGetPer) { $resultRewardType[] = 115000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(1,3)*$runeCountVal); }
        if ( rand(0,99) < 15*$runeGetPer) { $resultRewardType[] = 114000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(2,6)*$runeCountVal); }
        if ( rand(0,99) < 32*$runeGetPer) { $resultRewardType[] = 113000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(4,8)*$runeCountVal); }
        if ( rand(0,99) < 41*$runeGetPer) { $resultRewardType[] = 112000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(6,10)*$runeCountVal); }
        if ( rand(0,99) < 60*$runeGetPer) { $resultRewardType[] = 111000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(7,13)*$runeCountVal); }
        break;

      case 2:
        if ( rand(0,99) < 10*$questGetPer) { $resultRewardType[] = 106000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 25*$questGetPer) { $resultRewardType[] = 105000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 50*$questGetPer) { $resultRewardType[] = 104000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 100*$questGetPer) { $resultRewardType[] = 103000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 70*$questGetPer) { $resultRewardType[] = 102000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 60*$questGetPer) { $resultRewardType[] = 101000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }

        if ( rand(0,99) < 15*$runeGetPer) { $resultRewardType[] = 115000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(1,3)*$runeCountVal); }
        if ( rand(0,99) < 20*$runeGetPer) { $resultRewardType[] = 114000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(2,6)*$runeCountVal); }
        if ( rand(0,99) < 39*$runeGetPer) { $resultRewardType[] = 113000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(4,8)*$runeCountVal); }
        if ( rand(0,99) < 49*$runeGetPer) { $resultRewardType[] = 112000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(6,10)*$runeCountVal); }
        if ( rand(0,99) < 70*$runeGetPer) { $resultRewardType[] = 111000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(7,13)*$runeCountVal); }
        break;

      case 3:
        if ( rand(0,99) < 25*$questGetPer) { $resultRewardType[] = 106000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 50*$questGetPer) { $resultRewardType[] = 105000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 100*$questGetPer) { $resultRewardType[] = 104000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 70*$questGetPer) { $resultRewardType[] = 103000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 60*$questGetPer) { $resultRewardType[] = 102000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 50*$questGetPer) { $resultRewardType[] = 101000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }

        if ( rand(0,99) < 20*$runeGetPer) { $resultRewardType[] = 115000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(1,3)*$runeCountVal); }
        if ( rand(0,99) < 25*$runeGetPer) { $resultRewardType[] = 114000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(2,6)*$runeCountVal); }
        if ( rand(0,99) < 46*$runeGetPer) { $resultRewardType[] = 113000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(4,8)*$runeCountVal); }
        if ( rand(0,99) < 57*$runeGetPer) { $resultRewardType[] = 112000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(6,10)*$runeCountVal); }
        if ( rand(0,99) < 80*$runeGetPer) { $resultRewardType[] = 111000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(7,13)*$runeCountVal); }
        break;

      case 4:
        if ( rand(0,99) < 50*$questGetPer) { $resultRewardType[] = 106000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 100*$questGetPer) { $resultRewardType[] = 105000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 80*$questGetPer) { $resultRewardType[] = 104000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 60*$questGetPer) { $resultRewardType[] = 103000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 40*$questGetPer) { $resultRewardType[] = 102000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 40*$questGetPer) { $resultRewardType[] = 101000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }

        if ( rand(0,99) < 30*$runeGetPer) { $resultRewardType[] = 115000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(1,3)*$runeCountVal); }
        if ( rand(0,99) < 41*$runeGetPer) { $resultRewardType[] = 114000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(2,6)*$runeCountVal); }
        if ( rand(0,99) < 53*$runeGetPer) { $resultRewardType[] = 113000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(4,8)*$runeCountVal); }
        if ( rand(0,99) < 65*$runeGetPer) { $resultRewardType[] = 112000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(6,10)*$runeCountVal); }
        if ( rand(0,99) < 90*$runeGetPer) { $resultRewardType[] = 111000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(7,13)*$runeCountVal); }
        break;

      case 5:
        if ( rand(0,99) < 100*$questGetPer) { $resultRewardType[] = 106000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 90*$questGetPer) { $resultRewardType[] = 105000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 60*$questGetPer) { $resultRewardType[] = 104000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 50*$questGetPer) { $resultRewardType[] = 103000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 40*$questGetPer) { $resultRewardType[] = 102000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }
        if ( rand(0,99) < 40*$questGetPer) { $resultRewardType[] = 101000 + $chapter;   $resultRewardCount[] = 1*$questCountVal; }

        if ( rand(0,99) < 40*$runeGetPer) { $resultRewardType[] = 115000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(1,3)*$runeCountVal); }
        if ( rand(0,99) < 50*$runeGetPer) { $resultRewardType[] = 114000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(2,6)*$runeCountVal); }
        if ( rand(0,99) < 70*$runeGetPer) { $resultRewardType[] = 113000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(4,8)*$runeCountVal); }
        if ( rand(0,99) < 80*$runeGetPer) { $resultRewardType[] = 112000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(6,10)*$runeCountVal); }
        if ( rand(0,99) < 100*$runeGetPer) { $resultRewardType[] = 111000 + ((rand(0,100)<40) ? GetGoddessAttribute($templeId) : rand(0,4));   $resultRewardCount[] = floor(rand(7,13)*$runeCountVal); }
        break;

      default:
        break;
      }

    }
  }


  $ticketPercent = 4000;
  getBonus($db, $redis, $stageId, $isClear, $idx, $resultRewardType, $resultRewardCount, $ticketPercent, $id);

  if ( $isClear>0 && $difficulty >= 0 && rand(0, 99999) < $ticketPercent ) {
    $resultRewardType[] = 5002;
    $resultRewardCount[] = rand(1,3);
  }


  //event
  // $day = (int)(date('d'));
  // $data["day"] = $day;
  // if ( ($stageId>20) && ($day >= 25 || $day < 7) ) {
  //   $eval = rand(0, 999);
  //   if ( $eval < $needHeart*21.42 ) {
  //     $resultRewardType[] = 190000;
  //     $resultRewardCount[] = 1;
  //   }
  //   else if ( $eval < $needHeart*27.13 ) {
  //     $resultRewardType[] = 190002;
  //     $resultRewardCount[] = 1;
  //   }
  //   else if ( $eval < $needHeart*28 ) {
  //     $resultRewardType[] = 190001;
  //     $resultRewardCount[] = 1;
  //   }
  // }
  //~event

  // ----------------- reward set complete ----------------------


  if ( $difficulty < 0 ) {
    for ( $i=0; $i<8; $i++ )
      $weaponIdxs[$i] = -1;

  }
  else {
    for ( $i=0; $i<8; $i++ )
      $weaponIdxs[$i] = $_REQUEST["weaponIdx".$i];
    if ( !is_null($_REQUEST["subWeapon"]) ) {
      for ( $i=8; $i<16; $i++ ) {
        $weaponIdxs[$i] = $_REQUEST["weaponIdx".$i];
      }
    }
  }

  $data["resultExp"] = 0;
  if ( 0 == getReward($db, $data, $resultRewardType, $resultRewardCount, $id, $weaponIdPointer, $artifactIdPointer, $weaponIdxs) )
      return;

  $data["rewardLength"] = 0;
  $data["resultRewardType"] = $resultRewardType;
  $data["resultRewardCount"] = $resultRewardCount;

//  ----- event -----
//      $query = sprintf("insert into frdUserPost values (0, '%s', 0, 5003, %d, %d)", $id, (int)ceil($needHeart/2), time());
//      do {
//        $isGood = $db->query($query);
//     }while($isGood == false);
//  -----------------

//  ----- event -----
  /*
  if ( !$isClear ) {
    $query = sprintf("insert into frdUserPost values (0, '%s', 0, 5003, %d, %d)", $id, (int)ceil($needHeart*0.7), time());
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }
  */
//  -----------------

  $redis->close();
  $res->close();
  $db->query("commit");
  $db->close();

  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
