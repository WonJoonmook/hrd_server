<?php
  // 상점에서 상자, 패키지를 제외한 자원 구매 요청시 처리후 데이터를 뿌려줍니다.
  include_once("../myAes.php");
  include_once("./_calculate.php");

  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];
  $itemId = $_REQUEST["itemId"];
  $itemCount = $_REQUEST["itemCount"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");


  $query = sprintf("select gold, session from frdUserData where privateId = %d", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;

      $redis = openRedis();
      if ( $redis == false ) {
        echo 0;
        $db->close();
        return;
      }

      $query = sprintf("select itemCount from frdHavingItems where userId = %d and itemId=%d", $id, $itemId);
      $sres = $db->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      if ($sres->num_rows <= 0) {
      //  addBlacklist($id, "hack_NoItem");
        echo 1;
        $db->close();
        return;
      }

      $srow = $sres->fetch_assoc();
      if ( $itemCount > $srow["itemCount"] ) {
      //  addBlacklist($id, "hack_ItemCount");
        echo 1;
        $db->close();
        return;
      }


      $price = GetItemPrice($itemId);
      $resultGold = $row["gold"] + ($price*$itemCount);
      $resultItemCount = $srow["itemCount"] - $itemCount;
      if ( $resultItemCount <= 0 ) {
        $query = sprintf("delete from frdHavingItems where userId = %d and itemId=%d", $id, $itemId);
      }
      else {
        $query = sprintf("update frdHavingItems set itemCount=%d where userId = %d and itemId=%d", $resultItemCount, $id, $itemId);
      }
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }    

      $query = sprintf("update frdUserData set gold=%d, session=%d where privateId=%d", $resultGold, $newSession, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      
      $data["gold"] = $resultGold;
      $data["itemCount"] = $resultItemCount;
      $redis->close();
    }
    else {
      addBlacklist($id, "sellItem_Session");
      echo 1;
      $db->close();
      return;
    }
  }

  $db->query("commit");
  $db->close();
  
  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
