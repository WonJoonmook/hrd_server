<?php
  // 캐릭 정보창에서 캐릭 구매 요청 ( $idx는 버튼 번호인데 구매 버튼은 최대 2개까지 있습니다. )
  include_once("../myAes.php");
  $userId = $_REQUEST["userId"];
  $characId = (int)$_REQUEST["characId"];

  $data = array();
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }

  $db->query("set autocommit=0");
  
  $pass_SpecialExpStone = 100004;
  $addExpCount = 50;
  
  $query = sprintf("select itemCount from frdHavingItems where userId = %d and itemId=%d", $userId, $pass_SpecialExpStone);
  $sres = $db->query($query);
  if ($sres == false || $sres->num_rows <= 0) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }
  $srow = $sres->fetch_assoc();
  $resultItemCount = $srow["itemCount"]-1;
  if ( $resultItemCount <= 0 ) 
    $query = sprintf("delete from frdHavingItems where userId=%d and itemId=%d", $userId, $pass_SpecialExpStone);
  else 
    $query = sprintf("update frdHavingItems set itemCount=%d where userId=%d and itemId=%d", $resultItemCount, $userId, $pass_SpecialExpStone);   
  $isGood = $db->query($query);
  if ($isGood == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }


  $bundleIdx = ($characId>>3);
  $query = "select exps from frdCharacEvolveExps where userId=$userId and bundleIdx=$bundleIdx";
  $sres = $db->query($query);
  if ($sres == false || $sres->num_rows <= 0) {
    echo 0;
    $db->query("rollback");
    $db->close();
    $redis->close();
    return 0;
  }
  $srow = $sres->fetch_assoc();
  $longData = $srow["exps"];
  $bitIdx = 8*($characId&7);
  $preExp = ( ($longData >> $bitIdx) & 0xff);
  $mask = $preExp;
  $mask <<= $bitIdx;
  $longData -= $mask;
  $mask = ($preExp + $addExpCount);
  if ( $mask > 255 )
      $mask = 255;

  $data["preExp"] = $preExp;
  $data["characId"] = (int)$characId;
  $data["characExp"] = $mask;

  $mask <<= $bitIdx;
  $resultData = ($longData | $mask);
  $query = sprintf("update frdCharacEvolveExps set exps='%s' where userId=%d and bundleIdx=%d", $resultData, $userId, $bundleIdx);
  $isGood = $db->query($query);
  if ($isGood == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    $redis->close();
    return 0;
  }
  $data["characBundleIdx"] = $bundleIdx;

  $db->query("commit");
  $db->close();

  echo json_encode($data);
?>
