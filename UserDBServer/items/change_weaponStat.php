<?php
  include_once("../myAes.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];
  $weaponPrivateIdx = $_REQUEST["weaponPrivateIdx"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }
  $db->query("set autocommit=0");
  $query = sprintf("select session from frdUserData where privateId = %d", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo $query."\n".$mysqli_error($db)."\n";
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;

      $query = sprintf("update frdUserData set session=%d where privateId=%d", $newSession, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      $query = sprintf("select itemCount from frdHavingItems where userId = %d and itemId=%d", $id, 100000);
      // 100000 = pass_ResetWeaponStat
      $sres = $db->query($query);
      if ($sres == false || $sres->num_rows <= 0) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      $srow = $sres->fetch_assoc();
      $resultItemCount = $srow["itemCount"]-1;
      if ( $resultItemCount <= 0 ) 
        $query = sprintf("delete from frdHavingItems where userId=%d and itemId=%d", $id, 100000);
      else 
        $query = sprintf("update frdHavingItems set itemCount=%d where userId=%d and itemId=%d", $resultItemCount, $id, 100000);
          
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      $data["stat"] = rand(0,10000);
      $query = sprintf("update frdHavingWeapons set stat=%d where privateId=%d and userId=%d"
                                ,$data["stat"], $weaponPrivateIdx, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
    }
    else {
      addBlacklist($id, "changeOption_session");
      echo 1;
      $db->close();
      return;
    }
  }
  $res->close();
  $db->query("commit");
  $db->close();
  
  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
