<?php
  // 상점에서 상자, 패키지를 제외한 자원 구매 요청시 처리후 데이터를 뿌려줍니다.
  include_once("../myAes.php");
  include_once("./_calculate.php");

  $id = $_REQUEST["id"];
  $matRuneId = (int)$_REQUEST["matRuneId"];
  $resultRuneId = $matRuneId + 1000;
  $resultAddCount = (int)$_REQUEST["resultAddCount"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");


  $query = sprintf("select powder from frdUserData where privateId = %d", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;$db->query("rollback");$db->close();
    return;
  }
  if ( $res->num_rows <= 0) {
    echo 1;addBlacklist($id, "hack_ComposeRune_Powder");$db->close();
    return;
  }

  $row = $res->fetch_assoc();
  $havingPowder = $row["powder"];
  $needPowder = GetNeedPowderWhenComposeRune($matRuneId) * $resultAddCount;
  if ( $havingPowder < $needPowder ) {
    echo 1;addBlacklist($id, "hack_ComposeRune_Powder");$db->close();
    return;
  }

  
  $query = "select itemCount from frdHavingItems where userId = $id and (itemId=$matRuneId or itemId=$resultRuneId) order by itemId asc";
  $sres = $db->query($query);
  if ($sres == false) {
    echo 0;$db->query("rollback");$db->close();
    return;
  }
  if ( $sres->num_rows <= 0) {
    echo 1;
    //addBlacklist($id, "hack_ComposeRune_Rune");
    $db->close();
    return;
  }

  $srow = $sres->fetch_assoc();
  $havingRune = $srow["itemCount"];
  $needRune = GetNeedRuneWhenComposeRune($matRuneId) * $resultAddCount;
  if ( $havingRune < $needRune ) {
    echo 1;
    //addBlacklist($id, "hack_ComposeRune_Rune");
    $db->close();
    return;
  }

  $resultPowder = $havingPowder - $needPowder;
  $resultMatRune = $havingRune - $needRune;
  $srow = $sres->fetch_assoc();
  $resultResultRune = (int)$srow["itemCount"] + $resultAddCount;

  $query = "update frdUserData set powder=$resultPowder where privateId=$id";
  $isGood = $db->query($query);
  if ($isGood == false) {
    echo 0;$db->query("rollback");$db->close();return;
  }

  if ( $resultMatRune <= 0 )
    $query ="delete from frdHavingItems where userId = $id and itemId=$matRuneId";
  else 
    $query = "update frdHavingItems set itemCount=$resultMatRune where userId = $id and itemId=$matRuneId";
  $isGood = $db->query($query);
  if ($isGood == false) {
    echo 0;$db->query("rollback");$db->close();return;
  }

  if ( $resultResultRune == $resultAddCount )
    $query = "insert into frdHavingItems values ($id, $resultRuneId, $resultResultRune)";
  else
    $query = "update frdHavingItems set itemCount=$resultResultRune where userId = $id and itemId=$resultRuneId";
  $isGood = $db->query($query);
  if ($isGood == false) {
    echo 0;$db->query("rollback");$db->close();return;
  }

  $data["powder"] = $resultPowder;
  $data["resultMatRune"] = $resultMatRune;
  $data["resultResultRune"] = $resultResultRune;

  $db->query("commit");
  $db->close();

  echo json_encode($data);
?>
