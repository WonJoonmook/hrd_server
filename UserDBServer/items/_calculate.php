<?php
	function GetNeedRuneWhenComposeRune($matRuneId) {
		$runeGrade = (int)round(($matRuneId - 110000)/1000);
		switch ($runeGrade) {
		case 1:
			return 3;
		case 2:
			return 5;
		case 3:
			return 7;
		case 4:
			return 9;
		default:
			return 10;
		}
	}

	function GetNeedPowderWhenComposeRune($matRuneId) {
		$runeGrade = (int)round(($matRuneId - 110000)/1000);
		switch ($runeGrade) {
		case 1:
			return 5;
		case 2:
			return 10;
		case 3:
			return 50;
		case 4:
			return 100;
		default:
			return 100;
		}
	}

	function GetItemPrice($itemId) {
		$itemVal = floor(($itemId-100000)/1000);
		switch ($itemVal) {
		case 0:

			switch($itemId) {
			case 100000:	//유물 변경권
				return 1500;
	    	case 100001:	//특포 초기화권
				return 2500;
			default:
				return 2000;
			}

			break;

		case 1:				//1티어 잡동사니
			return 100;

		case 2:
			return 200;

		case 3:
			return 400;

		case 4:
			return 800;

		case 5:
			return 1500;

		case 6:				//6티어 잡동사니
			return 3000;

		case 10:
			return 3333;

		case 11:				//1티어 Rune
			return 44;

		case 12:
			return 98;

		case 13:
			return 220;

		case 14:
			return 538;

		case 15:
			return 1412;

		case 20:		//히든 잡동사니
			return 50000;

			case 90:	//event item
			return 1;
		default:
			return 50000;
		}
		
	}

?>