<?php
  // 마법 강화 요청.
  // 성공,실패 계산 후 처리하고 뿌려줍니다.
  include_once("./_calculate1.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["userId"];
  $privateIdx = $_REQUEST["privateIdx"];
  $matPrivateIdx = $_REQUEST["matPrivateIdx"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }
  $db->query("set autocommit=0");
  $query = sprintf("select gold, session from frdUserData where privateId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;

      $query = sprintf("select * from frdHavingArtifacts where ");
      $query .= sprintf("(privateId='%s' and userId='%s') or ", $privateIdx, $id);
      $query .= sprintf("(privateId='%s' and userId='%s')", $matPrivateIdx, $id);
      $sres = $db->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      if ($sres->num_rows < 2 ) {
        addBlacklist($id, "levelup_artifact_dontMatchCount");
        echo 1;
        $db->close();
        return;
      }



      for ($idx = 0; $idx < $sres->num_rows; $idx++) {
        $sres->data_seek($idx);
        $srow = $sres->fetch_assoc();
        $curPIdx = (int)$srow["privateId"];
        if ( $curPIdx == $privateIdx ) {  // desktop
          $deskGrade = GetGrade($srow["itemId"]);
          $deskExp = $srow["itemExp"];
          $deskLevel = $srow["itemLevel"];
        }
        else {                            // material
          $matGrade = GetGrade($srow["itemId"]);
        }
      }
      $sres->close();

      $matExp = GetHavingExp($matGrade);
      $percent = GetLevelUpPercent($db, $id, $deskGrade, $matExp);
      $bonusPercent = GetLevelUpPercent($db, $id, $deskGrade, $deskExp);
      $price = GetLevelUpPrice($deskGrade, $deskLevel);

      $resultGold = $row["gold"]-$price;
      if ( $resultGold < 0) {
        addBlacklist($id, "levelup_artifact_noGold");
        echo 1;
        $db->close();
        return;
      }

      $data["gold"] = $resultGold;
      $query = sprintf("update frdUserData set gold=%d, session=%d where privateId='%s'", $resultGold, $newSession, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      $query = sprintf("delete from frdHavingArtifacts where privateId=%d and userId='%s'", $matPrivateIdx, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      $itemNeedExp = GetNeedExp($deskGrade);
      $itemExp = $deskExp + GetHavingExp($matGrade);
      if ( rand(0, $itemNeedExp) <= $itemExp ) {
        $data["isSuccess"] = 1;
        $resultLevel = $deskLevel+1;
        $query = sprintf("update frdHavingArtifacts set itemLevel=%d, itemExp=0 where privateId=%d and userId='%s'", $resultLevel, $privateIdx, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $data["itemLevel"] = $resultLevel;
      }
      else {
        $data["isSuccess"] = 0;
        $resultExp = $deskExp + (int)($matExp/5);
        $query = sprintf("update frdHavingArtifacts set itemExp=%d where privateId=%d and userId='%s'", $resultExp, $privateIdx, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $data["itemExp"] = $resultExp;
      }
    }
    else {
      addBlacklist($id, "levelup_artifact_session");
      echo 1;
      $db->close();
      return;
    }
  }
  $res->close();
  $db->query("commit");
  $db->close();

  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
