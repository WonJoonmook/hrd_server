<?php
  // 마법 판매 요청.
  include_once("../globalDefine.php");
  include_once("./_calculate1.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];
  $isExtraction = $_REQUEST["isExtraction"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");

  $query = sprintf("select gold, powder, session from frdUserData where privateId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;

      $sellCount =0;
      for ( $idx = 0; $idx<9; $idx++ ) {
        $val = $_REQUEST["matPId".$idx];
        if ( !is_null($val) && $val >= 0 ) {
          $idxArr[$sellCount++] = $val;
        }
      }

      $query = sprintf("select privateId, itemId, itemLevel from frdHavingArtifacts where ");
      $redis = openRedis();
      if ( $redis == false ) {
        echo 0;
        $db->close();
        return;
      }

      for ( $idx=0; $idx<$sellCount; $idx++ ) {
        if ( $idx < $sellCount-1 )
          $query .= sprintf("(privateId='%s' and userId='%s') or ", $idxArr[$idx], $id);
        else
          $query .= sprintf("(privateId='%s' and userId='%s')", $idxArr[$idx], $id);
      }
      $sres = $db->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      if ($sres->num_rows !== $sellCount ) {
        addBlacklist($id, "sell_magic_dontMatchCount");
        echo 1;
        $db->close();
        $redis->close();
        return;
      }

      if ( $isExtraction <= 0 ) {

        $addGold = 0;
        for ($idx = 0; $idx < $sres->num_rows; $idx++) {
          $sres->data_seek($idx);
          $srow = $sres->fetch_assoc();

          $addGold += GetSellPrice(GetGrade((int)$srow["itemId"]), (int)$srow["itemLevel"]);
        }
        $sres->close();

        for ( $idx=0; $idx<$sellCount; $idx++ ) {
          $query = sprintf("delete from frdHavingArtifacts where privateId='%s' and userId='%s'", $idxArr[$idx], $id);
          $isGood = $db->query($query);
          if ($isGood == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }
        }

        $resultGold = $row["gold"]+$addGold;
        $query = sprintf("update frdUserData set gold=%d, session=%d where privateId='%s'", $resultGold, $newSession, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $data["gold"] = $resultGold;

      }
      else {

        $amountPercent = $redis->get('magicPowderValue');
        $addPowder = 0;
        for ($idx = 0; $idx < $sres->num_rows; $idx++) {
          $sres->data_seek($idx);
          $srow = $sres->fetch_assoc();

          $grade = GetGrade($srow["itemId"]);
          $amountValue = $redis->lindex('magicPowder', $grade);
          $query = sprintf("select val from frdEffectForEtc where userId = %d and type=%d", $id, $GLOBALS['$ABILL_Bonus_Extraction']);
          $ssres = $db->query($query);
          if ( $ssres == false ) {
            echo 0;$db->close();return;
          }
          if ( $ssres->num_rows > 0 ) {
            $ssrow = $ssres->fetch_assoc();
            $percent = $ssrow["val"] / 100000;
            $amountValue += round($amountValue * $percent);
          }

          $min = $amountValue - ceil($amountValue*$amountPercent/100);
          $max = $amountValue + (int)($amountValue*$amountPercent/100);

          $resultAmount = rand($min, $max);
          $addPowder += $resultAmount;
        }
        $sres->close();

        for ( $idx=0; $idx<$sellCount; $idx++ ) {
          $query = sprintf("delete from frdHavingArtifacts where privateId='%s' and userId='%s'", $idxArr[$idx], $id);
          $isGood = $db->query($query);
          if ($isGood == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }
        }

        $resultPowder = $row["powder"]+$addPowder;
        $query = sprintf("update frdUserData set powder=%d, session=%d where privateId='%s'", $resultPowder, $newSession, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $data["powder"] = $resultPowder;

      }

      $redis->close();
    }
    else {
      addBlacklist($id, "sell_artifact_session");
      echo 1;
      $db->close();
      return;
    }

  }
  $res->close();
  $db->query("commit");
  $db->close();

  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
