<?php
include_once("../myAes.php");

	function GetGrade($itemId) {
		return (int)($itemId/100)+1;
	}


	function GetMaxLevel() {
		return 5;
	}

	function GetMaxGrade() {
		return 5;
	}

	function GetHavingExp($grade) {
		switch($grade) {
		case 0:
			return 100;
		case 1:
			return 200;
		case 2:
			return 400;
		case 3:
			return 800;
		case 4:
			return 1600;
		case 5:
			return 3200;
		default:
			return 3200;
		}
	}
	
	function GetNeedExp($grade) {
		switch($grade) {
		case 0:
			return 100;
		case 1:
			return 200;
		case 2:
			return 400;
		case 3:
			return 800;
		case 4:
			return 1600;
		case 5:
			return 3200;
		default:
			return 3200;
		}
	}

	function GetGradeUpPrice($grade) {
		switch($grade) {
		case 1:
			return 1000;
		case 2:
			return 2000;
		case 3:
			return 5000;
		case 4:
			return 27000;
		default:
			return 120000;
		}
	}
	
	function GetLevelUpPrice($grade, $level) {
		$price=0;
		$intervalPrice=0;
		switch($grade) {
		case 1:
			$price = 300;
			$intervalPrice = 100;
			break;
		case 2:
			$price = 500;
			$intervalPrice = 200;
			break;
		case 3:
			$price = 1000;
			$intervalPrice = 500;
			break;
		case 4:
			$price = 2000;
			$intervalPrice = 1000;
			break;
		case 5:
			$price = 4000;
			$intervalPrice = 2000;
			break;
		default:
			break;
		}
		
		return $price + $intervalPrice*$level;
	}

	function GetLevelUpPercent($db, $userId, $targetGrade, $matExp) {
		$resultPercent = (int)(($matExp*100) / GetNeedExp($targetGrade));

		$query = sprintf("select val from frdEffectForEtc where userId=%d and type=%d", $userId, $GLOBALS['$ABILL_Bonus_MagicLvUpPercent']);
	    $sres = $db->query($query);
	    if ($sres == false) {
	      echo 0;
	      $db->query("rollback");
	      $db->close();
	      return;
	    }
	    if ( $sres->num_rows > 0 ) {
	      $srow = $sres->fetch_assoc();
	      $resultPercent += round($resultPercent*$srow["val"]*0.00001);
	    }
		return $resultPercent;
	}

	function GetSellPrice($deskGrade, $deskLevel) {
		$price=0;
		switch($deskGrade) {
		case 1:
			$price = 75;
			for ( $i=1; $i<=$deskLevel; $i++ )
				$price += 100 +50*$i;
			break;
		case 2:
			$price = 125;
			for ( $i=1; $i<=$deskLevel; $i++ )
				$price += 150 +100*$i;
			break;
		case 3:
			$price = 250;
			for ( $i=1; $i<=$deskLevel; $i++ )
				$price += 250 +250*$i;
			break;
		case 4:
			$price = 500;
			for ( $i=1; $i<=$deskLevel; $i++ )
				$price += 500 +500*$i;
			break;
		case 5:
			$price = 1000;
			for ( $i=1; $i<=$deskLevel; $i++ )
				$price += 1000 +1000*$i;
			break;
		default:
			break;
		}
		return $price;
	}

	function GetManaLevelUpPrice($curLevel) {
		$sum=0;
		$level = $curLevel;
		$sum += 200 + $level*200;

		while($level>0) {
			$level -= 10;
			if ( $level >= 0 ) {
				$sum += 100*($level+1);
			}
		}
		return $sum;
	}


?>