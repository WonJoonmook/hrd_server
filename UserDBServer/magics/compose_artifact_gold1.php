<?php
  // 마법 진화 요청.
  // 처리후 결과를 뿌려줍니다.
  include_once("./_calculate1.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["userId"];
  $privateIdx = $_REQUEST["privateIdx"];
  $matPrivateIdx = $_REQUEST["matPrivateIdx"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }

  $db->query("set autocommit=0");
  $query = sprintf("select gold, session from frdUserData where privateId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $redis = openRedis();
      if ( $redis == false ) {
        echo 0;
        $db->close();
        return;
      }

      $newSession = mt_rand();
      $data["session"] = $newSession;

      $query = sprintf("select * from frdHavingArtifacts where ");
      $query .= sprintf("(privateId='%s' and userId='%s') or ", $privateIdx, $id);
      $query .= sprintf("(privateId='%s' and userId='%s')", $matPrivateIdx, $id);
      $sres = $db->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      if ($sres->num_rows < 2 ) {
        addBlacklist($id, "gradeup_artifact_dontMatchCount");
        echo 1;
        $db->close();
        $redis->close();
        return;
      } 

      for ($idx = 0; $idx < $sres->num_rows; $idx++) {
        $sres->data_seek($idx);
        $srow = $sres->fetch_assoc();
        $curPIdx = (int)$srow["privateId"];
        if ( $curPIdx == $privateIdx ) {  // desktop
          $deskGrade = (int)GetGrade($srow["itemId"]);
          $deskLevel = (int)$srow["itemLevel"];
        }
        else {                            // material
          $matGrade = (int)GetGrade($srow["itemId"]);
          $matLevel = (int)$srow["itemLevel"];
        }
      }
      $sres->close();
      if ( (int)$matGrade !== (int)$deskGrade ) {
        addBlacklist($id, "gradeup_artifact_dontMatchGrade");
        echo 1;
        $db->close();
        $redis->close();
        return;
      }
      if ( ($matLevel !== GetMaxLevel()) || ($deskLevel !== GetMaxLevel()) ) {
        addBlacklist($id, "gradeup_artifact_dontMaxLevel");
        echo 1;
        $db->close();
        $redis->close();
        return;
      }

      $price = GetGradeUpPrice($deskGrade);

      $resultGold = $row["gold"]-$price;
      if ( $resultGold < 0) {
        addBlacklist($id, "gradeup_artifact_noGold");
        echo 1;
        $db->close();
        $redis->close();
        return;
      }

      $data["gold"] = $resultGold;
      $query = sprintf("update frdUserData set gold=%d, session=%d where privateId='%s'", $resultGold, $newSession, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      $query = sprintf("delete from frdHavingArtifacts where privateId=%d and userId='%s'", $matPrivateIdx, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      if ( $deskGrade >= GetMaxGrade() )
        $deskGrade = GetMaxGrade()-1;
      
      $min = $redis->lindex('magicMinIdx1', $deskGrade+1);
      $max = $redis->lindex('magicMaxIdx1', $deskGrade+1);
      
      $resultId = rand($min, $max);
      $query = sprintf("update frdHavingArtifacts set itemId=%d, itemLevel=0, itemExp=0 where privateId=%d and userId='%s'", $resultId, $privateIdx, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
    
      $data["resultId"] = $resultId;

      $redis->close();
    }
    else {
      addBlacklist($id, "gradeup_artifact_session");
      echo 1;
      $db->close();
      return;
    }
  }
  $res->close();
  $db->query("commit");
  $db->close();
  
  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
