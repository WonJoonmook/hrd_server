<?php
include_once("../myAes.php");

  $data = array();
  $data["error"] = 0;

  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    return;
  }
  else {
    $data["magicPowderValue"] = $redis->get('magicPowderValue');
    $data["magicPowder"] = $redis->lrange('magicPowder',0, -1);
    $data["magicMinIdx"] = $redis->lrange('magicMinIdx1',0, -1);
    $data["magicMaxIdx"] = $redis->lrange('magicMaxIdx1', 0, -1);
    $redis->close();
  }

  echo json_encode($data);
?>
