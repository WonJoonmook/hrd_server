<?php
  // 마나 업그레이드 요청.
  // 처리후 결과를 뿌려줍니다.
  include_once("./_calculate1.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }

  $db->query("set autocommit=0");
  $query = sprintf("select gold, manaLevel, session from frdUserData where privateId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;

      $price = GetManaLevelUpPrice($row["manaLevel"]);

      if ( (int)$price !== (int)$_REQUEST["price"]) {
        addBlacklist($id, "hack_buyMana");
        echo 1;
        $db->close();
        return;
      }


      $resultGold = $row["gold"]-$price;
      $resultLevel = $row["manaLevel"]+1;
      if ( $resultGold < 0) {
        addBlacklist($id, "buy_mana_noMoney");
        echo 1;
        $db->close();
        return;
      }

      $query = sprintf("update frdUserData set manaLevel=%d, gold=%d, session=%d where privateId='%s'", $resultLevel, $resultGold, $newSession, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      $data["gold"] = $resultGold;
      $data["resultLevel"] = $resultLevel;
    }
    else {
      addBlacklist($id, "buy_mana_session");
      echo 1;
      $db->close();
      return;
    }
  }
  $res->close();
  $db->query("commit");
  $db->close();

  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
