<?php
  
  function getBonus(&$db, &$redis, $efr_res, $stageId, $isClear, &$idx, &$rewardTypes, &$rewardCounts, &$ticketPercent, $userId) {
    $rewardLength = count($rewardTypes);

    if ( $efr_res == null ) {
      $query = sprintf("select type, val from frdEffectForRewards where userId=%d",$userId);
      $sres = $db->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return 0;
      }
    }
    else {
      $sres = $efr_res;
    }

    for ( $i=0; $i<$sres->num_rows; $i++ ) {
      $sres->data_seek($i);
      $row = $sres->fetch_assoc();
      $type = (int)$row["type"];
      $val = (int)$row["val"];
 //     echo $type.", ".$val."\n";

      switch ($type) {
        case 1:   //exp up percent 100
          for ( $j=0; $j<$rewardLength; $j++ ) {
            if ( $rewardTypes[$j] == 5001 ) {//exp
              $rewardCounts[$j] += (int)($rewardCounts[$j]*$val/100000);
            }
          }
          break;

        case 2: //medal double get percent 100
          for ( $j=0; $j<$rewardLength; $j++ ) {
            if ( $rewardTypes[$j] == 5005 ) {//medal
              if ( rand(0,99) < $val )
                $rewardCounts[$j] *= 2;
            }
          }
          break;

        case 3: //보상 2배
          if ( $isClear && rand(0,99999) < $val ) {
            for ( $j=0; $j<$rewardLength; $j++ ) {
              if ( ($rewardTypes[$j] > 1000 && $rewardTypes[$j] < 1600) || ($rewardTypes[$j] > 10000 && $rewardTypes[$j] < 10600) )//except weapon & magic
                continue;
              $rewardCounts[$j] *= 2;
            }
          }
          break;

        case 4: //추가 보석 10개 지급
          if ( $isClear && rand(0,99999) < $val ) {
            $useHeart = $redis->hget('stageHeart_New', $stageId);
            $rewardTypes[] = 5004;
            $rewardCounts[] = (int)floor($useHeart/3);
            $idx++;
          }
          break;

        case 5: //영혼석 2배
          if ( rand(0,99999) < $val ) {
            for ( $j=0; $j<$rewardLength; $j++ ) {
              if ( $rewardTypes[$j] <600 ) {//soulStone
                if ( rand(0,99) < $val )
                  $rewardCounts[$j] *= 2;
              }
            }
          }
          break;

        case 6: //티켓확률증가
          $ticketPercent += $val;
          break;
        
        case 7: // 번개 회수
          $useHeart = $redis->hget('stageHeart_New', $stageId);
          $rewardTypes[] = 5003;
          $rewardCounts[] = (int)($useHeart * $val / 100000);
          $idx++;
          break;

        case 5: //영혼석 2배
          if ( rand(0,99999) < $val ) {
            for ( $j=0; $j<$rewardLength; $j++ ) {
              if ( $rewardTypes[$j] <600 ) {//soulStone
                if ( rand(0,99) < $val )
                  $rewardCounts[$j] *= 2;
              }
            }
          }
          break;

        case 8: //rune double get
          if ( rand(0,99999) < $val ) {
            for ( $j=0; $j<$rewardLength; $j++ ) {
              if ( $rewardTypes[$j] > 110000 &&  $rewardTypes[$j] < 120000 ) {
                $rewardCounts[$j] *= 2;
              }
            }
          }
          break;

        case 9: //medal double get
          for ( $j=0; $j<$rewardLength; $j++ ) {
            if ( $rewardTypes[$j] == 5005 ) {//medal
              $rewardCounts[$j] *= 2;
            }
          }
          break;

        case 10: //questItem double get
          if ( rand(0,99999) < $val ) {
            for ( $j=0; $j<$rewardLength; $j++ ) {
              if ( $rewardTypes[$j] >= 101000 &&  $rewardTypes[$j] < 110000 ) {
                $rewardCounts[$j] *= 2;
              }
            }
          }
          break;

        case 1002: //medal get more
          for ( $j=0; $j<$rewardLength; $j++ ) {
            if ( $rewardTypes[$j] == 5005 ) {//medal
                $rewardCounts[$j] += (int)($rewardCounts[$j]*$val/100000);
            }
          }
          break;

        case 1006: // gold get more
          for ( $j=0; $j<$rewardLength; $j++ ) {
            if ( $rewardTypes[$j] == 5000 ) {//gold
                $rewardCounts[$j] += (int)($rewardCounts[$j]/100000*$val);
            }
          }
          break;

        case 1015: // rune get more
          for ( $j=0; $j<$rewardLength; $j++ ) {
            if ( $rewardTypes[$j] > 110000 && $rewardTypes[$j] < 120000 ) {//rune
                $rewardCounts[$j] += (int)($rewardCounts[$j]/100000*$val);
            }
          }
          break;

        default:
          # code...
          break;
      }
    }

    $query = sprintf("select type, subType, val from frdEffectForPreCheckRewards where userId=%d and isSuccess=1",$userId);
    $sres = $db->query($query);
    if ($sres == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return 0;
    }
  //  echo "Count =".$sres->num_rows."\n";

    for ( $i=0; $i<$sres->num_rows; $i++ ) {
      $sres->data_seek($i);
      $row = $sres->fetch_assoc();
      $type = (int)$row["type"];
      $subType = (int)$row["subType"];
      $val = (int)$row["val"];
    //  echo $type.", ".$subType.", ".$val;
      switch ($type) {
        case 1100: //특수퀘스트
          $rewardTypes[] = $subType;
          $rewardCounts[] = $val;
          $idx++;
          break;

        default:
          break;
      }

      $query = sprintf("update frdEffectForPreCheckRewards set val=0, isSuccess=0 where userId=%d", $userId);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return 0;
      }
    }
  }
?>