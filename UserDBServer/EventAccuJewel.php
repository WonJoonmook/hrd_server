<?php

  function AccuBuyJewel($db, $id, $amount) {

    $now = time();
    $date = intval(date("ymd", $now));
    if ($date < 161222 || $date > 161228) {
      if ($id != 800002186 && $id != 800026064)
        return 100;
    }


    // event accu buy jewel
    $query = "select * from Event_Accu_Action where userId = $id";
    $res = $db->query($query);
    if ($res == false) {
      return 0;
    }

    if ($res->num_rows <= 0) {
      $query = "insert into Event_Accu_Action value ($id, 0, 0, 0, 0)";
      $res = $db->query($query);
      if ($res == false) {
        return 0;
      }
      $accuBuyJewel = $amount;
      $additionalData = 0;
    }
    else {
      $row = $res->fetch_assoc();
      $accuBuyJewel = $row["accuBuyJewelCount"] + $amount;
      $additionalData = $row["additionalData"];
    }

    $postArr = array();
    if ( $accuBuyJewel >= 100 && (($additionalData & 0x1) == 0) ) { // 100개 구매
      $additionalData |= 0x1;
      {
        $postItemList = array();
        $itemData["id"] = rand(115000, 115004);
        $itemData["count"] = 3;
        $postItemList[] = $itemData;
      }
      $postArr[] = $postItemList;
    }
    if ( $accuBuyJewel >= 300 && (($additionalData & 0x2) == 0) ) { // 300개 구매
      $additionalData |= 0x2;
      {
        $postItemList = array();
        $itemData["id"] = rand(115000, 115004);
        $itemData["count"] = 5;
        $postItemList[] = $itemData;
      }
      $postArr[] = $postItemList;
    }
    if ( $accuBuyJewel >= 500 && (($additionalData & 0x4) == 0) ) { // 500개 구매
      $additionalData |= 0x4;
      {
        $postItemList = array();
        $itemData["id"] = 115000;
        $itemData["count"] = 3;
        $postItemList[] = $itemData;
        $itemData["id"] = 115001;
        $itemData["count"] = 3;
        $postItemList[] = $itemData;
        $itemData["id"] = 115002;
        $itemData["count"] = 3;
        $postItemList[] = $itemData;
        $itemData["id"] = 115003;
        $itemData["count"] = 3;
        $postItemList[] = $itemData;
        $itemData["id"] = 115004;
        $itemData["count"] = 3;
        $postItemList[] = $itemData;
      }
      $postArr[] = $postItemList;
    }
    if ( $accuBuyJewel >= 1000 && (($additionalData & 0x8) == 0) ) {  // 1000개 구매
      $additionalData |= 0x8;
      {
        $postItemList = array();
        $itemData["id"] = 115000;
        $itemData["count"] = 5;
        $postItemList[] = $itemData;
        $itemData["id"] = 115001;
        $itemData["count"] = 5;
        $postItemList[] = $itemData;
        $itemData["id"] = 115002;
        $itemData["count"] = 5;
        $postItemList[] = $itemData;
        $itemData["id"] = 115003;
        $itemData["count"] = 5;
        $postItemList[] = $itemData;
        $itemData["id"] = 115004;
        $itemData["count"] = 5;
        $postItemList[] = $itemData;
      }
      $postArr[] = $postItemList;
    }
    if ( $accuBuyJewel >= 2000 && (($additionalData & 0x10) == 0) ) { // 2000개 구매
      $additionalData |= 0x10;
      {
        $postItemList = array();
        $itemData["id"] = 110000;
        $itemData["count"] = 7;
        $postItemList[] = $itemData;
      }
      $postArr[] = $postItemList;
    }

    if (count($postArr) > 0) {
      $now = time();
      $query = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) values";
      for ($i = 0; $i < count($postArr); $i++) {
        $postItemList = $postArr[$i];
        for ($j=0; $j < count($postItemList); $j++) {
          $itemData = $postItemList[$j];
          $itemId = $itemData["id"];
          $itemCount = $itemData["count"];
          $query .= " ($id, 5, $itemId, $itemCount, $now),";
        }
      }
      $query = substr($query, 0, -1);

      $isGood = $db->query($query);
      if ($isGood == false) {
        return 0;
      }
    }

    $query = "update Event_Accu_Action set accuBuyJewelCount = $accuBuyJewel, additionalData = $additionalData where userId = $id";
    $isGood = $db->query($query);
    if ($isGood == false) {
      return 0;
    }

    return 200;
  }

  function AccuUseJewel($db, $id, $amount) {

    $now = time();
    $date = intval(date("ymd", $now));
    if ($date < 161222 || $date > 161226) {
      if ($id != 800002186 && $id != 800026064)
        return 100;
    }

    // event accu buy jewel
    $query = "select * from Event_Accu_Action where userId = $id";
    $res = $db->query($query);
    if ($res == false) {
      return 0;
    }

    if ($res->num_rows <= 0) {
      $query = "insert into Event_Accu_Action value ($id, 0, 0, 0, 0)";
      $res = $db->query($query);
      if ($res == false) {
        return 0;
      }
      $accuUseJewel = $amount;
      $additionalData = 0;
    }
    else {
      $row = $res->fetch_assoc();
      $accuUseJewel = $row["accuUseJewelCount"] + $amount;
      $additionalData = $row["additionalData"];
    }

    $postArr = array();
    if ( $accuUseJewel >= 150 && (($additionalData & 0x100) == 0) ) { // 150개 사용
      $additionalData |= 0x100;
      {
        $postItemList = array();
        $itemData["id"] = 5006;
        $itemData["count"] = 500;
        $postItemList[] = $itemData;
      }
      $postArr[] = $postItemList;
    }
    if ( $accuUseJewel >= 250 && (($additionalData & 0x200) == 0) ) { // 250개 사용
      $additionalData |= 0x200;
      {
        $postItemList = array();
        $itemData["id"] = 1505;
        $itemData["count"] = 1;
        $postItemList[] = $itemData;
      }
      $postArr[] = $postItemList;
    }
    if ( $accuUseJewel >= 550 && (($additionalData & 0x400) == 0) ) { // 550개 사용
      $additionalData |= 0x400;
      {
        $postItemList = array();
        $itemData["id"] = 10504;
        $itemData["count"] = 1;
        $postItemList[] = $itemData;
      }
      $postArr[] = $postItemList;
    }
    if ( $accuUseJewel >= 1000 && (($additionalData & 0x800) == 0) ) {  // 1000개 사용
      $additionalData |= 0x800;
      {
        $postItemList = array();
        $itemData["id"] = 5004;
        $itemData["count"] = 100;
        $postItemList[] = $itemData;
        $itemData["id"] = 5002;
        $itemData["count"] = 10;
        $postItemList[] = $itemData;
      }
      $postArr[] = $postItemList;
    }
    if ( $accuUseJewel >= 1300 && (($additionalData & 0x1000) == 0) ) { // 1300개 사용
      $additionalData |= 0x1000;
      {
        $postItemList = array();
        $itemData["id"] = 5004;
        $itemData["count"] = 150;
        $postItemList[] = $itemData;
        $itemData["id"] = 110000;
        $itemData["count"] = 3;
        $postItemList[] = $itemData;
      }
      $postArr[] = $postItemList;
    }

    if (count($postArr) > 0) {
      $now = time();
      $query = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) values";
      for ($i = 0; $i < count($postArr); $i++) {
        $postItemList = $postArr[$i];
        for ($j=0; $j < count($postItemList); $j++) {
          $itemData = $postItemList[$j];
          $itemId = $itemData["id"];
          $itemCount = $itemData["count"];
          $query .= " ($id, 5, $itemId, $itemCount, $now),";
        }
      }
      $query = substr($query, 0, -1);

      $isGood = $db->query($query);
      if ($isGood == false) {
        return 0;
      }
    }

    $query = "update Event_Accu_Action set accuUseJewelCount = $accuUseJewel, additionalData = $additionalData where userId = $id";
    $isGood = $db->query($query);
    if ($isGood == false) {
      return 0;
    }

    return 300;
  }
?>
