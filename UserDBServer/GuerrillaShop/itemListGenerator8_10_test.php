<?php
  include_once("../globalDefine.php");
  include_once("../myAes.php");
  ################# 1티어 #################
  $idx = 0;
  $t1ItemList[$idx]["itemType"] = 0; // 영혼석
  $t1ItemList[$idx]["itemGrade"] = 1;  // 1성
  $t1ItemList[$idx]["itemCount"] = array(10, 20, 30,50);
  $t1ItemList[$idx]["itemValue"] = array(15, 30, 45,75);
  $t1ItemList[$idx]["resourceType"] = array(1, 2, 3);
  $idx++;

  $t1ItemList[$idx]["itemType"] = 0; // 영혼석
  $t1ItemList[$idx]["itemGrade"] = 2;  // 2성
  $t1ItemList[$idx]["itemCount"] = array(10, 20, 30,50);
  $t1ItemList[$idx]["itemValue"] = array(20, 40, 60,100);
  $t1ItemList[$idx]["resourceType"] = array(1, 2, 3);
  $idx++;

  $t1ItemList[$idx]["itemType"] = 1; // 유물
  $t1ItemList[$idx]["itemGrade"] = 3;  // 3성
  $t1ItemList[$idx]["itemCount"] = array(1);
  $t1ItemList[$idx]["itemValue"] = array(70);
  $t1ItemList[$idx]["resourceType"] = array(1, 2, 3);
  $idx++;

  $t1ItemList[$idx]["itemType"] = 2; // 마법
  $t1ItemList[$idx]["itemGrade"] = 2;  // 2성
  $t1ItemList[$idx]["itemCount"] = array(1);
  $t1ItemList[$idx]["itemValue"] = array(40);
  $t1ItemList[$idx]["resourceType"] = array(1, 2, 3);
  $idx++;

  $t1ItemList[$idx]["itemType"] = 3; // 퀘템
  $t1ItemList[$idx]["itemGrade"] = 1;  // 1티어
  $t1ItemList[$idx]["itemCount"] = array(10, 20, 30);
  $t1ItemList[$idx]["itemValue"] = array(25, 50, 75);
  $t1ItemList[$idx]["resourceType"] = array(1, 2, 3);
  $idx++;

  $t1ItemList[$idx]["itemType"] = 3; // 퀘템
  $t1ItemList[$idx]["itemGrade"] = 2;  // 2티어
  $t1ItemList[$idx]["itemCount"] = array(5, 10, 15);
  $t1ItemList[$idx]["itemValue"] = array(20, 40, 60);
  $t1ItemList[$idx]["resourceType"] = array(1, 2, 3);
  $idx++;

  $t1ItemList[$idx]["itemType"] = 3; // 퀘템
  $t1ItemList[$idx]["itemGrade"] = 3;  // 3티어
  $t1ItemList[$idx]["itemCount"] = array(5, 10, 15);
  $t1ItemList[$idx]["itemValue"] = array(30, 60, 90);
  $t1ItemList[$idx]["resourceType"] = array(1, 2, 3);
  $idx++;

  $t1ItemList[$idx]["itemType"] = 4; // 룬
  $t1ItemList[$idx]["itemGrade"] = 1;  // 1티어
  $t1ItemList[$idx]["itemCount"] = array(10, 20, 30);
  $t1ItemList[$idx]["itemValue"] = array(15, 30, 45);
  $t1ItemList[$idx]["resourceType"] = array(1, 2, 3);
  $idx++;

  $t1ItemList[$idx]["itemType"] = 4; // 룬
  $t1ItemList[$idx]["itemGrade"] = 2;  // 2티어
  $t1ItemList[$idx]["itemCount"] = array(10, 20, 30);
  $t1ItemList[$idx]["itemValue"] = array(25, 50, 75);
  $t1ItemList[$idx]["resourceType"] = array(1, 2, 3);
  $idx++;

  $t1ItemList[$idx]["itemType"] = 502; // 2성 랜덤 영혼석
  $t1ItemList[$idx]["itemGrade"] = 0;  // none
  $t1ItemList[$idx]["itemCount"] = array(10, 20, 30, 50);
  $t1ItemList[$idx]["itemValue"] = array(15, 30, 45, 75);
  $t1ItemList[$idx]["resourceType"] = array(1, 2, 3);
  $idx++;

  $t1ItemList[$idx]["itemType"] = 503; // 3성 랜덤 영혼석
  $t1ItemList[$idx]["itemGrade"] = 0;  // none
  $t1ItemList[$idx]["itemCount"] = array(10, 20, 30 , 50);
  $t1ItemList[$idx]["itemValue"] = array(25, 50, 75, 125);
  $t1ItemList[$idx]["resourceType"] = array(1, 2, 3);
  $idx++;

  $t1ItemList[$idx]["itemType"] = 504; // 4성 랜덤 영혼석
  $t1ItemList[$idx]["it   emGrade"] = 0;  // none
  $t1ItemList[$idx]["itemCount"] = array(10, 20, 30);
  $t1ItemList[$idx]["itemValue"] = array(40, 80, 120);
  $t1ItemList[$idx]["resourceType"] = array(1, 2, 3);
  $idx++;
  ############### 1티어 END ###############

  ################# 2티어 #################
  $idx = 0;
  for ( $i=0; $i<2; $i++ ) {
    $t2ItemList[$idx]["itemType"] = 0; // 영혼석
    $t2ItemList[$idx]["itemGrade"] = 3;  // 3성
    $t2ItemList[$idx]["itemCount"] = array(10, 20, 30);
    $t2ItemList[$idx]["itemValue"] = array(30, 60, 90);
    $t2ItemList[$idx]["resourceType"] = array(1, 2, 3);
    $idx++;

    $t2ItemList[$idx]["itemType"] = 0; // 영혼석
    $t2ItemList[$idx]["itemGrade"] = 4;  // 4성
    $t2ItemList[$idx]["itemCount"] = array(10, 20, 30);
    $t2ItemList[$idx]["itemValue"] = array(50, 100, 150);
    $t2ItemList[$idx]["resourceType"] = array(1, 2, 3);
    $idx++;

    $t2ItemList[$idx]["itemType"] = 100001; // 특성포인트 초기화권
    $t2ItemList[$idx]["itemGrade"] = 0;  // none
    $t2ItemList[$idx]["itemCount"] = array(1 , 2);
    $t2ItemList[$idx]["itemValue"] = array(50, 100);
    $t2ItemList[$idx]["resourceType"] = array(1, 1, 2,  3);
    $idx++;

    $t2ItemList[$idx]["itemType"] = 100002; // 어빌리티슬롯 초기화권
    $t2ItemList[$idx]["itemGrade"] = 0;  // none
    $t2ItemList[$idx]["itemCount"] = array(1, 2);
    $t2ItemList[$idx]["itemValue"] = array(30, 60);
    $t2ItemList[$idx]["resourceType"] = array(2, 3);
    $idx++;

    $t2ItemList[$idx]["itemType"] = 5006; // 마법 정수
    $t2ItemList[$idx]["itemGrade"] = 0;  // none
    $t2ItemList[$idx]["itemCount"] = array(500, 1000, 2000);
    $t2ItemList[$idx]["itemValue"] = array(50, 100, 150);
    $t2ItemList[$idx]["resourceType"] = array(1);
    $idx++;

    $t2ItemList[$idx]["itemType"] = 4; // 룬
    $t2ItemList[$idx]["itemGrade"] = 3;  // 3티어
    $t2ItemList[$idx]["itemCount"] = array(10, 20, 30);
    $t2ItemList[$idx]["itemValue"] = array(50, 100, 150);
    $t2ItemList[$idx]["resourceType"] = array(1, 1, 2, 3);
    $idx++;

    $t2ItemList[$idx]["itemType"] = 3; // 퀘템
    $t2ItemList[$idx]["itemGrade"] = 4;  // 4티어
    $t2ItemList[$idx]["itemCount"] = array(3, 5, 7);
    $t2ItemList[$idx]["itemValue"] = array(75, 125, 175);
    $t2ItemList[$idx]["resourceType"] = array(1, 2);
    $idx++;

    $t2ItemList[$idx]["itemType"] = 5003; // 번개
    $t2ItemList[$idx]["itemGrade"] = 0;  // none
    $t2ItemList[$idx]["itemCount"] = array(30, 50, 100);
    $t2ItemList[$idx]["itemValue"] = array(21, 35, 70);
    $t2ItemList[$idx]["resourceType"] = array(1, 1, 2, 3);
    $idx++;

    $t2ItemList[$idx]["itemType"] = 1496; // 장인의 화려한 제련석
    $t2ItemList[$idx]["itemGrade"] = 0;  // none
    $t2ItemList[$idx]["itemCount"] = array(3, 6, 9);
    $t2ItemList[$idx]["itemValue"] = array(15, 30, 45);
    $t2ItemList[$idx]["resourceType"] = array(1, 2, 3);
    $idx++;

    $t2ItemList[$idx]["itemType"] = 505; // 5성 랜덤 영혼석
    $t2ItemList[$idx]["itemGrade"] = 0;  // none
    $t2ItemList[$idx]["itemCount"] = array(10, 20);
    $t2ItemList[$idx]["itemValue"] = array(70, 140);
    $t2ItemList[$idx]["resourceType"] = array(1, 2, 3);
    $idx++;

    $t2ItemList[$idx]["itemType"] = 4; // 룬
    $t2ItemList[$idx]["itemGrade"] = 4;  // 4티어
    $t2ItemList[$idx]["itemCount"] = array(5, 10, 15);
    $t2ItemList[$idx]["itemValue"] = array(60, 120, 180);
    $t2ItemList[$idx]["resourceType"] = array(1,1, 2, 3);
    $idx++;

    $t2ItemList[$idx]["itemType"] = 4; // 룬
    $t2ItemList[$idx]["itemGrade"] = 5;  // 5티어
    $t2ItemList[$idx]["itemCount"] = array(1, 2, 3);
    $t2ItemList[$idx]["itemValue"] = array(80, 160, 240);
    $t2ItemList[$idx]["resourceType"] = array(1,1, 2, 3);
    $idx++;
  }

  $t2ItemList[$idx]["itemType"] = 1504; // 4성 랜덤 유물
  $t2ItemList[$idx]["itemGrade"] = 0;  // none
  $t2ItemList[$idx]["itemCount"] = array(1);
  $t2ItemList[$idx]["itemValue"] = array(150);
  $t2ItemList[$idx]["resourceType"] = array(1, 2, 3);
  $idx++;

  $t2ItemList[$idx]["itemType"] = 2; // 마법
  $t2ItemList[$idx]["itemGrade"] = 3;  // 3성
  $t2ItemList[$idx]["itemCount"] = array(1);
  $t2ItemList[$idx]["itemValue"] = array(150);
  $t2ItemList[$idx]["resourceType"] = array(1, 2, 3);
  $idx++;

  ############### 2티어 END ###############

  ################# 3티어 #################
  $idx = 0;
  for ( $i=0; $i<3; $i++ ) {
    $t3ItemList[$idx]["itemType"] = 0; // 영혼석
    $t3ItemList[$idx]["itemGrade"] = 5;  // 5성
    $t3ItemList[$idx]["itemCount"] = array(10, 20, 30);
    $t3ItemList[$idx]["itemValue"] = array(80, 160, 240);
    $t3ItemList[$idx]["resourceType"] = array(1, 2, 3);
    $idx++;

    $t3ItemList[$idx]["itemType"] = 0; // 영혼석
    $t3ItemList[$idx]["itemGrade"] = 6;  // 6성
    $t3ItemList[$idx]["itemCount"] = array(10, 20, 30);
    $t3ItemList[$idx]["itemValue"] = array(100, 200, 300);
    $t3ItemList[$idx]["resourceType"] = array(1, 2, 3);
    $idx++;


    $t3ItemList[$idx]["itemType"] = 100003; // 황금상자 뽑기권
    $t3ItemList[$idx]["itemGrade"] = 0;  // none
    $t3ItemList[$idx]["itemCount"] = array(1, 2, 3);
    $t3ItemList[$idx]["itemValue"] = array(30, 60, 90);
    $t3ItemList[$idx]["resourceType"] = array(1, 1, 2, 3);
    $idx++;

    $t3ItemList[$idx]["itemType"] = 5002; // 티켓
    $t3ItemList[$idx]["itemGrade"] = 0;  // none
    $t3ItemList[$idx]["itemCount"] = array(20, 40);
    $t3ItemList[$idx]["itemValue"] = array(80, 160);
    $t3ItemList[$idx]["resourceType"] = array(1, 1, 2, 3);
    $idx++;

    $t3ItemList[$idx]["itemType"] = 4; // 룬
    $t3ItemList[$idx]["itemGrade"] = 4;  // 4티어
    $t3ItemList[$idx]["itemCount"] = array(10, 20, 30);
    $t3ItemList[$idx]["itemValue"] = array(120, 240, 360);
    $t3ItemList[$idx]["resourceType"] = array(1, 1, 2, 3);
    $idx++;

    $t3ItemList[$idx]["itemType"] = 3; // 퀘템
    $t3ItemList[$idx]["itemGrade"] = 5;  // 5티어
    $t3ItemList[$idx]["itemCount"] = array(3, 5, 7);
    $t3ItemList[$idx]["itemValue"] = array(120, 200, 280);
    $t3ItemList[$idx]["resourceType"] = array(1, 1, 2, 3);
    $idx++;

    $t3ItemList[$idx]["itemType"] = 5000; // 골드
    $t3ItemList[$idx]["itemGrade"] = 0;  // none
    $t3ItemList[$idx]["itemCount"] = array(30000, 60000, 100000);
    $t3ItemList[$idx]["itemValue"] = array(60, 120, 200);
    $t3ItemList[$idx]["resourceType"] = array(1);
    $idx++;

    $t3ItemList[$idx]["itemType"] = 1495; // 제련석
    $t3ItemList[$idx]["itemGrade"] = 0;  // none
    $t3ItemList[$idx]["itemCount"] = array(3, 6, 9);
    $t3ItemList[$idx]["itemValue"] = array(100, 200, 300);
    $t3ItemList[$idx]["resourceType"] = array(1, 2, 3);
    $idx++;

    $t3ItemList[$idx]["itemType"] = 4; // 룬
    $t3ItemList[$idx]["itemGrade"] = 5;  // 5티어
    $t3ItemList[$idx]["itemCount"] = array(3,4, 5);
    $t3ItemList[$idx]["itemValue"] = array(210, 280, 350);
    $t3ItemList[$idx]["resourceType"] = array(1, 1, 2, 3);
    $idx++;

    $t3ItemList[$idx]["itemType"] = 5005; // 메달 묶음
    $t3ItemList[$idx]["itemGrade"] = 0;  // none
    $t3ItemList[$idx]["itemCount"] = array(100, 200,300);
    $t3ItemList[$idx]["itemValue"] = array(60, 120, 180);
    $t3ItemList[$idx]["resourceType"] = array(1, 1, 2, 3);
    $idx++;
  }

  $t3ItemList[$idx]["itemType"] = 1; // 유물
  $t3ItemList[$idx]["itemGrade"] = 4;  // 4성
  $t3ItemList[$idx]["itemCount"] = array(1);
  $t3ItemList[$idx]["itemValue"] = array(190);
  $t3ItemList[$idx]["resourceType"] = array(1, 2, 3);
  $idx++;

  $t3ItemList[$idx]["itemType"] = 1505; // 5성 랜덤 유물
  $t3ItemList[$idx]["itemGrade"] = 0;  // none
  $t3ItemList[$idx]["itemCount"] = array(1);
  $t3ItemList[$idx]["itemValue"] = array(300);
  $t3ItemList[$idx]["resourceType"] = array(1, 1,2, 3);
  $idx++;

  ############### 3티어 END ###############

  ################# 4티어 #################
  $idx = 0;
  for ( $i=0; $i<3; $i++ ) {
    $t4ItemList[$idx]["itemType"] = 5006; // 마법 정수
    $t4ItemList[$idx]["itemGrade"] = 0;  // none
    $t4ItemList[$idx]["itemCount"] = array(3000, 5000);
    $t4ItemList[$idx]["itemValue"] = array(300, 500);
    $t4ItemList[$idx]["resourceType"] = array(1);
    $idx++;

    $t4ItemList[$idx]["itemType"] = 3; // 퀘템
    $t4ItemList[$idx]["itemGrade"] = 2;  // 2티어
    $t4ItemList[$idx]["itemCount"] = array(20, 30, 40);
    $t4ItemList[$idx]["itemValue"] = array(80, 120, 160);
    $t4ItemList[$idx]["resourceType"] = array(1, 1, 2, 3);
    $idx++;

    $t4ItemList[$idx]["itemType"] = 3; // 퀘템
    $t4ItemList[$idx]["itemGrade"] = 3;  // 3티어
    $t4ItemList[$idx]["itemCount"] = array(10, 20, 30);
    $t4ItemList[$idx]["itemValue"] = array(70, 140, 210);
    $t4ItemList[$idx]["resourceType"] = array(1, 1, 2, 3);
    $idx++;

    $t4ItemList[$idx]["itemType"] = 4; // 룬
    $t4ItemList[$idx]["itemGrade"] = 1;  // 1티어
    $t4ItemList[$idx]["itemCount"] = array(50, 70, 100);
    $t4ItemList[$idx]["itemValue"] = array(75, 105, 150);
    $t4ItemList[$idx]["resourceType"] = array(1, 2, 3);
    $idx++;

    $t4ItemList[$idx]["itemType"] = 4; // 룬
    $t4ItemList[$idx]["itemGrade"] = 2;  // 2티어
    $t4ItemList[$idx]["itemCount"] = array(50, 70);
    $t4ItemList[$idx]["itemValue"] = array(125, 175);
    $t4ItemList[$idx]["resourceType"] = array(1, 2, 3);
    $idx++;

    $t4ItemList[$idx]["itemType"] = 100000; // 유물 능력 변경권
    $t4ItemList[$idx]["itemGrade"] = 0;  // none
    $t4ItemList[$idx]["itemCount"] = array(1, 2, 3);
    $t4ItemList[$idx]["itemValue"] = array(100, 200, 300);
    $t4ItemList[$idx]["resourceType"] = array(1, 1, 2, 3);
    $idx++;

    $t4ItemList[$idx]["itemType"] = 100003; // 황금상자 뽑기권
    $t4ItemList[$idx]["itemGrade"] = 0;  // none
    $t4ItemList[$idx]["itemCount"] = array(5, 10, 15);
    $t4ItemList[$idx]["itemValue"] = array(150, 300, 450);
    $t4ItemList[$idx]["resourceType"] = array(1, 1, 2, 3);
    $idx++;

    $t4ItemList[$idx]["itemType"] = 5005; // 메달 묶음
    $t4ItemList[$idx]["itemGrade"] = 0;  // none
    $t4ItemList[$idx]["itemCount"] = array( 500,700, 1000);
    $t4ItemList[$idx]["itemValue"] = array(275, 385, 550);
    $t4ItemList[$idx]["resourceType"] = array(1, 1, 2, 3);
    $idx++;

    $t4ItemList[$idx]["itemType"] = 4; // 룬
    $t4ItemList[$idx]["itemGrade"] = 5;  // 5티어
    $t4ItemList[$idx]["itemCount"] = array(5, 10, 15);
    $t4ItemList[$idx]["itemValue"] = array(250, 500, 750);
    $t4ItemList[$idx]["resourceType"] = array(1,1,1,1,2,3);
    $idx++;

    $t4ItemList[$idx]["itemType"] = 3; // 퀘템
    $t4ItemList[$idx]["itemGrade"] = 6;  // 6티어
    $t4ItemList[$idx]["itemCount"] = array(1, 2, 3);
    $t4ItemList[$idx]["itemValue"] = array(100, 200, 300);
    $t4ItemList[$idx]["resourceType"] = array(1,1,1,1,2,3);
    $idx++;

    $t4ItemList[$idx]["itemType"] = 506; // 6성 랜덤 영혼석
    $t4ItemList[$idx]["itemGrade"] = 0;  // none
    $t4ItemList[$idx]["itemCount"] = array(10, 20, 30);
    $t4ItemList[$idx]["itemValue"] = array(120, 240, 360);
    $t4ItemList[$idx]["resourceType"] = array(1, 2, 3);
    $idx++;

    $t4ItemList[$idx]["itemType"] = 110000; // 무지개 룬
    $t4ItemList[$idx]["itemGrade"] = 0;  // none
    $t4ItemList[$idx]["itemCount"] = array(1);
    $t4ItemList[$idx]["itemValue"] = array(300);
    $t4ItemList[$idx]["resourceType"] = array(1);
    $idx++;
  }

  $t4ItemList[$idx]["itemType"] = 10504; // 4성 랜덤 마법
  $t4ItemList[$idx]["itemGrade"] = 0;  // none
  $t4ItemList[$idx]["itemCount"] = array(1);
  $t4ItemList[$idx]["itemValue"] = array(450);
  $t4ItemList[$idx]["resourceType"] = array(1,1, 2, 3);
  $idx++;

  $t4ItemList[$idx]["itemType"] = 1; // 유물
  $t4ItemList[$idx]["itemGrade"] = 5;  // 5성
  $t4ItemList[$idx]["itemCount"] = array(1);
  $t4ItemList[$idx]["itemValue"] = array(400);
  $t4ItemList[$idx]["resourceType"] = array(1, 1, 2, 3);
  $idx++;
  ############### 4티어 END ###############


  ################# 5티어 #################
  $idx=0;
  for ( $i=0; $i<3; $i ++ ) {
    $t5ItemList[$idx]["itemType"] = 110000; // 무지개 룬
    $t5ItemList[$idx]["itemGrade"] = 0;  // none
    $t5ItemList[$idx]["itemCount"] = array(1,2,3);
    $t5ItemList[$idx]["itemValue"] = array(300,600,900);
    $t5ItemList[$idx]["resourceType"] = array(1);
    $idx++;

    $t5ItemList[$idx]["itemType"] = 4; // 룬
    $t5ItemList[$idx]["itemGrade"] = 5;  // 5티어
    $t5ItemList[$idx]["itemCount"] = array(20);
    $t5ItemList[$idx]["itemValue"] = array(1000);
    $t5ItemList[$idx]["resourceType"] = array(1);
    $idx++;

    $t5ItemList[$idx]["itemType"] = 3; // 퀘템
    $t5ItemList[$idx]["itemGrade"] = 6;  // 6티어
    $t5ItemList[$idx]["itemCount"] = array(5);
    $t5ItemList[$idx]["itemValue"] = array(500);
    $t5ItemList[$idx]["resourceType"] = array(1);
    $idx++;

    $t5ItemList[$idx]["itemType"] = 100003; // 황금상자 뽑기권
    $t5ItemList[$idx]["itemGrade"] = 0;  // none
    $t5ItemList[$idx]["itemCount"] = array(10, 20);
    $t5ItemList[$idx]["itemValue"] = array(280, 560);
    $t5ItemList[$idx]["resourceType"] = array(1);
    $idx++;


    $t5ItemList[$idx]["itemType"] = 5005; // 메달 묶음
    $t5ItemList[$idx]["itemGrade"] = 0;  // none
    $t5ItemList[$idx]["itemCount"] = array(1500,2000);
    $t5ItemList[$idx]["itemValue"] = array(750, 1000);
    $t5ItemList[$idx]["resourceType"] = array(1);
    $idx++;

    $t5ItemList[$idx]["itemType"] = 100000; // 유물 능력 변경권
    $t5ItemList[$idx]["itemGrade"] = 0;  // none
    $t5ItemList[$idx]["itemCount"] = array(5,7,10);
    $t5ItemList[$idx]["itemValue"] = array(500, 700, 1000);
    $t5ItemList[$idx]["resourceType"] = array(1);
    $idx++;
  }

  $t5ItemList[$idx]["itemType"] = 10505; // 5성 랜덤 마법
  $t5ItemList[$idx]["itemGrade"] = 0;  // none
  $t5ItemList[$idx]["itemCount"] = array(1);
  $t5ItemList[$idx]["itemValue"] = array(900);
  $t5ItemList[$idx]["resourceType"] = array(1);
  $idx++;

  ############### 5티어 END ###############
  ############### 6티어 Start ###############
  $idx = 0;
  $t6ItemList[$idx]["itemType"] = 283; // 5성 랜덤 마법
  $t6ItemList[$idx]["itemGrade"] = 0;  // none
  $t6ItemList[$idx]["itemCount"] = array(1);
  $t6ItemList[$idx]["itemValue"] = array(900);
  $t6ItemList[$idx]["resourceType"] = array(1);
  $idx++;
  ############### 6티어 END ###############
  function getItemCost($itemValue, $resourceType, $discountRate) {

    if ($resourceType == 1) { // 다이아
      $valueRate = 1;
    }
    else if ($resourceType == 2) { // 골드
      $valueRate = 2000;
    }
    else if ($resourceType == 3) {  // 마법정수
      $valueRate = 40;
    }
    else {  // 잘못된 인자
      return 100000000;
    }

    $discountValue = (100 - $discountRate) / 100;

    $itemCost = $itemValue * $valueRate * $discountValue;
    return intval($itemCost);
  }

  function getItemList($tierWeight) {
    $redis = openRedis();
    if ( $redis == false )
      return null;

    $tiers = explode(",", $tierWeight);

    $sum = 0;
    for ($i=0; $i < count($tiers); $i++) {
      $sum += $tiers[$i];
    }

    global $t1ItemList, $t2ItemList, $t3ItemList, $t4ItemList, $t5ItemList, $t6ItemList;

    $itemList = array($t1ItemList, $t2ItemList, $t3ItemList, $t4ItemList, $t5ItemList, $t6ItemList);

    srand((double) microtime(true) * 1000000);

    // for TEST variable
    $tierIdx = 0;

    $hasJackpot = false;

/*
    $query = sprintf("select val from frdEffectForEtc where userId = %d and type=%d", $userId, $GLOBALS['$ABILL_Sale_BlackMarket']);
    $sres = $db->query($query);
    if ($sres == false)
      return null;
*/
    $plusDisVal=0;
    /*
    if ( $sres->num_rows > 0 ) {
      $srow = $sres->fetch_assoc();
      $plusDisVal = (int)$srow["val"];
    }
    */

    for ($i = 0; $i < 8; $i++) {

      $random = rand(0, $sum - 1);

      $targetTierList = null;
      $tierWeightSum = 0;
      for ($j=0; $j < count($tiers); $j++) {
        $tierWeightSum += $tiers[$j];
        if ($random < $tierWeightSum) {
          $targetTierList = $itemList[$j];
          $tierIdx = $j + 1;
          break;
        }
      }

      $index = rand() % count($targetTierList);

      // $characMinIdxs = $redis->lrange('characMinIdx', 0, -1);
      // $characMaxIdxs = $redis->lrange('characMaxIdx', 0, -1);
      // $hiddenCharacIdxs = $redis->lrange('characHiddenStartIdx', 0, -1);
      // $banCharacs = $redis->lrange('blockCharacInShop', 0, -1);
      // $weaponMinIdxs = $redis->lrange('weaponMinIdx', 0, -1);
      // $weaponMaxIdxs = $redis->lrange('weaponMaxIdx', 0, -1);
      // $magicMinIdxs = $redis->lrange('magicMinIdx', 0, -1);
      // $magicMaxIdxs = $redis->lrange('magicMaxIdx', 0, -1);

      // for TEST variables
      $characMinIdxs = array(0, 0, 10, 50, 100, 200, 300, 390);
      $characMaxIdxs = array(0, 7, 23, 75, 141, 234, 308);
      $hiddenCharacIdxs = array(0, 9, 49, 99, 192, 284, 386);
      $banCharacs = array(75, 193, 286, 386, 387);
      $weaponMinIdxs = array(0, 0, 10, 50, 100, 200, 300);
      $weaponMaxIdxs = array(0, 7, 25, 77, 131, 226, 301);
      $magicMinIdxs = array(0, 0, 100, 200, 300, 400);
      $magicMaxIdxs = array(0, 7, 107, 211, 310, 407);

      if ($targetTierList[$index]["itemType"] == 0) { // 영혼석
        $targetGrade = $targetTierList[$index]["itemGrade"];

        if ($targetGrade < 0) { // 히든
          $hiddenCharacIds = array();
          $hiddenIdx = 0;
          for ($j = 4; $j < count($hiddenCharacIdxs); $j++) {
            for ($k = $hiddenCharacIdxs[$j]; $k < $characMinIdxs[$j + 1]; $k++) {
              $hiddenCharacIds[$hiddenIdx++] = $k;
            }
          }

          do {
            $isReTry = false;
            $data["itemId"] = $hiddenCharacIds[rand() % count($hiddenCharacIds)];
            for ( $ii = 0; $ii < count($banCharacs); $ii++ ) {
              if ( $data["itemId"] == (int)$banCharacs[$ii] ) {
                $isReTry = true;
                break;
              }
            }
          } while($isReTry);
        }
        else {
          $minCIdx = $characMinIdxs[$targetGrade];
          $maxCIdx = $characMaxIdxs[$targetGrade];

          do {
            $isReTry = false;
            $data["itemId"] = rand($minCIdx, $maxCIdx);
            for ( $ii = 0; $ii < count($banCharacs); $ii++ ) {
              if ( $data["itemId"] == (int)$banCharacs[$ii] ) {
                $isReTry = true;
                break;
              }
            }
          } while($isReTry);
        }
      }
      else if ($targetTierList[$index]["itemType"] == 1) {  // 유물
        $targetGrade = $targetTierList[$index]["itemGrade"];
        $minWIdx = $weaponMinIdxs[$targetGrade];
        $maxWIdx = $weaponMaxIdxs[$targetGrade];

        $data["itemId"] = rand($minWIdx, $maxWIdx) + 1000;
      }
      else if ($targetTierList[$index]["itemType"] == 2) {  // 마법
        $targetGrade = $targetTierList[$index]["itemGrade"];
        $minMIdx = $magicMinIdxs[$targetGrade];
        $maxMIdx = $magicMaxIdxs[$targetGrade];

        $data["itemId"] = rand($minMIdx, $maxMIdx) + 10000;
      }
      else if ($targetTierList[$index]["itemType"] == 3) {  // 퀘템
        $targetGrade = $targetTierList[$index]["itemGrade"];
        $questIdx = rand() % 9;
        $data["itemId"] = 100000 + $targetGrade * 1000 + $questIdx;
      }
      else if ($targetTierList[$index]["itemType"] == 4) {  // 룬
        $targetGrade = $targetTierList[$index]["itemGrade"];
        $questIdx = rand() % 5;
        $data["itemId"] = 110000 + $targetGrade * 1000 + $questIdx;
      }
      else {  // item type == item id
        $data["itemId"] = $targetTierList[$index]["itemType"];
      }

      $countIdx = rand() % count($targetTierList[$index]["itemCount"]);
      $data["itemCount"] = $targetTierList[$index]["itemCount"][$countIdx];

      $resourceIdx = rand() % count($targetTierList[$index]["resourceType"]);
      $data["resourceType"] = $targetTierList[$index]["resourceType"][$resourceIdx];
      $discountRate;
      if (!$hasJackpot) {
        $jackpotValue = rand() % 100000;
        if ($jackpotValue < 10) {
          $discountRate = 50;
          $hasJackpot = true;
        }
        else if ($jackpotValue == 50000) {
          $discountRate = 90;
          $hasJackpot = true;
        }
        else if ($jackpotValue > 99994){
          $discountRate = 75;
          $hasJackpot = true;
        }
        else {
          if ( rand(0,9) <= 1 )
            $discountRate = 0;
          else
           $discountRate = rand(0, 20);
        }
      }
      else {
        if ( rand(0,9) <= 1 )
          $discountRate = 0;
        else
          $discountRate = rand(0, 20);
      }

      if ( $plusDisVal > 0 ) {
        $tPlusDisVal = rand($plusDisVal*0.666666, $plusDisVal*1.333333);
        $discountRate += ($tPlusDisVal * 0.001);
      }
      if ( $discountRate > 95 )
        $discountRate = 95;

      $data["discountRate"] = round($discountRate);
      $data["itemCost"] = getItemCost($targetTierList[$index]["itemValue"][$countIdx], $data["resourceType"], $data["discountRate"]);
      $data["isBuy"] = false;
      $data["tierIdx"] = $tierIdx;
      $res[$i] = $data;
    }

    return json_encode($res);
  }
?>
