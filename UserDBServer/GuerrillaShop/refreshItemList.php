<?php
  include_once("./itemListGenerator8_10.php");

  $session = $_REQUEST["session"];
  $userId = $_REQUEST["userId"];

  function sendErrorMessage($db, $session, $newSession, $userId, $errCode, $errMsg) {
    $db->query("rollback");
    $db->query("set autocommit=0");

    $error["code"] = $errCode;
    $error["msg"] = $errMsg;
    $data["error"] = $error;
    $data["session"] = intval($newSession);

    $query = "update frdUserData set session = $newSession where privateId = $userId";
    $res = $db->query($query);
    if ($res == false) {
      $db->query("rollback");
      $db->close();
      return 0;
    }

    $db->query("commit");
    $db->close();

    $keyAndIv = formatTo16String($session);
    return encrypt( $keyAndIv, json_encode($data), $keyAndIv);
  }

  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }

  $db->query("set autocommit=0");

  # session check
  $query .= "select jewel, session from frdUserData where privateId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;
      $userJewel = $row["jewel"];
    }
    else {
      // addBlacklist($id, "buy_chest_session");
      echo 1;
      $db->query("rollback");
      $db->close();
      return;
    }
  }

  # check shop is opened
  $query = "select * from frdGuerrillaShop";

  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $nextOpenTime = 2147483647;
  if ($res->num_rows > 0) {
    $guerrillaShopOpened = false;
    for ($idx = 0; $idx < $res->num_rows; $idx++) {
      $res->data_seek($idx);
      $row = $res->fetch_assoc();

      $shopId = (int)$row["shopId"];
      $openTime = $row["openTime"];
      $duration = $row["duration"];
      $tierWeight = $row["tierWeight"];

      $endTime = $openTime + $duration;

      $now = time();
      if ($openTime <= $now && $now <= $endTime) {
        $guerrillaShopOpened = true;
        break;
      }

      if ($openTime > $now) {
        $nextOpenTime = min($openTime, $nextOpenTime);
      }
    }
  }

  if ($guerrillaShopOpened == false) {
    $data["nextOpenTime"] = intval($nextOpenTime);
    $query = "update frdUserData set session = $newSession where privateId = $userId";
    $res = $db->query($query);
    if ($res == false) {
      $db->query("rollback");
      $db->close();
      return 0;
    }

    $db->query("commit");
    $db->close();

    $keyAndIv = formatTo16String($session);
    echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
    return;
  }

  $query = "select * from frdGuerrillaShopUserData where userId = " . $userId;

  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();

    $userShopId = (int)$row["shopId"];
    $resetCount = (int)$row["resetCount"];
    if ($shopId !== $userShopId) {
      $data["error"] = 100;   // need refresh shop!!
      echo $data;
      $db->query("rollback");
      $db->close();
      return;
    }
  }

  # check resources
  function getRefreshCost($resetCount) {
    switch ($resetCount) {
      case 0:           return 50;
      case 1:           return 100;
      case 2:           return 200;
      case 3: default:  return 300;
    }
  }

  $refreshCost = getRefreshCost($resetCount);
  if ($refreshCost > $userJewel) {
    echo sendErrorMessage($db, $session, $newSession, $userId, 103, "no jewel!!");
    return;
  }

  # item list reset
  $itemList = getItemList($db, $userId, $tierWeight);
  if ( $itemList == null ) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $resetCount += 1;
  $query = "update frdGuerrillaShopUserData set resetCount = " . $resetCount . ", itemList = '" . $itemList . "' where userId = " . $userId;
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  include_once("../EventAccuJewel.php");
  $resultCode = AccuUseJewel($db, $userId, $refreshCost);
  if ($resultCode == 0) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  # decrease user resource
  $query = "update frdUserData set jewel = jewel - $refreshCost, session = $newSession where privateId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $db->query("commit");
	$db->close();

  $data["resetCount"] = $resetCount;
  $data["itemList"] = json_decode($itemList);
  $data["userResources"] = $userJewel - $refreshCost;

  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
