<?php

  include_once("../myAes.php");
  include_once("./itemAdder.php");

  $userId = $_REQUEST["userId"];
  $itemId = $_REQUEST["itemId"];
  $itemCount = $_REQUEST["itemCount"];

  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }
  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    $db->close();
    return;
  }


  # check session and resources
  $query = "select weaponIdPointer, artifactIdPointer from frdUserData where privateId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    $weaponIdPointer = $row["weaponIdPointer"];
    $artifactIdPointer = $row["artifactIdPointer"];
  }

  $res = addItem($db, $redis, $userId, $itemId, $itemCount, $weaponIdPointer, $artifactIdPointer);

  echo json_encode($res);
?>
