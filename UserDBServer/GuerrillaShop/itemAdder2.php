<?php

  function addItem($db, $redis, $userId, $itemId, $itemCount) {
    $query = "select weaponIdPointer, artifactIdPointer from frdUserData where privateId = $userId";
    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    if ($res->num_rows > 0) {
      $row = $res->fetch_assoc();
      $weaponIdPointer = $row["weaponIdPointer"];
      $artifactIdPointer = $row["artifactIdPointer"];
    }

    if ( $itemId < 1000 ) {  // charac

      $banCharacs = $redis->lrange('blockCharacInShop', 0, -1);

      if ( $itemId > 500 ) {
        $minCIdx = $redis->lindex('characMinIdx', $itemId - 500);
        $maxCIdx = $redis->lindex('characMaxIdx', $itemId - 500);

				do {
					$isReTry = false;
					$itemId = rand($minCIdx, $maxCIdx);
					for ( $ii = 0; $ii < count($banCharacs); $ii++ ) {
						if ( $itemId == (int)$banCharacs[$ii] ) {
							$isReTry = true;
							break;
						}
					}
				} while($isReTry);
      }
      $amount = $itemCount;

      $bundleIdx = ($itemId>>3);
      $query = "select exps from frdCharacEvolveExps where userId = $userId and bundleIdx = $bundleIdx";
      $sres = $db->query($query);
      if ($sres == false) {
        return 0;
      }

      if ($sres->num_rows > 0) {
        $row = $sres->fetch_assoc();
        $longData = $row["exps"];
        $bitIdx = 8*($itemId&7);
        $preExp = ( ($longData >> $bitIdx) & 0xff);

        if ( $preExp > 0 ) {    // yes Bundle, yes Charac
          $mask = $preExp;
          $mask <<= $bitIdx;
          $longData -= $mask;

          $mask = ($preExp + $amount);
          if ( $mask > 255 )
            $mask = 255;

          $data["characId"] = $itemId;
          $data["characExp"] = $mask;

          $mask <<= $bitIdx;
          $resultData = ($longData | $mask);

          $query = "update frdCharacEvolveExps set exps = $resultData where userId = $userId and bundleIdx = $bundleIdx";
          $isGood = $db->query($query);
          if ($isGood == false) {
            return 0;
          }

          $data["characBundleIdx"] = $bundleIdx;
          $data["characExps"] = $resultData;
        }
        else {                    // yes Bundle, no Charac
          $data["newCharac"] = 1;
          $mask = $amount;
          $mask <<= $bitIdx;
          $resultData = ($longData | $mask);

          $query = "update frdCharacEvolveExps set exps = $resultData where userId = $userId and bundleIdx = $bundleIdx";
          $isGood = $db->query($query);
          if ($isGood == false) {
            return 0;
          }
          $data["characId"] = $itemId;
          $data["characExp"] = $amount;
          $data["characBundleIdx"] = $bundleIdx;
          $data["characExps"] = $resultData;
        }
      }
      else {                      //no Bundle
        $data["newCharac"] = 1;
        $resultData = $amount << (8*($itemId&7));
        $query = "insert into frdCharacEvolveExps values ($userId, $bundleIdx, $resultData)";
        $isGood = $db->query($query);
        if ($isGood == false) {
          return 0;
        }
        $data["characId"] = $itemId;
        $data["characExp"] = $amount;
        $data["characBundleIdx"] = $bundleIdx;
        $data["characExps"] = $resultData;
      }
    }
    else if ( $itemId < 2000 ) { // weapon
      if ( $itemId > 1500 ) {
        $minWIdx = $redis->lindex('weaponMinIdx', $itemId - 1500);
        $maxWIdx = $redis->lindex('weaponMaxIdx', $itemId - 1500);
        $itemId = rand($minWIdx, $maxWIdx) + 1000;
      }
      $itemId = $itemId - 1000;

      for ($idx = 0; $idx < $itemCount; $idx++) {
        $query = sprintf("update frdUserData set weaponIdPointer=%d where privateId='%s'", $weaponIdPointer+1, $userId);
        $isGood = $db->query($query);
        if ($isGood == false) {
          return 0;
        }

        $stat = rand(0, 10000);
        $query = "insert into frdHavingWeapons values ($weaponIdPointer, $userId, $itemId, 0, $stat)";
        $isGood = $db->query($query);
        if ($isGood == false) {
          return 0;
        }

        $data["weaponIdx".$idx] = $weaponIdPointer;
        $data["weaponId".$idx] = $itemId;
        $data["stat".$idx] = $stat;

        $weaponIdPointer++;
      }
      $data["weaponCount"] = $itemCount;
    }
    else if ( $itemId == 5000 ) {   //골드 지급
      $addedGold = $itemCount;

      $query = "update frdUserData set gold = gold + $addedGold where privateId = $userId";
      $isGood = $db->query($query);
      if ($isGood == false) {
        return 0;
      }
      $data["addedGold"] = $addedGold;
    }
    else if ( $itemId == 5001 ) {   // 경험치 지급
      $addedExp = $itemCount;
      $query = "update frdUserData set exp = exp + $addedExp where privateId = $userId";
      $isGood = $db->query($query);
      if ($isGood == false) {
        return 0;
      }
      $data["addedExp"] = $addedExp;
    }
    else if ( $itemId == 5002 ) {   // 티켓 지급
      $addedTicket = $itemCount;
      $query = "update frdUserData set ticket = ticket + $addedTicket where privateId = $userId";
      $isGood = $db->query($query);
      if ($isGood == false) {
        return 0;
      }
      $data["addedTicket"] = $addedTicket;
    }
    else if ( $itemId == 5003 ) {    // heart
      $addedHeart = $itemCount;
      $query = "select heart, startTime, givedHeartCount from frdUserData where privateId = $userId";
      $eres = $db->query($query);
      if ($eres == false) {
        return 0;
      }

      if ($eres->num_rows > 0) {
        $erow = $eres->fetch_assoc();

        $addTotalHeartCount = (int)((time() - $erow["startTime"]) / 180);
        if ( $erow["givedHeartCount"] < $addTotalHeartCount ) {
          $addHeartCount = $addTotalHeartCount - $erow["givedHeartCount"];
        }
      }

      $eres->close();
      $addedHeart += $addHeartCount;
      $query = "update frdUserData set heart = heart + $addedHeart, givedHeartCount = $addTotalHeartCount where privateId = $userId";

      $isGood = $db->query($query);
      if ($isGood == false) {
        return 0;
      }
      $data["addedHeart"] = $addedHeart;
    }
    else if ( $itemId == 5004 ) {    // jewel
      $addedJewel = $itemCount;
      $query = "update frdUserData set jewel = jewel + $addedJewel where privateId = $userId";
      $isGood = $db->query($query);
      if ($isGood == false) {
        return 0;
      }
      $data["addedJewel"] = $addedJewel;
    }
    else if ( $itemId == 5005 ) {   //메달 지급
      $addedMedal = $itemCount;
      $query = "update frdUserData set skillPoint = skillPoint + $addedMedal where privateId = $userId";
      $isGood = $db->query($query);
      if ($isGood == false) {
        return 0;
      }

      $query = "update frdSkillPoints set accuSkillPoint = accuSkillPoint + $addedMedal where userId = $userId";
      $isGood = $db->query($query);
      if ($isGood == false) {
        return 0;
      }
      $data["addedMedal"] = $addedMedal;
    }
    else if ( $itemId == 5006 ) {   //마법가루 지급
      $addedPowder = $itemCount;
      $query = "update frdUserData set powder = powder + $addedPowder where privateId = $userId";
      $isGood = $db->query($query);
      if ($isGood == false) {
        return 0;
      }
      $data["addedPowder"] = $addedPowder;
    }
    else if ($itemId < 12000) {
      if ( $itemId > 10500 ) {
        $minWIdx = $redis->lindex('magicMinIdx', $itemId - 10500);
        $maxWIdx = $redis->lindex('magicMaxIdx', $itemId - 10500);
        $itemId = rand($minWIdx, $maxWIdx) + 10000;
      }
      $itemId = $itemId - 10000;

      for ($idx = 0; $idx < $itemCount; $idx++) {
        $query = sprintf("update frdUserData set artifactIdPointer=%d where privateId='%s'", $artifactIdPointer+1, $userId);
        $isGood = $db->query($query);
        if ($isGood == false) {
          return 0;
        }

        $query = "insert into frdHavingArtifacts values ($artifactIdPointer, $userId, $itemId, 0, 0)";
        $isGood = $db->query($query);
        if ($isGood == false) {
          return 0;
        }

        $data["magicIdx".$idx] = $artifactIdPointer;
        $data["magicId".$idx] = $itemId;

        $artifactIdPointer++;
      }
      $data["magicCount"] = $itemCount;
    }
    else {
      $query = "select * from frdHavingItems where userId = $userId AND itemId = $itemId";
      $eres = $db->query($query);
      if ($eres == false) {
        return 0;
      }

      if ($eres->num_rows > 0) {
        $erow = $eres->fetch_assoc();
        $prevItemCount = $erow["itemCount"];
        $totalCount = $itemCount + $prevItemCount;
        $query = "update frdHavingItems set itemCount = $totalCount where userId = $userId AND itemId = $itemId";
      }
      else {
        $totalCount = $itemCount;
        $query = "insert into frdHavingItems value ($userId, $itemId, $totalCount)";
      }

      $eres = $db->query($query);
      if ($eres == false) {
        return 0;
      }

      $data["itemId"] = $itemId;
      $data["itemCount"] = $totalCount;
    }

    return $data;
  }
?>
