<?php
  include_once("./itemListGenerator8_10.php");

  $userId = $_REQUEST["userId"];

  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }

  $db->query("set autocommit=0");

  # check current guerrilla shop
  $query = "select * from frdGuerrillaShop";

  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $guerrillaShopOpened = false;
    $nextOpenTime = 2147483647;
    for ($idx = 0; $idx < $res->num_rows; $idx++) {
      $res->data_seek($idx);
      $row = $res->fetch_assoc();

      $shopId = $row["shopId"];
      $openTime = $row["openTime"];
      $duration = $row["duration"];
      $tierWeight = $row["tierWeight"];

      $endTime = $openTime + $duration;

      $now = time();
      if ($openTime <= $now && $now <= $endTime) {
        $guerrillaShopOpened = true;
        break;
      }

      if ($openTime > $now) {
        $nextOpenTime = min($openTime, $nextOpenTime);
      }
    }
  }

  if ($guerrillaShopOpened == false) {
    $data["nextOpenTime"] = intval($nextOpenTime);
    echo json_encode($data);
    return;
  }

  # check user has already opened guerrilla shop
  $query = "select * from frdGuerrillaShopUserData where userId = $userId AND shopId = $shopId";

  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    $resetCount = intval($row["resetCount"]);
    $itemList = $row["itemList"];
  } 
  else {
    $itemList = getItemList($db, $userId, $tierWeight);
    if ( $itemList == null ) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
    $resetCount = 0;

    $query = "insert into frdGuerrillaShopUserData value ($userId, $shopId, $resetCount , '$itemList')";
    $query .= " on duplicate key update shopId = $shopId, resetCount = $resetCount, itemList = '$itemList'";
    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }

  # send data
  $data["endTime"] = $endTime;
  $data["resetCount"] = $resetCount;
  $data["itemList"] = json_decode($itemList);

  $db->query("commit");
	$db->close();

  echo json_encode($data);
?>
