<?php
  include_once("../myAes.php");
  include_once("./itemAdder.php");

  $session = $_REQUEST["session"];
  $userId = $_REQUEST["userId"];
  $index = $_REQUEST["index"];
  $recvResources = (int)$_REQUEST["resources"];
  $recvResourceType = (int)$_REQUEST["resourceType"];

  function getResourceText($type) {
    switch ($type) {
      case 1: return "jewel";
      case 2: return "gold";
      case 3: return "powder";
      default:  return "";
    }
  }

  function sendErrorMessage($db, $session, $newSession, $userId, $errCode, $errMsg) {
    $db->query("rollback");
    $db->query("set autocommit=0");

    $error["code"] = $errCode;
    $error["msg"] = $errMsg;
    $data["error"] = $error;
    $data["session"] = intval($newSession);

    $query = "update frdUserData set session = $newSession where privateId = $userId";
    $res = $db->query($query);
    if ($res == false) {
      $db->query("rollback");
      $db->close();
      return 0;
    }

    $db->query("commit");
    $db->close();

    $keyAndIv = formatTo16String($session);
    return encrypt( $keyAndIv, json_encode($data), $keyAndIv);
  }

  $resourceText = getResourceText($recvResourceType);

  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }
  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    $db->close();
    return;
  }

  $db->query("set autocommit=0");

  # check session and resources
  $query = "select $resourceText, weaponIdPointer, artifactIdPointer, session from frdUserData where privateId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;
      $weaponIdPointer = $row["weaponIdPointer"];
      $artifactIdPointer = $row["artifactIdPointer"];
      $userResources = $row[$resourceText];
    }
    else {
      // addBlacklist($id, "buy_chest_session");
      echo 1;
      $db->query("rollback");
      $db->close();
      return;
    }
  }

  # check shop is opened
  $query = "select * from frdGuerrillaShop";

  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $guerrillaShopOpened = false;
    $nextOpenTime = 2147483647;
    for ($idx = 0; $idx < $res->num_rows; $idx++) {
      $res->data_seek($idx);
      $row = $res->fetch_assoc();

      $shopId = (int)$row["shopId"];
      $openTime = $row["openTime"];
      $duration = $row["duration"];
      $tierWeight = $row["tierWeight"];

      $endTime = $openTime + $duration;

      $now = time();
      if ($openTime <= $now && $now <= $endTime) {
        $guerrillaShopOpened = true;
        break;
      }

      if ($openTime > $now) {
        $nextOpenTime = min($openTime, $nextOpenTime);
      }
    }
  }

  if ($guerrillaShopOpened == false) {
    $data["nextOpenTime"] = intval($nextOpenTime);
    $query = "update frdUserData set session = $newSession where privateId = $userId";
    $res = $db->query($query);
    if ($res == false) {
      $db->query("rollback");
      $db->close();
      return 0;
    }

    $db->query("commit");
    $db->close();

    $keyAndIv = formatTo16String($session);
    echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
    return;
  }

  $query = "select * from frdGuerrillaShopUserData where userId = " . $userId;

  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();

    $userShopId = (int)$row["shopId"];
    if ($shopId !== $userShopId) {
      echo sendErrorMessage($db, $session, $newSession, $userId, 100, "need refresh shop!!");
      return;
    }

    $itemList = $row["itemList"];
  }

  # check item is able to buy
  $itemList = json_decode($itemList);
  if ($itemList[$index]->isBuy == true) {
    echo sendErrorMessage($db, $session, $newSession, $userId, 101, "already buy item!!");
    return;
  }

  # check item cost
  $itemCost = (int)$itemList[$index]->itemCost;
  $resourceType = (int)$itemList[$index]->resourceType;
  if (($itemCost !== $recvResources) || ($resourceType !== $recvResourceType)) {
    echo sendErrorMessage($db, $session, $newSession, $userId, 102, "not match data!!");
    return;
  }

  #check user has resources
  if ($itemCost > $userResources) {
    echo sendErrorMessage($db, $session, $newSession, $userId, 103, "not enough resources");
    return;
  }

  # update item list
  $itemList[$index]->isBuy = true;
  $encodedItemList = json_encode($itemList);
  $query = "update frdGuerrillaShopUserData set itemList = '" . $encodedItemList . "' where userId = " . $userId;
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  # add item to user
  $addItemRes = addItem($db, $redis, $userId, $itemList[$index]->itemId, $itemList[$index]->itemCount, $weaponIdPointer, $artifactIdPointer);
  if (is_int($addItemRes)) {
    echo $addItemRes;
    $db->query("rollback");
    $db->close();
    return;
  }

  $data["addItemRes"] = $addItemRes;

  if ($recvResourceType == 1) {
		include_once("../EventAccuJewel.php");
    $resultCode = AccuUseJewel($db, $userId, $itemCost);
    if ($resultCode == 0) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }

  # decrease user resource
  $query = "update frdUserData set $resourceText = $resourceText - $itemCost, session = $newSession where privateId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $db->query("commit");
	$db->close();

  $data["useResourceType"] = $resourceType;
  $data[$resourceText] = $userResources - $itemCost;

  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
