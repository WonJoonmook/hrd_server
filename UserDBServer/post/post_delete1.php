<?php
  // 우편 수령 요청.
  // 데이터 처리후 결과를 뿌려줍니다.
  // $pid = -1 -> 모두 수령,    else 하나 수령
  include_once("../myAes.php");
  $pId = $_REQUEST["pId"];
  $id = $_REQUEST["recvUserId"];


  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }

  $db->query("set autocommit=0");
  if ( ($pId == -1) || ($pId == '-1') ) {
    $query = sprintf("select * from frdUserPost where recvUserId = '%s'", $id);

  }
  else {
    $query = sprintf("select * from frdUserPost where recvUserId = '%s' and pId = '%s'", $id, $pId);
  }

  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    for ($idx = 0; $idx < $res->num_rows; $idx++) {
      $res->data_seek($idx);
      $row = $res->fetch_assoc();

      $rewardType = (int)$row["type"];
      $rewardCount = (int)$row["count"];

      if ( $rewardType < 1000 ) {        //캐릭터 소울스톤 지급
        if ( $rewardType > 500 ) {
          $redis = openRedis();
          if ( $redis == false ) {
            $db->query("rollback");
            $db->close();
            echo 0;
            return;
          }
          $banCharacs = $redis->lrange('blockCharacInShop', 0, -1);

          $minCIdx = $redis->lindex('characMinIdx', $rewardType - 500);
          $maxCIdx = $redis->lindex('characMaxIdx', $rewardType - 500);

  				do {
  					$isReTry = false;
  					$rewardType = rand($minCIdx, $maxCIdx);
  					for ( $ii = 0; $ii < count($banCharacs); $ii++ ) {
  						if ( $rewardType == (int)$banCharacs[$ii] ) {
  							$isReTry = true;
  							break;
  						}
  					}
  				} while($isReTry);
          $redis->close();
        }

        $bundleIdx = ($rewardType>>3);
        $query = sprintf("select exps from frdCharacEvolveExps where userId='%s' and bundleIdx=%d",$id, $bundleIdx);
        $sres = $db->query($query);
        if ($sres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        if ($sres->num_rows > 0) {
            $row = $sres->fetch_assoc();
            $longData = $row["exps"];
            $bitIdx = 8*($rewardType&7);
            $preExp = ( ($longData >> $bitIdx) & 0xff);


            $mask = $preExp;
            $mask <<= $bitIdx;
            $longData -= $mask;

            $mask = ($preExp + $rewardCount);
            if ( $mask > 255 )
                $mask = 255;

            if ( $preExp < 10 && $mask >= 10 )
                $data["newCharac".$idx] = 1;

            $data["characId".$idx] = $rewardType;
            $data["characExp".$idx] = $mask;

            $mask <<= $bitIdx;
            $resultData = ($longData | $mask);

            $query = sprintf("update frdCharacEvolveExps set exps='%s' where userId='%s' and bundleIdx=%d", $resultData, $id, $bundleIdx);
            $isGood = $db->query($query);
            if ($isGood == false) {
              echo 0;
              $db->query("rollback");
              $db->close();
              return;
            }
            $data["characBundleIdx".$idx] = $bundleIdx;
            $data["characExps".$idx] = $resultData;
        }
        else {                      //no Bundle
            $mask = $rewardCount;
            if ( $mask >= 10 )
              $data["newCharac".$idx] = 1;

            $resultData = $mask << (8*($rewardType&7));
            $query = sprintf("insert into frdCharacEvolveExps values ('%s', %d, '%s')", $id, $bundleIdx, $resultData);
            $isGood = $db->query($query);
            if ($isGood == false) {
              echo 0;
              $db->query("rollback");
              $db->close();
              return;
            }
            $data["characId".$idx] = $rewardType;
            $data["characExp".$idx] = $mask;
            $data["characBundleIdx".$idx] = $bundleIdx;
            $data["characExps".$idx] = $resultData;
        }
        $sres->close();

      }

      else if ( $rewardType < 2000 ) {    // 보구 지급
        if ( $rewardType > 1500 ) {
          $redis = openRedis();
          if ( $redis == false ) {
            echo 0;
            $db->query("rollback");
            $db->close();
            $redis->close();
            return;
          }

          $minWIdx = $redis->lindex('weaponMinIdx', $rewardType-1500);
          $maxWIdx = $redis->lindex('weaponMaxIdx', $rewardType-1500);
          $rewardType = rand($minWIdx, $maxWIdx)+1000;
          $redis->close();
        }
        $val = $rewardType - 1000;

        //보구 지급
        $query = sprintf("select weaponIdPointer from frdUserData where privateId = '%s'", $id);
        $sres = $db->query($query);
        if ($sres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        if ( $sres->num_rows > 0) {
          $row = $sres->fetch_assoc();

          $stat = rand(0,10000);
          $query = sprintf("insert into frdHavingWeapons values (%d, '%s', %d, 0, %d)", $row["weaponIdPointer"], $id, $val, $stat);
          $isGood = $db->query($query);
          if ($isGood == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }

          $data["weaponIdPointer".$idx] = $row["weaponIdPointer"];
          $data["weaponId".$idx] = $val;
          $data["stat".$idx] = $stat;

          $query = sprintf("update frdUserData set weaponIdPointer=%d where privateId='%s'", $row["weaponIdPointer"]+1, $id);
          $isGood = $db->query($query);
          if ($isGood == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }
          $sres->close();
        }

      }



      else if ( $rewardType == 5000 ) {   //골드 지급
        $addedGold = $rewardCount;

        $query = sprintf("update frdUserData set gold=gold+%d where privateId='%s'", $addedGold, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $data["addedGold".$idx] = $addedGold;
      }

      else if ( $rewardType == 5001 ) {   //경험치 지급
        $addedExp = $rewardCount;
        $query = sprintf("update frdUserData set exp=exp+%d where privateId='%s'", $addedExp, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $data["addedExp".$idx] = $addedExp;
      }

      else if ( $rewardType == 5002 ) {   //티켓 지급
        $addedTicket = $rewardCount;
        $query = sprintf("update frdUserData set ticket=ticket+%d where privateId='%s'", $addedTicket, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $data["addedTicket".$idx] = $addedTicket;
      }

      else if ( $rewardType == 5003 ) {    // heart
        $addedHeart = $rewardCount;
        $query = sprintf("select exp, heart, startTime, givedHeartCount from frdUserData where privateId = '%s'", $id);
        $eres = $db->query($query);
        if ($eres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        if ($eres->num_rows > 0) {
          $erow = $eres->fetch_assoc();

          $redis = openRedis();
          if ( $redis == false ) {
            $db->query("rollback");
            $db->close();
            echo 0;
            return;
          }
          $addHeartCount = 0;
          $addTotalHeartCount = getAddHeartCount($db, $redis, $id, $erow["exp"], $erow["startTime"], $erow["givedHeartCount"], $erow["heart"], $addHeartCount);
          if ( $addTotalHeartCount < 0 ) {
            echo 0;$db->query("rollback");$db->close();
            return;
          }
  
          $redis->close();
        }
        $eres->close();
        $addedHeart += $addHeartCount;
        $query = sprintf("update frdUserData set heart=heart+%d, givedHeartCount=%d where privateId='%s'",
          $addedHeart, $addTotalHeartCount, $id);

        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $data["addedHeart".$idx] = $addedHeart;
      }

      else if ( $rewardType == 5004 ) {    // jewel
        $addedJewel = $rewardCount;
        $query = sprintf("update frdUserData set jewel=jewel+%d where privateId='%s'", $addedJewel, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $data["addedJewel".$idx] = $addedJewel;
      }

      else if ( $rewardType == 5005 ) {   //메달 지급
        $addedMedal = $rewardCount;
        $query = sprintf("update frdUserData set skillPoint=skillPoint+%d where privateId='%s'", $addedMedal, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $query = sprintf("select accuSkillPoint from frdSkillPoints where userId=%d", $id);
        $isGood = $db->query($query);
        if ( $isGood->num_rows <= 0 )
          $query = "insert into frdSkillPoints values ($id, $addedMedal)";
        else
          $query = sprintf("update frdSkillPoints set accuSkillPoint=accuSkillPoint+%d where userId=%d", $addedMedal, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return 0;
        }
        $data["addedMedal".$idx] = $addedMedal;
      }

      else if ( $rewardType == 5006 ) {   //마법가루 지급
        $addedPowder = $rewardCount;
        $query = sprintf("update frdUserData set powder=powder+%d where privateId='%s'", $addedPowder, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $data["addedPowder".$idx] = $addedPowder;
      }

      // 5006 powder

      else if ( $rewardType >= 10000 && $rewardType < 10510 ) {  //마법 지급
        if ( $rewardType > 10500 ) {
          $redis = openRedis();
          if ( $redis == false ) {
            echo 0;
            $db->query("rollback");
            $db->close();
            $redis->close();
            return;
          }

          $minWIdx = $redis->lindex('magicMinIdx1', $rewardType-10500);
          $maxWIdx = $redis->lindex('magicMaxIdx1', $rewardType-10500);
          $rewardType = rand($minWIdx, $maxWIdx)+10000;
          $redis->close();
        }

        $resultId = $rewardType-10000;


        $query = sprintf("select artifactIdPointer from frdUserData where privateId = '%s'", $id);
        $sres = $db->query($query);
        if ($sres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        if ( $sres->num_rows > 0) {
          $row = $sres->fetch_assoc();

          $query = sprintf("insert into frdHavingArtifacts values (%d, '%s', %d, 0, 0)", $row["artifactIdPointer"], $id, $resultId);
          $isGood = $db->query($query);
          if ($isGood == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }

          $data["artifactIdPointer".$idx] = $row["artifactIdPointer"];
          $data["artifactId".$idx] = $resultId;

          $query = sprintf("update frdUserData set artifactIdPointer=%d where privateId='%s'", $row["artifactIdPointer"]+1, $id);
          $isGood = $db->query($query);
          if ($isGood == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }
          $sres->close();
        }

      }

      else if ( $rewardType >= 100000 && $rewardType < 200000 ) {

        $query = sprintf("select itemCount from frdHavingItems where userId = %d and itemId = %d", $id, $rewardType);
        $ssres = $db->query($query);
        if ($ssres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return 0;
        }

        if ( $ssres->num_rows > 0 ) {
          $ssrow = $ssres->fetch_assoc();
          $rewardCount = $ssrow["itemCount"] + $rewardCount;
          $query = sprintf("update frdHavingItems set itemCount=%d where userId=%d and itemId=%d", $rewardCount, $id, $rewardType);
        }
        else {
          $query = sprintf("insert into frdHavingItems values (%d, %d, %d)", $id, $rewardType, $rewardCount);
        }

        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return 0;
        }

        $data["itemId".$idx] = $rewardType;
        $data["itemCount".$idx] = $rewardCount;

      }



      if ( ($pId !== -1) && ($pId !== "-1") ) {
        $query = sprintf("delete from frdUserPost where recvUserId = '%s' and pId = '%s'", $id, $pId);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
      }
    }
    if ( ($pId == -1) || ($pId == '-1') ) {
      $query = sprintf("delete from frdUserPost where recvUserId = '%s'", $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
    }

  }
  $res->close();
  $db->query("commit");
  $db->close();

  echo json_encode($data);
?>
