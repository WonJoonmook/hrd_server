<?php
  // 보유중인 우편 데이터 요청.
  include_once("../myAes.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];
 
  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");

  $query = sprintf("select session from frdUserData where privateId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;
      $query = sprintf("update frdUserData set session=%d where privateId='%s'", $newSession, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      $res->close();

      $query = sprintf("select * from frdUserPost where recvUserId = '%s'", $id);
      $res = $db->query($query);
      if ($res == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      $data["post"] = array();

      if ($res->num_rows > 0) {
        for ($idx = 0; $idx < $res->num_rows; $idx++) {
          $res->data_seek($idx);
          $row = $res->fetch_assoc();
          $post = array();
          $post["pId"] = $row["pId"];
          $post["sendUserId"] = $row["sendUserId"];
          $post["type"] = (int)$row["type"];
          $post["count"] = (int)$row["count"];
          $post["time"] = 604800 - (time() - (int)$row["time"]);    //7일 = 604,800초

          if ( $post["time"] <= 0 ) {   
            $query = sprintf("delete from frdUserPost where recvUserId='%s' and pId = '%s'", $id, $post["pId"]);
            $isGood = $db->query($query);
            if ($isGood == false) {
              echo 0;
              $db->query("rollback");
              $db->close();
              return;
            }
          }
          else {
            $data["post"][] = $post;
          }
         
        }
      }
      
    }
    else {
      addBlacklist($id, "post_recv_session");
      echo 1;
      $db->close();
      return;
    }
  }

  $res->close();
  $db->query("commit");
  $db->close();
  
  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);

?>