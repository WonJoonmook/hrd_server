<?php
  include_once("./myAes.php");
  include_once("./EventAccuWeaponMaxUpgrade.php");

  $id = $_REQUEST["id"];
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }

  echo "use result >> " . AccuWeaponMaxUpgrade($db, $id);

  $db->commit();
  $db->close();
?>
