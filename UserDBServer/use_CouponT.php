<?php
  // 공개쿠폰, 사전등록 쿠폰 사용 요청입니다.
  include_once("./myAes.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];
  $coupon = $_REQUEST["coupon"];


  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }

  $db->query("set autocommit=0");
  $query = sprintf("select session from frdUserData where privateId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $session;

      // $query = sprintf("update frdUserData set session=%d where privateId='%s'", $newSession, $id);
      // $isGood = $db->query($query);
      // if ($isGood == false) {
      //   echo 0;
      //   $db->query("rollback");
      //   $db->close();
      //   return;
      // }

      //146 = 4, 77 = 3
      $loginDb = new mysqli("172.27.0.77", "root", "eoqkrskwk12", "ddookdak", 3306);
      if (mysqli_connect_errno()) {
        echo 0;
        return;
      }


      $query = sprintf("select * from frdCoupon where id = '%s'", $coupon);
      $sres = $loginDb->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      if ($sres->num_rows <= 0) {
        $data["error"] = 1;           //없어
      }
      else {
        $row = $sres->fetch_assoc();
        $isGet = $row["isGet"];
        if ( $isGet > 0)
          $data["error"] = 2;         //썼어
        else {

          $couponType = (int)$row["type"];
          if ( $couponType >= 100 ) 
            $query = sprintf("select type from frdUsedPublicCoupon where type=%d and id = '%s'", $couponType, $id);
          else
            $query = sprintf("select isGet from frdCoupon where getId = '%s' and type=%d", $id, $couponType);

          $ssres = $loginDb->query($query);
          if ($ssres == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }

          $d = (int)date('d');
          $h = (int)date('H');
          if ($ssres->num_rows > 0 ) {
            $data["error"] = 2;           //한번만 사용 가능해
          }
          else {
            $fp = fopen("/home/www/couponLog/".date("md").".txt", 'a');
            if ( $fp == false ) {
              echo 0;
              $db->query("rollback");
              $db->close();
              return;
            }

            $onlyDayTime = time();//mktime(0,0,0, date('m'), date('d')-6, date('y'));
            $query = "";
            switch($couponType) {
              case 0:   // top ten
                $query = "insert into frdUserPost values ";
                $query.= sprintf("(0, '%s', 4, 111, 10, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5004, 100, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5002, 10, %d)", $id, time());
                break;
              case 1:   // mobi
                $query = "insert into frdUserPost values ";
                $query.= sprintf("(0, '%s', 4, 105, 10, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5004, 100, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5002, 10, %d)", $id, time());
                break;
              case 2:   // mobi special
                $query = "insert into frdUserPost values ";
                $query.= sprintf("(0, '%s', 4, 126, 10, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 120, 10, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 115, 10, %d), ", $id, time());

                $query.= sprintf("(0, '%s', 4, 5004, 100, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5003, 100, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5005, 20, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5002, 10, %d)", $id, time());
                break;
                case 3:   //normal
                $query = "insert into frdUserPost values ";
                $query.= sprintf("(0, '%s', 4, 5000, 2000, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5003, 30, %d)", $id, time());
                break;

                case 4:   //day1
                $query = "insert into frdUserPost values ";
                $query.= sprintf("(0, '%s', 4, 5000, 2000, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5002, 5, %d)", $id, time());
                break;

                case 5:   //day2
                $query = "insert into frdUserPost values ";
                $query.= sprintf("(0, '%s', 4, 122, 10, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5000, 3000, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5002, 7, %d)", $id, time());
                break;

                case 6:   //day3
                $query = "insert into frdUserPost values ";
                $query.= sprintf("(0, '%s', 4, 208, 10, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5004, 30, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5002, 7, %d)", $id, time());
                break;

                case 7:   //plus
                $query = "insert into frdUserPost values ";
                $query.= sprintf("(0, '%s', 4, 301, 10, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5000, 6000, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5004, 70, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5002, 10, %d)", $id, time());
                break;

                case 8:   //unique
                $query = "insert into frdUserPost values ";
                $query.= sprintf("(0, '%s', 4, 1504, 1, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5004, 30, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5005, 30, %d)", $id, time());
                break;

                case 10:case 11:   //모비 대규모 , 게임버프?? 대규모
                $query = "insert into frdUserPost values ";
                $query.= sprintf("(0, '%s', 4, 505, 10, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 504, 10, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 504, 10, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 504, 10, %d), ", $id, time());

                $query.= sprintf("(0, '%s', 4, 5004, 200, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5000, 20000, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5006, 500, %d)", $id, time());
                break;

              case 12:case 13:case 14:  //NHN S, A, C
                $query = sprintf("select isGet from frdCoupon where (getId = %d and (type>=12 and type<=14))", $id);
                $sssres = $loginDb->query($query);
                if ($sssres == false) {
                  echo $mysqli_error($loginDb);
                  $db->query("rollback");
                  $db->close();
                  return;
                }
                if ( $sssres->num_rows > 0 ) {
                  $data["error"] = 2;
                  $query = "";
                  break;
                }
                switch($couponType) {
                  case 12:
                  $query = "insert into frdUserPost values ";
                  $query.= sprintf("(0, '%s', 4, 5004, 200, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 5000, 40000, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 5006, 500, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 506, 10, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 505, 10, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 505, 10, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 504, 10, %d)", $id, time());
                  break;
                  case 13:
                  $query = "insert into frdUserPost values ";
                  $query.= sprintf("(0, '%s', 4, 5004, 100, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 5000, 20000, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 5006, 300, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 505, 10, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 504, 10, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 504, 10, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 504, 10, %d)", $id, time());
                  break;
                  case 14:
                  $query = "insert into frdUserPost values ";
                  $query.= sprintf("(0, '%s', 4, 5004, 50, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 100003, 2, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 504, 10, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 504, 10, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 504, 10, %d)", $id, time());
                  break;
                  default:
                  break;
                }
                break;

              case 15:  //NHN Quest Coupon
                $query = "insert into frdUserPost values ";
                $query.= sprintf("(0, '%s', 4, 5004, 100, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5000, 20000, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 5006, 300, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 505, 10, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 504, 10, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 504, 10, %d), ", $id, time());
                $query.= sprintf("(0, '%s', 4, 504, 10, %d)", $id, time());
                break;

        //      case 100:   // facebook .1
        //        $query = sprintf("insert into frdUserPost values (0, '%s', 4, 5004, 10, %d)", $id, time());
        //        break;
        //      case 101:     // facebook .2
        //        $query = sprintf("insert into frdUserPost values (0, '%s', 4, 5004, 30, %d)", $id, time());
        //        break;
        //        case 102:     // chool check
        //          $query = sprintf("insert into frdUserPost values (0, '%s', 4, 5004, 20, %d)", $id, time());
        //          break;
                case 103:     // for Present 20,000
                  $query = sprintf("insert into frdUserPost values (0, '%s', 4, 5004, 200, %d)", $id, time());
                  break;
                  /*
                  case 104:     // for Present 20,000
                  $query = sprintf("insert into frdUserPost values (0, '%s', 4, 5004, 10, %d)", $id, time());
                  break;
                  case 105:     // for Present 20,000
                  $query = sprintf("insert into frdUserPost values (0, '%s', 4, 5004, 15, %d)", $id, time());
                  break;
                  case 106:     // for Present 20,000
                  $query = sprintf("insert into frdUserPost values (0, '%s', 4, 5004, 25, %d)", $id, time());
                  break;
                  
                  case 107:     // facebook event like 2500 -> jewel 10, random grade 4 magic
                  $query = "insert into frdUserPost values ";
                  $query.= sprintf("(0, '%s', 4, 10504, 1, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 5004, 10, %d)", $id, time());
                  break;
                  case 108:     // facebook event like 2500 -> jewel 10, random grade 4 magic
                  $query = "insert into frdUserPost values ";
                  $query.= sprintf("(0, '%s', 4, 1504, 1, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 5004, 30, %d)", $id, time());
                  break; 
                  case 109:     // facebook event like 2500 -> jewel 10, random grade 4 magic
                  $query = "insert into frdUserPost values ";
                  $query.= sprintf("(0, '%s', 4, 10505, 1, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 5004, 70, %d)", $id, time());
                  break;
                  case 110:     // facebook event like 2500 -> jewel 10, random grade 4 magic
                  $query = "insert into frdUserPost values ";
                  $query.= sprintf("(0, '%s', 4, 1505, 1, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 5004, 120, %d)", $id, time());
                  break;
              
                  case 112:     // facebook event like 2500 -> jewel 10, random grade 4 magic
                  $query = "insert into frdUserPost values ";
                  $query.= sprintf("(0, '%s', 4, 5002, 5, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 5004, 30, %d)", $id, time());
                  break;
                  case 113:     // facebook event like 2500 -> jewel 10, random grade 4 magic
                  $query = "insert into frdUserPost values ";
                  $query.= sprintf("(0, '%s', 4, 114002, 10, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 100003, 1, %d)", $id, time());
                  break;
                  case 114:     // facebook event like 2500 -> jewel 10, random grade 4 magic
                  $query = "insert into frdUserPost values ";
                  $query.= sprintf("(0, '%s', 4, 100001, 1, %d)", $id, time());
                  break;
                  case 115:     // facebook event like 2500 -> jewel 10, random grade 4 magic
                  $query = "insert into frdUserPost values ";
                  $query.= sprintf("(0, '%s', 4, 505, 10, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 5003, 50, %d)", $id, time());
                  break;
                  case 116:     // facebook event like 2500 -> jewel 10, random grade 4 magic
                  $query = "insert into frdUserPost values ";
                  $query.= sprintf("(0, '%s', 4, 104000, 1, %d), ", $id, time());
                  $query.= sprintf("(0, '%s', 4, 5004, 20, %d)", $id, time());
                  break;
                  case 117:     // facebook event like 2500 -> jewel 10, random grade 4 magic
                  $query = "insert into frdUserPost values ";
                  $query.= sprintf("(0, '%s', 4, 100000, 1, %d)", $id, time());
                  break;
              */
                  case 125:     // 9_18 event -> 황금상자뽑기권 2개
                  if ( !(($d == 18 && $h >= 15) || ($d == 19 && $h < 15)) )
                    $data["error"] = 4;
                  else {
                    $query = "insert into frdUserPost values ";
                    $query.= sprintf("(0, '%s', 4, 5000, 1000, %d),", $id, $onlyDayTime);
                    $query.= sprintf("(0, '%s', 4, 5006, 100, %d)", $id, $onlyDayTime);
                  }
                  break;
                  case 126:     // 9_18 event -> 특성초기화권 1개
                  if ( !(($d == 19 && $h >= 15) || ($d == 20 && $h < 15)) )
                    $data["error"] = 4;
                  else {
                    $query = "insert into frdUserPost values ";
                    $query.= sprintf("(0, '%s', 4, 100003, 1, %d),", $id, $onlyDayTime);
                    $query.= sprintf("(0, '%s', 4, 5003, 40, %d)", $id, $onlyDayTime);
                  }
                  break;
                  case 127:     // 9_18 event -> 랜덤5성영웅 영혼석30개
                  if ( !(($d == 20 && $h >= 15) || ($d == 21 && $h < 15)) )
                    $data["error"] = 4;
                  else {
                    $query = "insert into frdUserPost values ";
                    $query.= sprintf("(0, '%s', 4, 5003, 60, %d),", $id, $onlyDayTime);
                    $query.= sprintf("(0, '%s', 4, 5002, 5, %d)", $id, $onlyDayTime);
                  }
                  break;
                  case 128:     // 9_18 event -> 티켓 10개
                  if ( !(($d == 21 && $h >= 15) || ($d == 22 && $h < 15)) )
                    $data["error"] = 4;
                  else {
                    $query = "insert into frdUserPost values ";
                    $query.= sprintf("(0, '%s', 4, 100001, 1, %d),", $id, $onlyDayTime);
                    $query.= sprintf("(0, '%s', 4, 10503, 1, %d)", $id, $onlyDayTime);
                  }
                  break;
                  case 129:     // 9_18 event -> 무지개룬 3개
                  if ( !(($d == 22 && $h >= 15) || ($d == 23 && $h < 15)) )
                    $data["error"] = 4;
                  else {
                    $query = "insert into frdUserPost values ";
                    $query.= sprintf("(0, '%s', 4, 5004, 50, %d),", $id, $onlyDayTime);
                    $query.= sprintf("(0, '%s', 4, 100003, 1, %d)", $id, $onlyDayTime);
                  }
                  break;
                  case 130:     // 9_18 event -> 각종 4성룬 10개
                  if ( !(($d == 23 && $h >= 15) || ($d == 24 && $h < 15)) )
                    $data["error"] = 4;
                  else {
                    $query = "insert into frdUserPost values ";
                    $query.= sprintf("(0, '%s', 4, 100000, 1, %d),", $id, $onlyDayTime);
                    $query.= sprintf("(0, '%s', 4, 100002, 1, %d)", $id, $onlyDayTime);
                  }
                  break;
                  case 131:     // 9_18 event -> 유물옵션변경권 1개
                  if ( !(($d == 24 && $h >= 15) || ($d == 25 && $h < 15)) )
                    $data["error"] = 4;
                  else {
                    $query = "insert into frdUserPost values ";
                    $query.= sprintf("(0, '%s', 4, 114000, 5, %d),", $id, $onlyDayTime);
                    $query.= sprintf("(0, '%s', 4, 114001, 5, %d),", $id, $onlyDayTime);
                    $query.= sprintf("(0, '%s', 4, 114002, 5, %d),", $id, $onlyDayTime);
                    $query.= sprintf("(0, '%s', 4, 114003, 5, %d),", $id, $onlyDayTime);
                    $query.= sprintf("(0, '%s', 4, 114004, 5, %d),", $id, $onlyDayTime);
                    $query.= sprintf("(0, '%s', 4, 110000, 1, %d)", $id, $onlyDayTime);
                  }
                  break;
              default:
                $query = "";
                $data["error"] = 4;
                break;
            }
            
            if ( $query !== "" ) {
              $isGood = $db->query($query);
              if ($isGood == false) {
                echo 0;
                $db->query("rollback");
                $db->close();
                return;
              }

              fwrite($fp, $query."\n");
            }

            if ( (int)$data["error"] == 0 ) {
              if ( $couponType >= 100 )
                $query = sprintf("insert into frdUsedPublicCoupon values (%d, '%s')", $couponType, $id);
              else
                $query = sprintf("update frdCoupon set isGet=1, getId='%s' where id='%s'", $id, $coupon);
              
              $isGood = $loginDb->query($query);
              if ($isGood == false) {
                echo 0;
                $db->query("rollback");
                $db->close();
                return;
              }
              fwrite($fp, $query."\n");
            }

            fclose($fp);
          }
        }
      }
      $sres->close();

    }
    else {
      addBlacklist($id, "use_coupon");
      echo 1;
      $db->close();
      return;
    }
  }
  $res->close();
  $db->query("commit");
  $db->close();

  $keyAndIv = formatTo16String($session);

  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
