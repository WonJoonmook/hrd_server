<?php

  include_once("../myAes.php");

  $db = getDB();
  if (mysqli_connect_errno()) {
    echo "db connect fail";
    return;
  }

  $db->query("set autocommit=0");


  $now = time();
  $date = date("H:i:s", $now);
  $todayTime = explode(":", $date);
  $todayTimeSpan = $todayTime[0] * 3600 + $todayTime[1] * 60 + $todayTime[2];
  $today = $now - $todayTimeSpan;
  $fourDaysBefore = $today - 4 * 86400;
  $checkingDate = date("ymdHis", $fourDaysBefore);
  # check session
  $query = "select id, purchaseTime from frdLogBuy where productId = 'buy_monthly_card0' AND purchaseTime > $checkingDate";
  $res = $db->query($query);
  if ($res == false) {
    echo "select frdLogBuy query fail!";
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows == 0) {
    echo $db->error;
    echo "<br/>";
    echo "No person find!";
    $db->query("rollback");
    $db->close();
    return;
  }

  for ($idx = 0; $idx < $res->num_rows; $idx++) {
    $res->data_seek($idx);
    $row = $res->fetch_assoc();

    $userId = $row["id"];
    $purchaseTime = $row["purchaseTime"];
    $query = "select * from frdMonthlyCard where userId = $userId";

    $findRes = $db->query($query);
    if ($findRes == false) {
      echo $db->error;
      echo "<br/>";
      echo "select frdMonthlyCard query fail! index = $idx, userId = $userId";
      $db->query("rollback");
      $db->close();
      return;
    }

    $purchaseTime = intval($purchaseTime / 1000000);
    $purchaseTime += 20000000;
    $purchaseYear = intval($purchaseTime / 10000);
    $purchaseMonth = intval(($purchaseTime % 10000) / 100);
    $purchaseDay = intval($purchaseTime % 100);

    $startDate = $purchaseYear . "/". $purchaseMonth . "/" . $purchaseDay;
    $startTime = (int)(strtotime($startDate));

    $now = time();
    $gapTime = $now - $startTime;
    $gapDay = ceil($gapTime / 86400);

    $errored = false;

    if ($findRes->num_rows > 0) {
      $findRow = $findRes->fetch_assoc();
      $dbStartTime = $findRow["startTime"];

      if ((int)$findRow["startTime"] !== $startTime) {
        // echo "buy date : $startDate, ";
        $query = "update frdMonthlyCard set startTime = $startTime, lastRecvDay = $gapDay, duration = 30 where userId = $userId";

        $updateRes = $db->query($query);
        if ($updateRes == false) {
          echo $db->error;
          echo "<br/>";
          echo "update frdMonthlyCard query fail! index = $idx, userId = $userId";
          $db->query("rollback");
          $db->close();
          return;
        }

        $errored = true;
      }
    }
    else {
      $query = "insert into frdMonthlyCard (userId, startTime, lastRecvDay, duration) value ($userId, $startTime, $gapDay, 30)";

      $insertRes = $db->query($query);
      if ($insertRes == false) {
        echo $db->error;
        echo "<br/>";
        echo "insert frdMonthlyCard query fail! index = $idx, userId = $userId";
        $db->query("rollback");
        $db->close();
        return;
      }

      $errored = true;
    }

    if ($errored) {
      $query = "select name from frdUserData where privateId = $userId";
      $selectUserNameRes = $db->query($query);
      if ($selectUserNameRes == false) {
        echo $db->error;
        echo "<br/>";
        echo "select user name fail! index = $idx, userId = $userId";
        $db->query("rollback");
        $db->close();
        return;
      }

      if ($selectUserNameRes->num_rows > 0) {
        $selectUserNameRow = $selectUserNameRes->fetch_assoc();
        $userName = iconv('utf8', 'euckr', $selectUserNameRow["name"]);
        echo "$userId : $userName";
        echo "<br/>";

      	$query = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) values ";
        for ($postIdx = 0; $postIdx < $gapDay; $postIdx++) {
          $postEndTime = $startTime + 86400 * $postIdx;
          $postSendId =  101 + $postIdx;
          $query .= "($userId, $postSendId, 5004, 20, $postEndTime), ";  // jewel
          if ($postIdx == $gapDay - 1)
            $query .= "($userId, $postSendId, 5003, 20, $postEndTime)"; // heart
          else
            $query .= "($userId, $postSendId, 5003, 20, $postEndTime), "; // heart
        }

        $sendPostRes = $db->query($query);
        if ($sendPostRes == false) {
          echo "Send reward fail! index = $idx, userId = $userId";
          $db->query("rollback");
          $db->close();
          return;
        }
      }
      else {
        echo "no user found! index = $idx, userId = $userId";
        echo "<br/>";
      }
    }
  }

  $db->query("commit");
  $db->close();
?>
