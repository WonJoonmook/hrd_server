<?php

  include_once("../myAes.php");

  $session = $_REQUEST["session"];
  $userId = $_REQUEST["userId"];


  function makeResultMsg($db, $session, $newSession, $userId, $hasCard, $data) {

    $data["hasCard"] = $hasCard;
    $data["session"] = intval($newSession);

    $query = "update frdUserData set session = $newSession where privateId = $userId";
    $res = $db->query($query);
    if ($res == false) {
      $db->query("rollback");
      $db->close();
      return 0;
    }

    $db->query("commit");
    $db->close();

    $keyAndIv = formatTo16String($session);
    return encrypt( $keyAndIv, json_encode($data), $keyAndIv);
  }


  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }

  $db->query("set autocommit=0");

  # check session
  $query = "select session from frdUserData where privateId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;
    }
    else {
      // addBlacklist($id, "buy_chest_session");
      echo 1;
      $db->query("rollback");
      $db->close();
      return;
    }
  }

  $query = "select * from frdMonthlyCard where userId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows == 0) {
    $hasCard = false;
    echo makeResultMsg($db, $session, $newSession, $userId, $hasCard, $data);
    return;
  }

  $row = $res->fetch_assoc();
  $startTime = $row["startTime"];
  $lastRecvDay = $row["lastRecvDay"];
  $duration = $row["duration"];

  $now = time();

  $gap = $now - $startTime;
  $gapDay = ceil($gap / 86400);

  if ($gapDay > $duration) {
    $hasCard = false;
    echo makeResultMsg($db, $session, $newSession, $userId, $hasCard, $data);
    return;
  }

  if ($gapDay > $lastRecvDay) {
    #send present
    $postEndTime = $startTime + 86400 * ($gapDay);
    $postSendId = $gapDay + 100;

  	$query = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) ";
    $query .= " values ($userId, $postSendId, 5004, 20, $postEndTime), ";  // jewel
    $query .= "($userId, $postSendId, 5003, 20, $postEndTime)"; // heart

    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    #update table
    $query = "update frdMonthlyCard set lastRecvDay = $gapDay where userId = $userId";

    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    $data["GiftCount"] = 2;
  }

  $hasCard = $gapDay < $duration;
  echo makeResultMsg($db, $session, $newSession, $userId, $hasCard, $data);
?>
