<?php

  include_once("../myAes.php");

  $session = $_REQUEST["session"];
  $userId = $_REQUEST["userId"];
  $duration = $_REQUEST["duration"];

  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }

  $db->query("set autocommit=0");

  # check session
  $query = "select session from frdUserData where privateId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;
    }
    else {
      // addBlacklist($id, "buy_chest_session");
      echo 1;
      $db->query("rollback");
      $db->close();
      return;
    }
  }

  $now = time();
  $date = date("H:i:s", $now);
  $todayTime = explode(":", $date);
  $todayTimeSpan = $todayTime[0] * 3600 + $todayTime[1] * 60 + $todayTime[2];
  $today = $now - $todayTimeSpan;

  $query = "insert into frdMonthlyCard (userId, startTime, lastRecvDay, duration) values ($userId, $today, 0, $duration) on duplicate key update startTime = $today, lastRecvDay = 0, duration = $duration";

  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  # update session
  $query = "update frdUserData set session = $newSession where privateId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $db->query("commit");
	$db->close();

  $data["session"] = $newSession;
  $data["hasCard"] = true;
  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
