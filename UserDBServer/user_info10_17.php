<?php
  // 게임 시작 시 본인 데이터를 요청합니다.

  include_once("./myAes.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];


  $hashcodeStr = $_REQUEST["hashcodeStr"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
    echo $query.", ".mysqli_error($db)." << \n";
    echo 0;
    return;
  }


  if ( $hashcodeStr !== "1C85B667" ) {
  //  echo $hashcodeStr."<br>";
    $data["error"] = 2;
    $keyAndIv = formatTo16String($session);
    echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
    $db->close();
    return;
  }

  $redis = openRedis();
  if ( $redis == false ) {
    echo mysqli_error($db)." << \n";
    $db->close();
    return;
  }

  $db->query("set autocommit=0");
  $time = time();
  $query = sprintf("select * from frdUserData where privateId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo $query.", ".mysqli_error($db)." << \n";
    $db->query("rollback");
    $db->close();
    return;
  }
  if ($res->num_rows > 0) {
    $query = sprintf("select * from frdBlacklist where id = '%s'", $id);
    $bres = $db->query($query);
    if ($bres == false) {
      echo $query.", ".mysqli_error($db)." << \n";
      $db->query("rollback");
      $db->close();
      return;
    }
    if ($bres->num_rows > 0) {
      $brow = $bres->fetch_assoc();

      if ( $brow["count"] >= 1000 ) {
        $data["error"] = 2;
        $keyAndIv = formatTo16String($session);
        echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
        $db->close();
        $redis->close();
        return;
      }

    }
    $bres->close();

    $row = $res->fetch_assoc();



    $rankRedis = openRankRedis($redis);
    if ( $rankRedis ) {
      $data["myRank"] = $rankRedis->zrevrank('climbRank_New', $row["name"]);
      $data["userCount"] = $rankRedis->zcard('climbRank_New');
      if ( !$data["myRank"] )
        $data["myRank"] = 0;

      $rankRedis->close();
    }
    else {
      $data["myRank"] = 0;
      $data["userCount"] = 0;
    }

    $query = sprintf("select * from frdClimbTopData where userId = '%s'", $id);
    $sres = $db->query($query);
    if ($sres == false) {
      echo $query.", ".mysqli_error($db)." << \n";
      $db->query("rollback");
      $db->close();
      return;
    }

    if ($sres->num_rows > 0) {
      $srow = $sres->fetch_assoc();
      $data["bestRank"] = $srow["bestRank"];
      $data["bestTotalUserCount"] = $srow["bestTotalUserCount"];
      $data["playCount"] = $srow["playCount"];
      $data["averageRank"] = $srow["averageRank"];
      $data["avrgTotalUserCount"] = $srow["avrgTotalUserCount"];
      $data["lastRank"] = $srow["lastRank"];
      $data["lastTotalUserCount"] = $srow["lastTotalUserCount"];
    }
    else {
      $data["bestRank"] = 0;
      $data["bestTotalUserCount"] = 0;
      $data["playCount"] = 0;
      $data["averageRank"] = 0;
      $data["avrgTotalUserCount"] = 0;
      $data["lastRank"] = 0;
      $data["lastTotalUserCount"] = 0;
    }

    $addHeartCount=0;
    $addTotalHeartCount = getAddHeartCount($db, $redis, $id, $row["exp"], $row["startTime"], $row["givedHeartCount"], $row["heart"], $addHeartCount);
    if ( $addTotalHeartCount < 0 ) {
      echo 0;$db->query("rollback");$db->close();
      return;
    }
  
    $isGood = $db->query(sprintf("update frdUserData set heart=heart+%d, givedHeartCount=%d, session=%d where privateId = '%s'",
      $addHeartCount, $addTotalHeartCount, $session, $id));
    if ($isGood == false) {

      echo $query.", ".mysqli_error($db)." << \n";
      $db->query("rollback");
      $db->close();
      return;
    }

    $data["privateId"] = $row["privateId"];
    $data["name"] = $row["name"];
    $data["tutoLevel"] = $row["tutoLevel"];
    $data["manaLevel"] = $row["manaLevel"];
    $data["exp"] = $row["exp"];
    $data["gold"] = $row["gold"];
    $data["jewel"] = $row["jewel"];
    $data["powder"] = $row["powder"];
    $data["stageLevel"] = $row["stageLevel"];
    $data["chapterReward"] = $row["chapterReward"];
    $data["skillPoint"] = $row["skillPoint"];
    $data["ticket"] = $row["ticket"];
    $data["heart"] = $row["heart"]+$addHeartCount;
    $data["artifactIdPointer"] = $row["artifactIdPointer"];
    $data["weaponIdPointer"] = $row["weaponIdPointer"];

    $data["everydayQuest"] = $row["everydayQuest"];
    $data["lastLoginDate"] = $row["lastLoginDate"];
    $data["startTime"] = $row["startTime"];
    $data["givedHeartCount"] = $addTotalHeartCount;
    $data["time"] = $time;

    $query = sprintf("select * from frdCharacEvolveExps where userId = %d", $id);
    $res = $db->query($query);
    if ($res == false) {

      echo $query.", ".mysqli_error($db)." << \n";
      $db->query("rollback");
      $db->close();
      return;
    }
    $data["characEvolveExps"] = array();
    if ($res->num_rows > 0) {
      for ($idx = 0; $idx < $res->num_rows; $idx++) {
        $res->data_seek($idx);
        $row = $res->fetch_assoc();
        $char = array();
        $char["bundleIdx"] = $row["bundleIdx"];
        $char["exps"] = $row["exps"];
        $data["characEvolveExps"][] = $char;
      }
    }



    $query = sprintf("select * from frdSkillLevels where userId = %d", $id);
    $res = $db->query($query);
    if ($res == false) {
      echo $query.", ".mysqli_error($db)." << \n";
      $db->query("rollback");
      $db->close();
      return;
    }
    $data["skillLevels"] = array();
    if ($res->num_rows > 0) {
      for ($idx = 0; $idx < $res->num_rows; $idx++) {
        $res->data_seek($idx);
        $row = $res->fetch_assoc();
        $char = array();
        $char["bundleIdx"] = $row["bundleIdx"];
        $char["levels"] = $row["levels"];
        $data["skillLevels"][] = $char;
      }
    }

    $query = sprintf("select * from frdHavingArtifacts where userId = %d", $id);
    $res = $db->query($query);
    if ($res == false) {
      echo $query.", ".mysqli_error($db)." << \n";
      $db->query("rollback");
      $db->close();
      return;
    }
    $data["artifacts"] = array();
    if ($res->num_rows > 0) {
      for ($idx = 0; $idx < $res->num_rows; $idx++) {
        $res->data_seek($idx);
        $row = $res->fetch_assoc();
        $char = array();
        $char["privateId"] = $row["privateId"];
        $char["itemId"] = $row["itemId"];
        $char["itemLevel"] = $row["itemLevel"];
        $char["itemExp"] = $row["itemExp"];
        $data["artifacts"][] = $char;
      }
    }

    $query = sprintf("select * from frdHavingWeapons where userId = %d", $id);
    $res = $db->query($query);
    if ($res == false) {
      echo $query.", ".mysqli_error($db)." << \n";
      $db->query("rollback");
      $db->close();
      return;
    }
    $data["weapons"] = array();
    if ($res->num_rows > 0) {
      for ($idx = 0; $idx < $res->num_rows; $idx++) {
        $res->data_seek($idx);
        $row = $res->fetch_assoc();
        $char = array();
        $char["privateId"] = $row["privateId"];
        $char["weaponId"] = $row["weaponId"];
        $char["exp"] = $row["exp"];
        $char["stat"] = $row["stat"];
        $data["weapons"][] = $char;
      }
    }

    $query = sprintf("select * from frdHavingItems where userId = %d", $id);
    $res = $db->query($query);
    if ($res == false) {
      echo $query.", ".mysqli_error($db)." << \n";
      $db->query("rollback");
      $db->close();
      return;
    }
    $data["items"] = array();
    if ($res->num_rows > 0) {
      for ($idx = 0; $idx < $res->num_rows; $idx++) {
        $res->data_seek($idx);
        $row = $res->fetch_assoc();
        $char = array();
        $char["itemId"] = $row["itemId"];
        $char["itemCount"] = $row["itemCount"];
        $data["items"][] = $char;
      }
    }


    $query = sprintf("select * from frdAbillity where userId=%d", $id);
    $res = $db->query($query);
    if ($res == false) {
      echo $query.", ".mysqli_error($db)." << \n";
      $db->query("rollback");
      $db->close();
      return;
    }

    if ( $res->num_rows > 0 ) {
      $row = $res->fetch_assoc();
      $data["abillity"] = array();
      $char = array();
      $char["openedSlot"] = $row["openedSlot"];
      $char["bundle0"] = $row["bundle0"];
      $char["bundle1"] = $row["bundle1"];
      $char["usingBundle0"] = $row["usingBundle0"];
      $char["usingBundle1"] = $row["usingBundle1"];
      $data["abillity"][] = $char;
    }

    $query = "select dailyCount from frdTempleClearCount where userId=$id";
    $res = $db->query($query);
    if ($res == false) {
      echo $query.", ".mysqli_error($db)." << \n";
      $db->query("rollback");
      $db->close();
      return;
    }
    if ( $res->num_rows <= 0 )
      $data["templeDCC"] = 0;
    else {
      $row = $res->fetch_assoc();
      $data["templeDCC"] = $row["dailyCount"];
    }

    

  }
  else {



  }
  $redis->close();
  $res->close();
  $db->query("commit");
  $db->close();




  $keyAndIv = formatTo16String($session);


  $data["isCollectToast"] = 1;
  try {
    $data = encrypt( $keyAndIv, json_encode($data), $keyAndIv );
  }
  catch(Exception $e) {
    $data = 0;
  }
  echo $data;
?>
