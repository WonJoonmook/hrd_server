<?php
include_once("../myAes.php");
$userId = $_REQUEST["userId"];
$db = getDB();
if (mysqli_connect_errno()) {
    echo 0;
    return;
}
$db->query("set autocommit=0");

# This Query is trash. But This Table is Very simple.
# We can used trash query.
# -- state column is Key. first condition for WEHRE.
# 0 : disable , 1: available
$query = "
SELECT
WebViewID,
    TIMESTAMPDIFF(SECOND, start_date, NOW()) as startTime,
    TIMESTAMPDIFF(SECOND, NOW(), end_date) as endTime
    FROM frdWebViewInfo
    WHERE
    state = 1 AND
    TIMESTAMPDIFF(SECOND, start_date, NOW()) > 0 AND
    TIMESTAMPDIFF(SECOND, NOW(), end_date) > 0
    ";

    $res = $db->query($query);
    if ($res == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
    }

$WebViewOpend = false;
if ($res->num_rows > 0) {
    $WebViewOpend = true;

    for ($idx = 0; $idx < $res->num_rows; $idx++) {
        $res->data_seek($idx);
        $row = $res->fetch_assoc();

        $startTime  = $row["startTime"];
        $endTime    = $row["endTime"];
    }
}

# send data
if ($WebViewOpend) {
    $data["WebViewOpend"] 	= $WebViewOpend;
    $data["startTime"] 		= $startTime;
    $data["endTime"] 		= $endTime;
} else {
    $data["WebViewOpend"] 	= $WebViewOpend;
    $data["startTime"] 		= null;
    $data["endTime"] 		= null;
}

$db->query("commit");
$db->close();

echo json_encode($data);
?>

