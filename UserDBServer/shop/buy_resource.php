<?php
  // 상점에서 상자, 패키지를 제외한 자원 구매 요청시 처리후 데이터를 뿌려줍니다.
  include_once("../myAes.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];
  $itemIdx = $_REQUEST["itemIdx"];
  $tapIdx = $_REQUEST["tapIdx"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");

  $getResourceName;
  switch($tapIdx) {
  case 1:
    $getResourceName = "gold";
    break;
  case 3:
    $getResourceName = "heart";
    break;
  case 4:
    $getResourceName = "ticket";
    break;
  default:
    break;
  }

  $query = sprintf("select %s, jewel, session from frdUserData where privateId = %d", $getResourceName, $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;

      $redis = openRedis();
      if ( $redis == false ) {
        echo 0;
        $db->close();
        return;
      }

      $itemIdx = ($tapIdx*5 + $itemIdx);

      $priceStr = $redis->lindex('shopItemPrice', $itemIdx);
      $priceData = explode(",", $priceStr);
      $needJewelAmount = $priceData[1];

      $resultJewel = $row["jewel"] - $needJewelAmount;
      if ( $resultJewel < 0) {
        addBlacklist($id, "hack_buy_".$getResourceName."_notEnoughJewel");
        echo 1;
        $db->close();
        $redis->close();
        return;
      }

      include_once("../EventAccuJewel.php");
      $resultCode = AccuUseJewel($db, $id, $needJewelAmount);
      if ($resultCode == 0) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      if ($tapIdx == 4 && $itemIdx % 5 == 0) {
        include_once("../GuerrillaShop/itemAdder.php");
        $data["inventoryItem"] = addItem($db, $redis, $id, 100000, 1, 0, 0);

        $query = sprintf("update frdUserData set jewel=%d, session=%d where privateId=%d", $resultJewel, $newSession, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
      }
      else {
        $resultResource = $row[$getResourceName] + $redis->lindex('shopItemCount1', $itemIdx);

        $query = sprintf("update frdUserData set %s=%d, jewel=%d, session=%d where privateId=%d", $getResourceName, $resultResource, $resultJewel, $newSession, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $data[$getResourceName] = $resultResource;
      }

      $data["jewel"] = $resultJewel;
      $redis->close();
    }
    else {
      addBlacklist($id, "buy_".$getResourceName."_session");
      echo 1;
      $db->close();
      return;
    }
  }
  $res->close();
  $db->query("commit");
  $db->close();

  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
