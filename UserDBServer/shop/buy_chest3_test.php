<?php

  function getRandomWeaponGrade($isEventChest, $chestCount) {
    $val = rand(0,999);
    if (!$isEventChest) {
      if ( $val < 600 ) return 2;
      if ( $val < 950 ) return 3;
      else return 4;
    }
    else {
      if ( $chestCount > 5 ) {
        if ( $val < 450 ) return 2;
        else if ( $val < 850 ) return 3;
        else if ( $val < 990 ) return 4;
        else return 5;
      }
      else {
        if ( $val < 420 ) return 2;
        else if ( $val < 800 ) return 3;
        else if ( $val < 950 ) return 4;
        else return 5;
      }
    }
  }

  function getRandomMagicGrade($isEventChest, $chestCount) {
    $val = rand(0,999);
    if (!$isEventChest) {
      if ( $val < 600 ) return 2;
      else if ( $val < 990 ) return 3;
      else return 4;
    }
    else {
      if ( $chestCount > 5 ) {
        if ( $val < 420 ) return 2;
        else if ( $val < 950 ) return 3;
        else if ( $val < 990 ) return 4;
        else return 5;
      }
      else {
        if ( $val < 400 ) return 2;
        else if ( $val < 950 ) return 3;
        else if ( $val < 990 ) return 4;
        else return 5;
      }
    }
  }

  // 상점에서 상자 구매 요청.
  // 데이터 처리 후 결과를 뿌려줍니다.
  include_once("../myAes.php");
  include_once("../GuerrillaShop/itemAdder2.php");

  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];
  $itemIdx = $_REQUEST["itemIdx"];
  $tapIdx = 2;

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");
  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    $db->close();
    return;
  }

  $shopData = json_decode($redis->get('shopDataForTest'));
  $shopData = $shopData->data;

  $now = time();
  $curDate = date("ymdHi", $now);
  $minSalePeriods = 2147483647;
  for ($i = 0; $i < count($shopData); $i++) {
    $goods = $shopData[$i]->goods;
    for ($j = 0; $j < count($goods); $j++) {
      if (property_exists($goods[$j], "salePeriod")) {
        $tempStr = $goods[$j]->salePeriod;
        $salePeriods = explode('-', $tempStr);
        $startDate = $salePeriods[0];
        $endDate = $salePeriods[1];
        if (($curDate < $startDate) || ($curDate >= $endDate)) {
          unset($shopData[$i]->goods[$j]);
        }
      }
    }
    $shopData[$i]->goods = array_values($shopData[$i]->goods);
  }

  $goodsData = $shopData[$tapIdx]->goods[$itemIdx];
  $priceType = $goodsData->costType;
  $price = $goodsData->cost;

  $isUseFreePass = false;
  // free premium chest pass check
  if ((int)$priceType == 1) {
    $query = "select itemCount from frdHavingItems where userId = $id AND itemId = 100003";
    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    if ($res->num_rows > 0) {
      $row = $res->fetch_assoc();
      $freePremiumChestPassCount = $row["itemCount"];
      $usePassCount = $price / 30;
      $isUseFreePass = $freePremiumChestPassCount - $usePassCount >= 0;

      if ($isUseFreePass) {
        $restPassCount = $freePremiumChestPassCount - $usePassCount;
        $data["freePremiumChestPassCount"] = $restPassCount;
        if ($restPassCount == 0) {
          $query = "delete from frdHavingItems where userId = $id AND itemId = 100003";
          $res = $db->query($query);
          if ($res == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }
        }
        else {
          $query = "update frdHavingItems set itemCount = $restPassCount where userId = $id AND itemId = 100003";
          $res = $db->query($query);
          if ($res == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }
        }
      }
    }
  }

  $itemTotalIdx = ($tapIdx*5 + $itemIdx);

  $chestCount = $goodsData->chestCount;

  if ( (int)$priceType == 0 )
    $getResourceName = "gold";
  else if ( (int)$priceType == 1 )
    $getResourceName = "jewel";
  else
    $getResourceName = "heart";

  $query = sprintf("select %s, weaponIdPointer, artifactIdPointer, session from frdUserData where privateId = '%s'",$getResourceName, $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;

      $havingDollor = $row[$getResourceName];
      if ( (int)$priceType == 2 ) {  //heart

        $query = sprintf("select exp, startTime, givedHeartCount from frdUserData where privateId = '%s'", $id);
        $eres = $db->query($query);
        if ($eres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        if ($eres->num_rows > 0) {
          $erow = $eres->fetch_assoc();
          $addHeartCount=0;
          $addTotalHeartCount = getAddHeartCount($db, $redis, $id, $erow["exp"], $erow["startTime"], $erow["givedHeartCount"], $row["heart"], $addHeartCount);
          if ( $addTotalHeartCount < 0 ) {
            echo 0;$db->query("rollback");$db->close();
            return;
          }

        }

        $havingDollor += $addHeartCount;
      }

      $restDollor = $havingDollor - $price;
      // echo "havingDollor : $havingDollor, price : $price, restDollor : $restDollor<br/>";
      if ( !$isUseFreePass && $restDollor < -1 ) {
        addBlacklist($id, "buy_chest_notEnoughMoney");
        echo 1;
        $db->close();
        $redis->close();
        return;
      }

      //-------   reward set start  -----------
      if ( (int)$priceType == 1)
        $isPremium = true;
      else
        $isPremium = false;

      $selGrade;
      if ( $isPremium ) {
        $tempChestCount = $chestCount;

        if ( $chestCount > 5 ) {
          $isEventChest = false;

          for ($itemIdx = 0; $itemIdx < count($goodsData->items); $itemIdx++) {
            if ($goodsData->items[$itemIdx]->id == 1000) {
              $selGrade = 5;
              $minWIdx = $redis->lindex('weaponMinIdx', $selGrade);
              $maxWIdx = $redis->lindex('weaponMaxIdx', $selGrade);
              $type = rand(1000+$minWIdx, 1000+$maxWIdx);
              $typeList[] = $type;
              $tempChestCount--;
              $isEventChest = false;
            }
            else if ($goodsData->items[$itemIdx]->id == 10000) {
              $rand = rand(0, 999);
              if ($rand < 900)
                $selGrade = 4;
              else
                $selGrade = 5;

              $minAIdx = $redis->lindex('magicMinIdx1', $selGrade);
              $maxAIdx = $redis->lindex('magicMaxIdx1', $selGrade);
              $type = rand(10000+$minAIdx, 10000+$maxAIdx);
              $typeList[] = $type;
              $tempChestCount--;
              $isEventChest = false;
            }
            else if ($goodsData->items[$itemIdx]->id == 100004) {
              $typeList[] = 100004;
              $tempChestCount--;
              $isEventChest = false;
            }
          }

          if ($isEventChest) {
            $eventItemId = 100003;
            $rand = rand(0, 99);
            if ($rand < 5)
              $eventItemCount = 3;
            else if ($rand < 15)
              $eventItemCount = 2;
            else
              $eventItemCount = 1;

            $now = time();
            $postEndTime = $now + 86400 * 23;
            $postSendId = 999;
            $postAddQuery = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) value ($id, $postSendId, $eventItemId, $eventItemCount, $postEndTime)";
            $isGood = $db->query($postAddQuery);
            if ($isGood == false) {
              echo 0;
              $db->query("rollback");
              $db->close();
              return;
            }
          }
        }

        for ( $i = 0; $i<$tempChestCount; $i++ ) {
          $isGetEventItem = false;
          for ($itemIdx = 0; $itemIdx < count($goodsData->items); $itemIdx++) {
            if ($goodsData->items[$itemIdx]->id == 283) {
              $val = rand(0, 999);
              if ($val < 30) {
                $isGetEventItem = true;
                $type = 283;
                break;
              }
            }
          }

          if (!$isGetEventItem) {
            $type = rand(0,99);
            if ( $type < 40 ) {       //charac 40%
              $val = rand(0,999);
              if ( $val < 700 )
                $selGrade = 4;
              else if ( $val < 950 )
                $selGrade = 5;
              else
                $selGrade = 6;

              $minCIdx = $redis->lindex('characMinIdx', $selGrade);
              $maxCIdx = $redis->lindex('characMaxIdx', $selGrade);  //except hidden
              if ( is_null($banCharacs) )
                $banCharacs = $redis->lrange('blockCharacInShop', 0, -1);

              do {
                $isReTry = false;
                $type = rand($minCIdx, $maxCIdx);
                for ( $ii=0; $ii<count($banCharacs); $ii++ ) {
                  if ( $type == (int)$banCharacs[$ii] ) {
                    $isReTry = true;
                    break;
                  }
                }
              }while($isReTry);
            }
            else if ( $type < 80 ) {   //weapon 40%
              $isEventChest = false;
              for ($itemIdx = 0; $itemIdx < count($goodsData->items); $itemIdx++) {
                if ($goodsData->items[$itemIdx]->id == 1000) {
                  $isEventChest = true;
                  break;
                }
              }

              $selGrade = getRandomWeaponGrade($isEventChest, $chestCount);
              // echo "weapon selGrade : $selGrade, isEventChest : $isEventChest, chestCount : $chestCount";
              $minWIdx = $redis->lindex('weaponMinIdx', $selGrade);
              $maxWIdx = $redis->lindex('weaponMaxIdx', $selGrade);
              $type = rand(1000+$minWIdx, 1000+$maxWIdx);
            }
            else {                    //magic 20%
              $isEventChest = false;
              for ($itemIdx = 0; $itemIdx < count($goodsData->items); $itemIdx++) {
                if ($goodsData->items[$itemIdx]->id == 10000) {
                  $isEventChest = true;
                  break;
                }
              }

              $selGrade = getRandomMagicGrade($isEventChest, $chestCount);
              // echo "magic selGrade : $selGrade, isEventChest : $isEventChest, chestCount : $chestCount";

              $minAIdx = $redis->lindex('magicMinIdx1', $selGrade);
              $maxAIdx = $redis->lindex('magicMaxIdx1', $selGrade);
              $type = rand(10000+$minAIdx, 10000+$maxAIdx);
            }
          }

          $typeList[] = $type;
        }
      }
      else {  // !isPremium

        for ( $i = 0; $i<$chestCount; $i++ ) {

          $type = rand(0,99);
          if ( $type < 40 ) {       //charac 40%
            $val = rand(0,999);
            if ( $val < 400 )
              $selGrade = 1;
            else if ( $val < 700 )
              $selGrade = 2;
            else if ( $val < 980 )
              $selGrade = 3;
            else
              $selGrade = 4;

            $minCIdx = $redis->lindex('characMinIdx', $selGrade);
            $maxCIdx = $redis->lindex('characMaxIdx', $selGrade);  //except hidden
            if ( is_null($banCharacs) )
              $banCharacs = $redis->lrange('blockCharacInShop', 0, -1);

            do {
              $isReTry = false;
              $type = rand($minCIdx, $maxCIdx);
              for ( $ii=0; $ii<count($banCharacs); $ii++ ) {
                if ( $type == (int)$banCharacs[$ii] ) {
                  $isReTry = true;
                  break;
                }
              }
            }while($isReTry);
          }
          else if ( $type < 80 ) {   //weapon 40%
            $val = rand(0,999);
            if ( $val < 750 )
              $selGrade = 1;
            else if ( $val < 980 )
              $selGrade = 2;
            else
              $selGrade = 3;

            $minWIdx = $redis->lindex('weaponMinIdx', $selGrade);
            $maxWIdx = $redis->lindex('weaponMaxIdx', $selGrade);
            $type = rand(1000+$minWIdx, 1000+$maxWIdx);
          }
          else {                    //magic 20%
            $val = rand(0,999);
            if ( $val < 900 )
              $selGrade = 1;
            else
              $selGrade = 2;

            $minAIdx = $redis->lindex('magicMinIdx1', $selGrade);
            $maxAIdx = $redis->lindex('magicMaxIdx1', $selGrade);
            $type = rand(10000+$minAIdx, 10000+$maxAIdx);
          }

          $typeList[$i] = $type;

        }


      }
      $data["typeList"] = $typeList;
      //-------   reward set complete  -----------


      $curWeaponIdPointer = $row["weaponIdPointer"];
      $curMagicIdPointer = $row["artifactIdPointer"];
      for ( $idx = 0; $idx < $chestCount; $idx++ ) {
        $chestId = (int)$typeList[$idx];
        if ( $chestId < 1000 ) {

          $characId = $chestId;
          $bundleIdx = ($characId>>3);
          $query = sprintf("select exps from frdCharacEvolveExps where userId=%d and bundleIdx=%d",$id, $bundleIdx);
          $sres = $db->query($query);
          if ($sres == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }
          if ($sres->num_rows > 0) {
            $row = $sres->fetch_assoc();
            $longData = $row["exps"];
            $bitIdx = 8*($characId%8);
            $preExp = ( ($longData >> $bitIdx) & 255);

            if ( $preExp < 10 )
              $amount = 10;
            else
              $amount = 5;
          }
          else {
            $amount = 10;
          }
        }
        else
          $amount = 1;
        $itemData = addItem($db, $redis, $id, $chestId, $amount);
        $data["itemDatas"][] = $itemData;
      }


      if ( (int)$priceType == 0 ) {
        $query = sprintf("update frdUserData set gold=%d, session=%d where privateId='%s'", $restDollor, $newSession, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
      }
      else if ( (int)$priceType == 1 ) {
        $jewelUpdate = "";
        if (!$isUseFreePass) {
          include_once("../EventAccuJewel.php");
          $resultCode = AccuUseJewel($db, $id, $price);
          if ($resultCode == 0) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }

          $jewelUpdate = "jewel = $restDollor,";
        }

        $query = "update frdUserData set $jewelUpdate session = $newSession where privateId = $id";
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
      }
      else {
        $query = sprintf("update frdUserData set heart=%d, givedHeartCount=%d, session=%d where privateId='%s'",
          $restDollor, $addTotalHeartCount, $newSession, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $eres->close();

        $data["givedHeartCount"] = $addTotalHeartCount;
        $data["time"] = time();
      }

      if (!$isUseFreePass) {
        $data[$getResourceName] = $restDollor;
      }
    }
    else {
      addBlacklist($id, "buy_chest_session");
      echo 1;
      $db->close();
      $redis->close();
      return;
    }
  }
  $res->close();
  $db->query("commit");
  $db->close();
  $redis->close();

  $keyAndIv = formatTo16String($session);
  // echo json_encode($data);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
