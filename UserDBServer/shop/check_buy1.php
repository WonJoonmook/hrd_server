<?php
  // 구글 결제 직후 영수증 검사후, 저희 서버에 기록함과 동시에 보상을 지급합니다.
  include_once("../myAes.php");


  $data = array();
  $data["error"] = 0;

  $id = $_REQUEST["privateId"];
  $productId = $_REQUEST["productId"];

  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");
  $query = "select productId, purchaseTime from frdLogBuy where productId = '$productId' and id = $id";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $now = time();
  $currentMonthDay = date("n,j", $now);
  $tempArray = explode(",", $currentMonthDay);
  $currentMonth = intval($tempArray[0]);
  $currentDay = intval($tempArray[1]);

  if ($res->num_rows > 0) {

    $recentIdx = 0;
    $recentPurchaseTime = 0;
    for ($idx = 0; $idx < $res->num_rows; $idx++) {
      $res->data_seek($idx);
      $row = $res->fetch_assoc();
      if ($row["purchaseTime"] > $recentPurchaseTime) {
        $recentPurchaseTime = $row["purchaseTime"];
        $recentIdx = $idx;
      }
    }

    $res->data_seek($recentIdx);
    $row = $res->fetch_assoc();
   
    if ( strncmp($row["productId"], "package", 7) == 0 )
      $data["error"] = 1;
    else {  // monthly event package

      $purchaseTime = $row["purchaseTime"];
      $purchaseMonth = substr($purchaseTime, 2, 2);
      $purchaseMonth = intval($purchaseMonth);

      if ($purchaseMonth != $currentMonth)
        $data["error"] = 0;
      else
        $data["error"] = 1;
    }
  } 
/*
  else {
    if ((strcmp($productId, "package_honeyjam") != 0) && (strcmp($productId, "package_start") != 0)) {
      $data["error"] = 2;
      if ($currentMonth == 8 && ($currentDay > 4 && $currentDay < 8))
        $data["error"] = 0;
    }
  }
*/
  $res->close();
  $db->query("commit");
  $db->close();
  echo json_encode($data);
?>
