<?php
include_once("../myAes.php");

  $data = array();
  $data["error"] = 0;

  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    return;
  }
  else {
    $data["isSpecialBox"] = $redis->get('isSpecialBox');
    $data["shopItemCount"] = $redis->lrange('shopItemCount1',0, -1);
    $data["shopItemPrice"] = $redis->lrange('shopItemPrice', 0, -1);
    $redis->close();
  }

  #add hrd package
  $data["shopItemCount"][count($data["shopItemCount"])] = "1";
  $data["shopItemPrice"][count($data["shopItemPrice"])] = "3,50000";

  #add Monthly Card
  $data["shopItemCount"][count($data["shopItemCount"])] = "1";
  $data["shopItemPrice"][count($data["shopItemPrice"])] = "3,6000";

  #add ability package
  $data["shopItemCount"][count($data["shopItemCount"])] = "1";
  $data["shopItemPrice"][count($data["shopItemPrice"])] = "3,30000";

  $now = time();
  $currentMonthDay = date("n,j", $now);
  $tempArray = explode(",", $currentMonthDay);
  $currentMonth = intval($tempArray[0]);
  $currentDay = intval($tempArray[1]);

  if ( $currentMonth == 10 && ($currentDay >= 14 && $currentDay <= 16) )
  {
    $costArr = array( "3,30000", "3,15000", "3,30000" );
    for ($i=0; $i < 3; $i++) {
      $data["shopItemCount"][count($data["shopItemCount"])] = "1";
      $data["shopItemPrice"][count($data["shopItemPrice"])] = $costArr[$i];
    }
    // $data["isSpecialBox0"] = "5성유물 획득확률 UP!!\n16.09.30 ~ 16.10.03";
    // $data["isSpecialBox1"] = "5성유물 1개 확정 지급!!\n무지개룬 or 빛나는룬 추가 지급!\n16.09.30 ~ 16.10.03";
  }

  echo json_encode($data);
?>
