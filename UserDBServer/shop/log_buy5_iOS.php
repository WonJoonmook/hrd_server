<?php
  // 구글 결제 직후 영수증 검사후, 저희 서버에 기록함과 동시에 보상을 지급합니다.
  include_once("../myAes.php");
  include_once("../GuerrillaShop/itemAdder.php");

  function verify($sandbox, $receipt) {
    $endpoint = "";

    // 샌드박스일 경우
    if($sandbox == 1) {
      $endpoint = 'https://sandbox.itunes.apple.com/verifyReceipt';
    }
    else {// 실결제일 경우
      $endpoint = 'https://buy.itunes.apple.com/verifyReceipt';
    }
    $postData = json_encode(array('receipt-data' => $receipt));

    $ch = curl_init($endpoint);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION , true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    $response = curl_exec($ch);
    $errno    = curl_errno($ch);
    $errmsg   = curl_error($ch);
    curl_close($ch);

    $data = json_decode($response);

    if(!is_object($data) || !isset($data->status) || $data->status != 0) {
      return false;
    }
    return true;
  }

  function getPostSendId($packageId) {
    if (strcmp($packageId, "package_start_new") == 0) {
      return 200;
    }
    else if (strcmp($packageId, "package_honeyjam_new") == 0) {
      return 201;
    }
    else if (strcmp($packageId, "package_hrd") == 0) {
      return 202;
    }
    else if (strcmp($packageId, "package_abyss") == 0) {
      return 203;
    }
    else if (strcmp($packageId, "package_frame_of_soul_0") == 0) {
      return 204;
    }
    else if (strcmp($packageId, "package_frame_of_soul_1") == 0) {
      return 205;
    }
  }


  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");


  $id = $_REQUEST["privateId"];
  $receipt = $_REQUEST["receipt"];
  $productId = $_REQUEST["productId"];
  $quantity = $_REQUEST["quantity"];
  $transactionId = $_REQUEST["transactionId"];
  $transactionState = $_REQUEST["transactionState"];
  $query = sprintf("select orderId from frdLogBuy where orderId = '%s'", $receipt);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->close();
    return;
  }
  if ($res->num_rows > 0) {
    $data["error"] = 1;
    $db->close();
    echo json_encode($data);
    return;
  }

  // 2. 애플 영수증 검증
  $isVerified = true;
  if(strlen($receipt) > 0) {
    if(!verify(0, $receipt)) {// 실결제 검증
      if(!verify(1, $receipt)) {// 샌드박스 검증
        $isVerified = false;
      }
    }
  }

  if ( !$isVerified ) {
    $data["error"] = 2;

    $query = sprintf("insert into frdLogBuy values ('%s', '%s', '%s', '%s', '%s', %d, '%s', '%s', %d)",
          $id, "iOS", $receipt, $productId, date("ymdHis", time()), $transactionState, $quantity, $transactionId, 0);

    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }
  else {

    $data["error"] = 0;

    $redis = openRedis();
    while ( $redis == false ) {
      $redis = openRedis();
    }

    if ( strncmp($productId,"package",7) == 0 ) {

      $query = sprintf("select jewel, gold, skillPoint, weaponIdPointer, artifactIdPointer from frdUserData where privateId = '%s'", $id);

      $res = $db->query($query);
      if ($res == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      if ($res->num_rows > 0) {
        $row = $res->fetch_assoc();

        $weaponIdPointer = $row["weaponIdPointer"];
        $artifactIdPointer = $row["artifactIdPointer"];

        $shopData = json_decode($redis->get('shopData'));
        $shopData = $shopData->data;
        $isFound = false;
        for ($i = 0; $i < count($shopData); $i++) {
        	for	($j = 0; $j < count($shopData[$i]->goods); $j++) {
        		$goodsData = $shopData[$i]->goods[$j];
        		if (property_exists($goodsData, "iabId")) {
        			if (strcmp($goodsData->iabId, $productId) == 0) {
                $isFound = true;
        				break;
              }
        		}
        	}
        	if ($isFound)
            break;
        }

        if (!$isFound) {
          echo "There is no package name. PackageName : ".$productId;
          $db->query("rollback");
          $db->close();
          return;
        }

        $now = time();
        $postEndTime = $now + 86400 * 23;
        $postSendId = getPostSendId($productId);

        $postAddQuery = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) values ";
        for ($i = 0; $i < count($goodsData->items); $i++) {
          $goodsItemId = $goodsData->items[$i]->id;
          $goodsItemCount = $goodsData->items[$i]->count;
          $postAddQuery .= "($id, $postSendId, $goodsItemId, $goodsItemCount, $postEndTime), ";
        }

        $postAddQuery = rtrim($postAddQuery, ", ");

        $postAddRes = $db->query($postAddQuery);
        if ($postAddRes == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $data["postCount"] = count($goodsData->items);

        /*
        $addItemList = array();
        if ( strcmp($productId, "package_start") == 0 ) {
          $addItemList[0]["itemId"] = 5004; // jewel
          $addItemList[0]["itemCount"] = 120;
          $addItemList[1]["itemId"] = 5000; // 골드
          $addItemList[1]["itemCount"] = 10000;
          $addItemList[2]["itemId"] = 5005; // 메달
          $addItemList[2]["itemCount"] = 30;
        }
        for ($i=0; $i < count($addItemList); $i++) {
          $addedItemData = addItem($db, $redis, $id, $addItemList[$i]["itemId"], $addItemList[$i]["itemCount"], $weaponIdPointer, $artifactIdPointer);
          if ($addedItemData == 0) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }
          $data["addedItemList"][$i] = $addedItemData;
        }
        */
      }
    }
    else if ( strncmp($productId, "buy_jewel_", 10) == 0 ) {
      $amount = (int)str_replace("buy_jewel_", "", $productId);

      switch($amount) {
      case 0: $amount = $redis->lindex('shopItemCount1', 0);break;
      case 1: $amount = $redis->lindex('shopItemCount1', 1);break;
      case 2: $amount = $redis->lindex('shopItemCount1', 2);break;
      case 3: $amount = $redis->lindex('shopItemCount1', 3);break;
      case 4: $amount = $redis->lindex('shopItemCount1', 4);break;
      default:break;
      }

      $query = sprintf("select jewel from frdUserData where privateId = '%s'", $id);
      $res = $db->query($query);
      if ($res == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      if ($res->num_rows > 0) {
        $row = $res->fetch_assoc();

        $resultJewel = $row["jewel"] + $amount;

        $query = sprintf("update frdUserData set jewel=%d where privateId='%s'", $resultJewel, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $data["jewel"] = $resultJewel;
      }
    }


    $query = sprintf("select orderId from frdLogBuy where orderId = '%s'", $receipt);
    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
    if ($res->num_rows <= 0) {
      $res->close();
      $query = sprintf("insert into frdLogBuy values ('%s', '%s', '%s', '%s', '%s', %d, '%s', '%s', %d)",
        $id, "iOS", $receipt, $productId, date("ymdHis", time()), $transactionState, $quantity, $transactionId, 1);

      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

    }



    $redis->close();

  }
  $db->query("commit");
  $db->close();
  echo json_encode($data);
?>
