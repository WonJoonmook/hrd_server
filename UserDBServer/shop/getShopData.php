<?php
  include_once("../myAes.php");

  $cVersion = $_REQUEST["cVersion"];

  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    return;
  }
  else {
    $data = json_decode($redis->get('shopData'));
    $redis->close();
  }

  $now = time();
  $curDate = date("ymdHi", $now);

  $version = $data->version;
  $data = $data->data;

  $minSalePeriods = 2147483647;
  for ($i = 0; $i < count($data); $i++) {
    $goods = $data[$i]->goods;
    for ($j = 0; $j < count($goods); $j++) {
      if (property_exists($goods[$j], "salePeriod")) {
        $tempStr = $goods[$j]->salePeriod;
        $salePeriods = explode('-', $tempStr);
        $startDate = $salePeriods[0];
        $endDate = $salePeriods[1];
        if (($curDate < $startDate) || ($curDate >= $endDate)) {
          unset($data[$i]->goods[$j]);
        }
        else {
          $minSalePeriods = min($endDate, $minSalePeriods);
        }
        unset($goods[$j]->salePeriod);
      }

      unset($goods[$j]->items);
    }
    $data[$i]->goods = array_values($data[$i]->goods);

    if (count($data[$i]->goods) == 0) {
      unset($data[$i]);
    }
  }
  $version = $version * 1000000 + intval($minSalePeriods / 10000);
  if ($version != $cVersion) {
    $res["shopDatas"] = json_encode($data);
  }
  $res["version"] = $version;
  echo json_encode($res);
?>
