<?php
  // 구글 결제 직후 영수증 검사후, 저희 서버에 기록함과 동시에 보상을 지급합니다.
  include_once("../myAes.php");
  include_once("../GuerrillaShop/itemAdder.php");

  function getCurl($fUrl,$fMethod,$fParam) {
    $sUrl = $fUrl.(($fParam && strtolower($fMethod)=="get") ? "?$fParam": "");
    $sMethod = (strtolower($fMethod)=="get") ? "0" : "1" ;
    $sParam = (strtolower($fMethod)=="get") ? "" : $fParam ;

    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL,"$sUrl"); //접속할 URL 주소
    curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // 인증서 체크같은데 true 시 안되는 경우가 많다.
    curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    // default 값이 true 이기때문에 이부분을 조심 (https 접속시에 필요)
    curl_setopt ($ch, CURLOPT_SSLVERSION,4); // SSL 버젼 (https 접속시에 필요)
    curl_setopt ($ch, CURLOPT_HEADER, 0); // 헤더 출력 여부
    curl_setopt ($ch, CURLOPT_POST, $sMethod); // Post Get 접속 여부
    curl_setopt ($ch, CURLOPT_POSTFIELDS, "$fParam"); // Post 값  Get 방식처럼적는다.
    curl_setopt ($ch, CURLOPT_TIMEOUT, 30); // TimeOut 값
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); // 결과값을 받을것인지
    $result = curl_exec ($ch);
    curl_close ($ch);
    return $result;
  }

  function getPostSendId($packageId) {
    if (strcmp($packageId, "package_start_new") == 0) {
      return 200;
    }
    else if (strcmp($packageId, "package_honeyjam_new") == 0) {
      return 201;
    }
    else if (strcmp($packageId, "package_hrd") == 0) {
      return 202;
    }
    else if (strcmp($packageId, "package_abyss") == 0) {
      return 203;
    }
    else if (strcmp($packageId, "package_frame_of_soul_0") == 0) {
      return 204;
    }
    else if (strcmp($packageId, "package_frame_of_soul_1") == 0) {
      return 205;
    }
    else if (strcmp($packageId, "package_1201_0") == 0) {
      return 206;
    }
    else if (strcmp($packageId, "package_1201_1") == 0) {
      return 207;
    }
    else if (strcmp($packageId, "package_1201_2") == 0) {
      return 208;
    }
    else if (strcmp($packageId, "package_1201_3") == 0) {
      return 209;
    }
    else if (strcmp($packageId, "package_event_abillity") == 0) {
      return 210;
    }
    else if (strcmp($packageId, "package_1229_0") == 0) {
      return 211;
    }
    else if (strcmp($packageId, "package_1229_1") == 0) {
      return 212;
    }
    else if (strcmp($packageId, "package_1229_3") == 0) {
      return 213;
    }
    else if (strcmp($packageId, "package_1229_4") == 0) {
      return 214;
    }


    return 0;
  }


  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");


  $id = $_REQUEST["privateId"];
  $packageName = $_REQUEST["packageName"];
  $orderId = $_REQUEST["orderId"];
  $productId = $_REQUEST["productId"];
  $purchaseTime = $_REQUEST["purchaseTime"];
  $purchaseState = $_REQUEST["purchaseState"];
  $purchaseToken = $_REQUEST["purchaseToken"];
  $signature = $_REQUEST["signature"];
  $query = sprintf("select orderId from frdLogBuy where orderId = '%s'", $orderId);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->close();
    return;
  }
  if ($res->num_rows > 0) {
    $data["error"] = 1;
    $db->close();
    echo json_encode($data);
    return;
  }

  //refresh token
  $sFileName = dirname(__FILE__)."/token.txt";
  $sResult = json_decode(file_get_contents($sFileName),true);
  $refresh_token = addslashes($sResult["refresh_token"]);
  $regdate = $sResult["regdate"];


  if ((time()-$regdate)>1800) {

    $sParam = "";
    $sParam .= "refresh_token=".$refresh_token;
    $sParam .= "&client_id=1068641320169-00n3c9s58jt1n5351ida22l3i5o3jejv.apps.googleusercontent.com";
    $sParam .= "&client_secret=W3JEfPszL36KxCFQqtgqD7O-";
    $sParam .= "&grant_type=refresh_token";
    $rResult = getCurl("https://accounts.google.com/o/oauth2/token","post","$sParam");

    $sResult = json_decode($rResult,true);
    $sResult["regdate"] = "".time()."";
    $sResult["refresh_token"] = $refresh_token;

    $rResult = json_encode($sResult);
    $fp=fopen($sFileName,'w');
    fwrite($fp,$rResult);
    fclose($fp);

  }
  //~refresh token

  $data["sResult"] = $sResult;
  $sUrl = "https://www.googleapis.com/androidpublisher/v1.1/";
  $sUrl .= "applications/";
  $sUrl .= $packageName;
  $sUrl .= "/inapp/";
  $sUrl .= $productId;
  $sUrl .= "/purchases/";
  $sUrl .= $purchaseToken;
  $sUrl .= "/?access_token=";
  $sUrl .= $sResult['access_token'];

  sleep(2);

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $sUrl);

  // Set so curl_exec returns the result instead of outputting it.
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

  // Get the response and close the channel.
  $response = curl_exec($ch);
  curl_close($ch);

  $sResult = json_decode($response);

  $data["str"] = $sResult;

  $purchaseDate = $sResult->{'purchaseTime'};

  /*
  if ( strncmp($orderId, "GPA.", 4) !== 0 ) {
    $data["url"] = $sUrl;
    $data["str"] = $sResult;
    $data["error"] = 2;
    $query = sprintf("insert into frdLogBuy values ('%s', '%s', '%s', '%s', '%s', %d, '%s', '%s', %d)",
          $id, $packageName, $orderId, $productId, date("ymdHis", ($purchaseTime/1000)), $purchaseState, $purchaseToken, $signature, 0);

    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }
  */

  if ( $purchaseDate == null ) {
    $data["url"] = $sUrl;
    $data["str"] = $sResult;
    $data["error"] = 2;

    $query = sprintf("insert into frdLogBuy values ('%s', '%s', '%s', '%s', '%s', %d, '%s', '%s', %d)",
          $id, $packageName, $orderId, $productId, date("ymdHis", ($purchaseTime/1000)), $purchaseState, $purchaseToken, $signature, 0);

    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }
  else {

    $data["error"] = 0;

    $redis = openRedis();
    while ( $redis == false ) {
      $redis = openRedis();
    }

    if ( strncmp($productId,"package",7) == 0 ) {

      $query = sprintf("select jewel, gold, skillPoint, weaponIdPointer, artifactIdPointer from frdUserData where privateId = '%s'", $id);

      $res = $db->query($query);
      if ($res == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      if ($res->num_rows > 0) {
        $row = $res->fetch_assoc();

        $weaponIdPointer = $row["weaponIdPointer"];
        $artifactIdPointer = $row["artifactIdPointer"];

        $shopData = json_decode($redis->get('shopDataForTest'));
        $shopData = $shopData->data;
        $isFound = false;
        for ($i = 0; $i < count($shopData); $i++) {
        	for	($j = 0; $j < count($shopData[$i]->goods); $j++) {
        		$goodsData = $shopData[$i]->goods[$j];
        		if (property_exists($goodsData, "iabId")) {
        			if (strcmp($goodsData->iabId, $productId) == 0) {
                $isFound = true;
        				break;
              }
        		}
        	}
        	if ($isFound)
            break;
        }

        if (!$isFound) {
          echo "There is no package name. PackageName : ".$productId;
          $db->query("rollback");
          $db->close();
          return;
        }

        $now = time();
        $postEndTime = $now + 86400 * 23;
        $postSendId = getPostSendId($productId);

        $postAddQuery = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) values ";
        for ($i = 0; $i < count($goodsData->items); $i++) {
          $goodsItemId = $goodsData->items[$i]->id;
          $goodsItemCount = $goodsData->items[$i]->count;
          $postAddQuery .= "($id, $postSendId, $goodsItemId, $goodsItemCount, $postEndTime), ";
        }

        $postAddQuery = rtrim($postAddQuery, ", ");

        $postAddRes = $db->query($postAddQuery);
        if ($postAddRes == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $data["postCount"] = count($goodsData->items);

        /*
        $addItemList = array();
        if ( strcmp($productId, "package_start") == 0 ) {
          $addItemList[0]["itemId"] = 5004; // jewel
          $addItemList[0]["itemCount"] = 120;
          $addItemList[1]["itemId"] = 5000; // 골드
          $addItemList[1]["itemCount"] = 10000;
          $addItemList[2]["itemId"] = 5005; // 메달
          $addItemList[2]["itemCount"] = 30;
        }
        for ($i=0; $i < count($addItemList); $i++) {
          $addedItemData = addItem($db, $redis, $id, $addItemList[$i]["itemId"], $addItemList[$i]["itemCount"], $weaponIdPointer, $artifactIdPointer);
          if ($addedItemData == 0) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }
          $data["addedItemList"][$i] = $addedItemData;
        }
        */
      }
    }
    else if ( strncmp($productId, "buy_jewel_", 10) == 0 ) {
      $amount = (int)str_replace("buy_jewel_", "", $productId);

      switch($amount) {
      case 0: $amount = $redis->lindex('shopItemCount1', 0);break;
      case 1: $amount = $redis->lindex('shopItemCount1', 1);break;
      case 2: $amount = $redis->lindex('shopItemCount1', 2);break;
      case 3: $amount = $redis->lindex('shopItemCount1', 3);break;
      case 4: $amount = $redis->lindex('shopItemCount1', 4);break;
      default:break;
      }

      $query = sprintf("select jewel from frdUserData where privateId = '%s'", $id);
      $res = $db->query($query);
      if ($res == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      if ($res->num_rows > 0) {
        $row = $res->fetch_assoc();

        $resultJewel = $row["jewel"] + $amount;

        $query = sprintf("update frdUserData set jewel=%d where privateId='%s'", $resultJewel, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $data["jewel"] = $resultJewel;
      }

      // event accu buy jewel
      include_once("../EventAccuJewel.php");
      $res = AccuBuyJewel($db, $id, $amount);
      if ($res == 0) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
    }


    $query = sprintf("select orderId from frdLogBuy where orderId = '%s'", $orderId);
    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
    if ($res->num_rows <= 0) {
      $res->close();
      $query = sprintf("insert into frdLogBuy values ('%s', '%s', '%s', '%s', '%s', %d, '%s', '%s', %d)",
        $id, $packageName, $orderId, $productId, date("ymdHis", ($purchaseTime/1000)), $purchaseState, $purchaseToken, $signature, 1);

      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

    }



    $redis->close();

  }
  $db->query("commit");
  $db->close();
  echo json_encode($data);
?>
