<?php
  // 구글 결제 직후 영수증 검사후, 저희 서버에 기록함과 동시에 보상을 지급합니다.
  include_once("../myAes.php");
  include_once("../GuerrillaShop/itemAdder.php");

  function getPostCurl($fUrl,$postData) {
    $ch = curl_init($fUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION , true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type:  Application/json"));
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    $result = curl_exec ($ch);
    curl_close ($ch);
    return $result;
  }

  function getPostSendId($packageId) {
    if (strcmp($packageId, "package_start_new") == 0) {
      return 200;
    }
    else if (strcmp($packageId, "package_honeyjam_new") == 0) {
      return 201;
    }
    else if (strcmp($packageId, "package_hrd") == 0) {
      return 202;
    }
    else if (strcmp($packageId, "package_abyss") == 0) {
      return 203;
    }
    else if (strcmp($packageId, "package_frame_of_soul_0") == 0) {
      return 204;
    }
    else if (strcmp($packageId, "package_frame_of_soul_1") == 0) {
      return 205;
    }
    else if (strcmp($packageId, "package_1201_0") == 0) {
      return 206;
    }
    else if (strcmp($packageId, "package_1201_1") == 0) {
      return 207;
    }
    else if (strcmp($packageId, "package_1201_2") == 0) {
      return 208;
    }
    else if (strcmp($packageId, "package_1201_3") == 0) {
      return 209;
    }
  }


  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");

  // 전자 영수증 검증
  $id = $_REQUEST["privateId"];
  $appId = $_REQUEST["appId"];
  $productId = $_REQUEST["productId"];
  $oneStoreIapId = $_REQUEST["oneStoreIapId"];
  $txId = $_REQUEST["txId"];
  $receipt = $_REQUEST["receipt"];
  $tId = $_REQUEST["tId"];

  $postData = json_encode(array('txid' => $txId,
                                'appid' => $appId,
                                'signdata' => $receipt));

  // 라이브 환경 검증
  $rResult = getPostCurl("https://iap.tstore.co.kr/digitalsignconfirm.iap", $postData);
  $resData = json_decode($rResult);
  if (!is_object($resData) || !isset($resData->status) || $resData->status != 0) {
    // 개발 환경 검증
    $rResult = getPostCurl("https://iapdev.tstore.co.kr/digitalsignconfirm.iap", $postData);
    $resData = json_decode($rResult);
    if (!is_object($resData) || !isset($resData->status) || $resData->status != 0) {
      $data["error"] = 1;
      $data["status"] = $resData->status;
      $db->close();
      echo json_encode($data);
      return;
    }
  }

  if (strcmp($resData->product[0]->tid, $tId) != 0) {
    // 다른 tid!
    $data["error"] = 1;
    $data["message"] = "Different TID";
    $db->close();
    echo json_encode($data);
    return;
  }
  // 영수증 검증 끝

  $query = sprintf("select orderId from frdLogBuy where orderId = '%s'", $tId);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->close();
    return;
  }
  if ($res->num_rows > 0) {
    $data["error"] = 1;
    $data["message"] = "Already Provided orderId";
    $db->close();
    echo json_encode($data);
    return;
  }

  $data["error"] = 0;

  $redis = openRedis();
  while ( $redis == false ) {
    $redis = openRedis();
  }

  if ( strncmp($productId,"package",7) == 0 ) {

    $query = sprintf("select jewel, gold, skillPoint, weaponIdPointer, artifactIdPointer from frdUserData where privateId = '%s'", $id);

    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
    if ($res->num_rows > 0) {
      $row = $res->fetch_assoc();

      $weaponIdPointer = $row["weaponIdPointer"];
      $artifactIdPointer = $row["artifactIdPointer"];

      $shopData = json_decode($redis->get('shopData'));
      $shopData = $shopData->data;
      $isFound = false;
      for ($i = 0; $i < count($shopData); $i++) {
      	for	($j = 0; $j < count($shopData[$i]->goods); $j++) {
      		$goodsData = $shopData[$i]->goods[$j];
      		if (property_exists($goodsData, "iabId")) {
      			if (strcmp($goodsData->iabId, $productId) == 0) {
              $isFound = true;
      				break;
            }
      		}
      	}
      	if ($isFound)
          break;
      }

      if (!$isFound) {
        echo "There is no package name. PackageName : ".$productId;
        $db->query("rollback");
        $db->close();
        return;
      }

      $now = time();
      $postEndTime = $now + 86400 * 23;
      $postSendId = getPostSendId($productId);

      $postAddQuery = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) values ";
      for ($i = 0; $i < count($goodsData->items); $i++) {
        $goodsItemId = $goodsData->items[$i]->id;
        $goodsItemCount = $goodsData->items[$i]->count;
        $postAddQuery .= "($id, $postSendId, $goodsItemId, $goodsItemCount, $postEndTime), ";
      }

      $postAddQuery = rtrim($postAddQuery, ", ");

      $postAddRes = $db->query($postAddQuery);
      if ($postAddRes == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      $data["postCount"] = count($goodsData->items);

      /*
      $addItemList = array();
      if ( strcmp($productId, "package_start") == 0 ) {
        $addItemList[0]["itemId"] = 5004; // jewel
        $addItemList[0]["itemCount"] = 120;
        $addItemList[1]["itemId"] = 5000; // 골드
        $addItemList[1]["itemCount"] = 10000;
        $addItemList[2]["itemId"] = 5005; // 메달
        $addItemList[2]["itemCount"] = 30;
      }
      for ($i=0; $i < count($addItemList); $i++) {
        $addedItemData = addItem($db, $redis, $id, $addItemList[$i]["itemId"], $addItemList[$i]["itemCount"], $weaponIdPointer, $artifactIdPointer);
        if ($addedItemData == 0) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $data["addedItemList"][$i] = $addedItemData;
      }
      */
    }
  }
  else if ( strncmp($productId, "buy_jewel_", 10) == 0 ) {
    $amount = (int)str_replace("buy_jewel_", "", $productId);

    switch($amount) {
    case 0: $amount = $redis->lindex('shopItemCount1', 0);break;
    case 1: $amount = $redis->lindex('shopItemCount1', 1);break;
    case 2: $amount = $redis->lindex('shopItemCount1', 2);break;
    case 3: $amount = $redis->lindex('shopItemCount1', 3);break;
    case 4: $amount = $redis->lindex('shopItemCount1', 4);break;
    default:break;
    }

    $query = sprintf("select jewel from frdUserData where privateId = '%s'", $id);
    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
    if ($res->num_rows > 0) {
      $row = $res->fetch_assoc();

      $resultJewel = $row["jewel"] + $amount;

      $query = sprintf("update frdUserData set jewel=%d where privateId='%s'", $resultJewel, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      $data["jewel"] = $resultJewel;
    }

    // event accu buy jewel
    include_once("../EventAccuJewel.php");
    $res = AccuBuyJewel($db, $id, $amount);
    if ($res == 0) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }

  $query = sprintf("select orderId from frdLogBuy where orderId = '%s'", $tId);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }
  if ($res->num_rows <= 0) {
    $res->close();
    $query = sprintf("insert into frdLogBuy values ('%s', '%s', '%s', '%s', '%s', %d, '%s', '%s', %d)",
      $id, $oneStoreIapId, $tId, $productId, date("ymdHis", time()), 0, $txId, $receipt, 1);

    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }

  $redis->close();
  $db->query("commit");
  $db->close();
  echo json_encode($data);
?>
