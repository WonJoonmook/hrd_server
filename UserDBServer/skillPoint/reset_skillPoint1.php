<?php
  // 보석 50를 써서 특성을 초기화합니다.
  function RemEffectSP(&$redis, &$db, $id, $spId, $curLevel) {
      $sp = $redis->hget('effectRewardSP', $spId);
      if ( $sp !== false ) {
        $pieces = explode (",", $sp);
        $type = (int)$pieces[0];
        $val = (int)$pieces[1];
        $subVal = $val * $curLevel;
        $query = sprintf("select val from frdEffectForRewards where userId=%d and type=%d",$id, $type);
        $sres = $db->query($query);
        if ($sres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        if ( $sres->num_rows <= 0 ) {
          return;
        }

        $srow = $sres->fetch_assoc();
        $resultVal = $srow["val"] - $subVal;
        if ( $resultVal > 0 )
          $query = sprintf("update frdEffectForRewards set val=val-%d where userId=%d and type=%d", $subVal, $id, $type);
        else
          $query = sprintf("delete from frdEffectForRewards where userId=%d and type=%d", $id, $type);

        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
      }

      $sp = $redis->hget('effectRewardPreCheckSP', $spId);
      if ( $sp !== false ) {
        $pieces = explode (",", $sp);
        $type = (int)$pieces[0];
        $val = (int)$pieces[1];
        $query = sprintf("select type from frdEffectForPreCheckRewards where userId=%d and type=%d",$id, $type);
        $sres = $db->query($query);
        if ($sres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        if ( $sres->num_rows <= 0 ) {
          return;
        }

        $query = sprintf("delete from frdEffectForPreCheckRewards where userId=%d and type=%d", $id, $type);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
      }
  }

  include_once("../myAes.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];

  $price = (int)50;
  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }
  $db->query("set autocommit=0");

  $query = sprintf("select skillPoint, jewel, session from frdUserData where privateId = %d", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
      $row = $res->fetch_assoc();
      if ( $row["session"] == $session ) {
        $newSession = mt_rand();
        $data["session"] = $newSession;

        $query = sprintf("select itemCount from frdHavingItems where userId = %d and itemId=%d", $id, 100001);// 100001 = pass_ResetSP
        $sres = $db->query($query);
        if ($sres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        if ( $sres->num_rows > 0 ) {
          $restJewel = $row["jewel"];
          $srow = $sres->fetch_assoc();
          $data["restItem"] = (int)$srow["itemCount"] -1;
          if ( $data["restItem"] <= 0 )
            $query = sprintf("delete from frdHavingItems where userId=%d and itemId=%d", $id, 100001);
          else
            $query = sprintf("update frdHavingItems set itemCount=%d where userId=%d and itemId=%d", $data["restItem"], $id, 100001);

          $isGood = $db->query($query);
          if ($isGood == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }

        }
        else {
          $restJewel = $row["jewel"] - $price;
          if ( $restJewel < 0 ) {
            addBlacklist($id, "get_skillLevel_NoJewel");
            echo 1;
            $db->close();
            return;
          }
          $data["restJewel"] = $restJewel;

        }

        $query = sprintf("select accuSkillPoint from frdSkillPoints where userId = %d", $id);
        $sres = $db->query($query);
        if ($sres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $srow = $sres->fetch_assoc();
        $data["skillPoint"] = $srow["accuSkillPoint"];

        $query = sprintf("update frdUserData set skillPoint=%d, jewel=%d, session=%d where privateId=%d",
                            $srow["accuSkillPoint"], $restJewel, $newSession, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $redis = openRedis();
        if ( !$redis ) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $query = sprintf("select bundleIdx,levels from frdSkillLevels where userId = %d", $id);
        $sres = $db->query($query);
        for ($idx = 0; $idx < $sres->num_rows; $idx++) {
          $sres->data_seek($idx);
          $srow = $sres->fetch_assoc();
          $bundleIdx = $srow["bundleIdx"];
          $levels = $srow["levels"];

          for ( $i=0; $i<8; $i++ ) {
            $severalLv = ($levels & 0xff);
            if ( (int)$severalLv > 0 ) {
              $spId = ($bundleIdx*8+(int)$i);
              RemEffectSP(&$redis, &$db, $id, $spId, $severalLv);
            }
            $levels >>= 8;
          }
        }
        $redis->close();

        $query = sprintf("delete from frdSkillLevels where userId=%d", $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
      }
      else {
        addBlacklist($id, "reset_skillPoint");
        echo 1;
        $db->close();
        return;
      }
  }

  $res->close();
  $db->query("commit");
  $db->close();

  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);

?>
