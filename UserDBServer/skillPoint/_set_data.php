<?php
	include_once("../myAes.php");
    include_once("../globalDefine.php");

	$redis = openRedis();
  	if ( $redis == false )
  		return;

    $redis->del('effectRewardPreCheckSP');
    $redis->hset('effectRewardPreCheckSP', 1100, "1100,5004,2500");

    $redis->del('effectRewardSP');
    $redis->hset('effectRewardSP', 506, $GLOBALS['$SP_TicketGetPercent'].",100");
    $redis->hset('effectRewardSP', 905, $GLOBALS['$SP_AllDoubleGetPercent'].",100");
    $redis->hset('effectRewardSP', 906, $GLOBALS['$SP_Jewel10GetPercent'].",100");
    $redis->hset('effectRewardSP', 1002, $GLOBALS['$SP_HeartReturnGetPercent'].",30000");
    $redis->hset('effectRewardSP', 1101, $GLOBALS['$SP_ExpStoneDoubleGetPercent'].",8000");
    
    $redis->hset('effectRewardSP', 1209, $GLOBALS['$SP_RuneDoubleGetPercent'].",1000");
    $redis->hset('effectRewardSP', 1208, $GLOBALS['$SP_MedalDoubleGet'].",1");
    $redis->hset('effectRewardSP', 1211, $GLOBALS['$SP_QuestItemDoubleGetPercent'].",1000");
    

	$redis->close();
?>