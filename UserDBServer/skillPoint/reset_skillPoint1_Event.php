<?php
  // 보석 50를 써서 특성을 초기화합니다.

  include_once("../myAes.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];

  $price = (int)50;
  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }
  $db->query("set autocommit=0");

  $query = sprintf("select skillPoint, jewel, session from frdUserData where privateId = %d", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
      $row = $res->fetch_assoc();
      if ( $row["session"] == $session ) {
        $newSession = mt_rand();
        $data["session"] = $newSession;

        $query = sprintf("select itemCount from frdHavingItems where userId = %d and itemId=%d", $id, 100001);// 100001 = pass_ResetSP
        $sres = $db->query($query);
        if ($sres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        if ( $sres->num_rows > 0 ) {
          $restJewel = $row["jewel"];
          $srow = $sres->fetch_assoc();
          $data["restItem"] = (int)$srow["itemCount"] -1;
          if ( $data["restItem"] <= 0 )
            $query = sprintf("delete from frdHavingItems where userId=%d and itemId=%d", $id, 100001);
          else
            $query = sprintf("update frdHavingItems set itemCount=%d where userId=%d and itemId=%d", $data["restItem"], $id, 100001);

          $isGood = $db->query($query);
          if ($isGood == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }

        }
        else {
          $restJewel = $row["jewel"] - $price;
          if ( $restJewel < 0 ) {
            addBlacklist($id, "get_skillLevel_NoJewel");
            echo 1;
            $db->close();
            return;
          }
          $data["restJewel"] = $restJewel;

          include_once("../EventAccuJewel.php");
          $resultCode = AccuUseJewel($db, $id, $price);
          if ($resultCode == 0) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }
        }

        $query = sprintf("select accuSkillPoint from frdSkillPoints where userId = %d", $id);
        $sres = $db->query($query);
        if ($sres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $srow = $sres->fetch_assoc();
        $data["skillPoint"] = $srow["accuSkillPoint"];

        $query = sprintf("update frdUserData set skillPoint=%d, jewel=%d, session=%d where privateId=%d",
                            $srow["accuSkillPoint"], $restJewel, $newSession, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $query = sprintf("delete from frdSkillLevels where userId=%d", $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $query = sprintf("delete from frdEffectForPreCheckRewards where userId=%d", $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $query = sprintf("delete from frdEffectForRewards where userId=%d", $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
      }
      else {
        addBlacklist($id, "reset_skillPoint");
        echo 1;
        $db->close();
        return;
      }
  }

  $res->close();
  $db->query("commit");
  $db->close();

  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);

?>
