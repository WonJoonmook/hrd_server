<?php
	// 특성 레벨업 요청입니다.
	// 특성포인트를 사용하여 특정 특성을 레벨업 합니다.
include_once("../myAes.php");
$session = $_REQUEST["session"];
$id = $_REQUEST["id"];
$skillId = (int)$_REQUEST["skillId"];
$needPoint = $_REQUEST["needPoint"];
$count = $_REQUEST["count"];

$data = array();
$data["error"] = 0;
$db = getDB();
if (mysqli_connect_errno()) {
  echo 0;
  return;
}

$redis = openRedis();
if ( $redis == false ) {
	echo 0;
	$db->close();
	return;
}

$db->query("set autocommit=0");
$query = sprintf("select skillPoint, session from frdUserData where privateId=%d",$id);
$res = $db->query($query);
if ($res == false) {
	echo 0;
	$db->query("rollback");
	$db->close();
	return;
}

if ($res->num_rows > 0) {
	$row = $res->fetch_assoc();

	if ( $row["session"] == $session ) {
    	if ( $row["skillPoint"] < ($needPoint*$count) ) {
    		addBlacklist($id, "hack_SP");
    		echo 1;
    		$db->close();
    		return;
    	}
    	else {
    		$newSession = mt_rand();
  			$data["session"] = $newSession;

    		$restPoint = $row["skillPoint"] - ($needPoint*$count);
    		$query = sprintf("update frdUserData set skillPoint=%d, session=%d where privateId=%d",$restPoint, $newSession, $id);
  			$isGood = $db->query($query);
  			if ($isGood == false) {
		      echo 0;
		      $db->query("rollback");
		      $db->close();
		      return;
		    }
  			$data["restPoint"] = $restPoint;



  			$bundleIdx = ($skillId>>3);
		    $query = sprintf("select levels from frdSkillLevels where userId=%d and bundleIdx=%d",$id, $bundleIdx);
		    $sres = $db->query($query);
		    if ($sres == false) {
		      echo 0;
		      $db->query("rollback");
		      $db->close();
		      return;
		    }

		    $bitIdx = 8*($skillId&7);
		    $resultData = 0;
		    if ($sres->num_rows > 0) {
		        $row = $sres->fetch_assoc();
		        $longData = $row["levels"];
		        $preLevel = ( ($longData >> $bitIdx) & 0xff);

		        $mask = $preLevel;
		        $mask <<= $bitIdx;
		        $longData -= $mask;

		        $mask = ($preLevel + $count);
		        $resultLevel = $mask;

		        $mask <<= $bitIdx;
		        $resultData = ($longData | $mask);

		        $query = sprintf("update frdSkillLevels set levels='%s' where userId=%d and bundleIdx=%d", $resultData, $id, $bundleIdx);
		        $isGood = $db->query($query);
		        if ($isGood == false) {
			      echo 0;
			      $db->query("rollback");
			      $db->close();
			      return;
			    }

		    }
		    else {
		    	$preLevel = 0;
		    	$resultLevel = $count;
		        $resultData = $count;
		        $resultData <<= $bitIdx;

		        $query = sprintf("insert into frdSkillLevels values (%d, %d, '%s')", $id, $bundleIdx, $resultData);
				$isGood = $db->query($query);
				if ($isGood == false) {
			      echo 0;
			      $db->query("rollback");
			      $db->close();
			      return;
			    }
		    }
		    $data["skillBundleIdx"] = $bundleIdx;
		    $data["skillLevels"] = $resultData;

		    $psp = $redis->hget('effectRewardPreCheckSP', $skillId);
		    if ( $psp !== false ) {
		    	$pieces = explode (",", $psp);
		    	$type = (int)$pieces[0];
		    	$subType = (int)$pieces[1];
		    	$val = (int)$pieces[2];

		    	$query = sprintf("select * from frdEffectForPreCheckRewards where userId=%d and type=%d",$id, $type);
			    $sres = $db->query($query);
			    if ($sres == false) {
			      echo 0;
			      $db->query("rollback");
			      $db->close();
			      return;
			    }

			    if ( $sres->num_rows <= 0 ) {
			    	$query = sprintf("insert into frdEffectForPreCheckRewards values (%d, %d, %d, 0, 0)", $id, $type, $subType);
			    	$isGood = $db->query($query);
					if ($isGood == false) {
				      echo 0;
				      $db->query("rollback");
				      $db->close();
				      return;
				    }
			    }
		    }

		    $sp = $redis->hget('effectRewardSP', $skillId);
		    if ( $sp !== false ) {
		    	$pieces = explode (",", $sp);
		    	$type = (int)$pieces[0];
		    	$val = (int)$pieces[1];

		    	$query = sprintf("select * from frdEffectForRewards where userId=%d and type=%d",$id, $type);
			    $sres = $db->query($query);
			    if ($sres == false) {
			      echo 0;
			      $db->query("rollback");
			      $db->close();
			      return;
			    }

			    if ( $sres->num_rows <= 0 ) {
			    	$query = sprintf("insert into frdEffectForRewards values (%d, %d, %d)", $id, $type, ($resultLevel*$val));
			    }
			    else {
			    	$query = sprintf("update frdEffectForRewards set val=val+%d where userId=%d and type=%d",
			    		($resultLevel*$val - $preLevel*$val), $id, $type);
			    }
			    $isGood = $db->query($query);
				if ($isGood == false) {
			      echo 0;
			      $db->query("rollback");
			      $db->close();
			      return;
			    }
		    }
		}

	}
	else {
      addBlacklist($id, "update_skillLevel");
      echo 1;
      $db->close();
      return;
    }
}

$db->query("commit");
$res->close();
$db->close();


  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);

?>
