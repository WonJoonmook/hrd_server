<?php
  // 로비에서 내 정보 확인 요청.
  // 특수 스테이지들 클리어 횟수를 뿌려줍니다.
  include_once("../myAes.php");

  $id = $_REQUEST["id"];
  
  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }

  $data["stageId"] = array();
  $data["count"] = array();
  $db->query("set autocommit=0");
  $query = sprintf("select stageId, clearCount from frdClearCount where userId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    for ($idx = 0; $idx < $res->num_rows; $idx++) {
      $res->data_seek($idx);
      $row = $res->fetch_assoc();

      $data["stageId"][] = $row["stageId"];
      $data["count"][] = $row["clearCount"];
    }
  }

  $query = "select accuSkillPoint from frdSkillPoints where userId = $id";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    $data["accuSkillPoint"] = $row["accuSkillPoint"];
  }

  $res->close();
  $db->query("commit");
  $db->close();

  echo json_encode($data);
?>
