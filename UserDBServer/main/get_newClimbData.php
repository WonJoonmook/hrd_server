<?php
  // 게임 시작 시 본인 데이터를 요청합니다.

  include_once("../myAes.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
    echo $query.", ".mysqli_error($db)." << \n";
    echo 0;
    return;
  }

  $db->query("set autocommit=0");
  $time = time();
  $query = sprintf("select * from frdUserData where privateId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }
  if ($res->num_rows > 0) {

    $query = sprintf("select * from frdClimbTopData where userId = '%s'", $id);
    $sres = $db->query($query);
    if ($sres == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    if ($sres->num_rows > 0) {
      $srow = $sres->fetch_assoc();
      $data["bestRank"] = $srow["bestRank"];
      $data["bestTotalUserCount"] = $srow["bestTotalUserCount"];
      $data["playCount"] = $srow["playCount"];
      $data["averageRank"] = $srow["averageRank"];
      $data["avrgTotalUserCount"] = $srow["avrgTotalUserCount"];
      $data["lastRank"] = $srow["lastRank"];
      $data["lastTotalUserCount"] = $srow["lastTotalUserCount"];
    }
    else {
      $data["bestRank"] = 0;
      $data["bestTotalUserCount"] = 0;
      $data["playCount"] = 0;
      $data["averageRank"] = 0;
      $data["avrgTotalUserCount"] = 0;
      $data["lastRank"] = 0;
      $data["lastTotalUserCount"] = 0;
    }

    $newSession = mt_rand();
    $data["session"] = $newSession;

    $isGood = $db->query("update frdUserData set session=$newSession where privateId = $id");
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }
  $db->query("commit");
  $keyAndIv = formatTo16String($session);
  try {
    $data = encrypt( $keyAndIv, json_encode($data), $keyAndIv );
  }
  catch(Exception $e) {
    $data = 0;
  }
  echo $data;
?>
