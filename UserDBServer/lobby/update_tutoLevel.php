<?php
  // 튜토리얼 진행단계를 업데이트 요청.
  include_once("../myAes.php");
  $id = $_REQUEST["id"];
  $tutoLevel = $_REQUEST["tutoLevel"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }
  
  $db->query("set autocommit=0");
  $query = sprintf("select tutoLevel from frdUserData where privateId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }
  $row = $res->fetch_assoc();

  if ($res->num_rows <= 0) {
    echo 1;
    $db->close();
    return;
  }

  $data["tutoLevel"] = $tutoLevel;
  $query = sprintf("update frdUserData set tutoLevel=%d where privateId = '%s'", $tutoLevel, $id);
  $isGood = $db->query($query);
  if ($isGood == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $res->close();
  $db->query("commit");
  $db->close();
  

  echo json_encode($data);

?>
