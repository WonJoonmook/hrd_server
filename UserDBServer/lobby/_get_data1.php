<?php
  include_once("../myAes.php");

  $data = array();
  $data["error"] = 0;

  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    return;
  }
  else {
    $data["userNeedExp"] = $redis->lrange('userNeedExp1',0, -1);
    $data["userMaxHeart"] = $redis->lrange('userMaxHeart1',0, -1);

    $data["checkAttendRewards"] = $redis->lrange('checkAttendRewards'.(date('n')),0, -1); // 0이 붙지 않는 월 1~12

    $curTime = time();
    $minTime = date('ymdH', $curTime-86399); // -24h
    $maxTime = date('ymdH', $curTime+86400); // +24h
    $data["curTime"] = $curTime;
    $data["descendTT"] = $redis->zrangebyscore('descendTT', $minTime, $maxTime);

    $data["event"] = $redis->zrange('event', 0, -1);

  //  $data["bannerUrl"] = "ftp://ftp.hnjgames.com/";
    $data["bannerUrl"] = "http://14.49.38.192:3351/banners/";
    $data['loadingCount']= 9;

    //event
    $data["halloween"] = "10,25,11,06";
    // ~event
    
    $redis->close();
  }

  echo json_encode($data);
?>
