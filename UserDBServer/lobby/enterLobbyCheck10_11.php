<?php

  $attendanceReward["type"] = array( 5000, 5005, 5004, 5002, 1497, 5004, 110000 );
  $attendanceReward["count"] = array( 1000, 50, 20, 3, 5, 50, 1 );
  $mission = array( 0x2, 0x8, 0x20, 0x80, 0x200, 0x800, 0x2000, 0x8000, 0x20000, 0x80000 );

  function UniqueRandomNumbersWithinRange($min, $max, $quantity) {
      $numbers = range($min, $max);
      shuffle($numbers);
      return array_slice($numbers, 0, $quantity);
  }

  function genDailyMission(&$everydayQuest) {
    $everydayQuest = 0;

    global $mission;

    $arr = uniqueRandomNumbersWithinRange(0, 9, 5);

    for ($i = 0; $i < 5; $i++) {
      $everydayQuest |= $mission[$arr[$i]];
    }
  }


  include_once("../myAes.php");

  $session = $_REQUEST["session"];
  $userId = $_REQUEST["userId"];


  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }

  $db->query("set autocommit=0");

  # check session
  $query = "select session, curPId from frdUserData where privateId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;$db->query("rollback");$db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;
      $data["curPId"] = (int)$row["curPId"]+1;
      $query = "update frdUserData set curPId = curPId+1 where privateId = $userId";
      $res = $db->query($query);
      if ($res == false) {
        echo 0;$db->query("rollback");$db->close();
        return;
      }
    }
    else {
      // addBlacklist($id, "buy_chest_session");
      echo 1;
      $db->query("rollback");
      $db->close();
      return;
    }
  }

  $query = "select * from frdMonthlyCard where userId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows == 0) {
    $monthlyCardData["hasCard"] = false;
  }
  else {
    $row = $res->fetch_assoc();
    $startTime = $row["startTime"];
    $lastRecvDay = $row["lastRecvDay"];
    $duration = $row["duration"];

    $now = time();
    $gap = $now - $startTime;
    $gapDay = ceil($gap / 86400);

    if ($gapDay > $duration) {
      $monthlyCardData["hasCard"] = false;
    }
    else {
      if ($gapDay > $lastRecvDay) {
        #send present
        $postEndTime = $startTime + 86400 * ($gapDay);
        $postSendId = $gapDay + 100;

        $query = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) ";
        $query .= " values ($userId, $postSendId, 5004, 20, $postEndTime), ";  // jewel
        $query .= "($userId, $postSendId, 5003, 20, $postEndTime)"; // heart

        $res = $db->query($query);
        if ($res == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        #update table
        $query = "update frdMonthlyCard set lastRecvDay = $gapDay where userId = $userId";

        $res = $db->query($query);
        if ($res == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $monthlyCardData["GiftCount"] = 2;
      }

      $monthlyCardData["hasCard"] = $gapDay < $duration;
    }
  }

  $data["monthlyCardData"] = $monthlyCardData;

  $query = "select checkAttendanceVal, checkAttendancePointer, everydayQuest from frdUserData where privateId = '$userId'";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $row = $res->fetch_assoc();
  $lastEnterLobbyTime = (int)$row["checkAttendanceVal"];
  $attendanceCount = (int)($row["checkAttendancePointer"]+1);
  $everydayQuest = (int)($row["everydayQuest"]);
  $missionData["everydayQuest"] = $everydayQuest;

  $now = time();

  if ($lastEnterLobbyTime < 1467298800) { // 복귀 유저 보상
    $welcomebackEventRewardType = array(505, 504, 504, 504, 5004, 5000, 5006);
    $welcomebackEventRewardCount = array(10, 10, 10, 10, 200, 20000, 500);
    for ($i = 0; $i < count($welcomebackEventRewardType); $i++) {
      $rewardType = $welcomebackEventRewardType[$i];
      $rewardCount = $welcomebackEventRewardCount[$i];
      $query = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) value ($userId, 10, $rewardType, $rewardCount, $now)";
      $res = $db->query($query);
      if ($res == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
    }
    $data["welcomeback"] = 1;
  }

  // if ($lastEnterLobbyTime < 1470044200) // 1470044200 2016/08/01 18:36:40
  if ($lastEnterLobbyTime < 1470076200) // 1470044200 2016/08/02 03:30:00
  {
    $attendanceCount = 1;
  }

  $prevDate = date("j:n:Y", $lastEnterLobbyTime);
  $prevDateArr = explode(":", $prevDate);
  $curDate = date("j:n:Y", $now);
  $curDateArr = explode(":", $curDate);

  $isDateDiff = true;
  if ( ( intval($prevDateArr[0]) == intval($curDateArr[0]) )
    && ( intval($prevDateArr[1]) == intval($curDateArr[1]) )
    && ( intval($prevDateArr[2]) == intval($curDateArr[2]) ) ) {
      $isDateDiff = false;
  }

  if ($attendanceCount > 7) // 7일마다 초기화
    $attendanceCount = 1;

  if ($isDateDiff) { // 출석 보상 지급 & 미션 초기화
    genDailyMission($everydayQuest);

    $query = "update frdUserData set checkAttendanceVal = $now, checkAttendancePointer = $attendanceCount, everydayQuest = $everydayQuest  where privateId = $userId";
    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    $rewardType = $attendanceReward["type"][$attendanceCount - 1];
    $rewardCount = $attendanceReward["count"][$attendanceCount - 1];
    if ( ($rewardType>=1000&&$rewardType<1500) || ($rewardType>=10000&&$rewardType<10500) ) {
      $query = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) value ";
      for ( $ii=0; $ii<$rewardCount-1; $ii++ )
        $query .= "($userId, 2, $rewardType, 1, $now),";
      $query .= "($userId, 2, $rewardType, 1, $now)";
    }
    else {
      $query = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) value ($userId, 2, $rewardType, $rewardCount, $now)";
    }
    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    // 신전 일일 클리어 횟수 초기화
    $query = "update frdTempleClearCount set dailyCount = 0 where userId = $userId";
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }

    $missionData["isInitialize"] = true;
    $missionData["everydayQuest"] = $everydayQuest;
  } else {
    $attendanceCount--;
  }
  $missionData["attendanceCount"] = $attendanceCount;
  $missionData["attendanceRewardData"] = $attendanceReward;
  $data["missionData"] = $missionData;

  $query = "update frdUserData set session = $newSession where privateId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    $db->query("rollback");
    $db->close();
    return 0;
  }

  $db->query("commit");
  $db->close();

  // echo json_encode($data);
  // echo "<br/>";
  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
