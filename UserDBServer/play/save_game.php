<?php
  // 유저의 lastTime 값 업데이트 요청.
  include_once("../myAes.php");
  $length = $_REQUEST["length"];
  $id = $_REQUEST["id"];

  $data = array();
  $data["a"] = 1;
  
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");

  $query = sprintf("select userId from saveDatas where userId = %d", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ( $res->num_rows > 0 ) {
    $query = sprintf("update saveDatas set length=%d where userId = %d", $length, $id);
    $isGood = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }
  else {
    $query = sprintf("insert into saveDatas values(%d, %d)",$id, $length);
    $isGood = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }

  $db->query("commit");  
  $db->close();

  echo json_encode($data);

?>
