<?php
	// 특성 레벨업 요청입니다.
	// 특성포인트를 사용하여 특정 특성을 레벨업 합니다.	
  include_once("../myAes.php");
  $id = $_REQUEST["id"];
  $userId = $_REQUEST["userId"];
  $val = $_REQUEST["val"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  
  $db->query("set autocommit=0");

	$query = sprintf("select isSuccess from frdEffectForPreCheckRewards where userId=%d and type=%d",$userId, $id);
	$res = $db->query($query);
	if ($res == false) {
		echo 0;
	  $db->query("rollback");
	  $db->close();
	  return;
	}

	if ( $res->num_rows <= 0) {
		echo 1;
		$db->query("rollback");
	 	$db->close();
	}

	if ( is_null($val) )
    	$query = sprintf("update frdEffectForPreCheckRewards set isSuccess=1 where userId=%d and type=%d",$userId, $id);
    else
    	$query = sprintf("update frdEffectForPreCheckRewards set val=val+%d, isSuccess=1 where userId=%d and type=%d", $val, $userId, $id);

	$isGood = $db->query($query);
	if ($isGood == false) {
	  echo 0;
	  $db->query("rollback");
	  $db->close();
	  return;
	}

	$db->query("commit");
	$db->close(); 


  	echo 1;

?>
