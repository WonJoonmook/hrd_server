<?php
  // 유저의 lastTime 값 업데이트 요청.
  include_once("../myAes.php");
  $length = $_REQUEST["length"];
  $id = $_REQUEST["id"];

  $data = array();
  $data["a"] = 1;
  
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");

  $query = sprintf("select length from saveDatas where userId = %d", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ( $res->num_rows > 0 ) {
    $row = $res->fetch_assoc();
    $savedLength = (int)$row["length"];
    if ( $savedLength !== (int)$length ) {
      echo 10;
      addBlacklist($id, "save_Differ");
      $db->query("rollback");
      $db->close();
      return;
    }

    $query = sprintf("update saveDatas set length=0 where userId = %d", $id);
    $isGood = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }
  else {
    echo 10;
    addBlacklist($id, "save_NoData");
    $db->query("rollback");
    $db->close();
    return;
  }

  $db->query("commit");  
  $db->close();

  echo json_encode($data);

?>
