<?php
  // 고난의탑 스테이지 게임오버 후 요청합니다.
  // 보상, 경험치 등이 계산 처리되고 클라에 뿌려줍니다.
  include_once("../globalDefine.php");
  include_once("../myAes.php");
  include_once("../getReward2.php");
  include_once("../getBonus12_1.php");

  $session = $_REQUEST["session"];
  $isClear = $_REQUEST["isClear"];
  $id = $_REQUEST["id"];
  $stageId = $_REQUEST["stageId"];
  $lastWave = $_REQUEST["lastWave"];
  $mainSkillType = $_REQUEST["mainSkillType"];
  $accuSoul = $_REQUEST["accuSoul"];
  $accuDarkSoul = $_REQUEST["accuDarkSoul"];
  $seconds = $_REQUEST["seconds"];
  $lastFlowTime = $_REQUEST["lastFlowTime"];
//  $_REQUEST["characId0"] ~ $_REQUEST["characId3"]
//  $_REQUEST["characRank0"] ~ $_REQUEST["characRank3"]
//  $_REQUEST["weaponId".$idxJ]
//  $_REQUEST["weaponIdx".$idxJ]
//  $_REQUEST["weaponStatus".$idxJ]
//  $_REQUEST["climbTopData".$idxJ];

  $data = array();
  $data["error"] = 0;
  

  function IsSuccess($conditionType, $lastWave, $std, $seconds, $isClear) {

    switch($conditionType) {
    case 0:
      $value = (int)$seconds;
      if ( $std < $value || !$isClear )
        return false;
      else
        return true;
    
    case 4:
      if ( $lastWave >= $std )
        return true;
      else
        return false;

    case 5:
      if ( $isClear > 0 )
        return true;
      else
        return false;

    case 6:
      return true;
    default:
      return false;
    }
  }


  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }

  $db->query("set autocommit=0");
  $idx=0;
  $isHack = $_REQUEST["isHack"];
  if ( $isHack > 0 ) {
    addBlacklist($id, "hack_Data");
    echo 1;
    $db->close();
    return;
  }

  if ( $lastWave > 100 ) {  //check hack
    $query = sprintf("select * from frdLastPlayTime where userId = %d", $id);
    $res = $db->query($query);
    if ($res == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
    if ( $res->num_rows <= 0 ) {
      $query = sprintf("insert into frdLastPlayTime values (%d, %d, %d, %d, %d)", $id, time(), 0, $stageId, 0);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
    }
    else {
      $row = $res->fetch_assoc();
      $curTime = time();
      if ( $curTime - (int)$row["lastTime2"] < 300 ) { // 5분
        $one = $curTime - (int)$row["lastTime1"];
        $two = (int)$row["lastTime1"] - (int)$row["lastTime2"];
        addBlacklist($id, "hackSave_".$one."_".$two);
        echo 1;
        $db->close();
        return;
      }
      $query = sprintf("update frdLastPlayTime set lastTime1=%d, lastTime2=%d, lastStage1=%d, lastStage2=%d where userId=%d"
                      ,$curTime, (int)$row["lastTime1"], $stageId, (int)$row["lastStage1"], $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
    }
  }
  

        
  $redis = openRedis();
  if ( $redis == false ) {
    addBlacklist($id, "get_climbReward_openRedis");
    echo 0;
    $db->close();
    return;
  }

  $needHeart = $redis->hget('stageHeart_New', 0);
  $data["needHeart"] = $needHeart;

  // Log ~
  $query = sprintf("select id from frdLogPlay where id = '%s' and stageId=0", $id);
  $sres = $db->query($query);
  if ($sres == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($sres->num_rows <= 0) {
    $query = sprintf("insert into frdLogPlay values ('%s', 0, %d, '%s', 1, 0)", $id, $lastWave, $seconds);
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }   
  else {
    $query = sprintf("update frdLogPlay set totalWave=totalWave+%d, totalSecs=totalSecs+%d, playCount=playCount+1 where id='%s' and stageId=0", $lastWave, $seconds, $id);
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
  }
  $sres->close();
  // ~ Log

  if ( ($lastWave > 10) && (($seconds/$lastWave) <= 2) ) {
    addBlacklist($id, "hack_TimeAndWave");
    echo 1;
    $db->close();
    return;
  }


  $query = sprintf("select name, exp, heart, ticket, artifactIdPointer, weaponIdPointer, startTime, givedHeartCount, session from frdUserData where privateId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false || $res->num_rows <= 0) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }
  
  $row = $res->fetch_assoc();
  if ( $row["session"] !== $session ) {
    addBlacklist($id, "get_climbRewards_session");
    echo 1;
    $db->query("rollback");
    $db->close();
    return;
  }

  $newSession = mt_rand();
  $data["session"] = $newSession;
  
  $addHeartCount=0;
  $addTotalHeartCount = getAddHeartCount($db, $redis, $id, $row["exp"], $row["startTime"], $row["givedHeartCount"], $row["heart"], $addHeartCount);
  if ( $addTotalHeartCount < 0 ) {
    echo 0;$db->query("rollback");$db->close();
    return;
  }
  
  $resultHeart = $row["heart"] + $addHeartCount;
  $query = sprintf("update frdUserData set heart=%d, givedHeartCount=%d, session=%d where privateId='%s'", 
      $resultHeart, $addTotalHeartCount, $newSession, $id);

  $isGood = $db->query($query);
  if ($isGood == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $data["heart"] = $resultHeart;
  $data["givedHeartCount"] = $addTotalHeartCount;
  $data["time"] = time();

  $weaponIdPointer = $row["weaponIdPointer"];
  $artifactIdPointer = $row["artifactIdPointer"];



  $resultRewardType = array();
  $rankRedis = openRankRedis($redis);
  if ( $rankRedis !== 0 ) {
    $userName = $row["name"];
    $preTotalScore = (int)($rankRedis->zscore('climbRank_New', $userName));

    $totalSkipTime = (int)$_REQUEST["climbTopData0"];
    if ( (int)$lastWave > 300 ) {
      if ( $isClear > 0 )
        $lastWave++;
      $totalSkipTime -= $lastFlowTime;
    }
    else
      $totalSkipTime += $lastFlowTime;
    
    $bonusPercent = 0; // totalSkipTime, compose, kill, destroyStone, quest
    for ( $i=0; $i<5; $i++ ) {
      $val = (int)$_REQUEST["climbTopData".$i];
      $climbTopData[] = $val;
      if ( $i !== 0 )
        $bonusPercent += $val;
    }

    $query = sprintf("select val from frdEffectForEtc where userId = %d and type=%d", $id, $GLOBALS['$ABILL_BonusScore_ClimbTop']);
    $sres = $db->query($query);
    if ($sres == false) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return;
    }
    if ( $sres->num_rows > 0 ) {
      $srow = $sres->fetch_assoc();
      $abilBonusPercent = round($srow["val"]/10);
      $bonusPercent += $abilBonusPercent;
    }

    $levelPercent = (int)(FindLevel(50, $redis, 0, $redis->llen('userNeedExp1')-1, $row["exp"])*100);
    if ( $bonusPercent > 5000 ) {
      $bonusPercent = 5000;
    }

    $waveScore = 1000000*$lastWave;
    if ( $lastWave < 1 )
      $lastWave = 1;
    
    $timeScore = round($totalSkipTime * $totalSkipTime / $lastWave);
    $bonusScore = round($timeScore * ($levelPercent + $bonusPercent)*0.0001);
    $totalScore = $waveScore + $timeScore + $bonusScore;
//  $rewardScore = $lastFlowTime*150;
    

    $totalScore = round($totalScore);

    $data["totalScore"] = $totalScore;

    if ( $totalScore > $preTotalScore ) {
      $inform[] = $row["exp"];
      for ( $i=0; $i<4; $i++ ) {
        $inform[] = $_REQUEST["characId".$i];
        $inform[] = (int)$_REQUEST["characRank".$i];
      }
      for ( $i=0; $i<8; $i++ ) {
        $inform[] = $_REQUEST["weaponId".$i];
        $inform[] = (int)$_REQUEST["weaponStat".$i];
      }
      $inform[] = $lastFlowTime; //25 ~ 33, 34
      $inform[] = $lastWave;
      $inform[] = $seconds;
      for ( $i=0; $i<5; $i++ ) {
        $inform[] = (int)$climbTopData[$i];
      }
      $inform[] = $levelPercent;
      if ( !is_null($abilBonusPercent) ) {
        $inform[] = $abilBonusPercent;
      }

      $time = time() + 9*3600 - 24*3600*4-5*3600;
      $totalWeeks = (int)($time / 604800);
      $weekVal = (int)600 + ((int)($totalWeeks%5));
      
      $rankWriteRedis = new Redis();
      $rankWriteRedis->connect('172.27.0.196', 6379, 2.5);
      if ( (int)$weekVal == (int)$stageId ) {    
        $rankWriteRedis->zadd('climbRank_New', $totalScore, $userName);
        $rankWriteRedis->zadd('climbRank_Resource', $totalScore, ($mainSkillType.", ".$accuDarkSoul.", ".$accuSoul));
        $rankWriteRedis->hset('climbInform', $userName, json_encode($inform));
      }

      $data["myRank"] = (int)$rankWriteRedis->zrevrank('climbRank_New', $userName);
      $data["userCount"] = $rankRedis->zcard('climbRank_New')-1;
      $data["climbDatas"] = $inform;
      $rankWriteRedis->close();
    }
    else {
      $inform[] = $lastFlowTime; //25 ~ 33, 34
      $inform[] = $lastWave;
      $inform[] = $seconds;
      for ( $i=0; $i<5; $i++ ) {
        $inform[] = (int)$climbTopData[$i];
      }
      $inform[] = $levelPercent;
      if ( !is_null($abilBonusPercent) ) {
        $inform[] = $abilBonusPercent;
      }
      $data["climbDatas"] = $inform;
    }
    $rankRedis->close();
  }
  else {
    echo 0;
    $db->close();
    $redis->close();
    return;
  }



  // ----------------- reward set start ----------------------
  $json = json_decode($redis->hget('stageReward_New', 0), true);

  $length = count($json["clearConditionType"]);
  for ( $i=0; $i<$length; $i++ ) {
    $isSuccess[$i] = IsSuccess($json["clearConditionType"][$i], $lastWave, $json["clearConditionVal"][$i], $seconds, $isClear);

  }

  $length = count($json["rewardId"]);
  for ( $i=0; $i<$length; $i++ ) {
    $type = $json["rewardId"][$i];
    if ( $type == 5005 )   //medal
      $json["rewardPercent"][$i] += (int)($_REQUEST["bonus3"]);
  }



  $rewardItemIdxPointer=0;
  $lastRIPointer= $json["clearConditionIdx"][0];
 
  for ( $i=0; $i<$length; $i++ ) {
    $conditionIdx = $json["clearConditionIdx"][$i];
    if ( $isSuccess[$conditionIdx] && (rand(0, 999) < (int)($json["rewardPercent"][$i] * (1 + $difficulty*0.5))) ) {  //receive
      $rewardType = $json["rewardId"][$i];
      $rewardCount = $json["rewardCount"][$i];

      if ( $rewardType > 500 && $rewardType < 507 ) { //random soulstone
        $min = $redis->lindex('characMinIdx', $rewardType-500);
        $max = $redis->lindex('characMaxIdx', $rewardType-500);
        $rewardType = rand($min, $max);
      }
      else if ( $rewardType > 1500 && $rewardType < 1508 ) {  //random weapon
        $min = $redis->lindex('weaponMinIdx', $rewardType-1500)+1000;
        $max = $redis->lindex('weaponMaxIdx', $rewardType-1500)+1000;
        while ( $rewardCount > 1 ) {
          $rewardType = rand($min, $max);
          $resultRewardType[] = $rewardType;
          $resultRewardCount[] = 1;
          $rewardCount--;
        }
        $rewardType = rand($min, $max);
      }
      else if ( $rewardType > 10500 && $rewardType < 10508 ) {  //random magic
        $min = $redis->lindex('magicMinIdx1', $rewardType-10500)+10000;
        $max = $redis->lindex('magicMaxIdx1', $rewardType-10500)+10000;
        while ( $rewardCount > 1 ) {
          $rewardType = rand($min, $max);
          $resultRewardType[] = $rewardType;
          $resultRewardCount[] = 1;
          $rewardCount--;
        }
        $rewardType = rand($min, $max);
      }
      else if ( $rewardType == 5000 ) {    // gold
        if ( $conditionIdx == 0 )
          $rewardCount *= $lastWave;

        $rewardCount += (int)($rewardCount * $_REQUEST["bonus0"] / 1000);
      }
      else if ( $rewardType == 5001 ) {    // exp
        if ( $conditionIdx == 0 ) 
          $rewardCount *= $lastWave;
        
        $rewardCount += (int)($rewardCount * $_REQUEST["bonus1"] / 1000);
      //  $resultExp += $rewardCount;
      }
      //  5002 ticket   5003 heart    5004 jewel
      else if ( $rewardType == 5005 ) {    // medal
        if ( rand(0,999) < (int)($_REQUEST["bonus2"]) )
          $rewardCount *= 2;
      }
  
      if ( $lastRIPointer < $conditionIdx ) {
        $lastRIPointer = $conditionIdx;
        $rewardItemIdxPointer++;
      }
      
      if ( in_array($rewardType, $resultRewardType) && !($rewardType>=1000&&$rewardType<2000) && !($rewardType>=10000&&$rewardType<11000)) {
        $tempIdx = array_search($rewardType, $resultRewardType);
        $resultRewardCount[$tempIdx] += $rewardCount;
      }
      else {
        $resultRewardType[] = $rewardType; 
        $resultRewardCount[] = $rewardCount;
      }
    }
  }

  $ticketPercent = 4000;
  getBonus($db, $redis, null, $stageId, $isClear, $idx, $resultRewardType, $resultRewardCount, $ticketPercent, $id);


  // ----------------- reward set complete ----------------------



  $data["rewardLength"] = 0;
  $data["resultRewardType"] = $resultRewardType;
  $data["resultRewardCount"] = $resultRewardCount;

  for ( $i=0; $i<8; $i++ )
    $weaponIdxs[$i] = $_REQUEST["weaponIdx".$i];
  if ( !is_null($_REQUEST["subWeapon"]) ) {
    for ( $i=8; $i<16; $i++ ) {
      $weaponIdxs[$i] = $_REQUEST["weaponIdx".$i];
    }
  }

  $data["resultExp"] = 0;
  if ( 0 == getReward($db, $data, $resultRewardType, $resultRewardCount, $id, $weaponIdPointer, $artifactIdPointer, $weaponIdxs) )
      return;

//  ----- event -----
//      $query = sprintf("insert into frdUserPost values (0, '%s', 0, 5003, %d, %d)", $id, (int)ceil($needHeart/2), time());
//      do {
//        $isGood = $db->query($query);
//      }while($isGood == false);
//  -----------------


  $res->close();
  $redis->close();
  $db->query("commit");
  $db->close();
  
  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
