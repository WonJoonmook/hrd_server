<?php
  include_once("../myAes.php");

  $data = array();
  $data["error"] = 0;

  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    return;
  }
  else {
    $data["climbRankRewards"] = $redis->lrange('climbRankRewards',0, -1);
    $redis->close();
  }

  echo json_encode($data);
?>
