<?php
  // 고난의탑에서 본인 랭크 +- 2(총 5)명의 랭크 정보를 얻어옵니다.
  include_once("../myAes.php");


  $name = $_REQUEST["name"];
  $min = $_REQUEST["min"];
  $max = $_REQUEST["max"];
  
  $data = array();
  $data["error"] = 0;
  
  $redis = openRedis();
  if ( $redis <= 0 ) {
    echo 0;
    return;
  }

  $rankRedis = openRankRedis($redis);
  if ( $rankRedis <= 0 ) {
    echo 0;
    $redis->close();
    return;
  }
  $redis->close();
  

  if ( $name !== null ) {
    if ( !($data["myRank"] = $rankRedis->zrevrank('climbRank_New', $name)) ) {
      $data["myRank"] = -1;
      $data["totalUser"] = $rankRedis->zcard('climbRank_New')-1;

      echo json_encode($data);
      $rankRedis->close();
      return;
    }

    else {
      if ( $min < 0) {
        $min = $data["myRank"]-2;
        if ( $min < 1)
          $min = 1;
        $max = $min+5;
      }

      $data["totalUser"] = $rankRedis->zcard('climbRank_New')-1;

      $climbInform = json_decode($rankRedis->hget('climbInform', $name));

      $climbInformCount = count($climbInform);
      for ( $i=25; $i<$climbInformCount; $i++ ) {
        $data["climbInform"][] = (int)$climbInform[$i];
      }
    }
  }

  $data["names"] = $rankRedis->zrevrange('climbRank_New', $min, $max);
  
  $length = count($data["names"]);
  for ( $i=0; $i<$length; $i++ ) {
    $data["score"][] = $rankRedis->zscore('climbRank_New', $data["names"][$i]);
    $data["jsonInfo"][] = $rankRedis->hget('climbInform', $data["names"][$i]);
  }
  $rankRedis->close();
  echo json_encode($data);
?>
