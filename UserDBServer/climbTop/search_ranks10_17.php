<?php
  // 고난의탑 랭킹 검색
  include_once("../myAes.php");


  $name = $_REQUEST["name"];
  
  $data = array();
  $data["error"] = 0;
  
  $redis = openRedis();
  if ( $redis <= 0 ) {
    echo 0;
    return;
  }

  $rankRedis = openRankRedis($redis);
  if ( $rankRedis <= 0 ) {
    echo 0;
    $redis->close();
    return;
  }
  $redis->close();

  if ( !($targetRank = $rankRedis->zrevrank('climbRank_New', $name)) ) {

  }
  else {
    $min= $targetRank-2;
    if ( $min < 1)
      $min = 1;

    $data["min"] = $min;
    $data["names"] = $rankRedis->zrevrange('climbRank_New', $min, $targetRank+2);

    $length = count($data["names"]);
    for ( $i=0; $i<$length; $i++ ) {
      $data["score"][] = $rankRedis->zscore('climbRank_New', $data["names"][$i]);
      $data["jsonInfo"][] = $rankRedis->hget('climbInform', $data["names"][$i]);
    }
  }



  $rankRedis->close();
  echo json_encode($data);
?>
