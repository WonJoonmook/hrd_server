<?php

  $mission = array( 0x2, 0x8, 0x20, 0x80, 0x200, 0x800, 0x2000, 0x8000, 0x20000, 0x80000 );
  $missionComplete = array( 0x1, 0x4, 0x10, 0x40, 0x100, 0x400, 0x1000, 0x4000, 0x10000, 0x40000 );
	$missionRewardType = array(5002, 5000, 5000, 5004, 5006, 5000, 5006, 5000, 5004, 5006);
	$missionRewardCount = array(  3, 1500, 1500,    5,  100, 1500,  100, 1500,    5,  100);

  include_once("../myAes.php");

  $session = $_REQUEST["session"];
  $userId = $_REQUEST["userId"];
  $missionIdx = $_REQUEST["missionIdx"];

  function isBonusHeartTime($time) {
    if ( ( $time >= 12 && $time < 14 ) || ( $time >= 18 && $time < 20 ) ) {
      return true;
    }
    return false;
  }

  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }

  $db->query("set autocommit=0");

  # check session
  $query = "select session from frdUserData where privateId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;
    }
    else {
      // addBlacklist($id, "buy_chest_session");
      echo 1;
      $db->query("rollback");
      $db->close();
      return;
    }
  }

  $query = "select checkAttendanceVal, checkAttendancePointer, everydayQuest from frdUserData where privateId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $row = $res->fetch_assoc();
  $lastEnterLobbyTime = (int)$row["checkAttendanceVal"];
  $attendanceCount = (int)($row["checkAttendancePointer"]+1);
  $everydayQuest = (int)($row["everydayQuest"]);
  $missionData["everydayQuest"] = $everydayQuest;

  $now = time();

  $prevDate = date("j:n:Y", $lastEnterLobbyTime);
  $prevDateArr = explode(":", $prevDate);
  $curDate = date("j:n:Y", $now);
  $curDateArr = explode(":", $curDate);

  $isDateDiff = true;
  if ( ( intval($prevDateArr[0]) == intval($curDateArr[0]) )
    && ( intval($prevDateArr[1]) == intval($curDateArr[1]) )
    && ( intval($prevDateArr[2]) == intval($curDateArr[2]) ) ) {
      $isDateDiff = false;
  }

  $nowTime = intval(date("H", $now));

  if ($isDateDiff) { // 출석 보상 지급 & 미션 초기화
    $data["missionShouldRefreshed"] = true;
  } else {
    if ( ($missionIdx == 29) || ($missionIdx == 30) || ($missionIdx == 28) ) {  // 무료 하트 && 여신의 축복

      $index = 1 << ($missionIdx);

      if ( ( ($missionIdx == 29) || ($missionIdx == 30) ) && !isBonusHeartTime($nowTime) ) { // 무료 하트 시간 체크
        $data["errormsg"] = "time miss match";
        $data["missionShouldRefreshed"] = true;
      }
      else if ( ($everydayQuest & $index) === $index ) {  // 이미 받은 선물인지 체크
        $data["errormsg"] = "already take a present!";
        $data["missionShouldRefreshed"] = true;
      }
      else {
      	$query = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) ";
        if ( ($missionIdx == 29) || ($missionIdx == 30) )
          $query .= "value ($userId, 1, 5003, 15, $now)"; // heart
        else if ( $missionIdx == 28 ) {
          $query .= "value ($userId, 1, 110000, 1, $now)"; // 무지개빛 룬
        }

        $res = $db->query($query);
        if ($res == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
        $everydayQuest |= $index;
        $addedQuery = ", everydayQuest = $everydayQuest";
        $data["everydayQuest"] = $everydayQuest;
      }
    }
    else if ( ( ($everydayQuest & $mission[$missionIdx]) === $mission[$missionIdx] ) && ( ($everydayQuest & $missionComplete[$missionIdx]) == 0 ) ) { //활성화 미션인지 체크 && 완료하지않은 미션인지 체크
      $everydayQuest |= $missionComplete[$missionIdx];

      $rewardType = $missionRewardType[$missionIdx];
      $rewardCount = $missionRewardCount[$missionIdx];
    	$query = "insert into frdUserPost (recvUserId, sendUserId, type, count, time) value ($userId, 1, $rewardType, $rewardCount, $now)"; // 보상

      $res = $db->query($query);
      if ($res == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      $addedQuery = ", everydayQuest = $everydayQuest";
      $data["everydayQuest"] = $everydayQuest;
    }
    else {  // 데이터가 조작되었음
      $data["missionShouldRefreshed"] = true;
    }
  }

  $query = "update frdUserData set session = $newSession $addedQuery where privateId = $userId";
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  $db->query("commit");
  $db->close();

  // echo json_encode($data);
  // echo "<br/>";
  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
