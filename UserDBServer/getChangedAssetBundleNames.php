<?php

  $now = time();
  $currentMonthDay = date("nd", $now);

  if ( $currentMonthDay > 1024 && $currentMonthDay < 1107 )
  {
    $changedAssetBundles["LobbyBG"] = "lobby_bg_halloween";
    $changedAssetBundles["Season2Mat0"] = "season2mat0_halloween";
    echo json_encode($changedAssetBundles);
  }
  else if ( $currentMonthDay > 1211 && $currentMonthDay < 1231 ) 
  {
    $changedAssetBundles["LobbyBG"] = "lobby_bg_christmas";
    $changedAssetBundles["Season2Mat0"] = "season2mat0_christmas";
    echo json_encode($changedAssetBundles);
  }
  else
  {
    echo "{}";
  }
?>
