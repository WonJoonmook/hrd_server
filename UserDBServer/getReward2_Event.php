<?php
  function FindLevel($count, $redis, $min, $max, $exp) {
    $count--;
    if ( $count <= 0 )
      return 0;

    if ( $min == $max )
      return $min;



    $center = (int)(($min+$max)>>1);

    $lowExp = $redis->lindex('userNeedExpAccu1', $center);
    $highExp = $redis->lindex('userNeedExpAccu1', $center+1);
  //  echo($lowExp." ".$highExp." ".$exp." \n");
    if ( $exp < $lowExp ) {
      return FindLevel($count, $redis, $min, $center, $exp);
    }
    else if ( $exp >= $highExp ) {
      return FindLevel($count, $redis, $center+1, $max, $exp);
    }
    else {  //find
      return $center+1;
    }
  }

  function getReward(&$db, &$data, $rewardTypes, &$rewardCounts, $userId, &$weaponIdPointer, &$artifactIdPointer, &$weaponIdxs) {
    $resultExp = 0;
    $length = count($rewardTypes);
    $redis = openRedis();
    if ( $redis == false ) {
      echo 0;
      $db->query("rollback");
      $db->close();
      return 0;
    }

    //event
    $eventD = (int)date('d');
    $eventH = (int)date('H');
    //

    for ( $ii=0; $ii<$length; $ii++ ) {
      $rewardType = $rewardTypes[$ii];
      $rewardCount = $rewardCounts[$ii];

      if ( $rewardType < 1000 ) {        //고정 캐릭터 소울스톤 지급

        if ( $rewardType > 500 ) {

          $minCIdx = $redis->lindex('characMinIdx', $rewardType-500);
          $maxCIdx = $redis->lindex('characMaxIdx', $rewardType-500);
          $rewardType = rand($minCIdx, $maxCIdx);
        }

        //event
        if ( $eventD == 4 && ($eventH >= 12 && $eventH < 14)  ) {
          $exceptArr = array(98,99,192,193,194,195,196,197,198,199,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,384,385,386,387,388,389);
          
          if ( !in_array((int)$rewardType, $exceptArr) ) {
            $rewardCount = (int)floor($rewardCount * 1.5);
            $rewardCounts[$ii] = $rewardCount;
          }
        }
        //

        $bundleIdx = ($rewardType>>3);
        $query = sprintf("select exps from frdCharacEvolveExps where userId='%s' and bundleIdx=%d",$userId, $bundleIdx);
        $sres = $db->query($query);
        if ($sres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return 0;
        }
        if ($sres->num_rows > 0) {
            $srow = $sres->fetch_assoc();
            $longData = $srow["exps"];
            $bitIdx = 8*($rewardType&7);
            $preExp = ( ($longData >> $bitIdx) & 0xff);

            if ( $preExp > 0 ) {
              $mask = $preExp;
              $mask <<= $bitIdx;
              $longData -= $mask;

              $mask = ($preExp + $rewardCount);
              if ( $mask > 255 )
                  $mask = 255;

              if ( $preExp < 10 && $mask >= 10 )
                $data["newCharac".$ii] = 1;

              $data["characId".$ii] = (int)$rewardType;
              $data["characExp".$ii] = $mask;

              $mask <<= $bitIdx;
              $resultData = ($longData | $mask);

              $query = sprintf("update frdCharacEvolveExps set exps='%s' where userId='%s' and bundleIdx=%d", $resultData, $userId, $bundleIdx);
              $isGood = $db->query($query);
              if ($isGood == false) {

                echo 0;
                $db->query("rollback");
                $db->close();
                $redis->close();
                return 0;
              }
              $data["characBundleIdx".$ii] = $bundleIdx;
              $data["characExps".$ii] = $resultData;
            }
            else {                    // yes Bundle, no Charac
              $mask = $rewardCount;
              if ( $preExp < 10 && $mask >= 10 )
                $data["newCharac".$ii] = 1;

              $mask <<= $bitIdx;
              $resultData = ($longData | $mask);

              $query = sprintf("update frdCharacEvolveExps set exps='%s' where userId='%s' and bundleIdx=%d", $resultData, $userId, $bundleIdx);
              $isGood = $db->query($query);
              if ($isGood == false) {
                echo 0;
                $db->query("rollback");
                $db->close();
                $redis->close();
                return 0;
              }
              $data["characId".$ii] = $rewardType;
              $data["characExp".$ii] = $rewardCount;
              $data["characBundleIdx".$ii] = $bundleIdx;
              $data["characExps".$ii] = $resultData;
            }
        }
        else {                      //no Bundle
            $mask = $rewardCount;
            if ( $mask >= 10 )
              $data["newCharac".$ii] = 1;

            $resultData = $mask << (8*($rewardType&7));
            $query = sprintf("insert into frdCharacEvolveExps values ('%s', %d, '%s')", $userId, $bundleIdx, $resultData);
            $isGood = $db->query($query);
            if ($isGood == false) {
              echo 0;
              $db->query("rollback");
              $db->close();
              $redis->close();
              return 0;
            }
            $data["characId".$ii] = $rewardType;
            $data["characExp".$ii] = $mask;
            $data["characBundleIdx".$ii] = $bundleIdx;
            $data["characExps".$ii] = $resultData;
        }
        $sres->close();

      }


      else if ( $rewardType < 1510 ) {    //보구 지급
        $val = $rewardType - 1000;
        if ( $val > 500 ) {

          $minWIdx = $redis->lindex('weaponMinIdx', $val-500);
          $maxWIdx = $redis->lindex('weaponMaxIdx', $val-500);
          $val = rand($minWIdx, $maxWIdx);

        }

        if ( $weaponIdPointer < 0 ) {
          $query = sprintf("select weaponIdPointer from frdUserData where privateId = %d", $userId);
          $ssres = $db->query($query);
          if ($ssres == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            $redis->close();
            return 0;
          }
          $ssrow = $ssres->fetch_assoc();
          $weaponIdPointer = $ssrow["weaponIdPointer"];
        }
        //보구 지급
        $stat = rand(0,10000);
        $query = sprintf("insert into frdHavingWeapons values (%d, '%s', %d, 0, %d)", $weaponIdPointer, $userId, $val, $stat);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return 0;
        }
        $data["weaponIdPointer".$ii] = $weaponIdPointer;
        $data["weaponId".$ii] = $val;
        $data["stat".$ii] = $stat;

        $weaponIdPointer++;

        $query = sprintf("update frdUserData set weaponIdPointer=%d where privateId='%s'", $weaponIdPointer, $userId);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return 0;
        }
      }

      else if ( $rewardType == 5000 ) {   //골드 지급
        //event
        if ( $eventD == 2 && ($eventH >= 12 && $eventH < 14)  ) {
          $rewardCount = (int)floor($rewardCount * 1.5);
          $rewardCounts[$ii] = $rewardCount;
        }
        if ( $eventD == 7 && ($eventH >= 12 && $eventH < 14)  ) {
          $rewardCount = (int)floor($rewardCount * 1.2);
          $rewardCounts[$ii] = $rewardCount;
        }
        //

        $addedGold = $rewardCount;
        $query = sprintf("update frdUserData set gold=gold+%d where privateId='%s'", $addedGold, $userId);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return 0;
        }
        $data["addedGold".$ii] = $addedGold;
      }

      else if ( $rewardType == 5001 ) {   //경험치 지급
        //event
        if ( $eventD == 4 && ($eventH >= 20 && $eventH < 22)  ) {
          $rewardCount = (int)floor($rewardCount * 1.5);
          $rewardCounts[$ii] = $rewardCount;
        }
        if ( $eventD == 7 && ($eventH >= 20 && $eventH < 22)  ) {
          $rewardCount = (int)floor($rewardCount * 1.5);
          $rewardCounts[$ii] = $rewardCount;
        }
        //

        $addedExp = (int)$rewardCount;
        $resultExp += $addedExp;
        $data["addedExp".$ii] = $addedExp;
      }

      else if ( $rewardType == 5002 ) {   //티켓 지급
        $addedTicket = $rewardCount;
        $query = sprintf("update frdUserData set ticket=ticket+%d where privateId='%s'", $addedTicket, $userId);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return 0;
        }
        $data["addedTicket".$ii] = $addedTicket;
      }

      else if ( $rewardType == 5003 ) {   //행동력 지급
        $addedHeart = $rewardCount;
        $query = sprintf("update frdUserData set heart=heart+%d where privateId=%d", $addedHeart, $userId);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return 0;
        }
        $data["addedHeart".$ii] = $addedHeart;
      }

      else if ( $rewardType == 5004 ) {   //보석 지급
        $addedJewel = $rewardCount;
        $query = sprintf("update frdUserData set jewel=jewel+%d where privateId='%s'", $addedJewel, $userId);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return 0;
        }
        $data["addedJewel".$ii] = $addedJewel;
      }

      else if ( $rewardType == 5005 ) {   //메달 지급
        //event
        if ( $eventD == 6 && ($eventH >= 12 && $eventH < 14)  ) {
          $rewardCount = (int)floor($rewardCount * 1.2);
          $rewardCounts[$ii] = $rewardCount;
        }
        //

        $addedMedal = $rewardCount;
        $query = sprintf("update frdUserData set skillPoint=skillPoint+%d where privateId='%s'", $addedMedal, $userId);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return 0;
        }

        $query = sprintf("update frdSkillPoints set accuSkillPoint=accuSkillPoint+%d where userId=%d", $addedMedal, $userId);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return 0;
        }
        $data["addedMedal".$ii] = $addedMedal;
      }

      else if ( $rewardType >= 10000 && $rewardType < 10510 ) {  //마법 지급
        $resultId = $rewardType-10000;
        if ( $resultId > 500 ) {

          $minMIdx = $redis->lindex('magicMinIdx1', $resultId-500);
          $maxMIdx = $redis->lindex('magicMaxIdx1', $resultId-500);
          $resultId = rand($minMIdx, $maxMIdx);

        }

        if ( $artifactIdPointer < 0 ) {
          $query = sprintf("select artifactIdPointer from frdUserData where privateId = %d", $userId);
          $ssres = $db->query($query);
          if ($ssres == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            $redis->close();
            return 0;
          }
          $ssrow = $ssres->fetch_assoc();
          $artifactIdPointer = $ssrow["artifactIdPointer"];
        }
        $query = sprintf("insert into frdHavingArtifacts values (%d, '%s', %d, 0, 0)", $artifactIdPointer, $userId, $resultId);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return 0;
        }
        $data["artifactIdPointer".$ii] = $artifactIdPointer;
        $data["artifactId".$ii] = $resultId;

        $artifactIdPointer++;
        $query = sprintf("update frdUserData set artifactIdPointer=%d where privateId='%s'", $artifactIdPointer, $userId);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return 0;
        }
      }

      else if ( $rewardType >= 100000 && $rewardType < 200000 ) {   //아이템 지급

        $query = sprintf("select itemCount from frdHavingItems where userId = %d and itemId = %d", $userId, $rewardType);
        $ssres = $db->query($query);
        if ($ssres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return 0;
        }

        if ( $ssres->num_rows > 0 ) {
          $ssrow = $ssres->fetch_assoc();
          $resultRewardCount = $ssrow["itemCount"] + $rewardCount;
          $query = sprintf("update frdHavingItems set itemCount=%d where userId=%d and itemId=%d", $resultRewardCount, $userId, $rewardType);
        }
        else {
          $query = sprintf("insert into frdHavingItems values (%d, %d, %d)", $userId, $rewardType, $rewardCount);
        }

        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          $redis->close();
          return 0;
        }

        $data["itemId".$ii] = $rewardType;
        $data["itemCount".$ii] = $rewardCount;

      }
    }


    if ( $resultExp > 0 ) {

      $weaponExp = (int)($resultExp/10);
      $wLen = count($weaponIdxs);
      for ( $iiJ=0; $iiJ<$wLen; $iiJ++ ) {
        $pId = $weaponIdxs[$iiJ];
        if ( $iiJ == 8 )
          $weaponExp = round($weaponExp*0.3);
        
        if ( $pId >= 0) {
          $query = sprintf("update frdHavingWeapons set exp=exp+%d where privateId=%d and userId='%s'", $weaponExp, $pId, $userId);
          $isGood = $db->query($query);
          if ($isGood == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            $redis->close();
            return 0;
          }
        }
      }

      $query = sprintf("select exp from frdUserData where privateId=%d", $userId);
      $sres = $db->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        $redis->close();
        return 0;
      }
      $srow = $sres->fetch_assoc();
      $userLevel = FindLevel(50, $redis, 0, $redis->llen('userNeedExp1')-1, $srow["exp"]);
      $curUserLevel = FindLevel(50, $redis, 0, $redis->llen('userNeedExp1')-1, $srow["exp"]+$resultExp);
      if ( $curUserLevel > $userLevel ) {
        $maxHeart = $redis->lindex('userMaxHeart1', $curUserLevel);
        $data["preHeart"] = $data["heart"];

        if ( $data["heart"] < $maxHeart ) {
          $query = sprintf("update frdUserData set heart=%d where privateId='%s'", $maxHeart, $userId);
          $isGood = $db->query($query);
          if ($isGood == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            $redis->close();
            return 0;
          }
          $data["curHeart"] = $maxHeart;
          $data["heart"] = $maxHeart;
        }
        else {
          $data["curHeart"] = $data["heart"];
        }
        $data["resultLevel"] = $curUserLevel;
        $data["preLevel"] = $userLevel;
      }

      $query = sprintf("update frdUserData set exp=exp+%d where privateId='%s'", $resultExp, $userId);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        $redis->close();
        return 0;
      }

      $data["resultExp"] = $resultExp;
    }

    $redis->close();
    return 1;
  }
?>
