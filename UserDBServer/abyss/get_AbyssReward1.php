<?php
  include_once("../myAes.php");
  include_once("../getReward2.php");
  $id = $_REQUEST["id"];
  $reqIdx = $_REQUEST["reqIdx"];

  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }
  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    return;
  }

  $data = array();
  $data["error"] = 0;

  $query = sprintf("select level, recvVal from frdAbyss where userId = %d", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }
  if ( $res->num_rows <= 0 ){
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }
  $row = $res->fetch_assoc();
  $level = $row["level"];
  $recvVal = $row["recvVal"];

  $compareVal = (1 << $reqIdx);
  if ( ($recvVal & $compareVal) > 0 ) {
    addBlacklist($id, "hack_GetAbyss");
    echo 1;
    $db->close();
    return;
  }

  $resultRecvVal = ($recvVal | $compareVal);
  $query = sprintf("update frdAbyss set recvVal=%d where userId = %d", $resultRecvVal, $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }
  

  $rewards = $redis->lindex('abyssRewards', $reqIdx);
  $rewardData = explode (",", $rewards);
  $rewardType[] = (int)$rewardData[0];
  $rewardCount[] = (int)$rewardData[1];

  $weaponIdPointer = -1;
  $artifactIdPointer = -1;
  $weaponIdxs = array(-1,-1,-1,-1,-1,-1,-1,-1);
  $data["resultExp"] = 0;
  if ( 0 == getReward($db, $data, $rewardType, $rewardCount, $id, $weaponIdPointer, $artifactIdPointer, $weaponIdxs) )
    return;

  $redis->close();

  
  echo json_encode($data);

?>
