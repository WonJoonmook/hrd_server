<?php
  include_once("../myAes.php");
  $id = $_REQUEST["id"];

  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }

  $data = array();
  $data["error"] = 0;
  $data["count"] = 30;

  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    return;
  }

  $data["abyssRewards"] = $redis->lrange('abyssRewards',0, -1);
  $redis->close();

  $query = sprintf("select level, recvVal from frdAbyss where userId = %d", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ( $res->num_rows > 0 ) {
    $row = $res->fetch_assoc();
    $data["level"] = $row["level"];
    $data["recvVal"] = $row["recvVal"];
  }
  else {
    $data["level"] = 1000;
    $data["recvVal"] = 0;
  }
  
  echo json_encode($data);
?>
