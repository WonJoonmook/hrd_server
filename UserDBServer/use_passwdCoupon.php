<?php
  // 알파 테스터 전용 쿠폰 처리 요청입니다.
  include_once("./myAes.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];
  $couponId = $_REQUEST["couponId"];
  $passwd = $_REQUEST["passwd"];


  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }

  $db->query("set autocommit=0");
  $query = sprintf("select session from frdUserData where privateId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;

      $query = sprintf("update frdUserData set session=%d where privateId='%s'", $newSession, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      $query = sprintf("select * from alphaCoupon where couponId = '%s' and passwd = '%s'", $couponId, $passwd);
      $sres = $db->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      if ($sres->num_rows <= 0) {
        $data["error"] = 2;           //없어
      }
      else {
        $query = sprintf("select purchaseTime from frdLogBuy where id = '%s' and productId = 'package_start'", $id);
        $ssres = $db->query($query);
        if ($ssres == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        if ($ssres->num_rows > 0) {
           $data["error"] = 4;        //스타터패키지 삿어
           $ssres->close();
        }
        else {
          
          $row = $sres->fetch_assoc();
          $isGet = $row["isUsed"];

          if ( $isGet > 0)
            $data["error"] = 3;         //썼어
          else {
            $rewardType = $row["rewardType"];
            $price = $row["price"];

            if ( $rewardType > 0 ) {
              $jewel = 200;
              $medal = 30;

              $jewel += 100*($rewardType-1);
              $medal += 10*($rewardType-1);
              if ( $rewardType == 8 )
                $jewel += 100;
            }

            if ( $jewel > 0 ) {
              $query = sprintf("insert into frdUserPost values (0, '%s', 4, 5004, %d, %d)",
                        $id, $jewel, time());
              $isGood = $db->query($query);
              if ($isGood == false) {
                echo 0;
                $db->query("rollback");
                $db->close();
                return;
              }
            }
            if ( $medal > 0 ) {
              $query = sprintf("insert into frdUserPost values (0, '%s', 4, 5005, %d, %d)",
                        $id, $medal, time());
              $isGood = $db->query($query);
              if ($isGood == false) {
                echo 0;
                $db->query("rollback");
                $db->close();
                return;
              }
            }


            if ( $price > 0 ) {
              if ( $price < 10000 )
                $price = $price + (int)($price*0.1);
              else if ( $price < 50000 )
                $price = $price + (int)($price*0.15);
              else if ( $price < 100000 )
                $price = $price + (int)($price*0.2);
              else
                $price = $price + (int)($price*0.3);

              $redis = openRedis();
              if ( $redis == false ) {
                echo 0;
                $db->close();
                return;
              }

              $jewelAmount = $redis->lrange('shopItemCount1',0, 4);
              $buyPriceStr = $redis->lrange('shopItemPrice', 0, 4);
              $redis->close();

              for ( $i=0; $i<5; $i++ ) {
                $splitData = explode (",", $buyPriceStr[$i]);
                $buyPrice[] = $splitData[1];
              }
              
              $totalJewelAmount=0;
              for ( $i=4; $i>=0; $i-- ) {
                while($price > $buyPrice[$i]) {
                  $price -= $buyPrice[$i];
                  $totalJewelAmount += $jewelAmount[$i];
                }
              }

              $query = sprintf("insert into frdUserPost values (0, '%s', 4, 5004, %d, %d)",
                        $id, $totalJewelAmount, time());
              $isGood = $db->query($query);
              if ($isGood == false) {
                echo 0;
                $db->query("rollback");
                $db->close();
                return;
              }
            }

            $query = sprintf("insert into frdUserPost values (0, '%s', 4, 193, 10, %d)", $id, time());
            $isGood = $db->query($query);
            if ($isGood == false) {
              echo 0;
              $db->query("rollback");
              $db->close();
              return;
            }

            $query = sprintf("update alphaCoupon set isUsed=1, usedPrivateId = '%s' where couponId = '%s' and passwd = '%s'", $id, $couponId, $passwd);
            $isGood = $db->query($query);
            if ($isGood == false) {
              echo 0;
              $db->query("rollback");
              $db->close();
              return;
            }
          }

        }
      //  $ssres->close();
      }
      $sres->close();

    }
    else {
      addBlacklist($id, "use_passwdCoupon");
      echo 1;
      $db->close();
      return;
    }
  }
  $res->close();
  $db->query("commit");
  $db->close();

  $keyAndIv = formatTo16String($session);

  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
