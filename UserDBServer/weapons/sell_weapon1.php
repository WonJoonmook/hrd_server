<?php
  // 유물 판매 처리 요청.
  include_once("../globalDefine.php");
  include_once("./_calculate2.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];
  $isExtraction = $_REQUEST["isExtraction"];

  $matPIds = $_REQUEST["matPIds"];
  $matLevels = $_REQUEST["matLevels"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");

  $query = sprintf("select gold, powder, session from frdUserData where privateId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;

      $idxArr = explode(";", $matPIds);
      $levelArr = explode(";", $matLevels);
      $sellCount = count($idxArr);
      /*
      for ( $idx = 0; $idx<9; $idx++ ) {
        $val = $_REQUEST["matPId".$idx];
        if ( !is_null($val) && $val >= 0 ) {
          $idxArr[$sellCount++] = $val;
        }
      }
      */

      $query = sprintf("select privateId, weaponId, exp from frdHavingWeapons where ");


      $redis = openRedis();
      if ( $redis == false ) {
        echo 0;
        $db->close();
        return;
      }

      for ( $idx=0; $idx<$sellCount; $idx++ ) {
        if ( $idx < $sellCount-1 )
          $query .= sprintf("(privateId='%s' and userId='%s') or ", $idxArr[$idx], $id);
        else
          $query .= sprintf("(privateId='%s' and userId='%s')", $idxArr[$idx], $id);
      }
      $query .= " ORDER BY privateId ASC";
      $sres = $db->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      if ($sres->num_rows !== $sellCount ) {
        addBlacklist($id, "sell_weapon_dontMatchCount");
        echo 1;
        $db->close();
        $redis->close();
        return;
      }

      if ( $isExtraction <= 0 ) {

        $subGold = 0;
        for ($idx = 0; $idx < $sres->num_rows; $idx++) {
          $sres->data_seek($idx);
          $srow = $sres->fetch_assoc();
          $val = GetWeaponPrice($redis, (int)$srow["weaponId"], (int)$levelArr[$idx], (int)$srow["exp"]);

          if ( $val < 0 ) {
            addBlacklist($id, "sell_weapon_dontMatchLevel");
            echo 1;
            $db->close();
            $redis->close();
            return;
          }
          else {
            $subGold += $val;
          }
        }
        $sres->close();

        for ( $idx=0; $idx<$sellCount; $idx++ ) {
          $query = sprintf("delete from frdHavingWeapons where privateId='%s' and userId='%s'", $idxArr[$idx], $id);
          $isGood = $db->query($query);
          if ($isGood == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }
        }

        $resultGold = $row["gold"]+$subGold;

        $query = sprintf("update frdUserData set gold=%d, session=%d where privateId='%s'", $resultGold, $newSession, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $data["gold"] = $resultGold;

      }
      else {    //extraction

        $amountPercent = $redis->get('weaponPowderValue');
        $addPowder = 0;
        for ($idx = 0; $idx < $sres->num_rows; $idx++) {
          $sres->data_seek($idx);
          $srow = $sres->fetch_assoc();

          $grade = GetWeaponGrade((int)$srow["weaponId"]);
          $amountValue = $redis->lindex('weaponPowder', $grade);
          $query = sprintf("select val from frdEffectForEtc where userId = %d and type=%d", $id, $GLOBALS['$ABILL_Bonus_Extraction']);
          $ssres = $db->query($query);
          if ( $ssres == false ) {
            echo 0;$db->close();return;
          }
          if ( $ssres->num_rows > 0 ) {
            $ssrow = $ssres->fetch_assoc();
            $percent = $ssrow["val"] / 100000;
            $amountValue += round($amountValue * $percent);
          }

          $min = $amountValue - ceil($amountValue*$amountPercent/100);
          $max = $amountValue + (int)($amountValue*$amountPercent/100);
          $resultAmount = rand($min, $max);
          $addPowder +=  $resultAmount;

        }
        $sres->close();

        for ( $idx=0; $idx<$sellCount; $idx++ ) {
          $query = sprintf("delete from frdHavingWeapons where privateId='%s' and userId='%s'", $idxArr[$idx], $id);
          $isGood = $db->query($query);
          if ($isGood == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }
        }

        $resultPowder = $row["powder"]+$addPowder;

        $query = sprintf("update frdUserData set powder=%d, session=%d where privateId='%s'", $resultPowder, $newSession, $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }

        $data["powder"] = $resultPowder;

      }
      $redis->close();
    }
    else {
      addBlacklist($id, "sell_weapon");
      echo 1;
      $db->close();
      return;
    }

  }
  $res->close();
  $db->query("commit");
  $db->close();

  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
