<?php
  // 유물 강화 처리 요청.
  include_once("./_calculate2.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];
  $deskPId = $_REQUEST["deskPId"];

  $addExp = $_REQUEST["addExp"];
  $subGold = $_REQUEST["subGold"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }
  $db->query("set autocommit=0");

  $query = sprintf("select gold, session from frdUserData where privateId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;


      $redis = openRedis();
      if ( $redis == false ) {
        echo 0;
        $db->close();
        return;
      }

      
      
      $sellCount =0;
      for ( $idx = 0; $idx<8; $idx++ ) {
        $val = (int)$_REQUEST["matPId".$idx];
        if ( $val >= 0 ) {
          $idxArr[$sellCount++] = (int)$val;
        }
      }

      $query = sprintf("select privateId, weaponId, exp from frdHavingWeapons where ");
      for ( $idx=0; $idx<$sellCount; $idx++ ) {
        if ( $idx < $sellCount-1 )
          $query .= sprintf("(privateId=%d and userId=%d) or ", $idxArr[$idx], $id);
        else
          $query .= sprintf("(privateId=%d and userId=%d)", $idxArr[$idx], $id);
      }
      $query .= " ORDER BY privateId ASC";
      $sres = $db->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      if ($sres->num_rows !== $sellCount ) {
        addBlacklist($id, "lvUp_weapon_dontMatchCount");
        echo 1;
        $db->close();
        $redis->close();
        return;
      } 

      $subGold = 0;
      $addExp = 0;
      for ($idx = 0; $idx < $sres->num_rows; $idx++) {
        $sres->data_seek($idx);
        $srow = $sres->fetch_assoc();
        $grade = GetWeaponGrade((int)$srow["weaponId"]);
        $level = FindWeaponLevel(50, $redis, 0, GetMaxLevel($grade), (int)$srow["exp"]);
        $price = GetLevelUpMatPrice($redis, $grade, $level);
        
        $subGold += $price;

        $subGrade[] = $grade;
        $subLevel[] = $level;
        $subPrices[] = $price;
        $addExp += GetLevelUpMatExp($redis, (int)$srow["weaponId"], $grade, $level);
      }
  

      $resultGold = $row["gold"]-$subGold;
      if ( $resultGold < 0 ) {
        $str = "levelup_weapon_noMoney_".$row["gold"]."_".$subGold;
        for ( $i=0; $i<count($subPrices); $i++ ) {
          $str.= "_(".$subPrices[$i].",".$subGrade[$i].",".$subLevel[$i].")";
        }
        addBlacklist($id, $str);
        echo 1;
        $db->close();
        $redis->close();
        return;
      }

      for ($idx = 0; $idx < $sres->num_rows; $idx++) {
        $query = sprintf("delete from frdHavingWeapons where privateId=%d and userId=%d", $idxArr[$idx], $id);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo 0;
          $db->query("rollback");
          $db->close();
          return;
        }
      }

      $sres->close();

      $query = sprintf("update frdHavingWeapons set exp=exp+%d where privateId='%s' and userId='%s'",$addExp, $deskPId, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      $query = sprintf("select exp from frdHavingWeapons where privateId='%s' and userId='%s'", $deskPId, $id);
      $sres = $db->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      if ($sres->num_rows > 0) {
        $row = $sres->fetch_assoc();
        $data["exp"] = $row["exp"];
        $sres->close();
      }
      

      $query = sprintf("update frdUserData set gold=%d, session=%d where privateId='%s'", $resultGold, $newSession, $id);
      $isGood = $db->query($query); 
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      $data["gold"] = $resultGold;
      $redis->close();
    }
    else {
      addBlacklist($id, "levelup_weapon_session");
      echo 1;
      $db->close();
      return;
    }
  }
  $res->close();
  $db->query("commit");
  $db->close();
  
  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
