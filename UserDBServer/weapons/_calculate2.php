<?php
include_once("../myAes.php");
	function FindWeaponLevel($count, $redis, $min, $max, $exp) {
		$count--;
		if ( $count <= 0 )
		  return 0;

		if ( $min == $max )
		  return $min;


		$center = (int)(($min+$max)>>1);

		$lowExp = $redis->lindex('weaponNeedExpAccu', $center);
		$highExp = $redis->lindex('weaponNeedExpAccu', $center+1);
		//  echo($lowExp." ".$highExp." ".$exp." \n");
		if ( $exp < $lowExp ) {
		  return FindWeaponLevel($count, $redis, $min, $center, $exp);
		} 
		else if ( $exp >= $highExp ) {
		  return FindWeaponLevel($count, $redis, $center+1, $max, $exp);
		}
		else {  //find
		  return $center+1;
		}
	}

	function GetWeaponGrade($privateId) {
		switch($privateId) {
		case 498:
			return 1;
		case 497:
			return 2;
		case 496:
			return 3;
		case 495:
			return 4;
		default:
			if ( $privateId < 10 )
				return 1;
			else if ( $privateId < 50 )
				return 2;
			else if ( $privateId < 100 )
				return 3;
			else if ( $privateId < 200 )
				return 4;
			else if ( $privateId < 300 )
				return 5;
			else
				return 6;
		}
	}

	function GetMaxLevel($grade) {
		switch($grade) {
		case 1:
			return 4;
		case 2:
			return 9;
		case 3:
			return 19;
		case 4:
			return 29;
		case 5:
			return 39;
		default:
			return 49;
		}
	}

	function GetSellPrice($redis, $deskGrade, $deskLevel) {
		$sum = (int)$redis->lindex('weaponValue', $deskGrade);
		$percent = (int)$redis->get('weaponSellValue');

	//	echo "grade = ".$deskGrade.", value = ".$sum;
		$sum += (((int)$redis->lindex('weaponPriceAccu', $deskLevel) * $percent)/100);

		return (int)$sum;
	}


	function GetWeaponPrice($redis, $weaponId, $level, $exp) {
		if ( $level <= 1)
			$needExp = 0;
		else
			$needExp = $redis->lindex('weaponNeedExpAccu', $level-2);

		$grade = GetWeaponGrade($weaponId);
		if ( $exp < $needExp ) 
			return -1;

		return GetSellPrice($redis, $grade, $level);
	}

	function GetLevelUpMatPrice($redis, $deskGrade, $level) {
		$sum = (int)$redis->lindex('weaponValue', $deskGrade);
		if ( $level > 0 )
			$sum += (int)$redis->lindex('weaponPrice', ($level));
		return $sum;
	}
	
	function GetLevelUpMatExp($redis, $matId, $matGrade, $matLevel) {
		$sum = (int)$redis->lindex('weaponValue', $matGrade);
		$sum += (int)$redis->lindex('weaponNeedExpAccu', $matLevel);

		$sum = (int)round($sum * (int)$redis->get('weaponExpValue') / 100);
		if ( $matId >= 490 )
			$sum = (int)round($sum*1.5);
		return $sum;
	}

?>