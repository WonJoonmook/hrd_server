<?php
  include_once("../myAes.php");

  $data = array();
  $data["error"] = 0;

  $redis = openRedis();
  if ( $redis == false ) {
    echo 0;
    return;
  }
  else {
    $data["weaponSellValue"] = $redis->get('weaponSellValue');
    $data["weaponExpValue"] = $redis->get('weaponExpValue');
    $data["weaponPowderValue"] = $redis->get('weaponPowderValue');

    $data["weaponPowder"] = $redis->lrange('weaponPowder',0, -1);
    $data["weaponMinIdx"] = $redis->lrange('weaponMinIdx',0, -1);
    $data["weaponMaxIdx"] = $redis->lrange('weaponMaxIdx', 0, -1);
    $data["weaponValue"] = $redis->lrange('weaponValue',0, -1);
    $data["weaponPrice"] = $redis->lrange('weaponPrice', 0, -1);
    $data["weaponNeedExp"] = $redis->lrange('weaponNeedExp', 0 , -1);
    $data["weaponStat"] = $redis->lrange('weaponStat', 0 , -1);
    $redis->close();
  }
  
  echo json_encode($data);
?>
