<?php
  // 유물 추출 요청. 추출시 유물이 없어지면서 마법가루 x~x개 사이를 얻습니다.
  include_once("../globalDefine.php");
  include_once("./_calculate10_24.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];
  $objectIdx = $_REQUEST["objectIdx"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
    echo 0;
    return;
  }
  $db->query("set autocommit=0");
  $query = sprintf("select powder, session from frdUserData where privateId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;

      $query = sprintf("select * from frdHavingWeapons where privateId='%s' and userId='%s' ", $objectIdx, $id);
      $sres = $db->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      if ($sres->num_rows < 1 ) {
        addBlacklist($id, "extract_weapon_noWeapon");
        echo 1;
        $db->close();
        return;
      }

      $srow = $sres->fetch_assoc();
      $grade = GetWeaponGrade($srow["weaponId"]);
      $sres->close();

      $redis = openRedis();
      if ( $redis == false ) {
        echo 0;$db->close();return;
      }

      $amountValue = $redis->lindex('weaponPowder', $grade);
      $amountPercent = $redis->get('weaponPowderValue');

      $query = sprintf("select val from frdEffectForEtc where userId = %d and type=%d", $id, $GLOBALS['$ABILL_Bonus_Extraction']);
      $sres = $db->query($query);
      if ( $sres == false ) {
        echo 0;$db->close();return;
      }
      if ( $sres->num_rows > 0 ) {
        $srow = $sres->fetch_assoc();
        $percent = $srow["val"] / 100000;
        $amountValue += round($amountValue * $percent);
      }

      $min = $amountValue - ceil($amountValue*$amountPercent/100);
      $max = $amountValue + (int)($amountValue*$amountPercent/100);

      $resultAmount = rand($min, $max) + $row["powder"];

      $data["powder"] = $resultAmount;
      $query = sprintf("update frdUserData set powder=%d, session=%d where privateId='%s'", $resultAmount, $newSession, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      $query = sprintf("delete from frdHavingWeapons where privateId=%d and userId='%s'", $objectIdx, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }
      $redis->close();
    }
    else {
      addBlacklist($id, "extraction_weapon_session");
      echo 1;
      $db->close();
      return;
    }
  }
  $res->close();
  $db->query("commit");
  $db->close();

  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
