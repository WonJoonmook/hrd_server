<?php
  // 유물 진화 처리 요청.
  include_once("./_calculate10_24.php");
  $session = $_REQUEST["session"];
  $id = $_REQUEST["id"];
  $resultId = $_REQUEST["resultId"];

  $data = array();
  $data["error"] = 0;
  $db = getDB();
  if (mysqli_connect_errno()) {
      echo 0;
      return;
  }

  $db->query("set autocommit=0");
  $query = sprintf("select weaponIdPointer, session from frdUserData where privateId = '%s'", $id);
  $res = $db->query($query);
  if ($res == false) {
    echo 0;
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    if ( $row["session"] == $session ) {
      $newSession = mt_rand();
      $data["session"] = $newSession;

      $redis = openRedis();
      if ( $redis == false ) {
        echo 0;
        $db->close();
        return;
      }


      $sellCount =0;
      for ( $idx = 0; $idx<4; $idx++ ) {
        $val = $_REQUEST["matPId".$idx];
        if ( $val >= 0 ) {
          $idxArr[$sellCount++] = $val;
        }
      }

      $query = sprintf("select privateId, weaponId, exp from frdHavingWeapons where ");
      for ( $idx=0; $idx<$sellCount; $idx++ ) {
        if ( $idx < $sellCount-1 )
          $query .= sprintf("(privateId='%s' and userId='%s') or ", $idxArr[$idx], $id);
        else
          $query .= sprintf("(privateId='%s' and userId='%s')", $idxArr[$idx], $id);
      }
      $query .= " ORDER BY privateId ASC";
      $sres = $db->query($query);
      if ($sres == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      if ($sres->num_rows !== $sellCount ) {
        addBlacklist($id, "gradeup_weapon_dontMatchCount");
        echo 1;
        $db->close();
        $redis->close();
        return;
      }


      for ($idx = 0; $idx < $sres->num_rows; $idx++) {
        $sres->data_seek($idx);
        $srow = $sres->fetch_assoc();
        $grade = GetWeaponGrade((int)$srow["weaponId"]);
        $needExp = $redis->lindex('weaponNeedExpAccu', GetMaxLevel($grade)-1);
      //  echo $needExp." ".(int)$srow["exp"].'\n';

        if ( (int)$srow["exp"] < $needExp  ) {
          addBlacklist($id, "gradeup_weapon_dontMatchLevel");
          echo 1;
          $db->close();
          $redis->close();
          return;
        }
        else {
          $query = sprintf("delete from frdHavingWeapons where privateId='%s' and userId='%s'", $idxArr[$idx], $id);
          $isGood = $db->query($query);
          if ($isGood == false) {
            echo 0;
            $db->query("rollback");
            $db->close();
            return;
          }
        }
      }
      $sres->close();



      $stat = rand(0,10000);
      $resultWeaponIdPointer = $row["weaponIdPointer"];
      $query = sprintf("insert into frdHavingWeapons values (%d, '%s', %d, 0, %d)", $resultWeaponIdPointer, $id, $resultId, $stat);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }

      $query = sprintf("update frdUserData set weaponIdPointer=%d, session=%d where privateId='%s'", $resultWeaponIdPointer+1, $newSession, $id);
      $isGood = $db->query($query);
      if ($isGood == false) {
        echo 0;
        $db->query("rollback");
        $db->close();
        return;
      }



      $data["resultIdx"] = $resultWeaponIdPointer;
      $data["resultId"] = $resultId;
      $data["stat"] = $stat;

      $redis->close();
    }
    else {
      addBlacklist($id, "gradeup_weapon_session");
      echo 1;
      $db->close();
      return;
    }
  }
  $res->close();
  $db->query("commit");
  $db->close();

  $keyAndIv = formatTo16String($session);
  echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
