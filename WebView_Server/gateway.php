<?php
date_default_timezone_set('Asia/Seoul');

require_once __DIR__ . "/lib/JsonParserTest.php";
require_once __DIR__ . "/common/Controller.php";
require_once __DIR__ . "/common/Config.php";

# json parser
$getPostData = file_get_contents("php://input");

$date = date('Y-m-d H:i:s');

$jsonP = new JsonParser();
$isJson = $jsonP -> isJson($getPostData);
if (!$isJson) {
    error_log($_SERVER['REQUEST_URI'] . ' failed while parsing parameters');
    header("HTTP/1.0 400 Bad Request");
    // send 404 if parameter failed
    echo "<h1>400 Bad Request</h1>\n";
    echo "Error occured while parsing parameters";
    return;
}
$requestData = $jsonP -> parser($getPostData);
error_log("{$date}| {$getPostData}" . "\n", 3, "/home/WV_TEST/request_" . date('Ymd') . '.log');

# controller logic process
$controller = new Controller();
$resultData = $controller -> process($requestData);
# json encode
$resultJson = $jsonP -> encode($resultData);

error_log("{$date}| {$resultJson}" . "\n", 3, "/home/WV_TEST/response_" . date('Ymd') . '.log');

echo $resultJson;
?>
