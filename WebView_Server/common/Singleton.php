<?php

/**
 * @codeCoverageIgnore
 */

abstract class Singleton
{
	protected function __construct()	{}
	
	final public static function getInstance($class, $args = null /* , ... */)
	{
		static $instances = array();
		
		if (2 < func_num_args()) {
			$args = func_get_args();
			array_shift($args);
		}

		if (!isset($instances[$class])) {
			$object = $args ? new $class($args) : new $class();
			return $instances[$class] = $object;
		} else {
			return $instances[$class];
		}
	}
	
	final private function __clone() {}
	final private function __sleep() {}
};

?>