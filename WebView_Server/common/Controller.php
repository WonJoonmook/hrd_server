<?php
/*error_reporting(E_ALL);
 ini_set("display_errors", 1);*/
require_once './lib/Performance.php';
class Controller {
    public function __construct() {
    }

    public function write_log_to_file($cat, $mes) {

		$date = date('Y-m-d H:i:s');
		error_log("{$date}| {$cat} | {$mes}" . "\n", 3, "/home/WV_TEST/perfor_" . date('Ymd') . '.log');

    }

    public function process($param) {

		$date = date('Y-m-d H:i:s');
        // If Critical Problem in Server then use  this source

        /* DB OR REDIS DOWN!!
         $ReturnName = $ApiName;
         $result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
         $result['ResultCode'] = 1200; //FAIL CODE
         return $result;
         */
        $Perfor = new Performance();
        $Perfor -> timer_start('process');

        $ApiResult = null;

        /* Ready for API Send */
        if (!isset($param['Protocol'])) {
            $result['ResultCode'] = 1100;
            //FAIL CODE
			$mes = "error first";
			error_log("{$date}|{$mes}" . "\n", 3, "/home/WV_TEST/request_" . date('Ymd') . '.log');
            return $result;
        } else {
			$ApiName = $param['Protocol'];
		}

		$Data 	= $param['Data'];
		$userId = $Data['userId'];
		$GLOBALS['AccessNo'] = substr($userId, 0, 1);

        // API CALL
        switch ($ApiName) {

            case 'ReqUserWebView' :
                include_once "./api/UserWebViewApi.php";
                $Api = new UserWebViewApi();
                $ApiResult = $Api -> UserWebView($param);
                break;
            case 'ReqUserReceive' :
                include_once "./api/UserWebViewApi.php";
                $Api = new UserWebViewApi();
                $ApiResult = $Api -> UserReceive($param);
                break;

            default :
                $result['ResultCode'] = 1000;
                //FAIL CODE
                $ReturnName = $ApiName;
                $result['Protocol'] = str_replace('Req', 'Res', $ReturnName);
                return $result;
        }

        $per_log = $Perfor -> timer_stop('process');
        Controller::write_log_to_file($ApiName, $per_log['process']);
        return $ApiResult;
    }

}
