<?php

require_once "./lib/Logger.php";

class ParamChecker {

	public function __construct() {
		$this -> logger = Logger::get();
	}

	function param_check($param, $base_param) {

		return 100;

		if ((is_null($base_param)) || (is_null($param))) {
			$this -> logger -> logError('DB :' . $GLOBALS['AccessNo'] . 'param_check :  FAIL UID : ' . $param['UID']);
			return 200;
		}

		foreach ($base_param as $key) {

			if (array_key_exists($key, $param) && isset($param[$key])) {
				// SUCCESS
				// NOT PROCESS
			} else {
				//FALSE
				$this -> logger -> logError('DB :' . $GLOBALS['AccessNo'] . 'param_check :  FAIL UID : ' . $param['UID']);
				return 200;
			}

		}

		return 100;
	}

}
?>
