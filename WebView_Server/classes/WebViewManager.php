<?php
require_once './lib/Logger.php';
include_once './common/DB_Info.php';
include_once './common/DB.php';

class WebViewManager {
    public function __construct() {
        $this -> logger = Logger::get();
    }

	public function getUserWebView($param) {
    	$resultFail['ResultCode'] = 300;
		$Data = $param['Data'];
		$userId = $Data['userId'];

		// GAME DB 에 접속 하여 현재 유저의 지급 상황을 확인 한다
		// 보유 아이템 개tn 와 현재 지급 완료된 내역	
        $db = new DB();
        $sql = <<<SQL
			SELECT itemId, itemCount  FROM frdHavingItems 
			WHERE userId = :userId  AND itemId IN ( 190003, 190004, 190005 )
SQL;
        $db -> prepare($sql);
		$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> execute();
		$result = null;
		$Data = null;
		$Data['no190003Cnt'] = 0;
		$Data['no190004Cnt'] = 0;
		$Data['no190005Cnt'] = 0;
        while ($row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
			switch($row['itemId']) {
				case 190003:
					$Data['no190003Cnt'] = $row['itemCount'];
					break;
				case 190004:
					$Data['no190004Cnt'] = $row['itemCount'];
					break;
				case 190005:
					$Data['no190005Cnt'] = $row['itemCount'];
					break;
				default : 
					// error
					// 기록할것인지 정하라
					break;
			}
		}
		$Data['userId'] = $userId;
		$result['Data'] = $Data;
		return $result;
	}

    public function setUserReceive($param) {
    	$resultFail['ResultCode'] = 300;
		$Data = $param['Data'];


		$userId = $Data['userId'];
		$type 	= $Data['type'];

		// DEFAULT VALUE SETTING FOR ERROR CHECK
		$itemId 	= 0;
		$needCnt 	= 0;
		$addRune 	= 0; 

		switch ($type) {
			######### Gold
			case 1: 
				$itemId 	= 190003;
				$needCnt 	= 10;
				$add 		= 5000;
				$postType 	= 5000;
				break;
			case 2: 
				$itemId 	= 190003;
				$needCnt 	= 30;
				$add 		= 20000;
				$postType 	= 5000;
				break;
			case 3: 
				$itemId 	= 190003;
				$needCnt 	= 50;
				$add 		= 40000;
				$postType 	= 5000;
				break;
			######### Jewel
			case 4: 
				$itemId 	= 190004;
				$needCnt 	= 5;
				$add 		= 10;
				$postType 	= 5004;
				break;
			case 5: 
				$itemId 	= 190004;
				$needCnt 	= 10;
				$add 		= 30;
				$postType 	= 5004;
				break;
			case 6: 
				$itemId 	= 190004;
				$needCnt 	= 30;
				$add 		= 100;
				$postType 	= 5004;
				break;
			######### Rune
			case 7: 
				$itemId 	= 190005;
				$needCnt 	= 2;
				$add 		= 1;
				$postType 	= 115000 + rand(0,4);
				break;
			case 8: 
				$itemId 	= 190005;
				$needCnt 	= 5;
				$add 		= 3;
				$postType 	= 115000 + rand(0,4);
				break;
			case 9: 
				$itemId 	= 190005;
				$needCnt 	= 10;
				$add	 	= 3; // Rainbow Rune
				$postType 	= 110000;
				break;
			default:
				break;
		}

		if ($itemId == 0 ) {
			// error 	
			// log
			return $resultFail;			
		}

		// 게임 디비 에서 유저의 보유 상황을 확인하여검증후 지급 절차를 진행 한다.
        $db = new DB();
        $sql = <<<SQL
			SELECT itemCount  FROM frdHavingItems Where userId = :userId AND itemId = :itemId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> bindValue(':itemId',   $itemId, PDO::PARAM_INT);
        $db -> execute();
        $row = $db -> fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT);
		if ($row) {
			$myCount = $row['itemCount'];
		} else {
			$result = null;
			$result['ResultCode'] = 101;
			$this -> logger -> logError('setUserReceive : SELECT  FAIL frdUserPost userId.' . $userId. 'itemId : '.$itemId. 'sql: '.$sql);
			return $result;
		}

		if ($myCount < $needCnt) {
			$result['ResultCode'] = 303;
			return $result;			
		}
		$CurCnt = $myCount - $needCnt;

		$time = time();
		if ($type == 7 || $type == 8 ) {
			$sql = <<<SQL
INSERT INTO frdUserPost ( recvUserId, sendUserId, type, count, time) VALUES
( :userId, 0, 115000, :count, :time),
( :userId, 0, 115001, :count, :time),
( :userId, 0, 115002, :count, :time),
( :userId, 0, 115003, :count, :time),
( :userId, 0, 115004, :count, :time);
SQL;
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':count',  $add, PDO::PARAM_INT);
			$db -> bindValue(':time',  $time, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('setUserReceive : INSERT FAIL frdUserPost userId.' . $userId);
				$result['ResultCode'] = 300;
				return $result;
			}
		} else {
		// 우편 지급
			$sql = <<<SQL
INSERT INTO frdUserPost ( recvUserId, sendUserId, type, count, time)
VALUES( :userId, 0, :type, :count, :time)
SQL;
			$db -> prepare($sql);
			$db -> bindValue(':userId', $userId, PDO::PARAM_INT);
			$db -> bindValue(':type',   $postType, PDO::PARAM_INT);
			$db -> bindValue(':count',  $add, PDO::PARAM_INT);
			$db -> bindValue(':time',  $time, PDO::PARAM_INT);
			$row = $db -> execute();
			if (!isset($row) || is_null($row) || $row == 0) {
				$this -> logger -> logError('setUserReceive : INSERT FAIL frdUserPost userId.' . $userId);
				$result['ResultCode'] = 300;
				return $result;
			}
		}

		// 유저 디비에 아이템수량 차감
		$sql = <<<SQL
		UPDATE frdHavingItems SET itemCount = :CurCnt WHERE userId = :userId AND itemId = :itemId
SQL;
        $db -> prepare($sql);
        $db -> bindValue(':CurCnt', $CurCnt, PDO::PARAM_INT);
        $db -> bindValue(':userId', $userId, PDO::PARAM_INT);
        $db -> bindValue(':itemId',  $itemId, PDO::PARAM_INT);
        $row = $db -> execute();
        if (!isset($row) || is_null($row) || $row == 0) {
            $this -> logger -> logError('setUserReceive : UPDATE FAIL frdUserPost userId .' . $userId. ' itemId : '.$itemId);
            $result['ResultCode'] = 300;
            return $result;
        }

		$result = null;
    	$result['ResultCode'] = 100;
    	$result['type'] = $type;

        return $result;
    }

}
?>

