<?php

  $db[] = new mysqli("172.27.0.73", "root", "eoqkrskwk12", "ddookdak", 3306);
  if (mysqli_connect_errno())
    return ;

  $db[] = new mysqli("172.27.0.222", "root", "eoqkrskwk12", "ddookdak", 3306);
  if (mysqli_connect_errno())
    return ;

  $db[] = new mysqli("172.27.0.125", "root", "eoqkrskwk12", "ddookdak", 3306);
  if (mysqli_connect_errno())
    return ;

  $db[] = new mysqli("172.27.1.7", "root", "eoqkrskwk12", "ddookdak", 3306);
  if (mysqli_connect_errno())
    return ;


  function openRankRedis() {
      $redis = new Redis();
      try {
          $redis->connect('172.27.0.196', 6379, 2.5);//2.5 sec timeout
          return $redis;
      } catch (Exception $e) {
       //   exit( "Cannot connect to redis server : ".$e->getMessage() );
          return 0;
      }
  }

  do {
    sleep(1);
    $redis = openRankRedis();
  } while($redis == false);
  
  $rankData = $redis->zrevrange('climbRank_New', 0, -1);
  $redis->del('climbRank_New');
  $resourceData = $redis->zrevrange('climbRank_Resource', 0, -1);
  $redis->del('climbRank_Resource');
  $climbRankRewards = $redis->lrange('climbRankRewards',0, -1);
  $redis->zadd('climbRank_New', 2099999999, 'god');
  $redis->close();

  $ymd = date("ymd");
  $file = fopen("/home/redis/rankDatas/rankData".$ymd, "w");
  fwrite($file, json_encode($rankData)."\n\n".json_encode($resourceData));
  fclose($file);

  $fp = fopen("/home/www/climbTopLog/".date("ymd").".txt", 'w'); 


  $yearVal = ((int)date('y'))*100;
  $time = time() + 9*3600 - 24*3600*4-5*3600-3600;
  $totalWeeks = (int)($time / 604800);
  $weekVal = (int)600 + ((int)($totalWeeks%5));


  $rewardLength = count($climbRankRewards);
  $pointer=0;

  $length = count($rankData)-1;
  $pieces = explode (",", $climbRankRewards[0]);
  $lastRankk = (int) ($pieces[0] * $length * 0.0001);

  for ( $i=1; $i < $length; $i++ ) {
    if ( ($i&127)==0 ) {
      foreach ( $db as $dbs ) {
        $query = "select privateId from frdID where privateId = 1";
        $dbs->query($query);
      }
    }
    $name = $rankData[$i];


    $res;
    $findDB = null;
    $isOk = false;
    foreach ( $db as $dbs ) {
      $query = sprintf("select privateId from frdUserData where name = '%s'",$name);
      
      for ( $kk=0; $kk<10; $kk++ ) {
        $res = $dbs->query($query);
        if ( $res == false ) {
          fwrite($fp, $query."\n");
          fwrite($fp, "Error:".mysqli_error($dbs)."\n");
          continue;
        }
        $isOk = true;
        break;
      }

      if ($res->num_rows > 0) {
        $findDB = $dbs;
        break;
      }
    }
    if ( $isOk == false ) {
      fwrite($fp, "(".$i.", ".$name.") is cannot access!!\n");
      continue;
    }

    if ( $findDB !== null ) {
//      fwrite($fp, "start (".$i.", ".$name.")\n");


      $row = $res->fetch_assoc();
      $recvUserId = $row["privateId"];

      $count = count($pieces);
      $query = "insert into frdUserPost values ";
      for ( $j=1; $j<$count; $j+=2 ) {
        $rewardType = $pieces[$j];
        $rewardAmount = $pieces[$j+1];

        $query.= sprintf("(0, '%s', 3, %d, %d, %d), ", $recvUserId, $rewardType, $rewardAmount, time());
      }
      $query.= sprintf("(0, '%s', 3, 999, 0, %d)", $recvUserId, time());

      $isOk = false;
      for ( $kk=0; $kk<10; $kk++ ) {
        $isGood = $findDB->query($query);
        if ( $isGood == false ) {
          fwrite($fp, $query."\n");
          fwrite($fp, "Error:".mysqli_error($findDB)."\n");
          continue;
        }
        $isOk = true;
        break;
      }
      $res->close();
      if ( $isOk == false ) {
        fwrite($fp, "(".$i.", ".$name.") is cannot send post!!\n");
        continue;
      }

      $query = sprintf("select * from frdClimbTopData where userId = '%s'", $recvUserId);
      do {
        $res = $findDB->query($query);
      }while($res == false);
      if ($res->num_rows > 0) {
        $row = $res->fetch_assoc();

        $lastRank = $i;
        $playCount = $row["playCount"];
        $averageRank = ($row["averageRank"]*$playCount + $lastRank) / ($playCount+1);
        $avrgTotalUserCount = ($row["avrgTotalUserCount"]*$playCount + $length) / ($playCount+1);
        $playCount = (int)($playCount+1);

        if ( $lastRank < $row["bestRank"] ) {
          $bestRank = $lastRank;
          $bestTotalUserCount = $length;
          $query = sprintf("update frdClimbTopData set bestRank=%d, bestTotalUserCount=%d, playCount=%d, averageRank=%d, avrgTotalUserCount=%d, lastRank=%d, lastTotalUserCount=%d where userId = '%s'", 
                        $bestRank, $bestTotalUserCount, $playCount, $averageRank, $avrgTotalUserCount, $lastRank, $length, $recvUserId);
          do {
            $isGood = $findDB->query($query);
          }while($isGood == false);
        }
        else {
          $query = sprintf("update frdClimbTopData set playCount=%d, averageRank=%d, avrgTotalUserCount=%d, lastRank=%d, lastTotalUserCount=%d where userId = '%s'", 
                        $playCount, $averageRank, $avrgTotalUserCount, $lastRank, $length, $recvUserId);
          do {
            $isGood = $findDB->query($query);
          }while($isGood == false);
        }
      }
      else {
         $playCount = 1;
         $query = sprintf("insert into frdClimbTopData values ('%s', %d, %d, %d, %d, %d, %d, %d)",
                      $recvUserId, $i, $length, $playCount, $i, $length, $i, $length);

         $isOk = false;
         for ( $kk=0; $kk<10; $kk++ ) {
          $isGood = $findDB->query($query);
          if ( $isGood == false ) {
            fwrite($fp, $query."\n");
            fwrite($fp, "Error:".mysqli_error($findDB)."\n");
            continue;
          }
          $isOk = true;
          break;
         }
         if ( $isOk == false ) {
          fwrite($fp, "(".$i.", ".$name.") is cannot insert frdClimbTopData!!\n");
          continue;
         }
      }

      $year_count = $yearVal + $playCount;
      $query = sprintf("insert into frdClimbTopWeeklyData values (%d, %d, %d, %d, %d)",
                      $recvUserId, $year_count, $weekVal, $i, $length);
      $isOk = false;
      for ( $kk=0; $kk<10; $kk++ ) {
        $isGood = $findDB->query($query);
        if ( $isGood == false ) {
          fwrite($fp, $query."\n");
          fwrite($fp, "Error:".mysqli_error($findDB)."\n");
          continue;
        }
        $isOk = true;
        break;
      }
      if ( $isOk == false ) {
        fwrite($fp, "(".$i.", ".$name.") is cannot insert frdClimbTopWeeklyData!!\n");
        continue;
      }
    }
 //   findUserAndPresentReward($db, $name, $pieces, $i, $length);
    
    if ( $i > $lastRankk ) {
      $pointer++;
      if ( $pointer >=  $rewardLength )
        break;

      $pieces = explode (",", $climbRankRewards[$pointer]);
      $lastRankk = (int) ($pieces[0] * $length * 0.0001);
      fwrite($fp, $climbRankRewards[$pointer]."\n");
    }
    
  }

  fwrite($fp, "Finish!!\n");
  fclose($fp);

  foreach ($db as &$findDB)
    $findDB = null;
  unset($db);
  
  

?>