<?php
    // 기본 헤더 파일입니다. Aes128 암,복호화와 레디스, 디비 연결, 블랙리스트 처리 함수를 보유하고있습니다.
    function encrypt($key, $text, $iv) {

        $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $padding = $block - (strlen($text) % $block);
        $text .= str_repeat(chr($padding), $padding);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $text, MCRYPT_MODE_CBC, $iv);

        return base64_encode($crypttext);
    }

    function decrypt($key, $input, $iv) {
        $dectext = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($input), MCRYPT_MODE_CBC, $iv);
        return $dectext;
    }

    function formatTo16String($value) {

        $aDigitNum = intval( log10($value));
        
        $returnStr ;

        $zeroCount = 16 - $aDigitNum - 1;

        for ( $idx = 0; $idx<$zeroCount; $idx++ ) {
            $returnStr .= '0';
        }
        $returnStr .= $value;

        return $returnStr;
    }

    function addBlacklist($id, $informStr) {
        $db = new mysqli("localhost", "root", "eoqkrskwk12", "ddookdak", 21);
        if (mysqli_connect_errno()) {
        }
        else {
            $db->query("set autocommit=0");

            $query = sprintf("select * from frdBlacklist where id = '%s'", $id);
            $res = $db->query($query);
            if ($res == false) {
              echo 0;
              $db->query("rollback");
              $db->close();
              return 0;
            }

            if ( strncmp($informStr,"hack",4) == 0 )
                $amount = 1000;
            else
                $amount = 1;

            if ($res->num_rows > 0) {
                $row = $res->fetch_assoc();
                $informStr .= ", ".$row["informStr"];
                $query = sprintf("update frdBlacklist set count=count+%d, informStr='%s' where id='%s'", $amount, $informStr, $id);
                $isGood = $db->query($query);
                if ($isGood == false) {
                  echo 0;
                  $db->query("rollback");
                  $db->close();
                  return 0;
                }
            }
            else {
                $query = sprintf("insert into frdBlacklist values ('%s', %d, '%s')", $id, $amount, $informStr);
                $isGood = $db->query($query);
                if ($isGood == false) {
                  echo 0;
                  $db->query("rollback");
                  $db->close();
                  return 0;
                }
            }

            $res->close();
            $db->query("commit");
            $db->close();
            return 1;
        }
    }

    function getDB() {
        return new mysqli("localhost", "root", "eoqkrskwk12", "ddookdak", 3306);
    }

    function openRedis() {
        $redis = new Redis();
        try {
            $redis->connect('127.0.0.1', 6379, 2.5);//2.5 sec timeout
            //Auth Password(redis.conf requirepass)
            //$redis->auth('foobared');
            return $redis;
        } catch (Exception $e) {
         //   exit( "Cannot connect to redis server : ".$e->getMessage() );
            return 0;
        }
    }

    function openRankRedis($redis) {
        $rankRedis = new Redis();
        try {
            if ( $redis ) {
                $count = $redis->incr('counter');
                if ( $count >= 2 )
                    $count = 0;

                switch($count) {
                case 0:
                    $rankRedis->connect('172.27.0.196', 6379, 2.5);
                    break;
                case 1:
                //    $rankRedis->connect('172.27.0.77', 6379, 2.5);
                    $rankRedis->connect('172.27.0.77', 6379, 2.5);
                    break;
                default:
                    break;
                }
                $redis->set('counter', $count);
            }

            return $rankRedis;
        } 
        catch (Exception $e) {
         //   exit( "Cannot connect to redis server : ".$e->getMessage() );
            return 0;
        }
    }

?>