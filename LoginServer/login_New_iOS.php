<?php
  $id = $_REQUEST["id"];

  function encrypt($key, $text, $iv) {

      $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
      $padding = $block - (strlen($text) % $block);
      $text .= str_repeat(chr($padding), $padding);
      $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $text, MCRYPT_MODE_CBC, $iv);

      return base64_encode($crypttext);
  }

  function openRedis() {
      $redis = new Redis();
      try {
          $redis->connect('172.27.0.196', 6379, 2.5);//2.5 sec timeout
          // rank server because here is replication server
          return $redis;
      } catch (Exception $e) {
       //   exit( "Cannot connect to redis server : ".$e->getMessage() );
          return 0;
      }
  }

  if ( !is_null($_REQUEST["testId"]) && strlen ( $_REQUEST["testId"] ) > 5 )
    $id = $_REQUEST["testId"];
    
  $db = new mysqli("127.0.0.1", "root", "eoqkrskwk12", "ddookdak", 3306);
  if (mysqli_connect_errno()) {
      echo "mysql disconnect";
      return;
  }
  $db->query("set autocommit=0");

  $query = sprintf("select ip, useId, name from iOSUserData where id = '%s'",$id);
  $res = $db->query($query);
  if ($res == false) {
    echo $query.", ".mysqli_error($db);
    $db->query("rollback");
    $db->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    $resultIP = $row["ip"];
  }


  if ( $resultIP !== null ) {
    $data["ip"] = $resultIP;
    $data["id"] = $row["useId"];

    if ( $row["name"] == ' ' ) {
      switch($resultIP) {
        case "http://14.49.38.192:2351/hrd_server/UserDBServer/":
          $dataDb = new mysqli("172.27.0.73", "root", "eoqkrskwk12", "ddookdak", 3306);
          break;

        case "http://14.49.38.192:5551/hrd_server/UserDBServer/":
          $dataDb = new mysqli("172.27.0.222", "root", "eoqkrskwk12", "ddookdak", 3306);
          break;

        case "http://14.49.38.192:7351/hrd_server/UserDBServer/":
          $dataDb = new mysqli("172.27.0.125", "root", "eoqkrskwk12", "ddookdak", 3306);
          break;

        case "http://14.49.38.192:8351/hrd_server/UserDBServer/":
          $dataDb = new mysqli("172.27.1.7", "root", "eoqkrskwk12", "ddookdak", 3306);
          break;

        default:
          break;
      }
      if (mysqli_connect_errno()) {
        echo "mysql disconnect";
        $db->query("rollback");
        $db->close();
        return;
      }

      $query = sprintf("select name from frdUserData where privateId = %d", $row["useId"]);
      $sres = $dataDb->query($query);
      if ($sres == true) {
        $srow = $sres->fetch_assoc();
        $newName = $srow["name"];

        $query = sprintf("update iOSUserData set name='%s' where ip='%s' and useId = %d", $newName, $resultIP, $row["useId"]);
        $isGood = $db->query($query);
        if ($isGood == false) {
          echo $query.", ".mysqli_error($db);
          $db->query("rollback");
          $db->close();
          return;
        }
      }

      $query = sprintf("select privateId from frdID where privateId = '%s'",$row["useId"]);
      $sres = $dataDb->query($query);
      if ($sres == true) {
        if ($sres->num_rows <= 0 ) {
          $query = sprintf("insert into frdID values (%d, '%s')", $row["useId"], $id);
          $isGood = $dataDb->query($query);
          if ($isGood == false) {
            echo $query.", ".mysqli_error($db);
            $dataDb->query("rollback");
            $db->query("rollback");
            $dataDb->close();
            $db->close();
            return;
          }
        }
      }

    }

  }
  else {
    $redis = openRedis();
    if ( $redis <= 0 ) {
      echo "redis disconnect";
      $db->query("rollback");
      $db->close();
      return;
    }


    $counter = $redis->get('counter');
    switch($counter%4) {
      case 0:
        $ip = "http://14.49.38.192:2351/hrd_server/UserDBServer/";
        $dataDb = new mysqli("172.27.0.73", "root", "eoqkrskwk12", "ddookdak", 3306);
        break;
      case 1:
        $ip = "http://14.49.38.192:5551/hrd_server/UserDBServer/";
        $dataDb = new mysqli("172.27.0.222", "root", "eoqkrskwk12", "ddookdak", 3306);
        break;
      case 2:
        $ip = "http://14.49.38.192:7351/hrd_server/UserDBServer/";
        $dataDb = new mysqli("172.27.0.125", "root", "eoqkrskwk12", "ddookdak", 3306);
        break;
      default:
        $ip = "http://14.49.38.192:8351/hrd_server/UserDBServer/";
        $dataDb = new mysqli("172.27.1.7", "root", "eoqkrskwk12", "ddookdak", 3306);
        break;
    }

    $redis->INCR('counter');
    $redis->close();

    if (mysqli_connect_errno()) {
      echo $query.", ".mysqli_error($db);
      $db->query("rollback");
      $db->close();
      return;
    }




    $time = time();
    $query = sprintf("insert into frdUserData values (0, ' ', 0, 1, 0, 4000, 0, 0, 0, 0, 0, 0, 30, 0, 8, %d, 0, 0, 0, %d, 0, %d, 0)", $time - 86400, $time, $session);
    $isGood = $dataDb->query($query);
    if ($isGood == false) {
      echo $query.", ".mysqli_error($dataDb);
      $dataDb->query("rollback");
      $db->query("rollback");
      $db->close();
      $dataDb->close();
      return;
    }
    $newId = $dataDb->insert_id;

    $query = sprintf("insert into frdSkillPoints values (%d, 0)", $newId);
    $isGood = $dataDb->query($query);
    if ($isGood == false) {
      echo $query.", ".mysqli_error($dataDb);
      $dataDb->query("rollback");
      $db->query("rollback");
      $db->close();
      $dataDb->close();
      return;
    }

    $query = sprintf("insert into frdID values (%d, '%s')", $newId, $id);
    $isGood = $dataDb->query($query);
    if ($isGood == false) {
      echo $query.", ".mysqli_error($dataDb);
      $dataDb->query("rollback");
      $db->query("rollback");
      $db->close();
      $dataDb->close();
      return;
    }


    $query = sprintf("insert into iOSUserData values ('%s', '%s', %d, ' ')", $id, $ip, $newId);
    $isGood = $db->query($query);
    if ($isGood == false) {
      echo $query.", ".mysqli_error($db);
      $dataDb->query("rollback");
      $db->query("rollback");
      $db->close();
      $dataDb->close();
      return;
    }


    for ( $j=0; $j<9; $j++ ) {
      $temp = 0;
      for ( $idx=0; $idx<8; $idx++) {
        $temp <<= 8;
        $temp |= 10;
      }
      $query = sprintf("insert into frdCharacEvolveExps values (%d, %d, '%s')", $newId, $j, $temp);
      $isGood = $dataDb->query($query);
      if ($isGood == false) {
        echo $query.", ".mysqli_error($dataDb);
        $dataDb->query("rollback");
        $db->query("rollback");
        $dataDb->close();
        $db->close();
        return;
      }
    }

    $temp = 0;
    for ( $idx=0; $idx<3; $idx++) {
      $temp <<= 8;
      $temp |= 10;
    }
    $query = sprintf("insert into frdCharacEvolveExps values (%d, 9, '%s')", $newId, $temp);
    $isGood = $dataDb->query($query);
    if ($isGood == false) {
      echo $query.", ".mysqli_error($dataDb);
      $dataDb->query("rollback");
      $db->query("rollback");
      $dataDb->close();
      $db->close();
      return;
    }



    $data["id"] = $newId;
    $data["ip"] = $ip;

  }

  $data = encrypt( "1351252013512520", json_encode($data), "1351252013512520" );
  echo $data;

  if ( !is_null($dataDb) ) {
    $dataDb->query("commit");
    $dataDb->close();
  }
  $db->query("commit");
  $db->close();
?>
