<?php
  $idToken = $_REQUEST["idToken"];
  $email = $_REQUEST["email"];

  function getCurl($fUrl,$fMethod,$fParam) {
    $sUrl = $fUrl.(($fParam && strtolower($fMethod)=="get") ? "?$fParam": "");
    $sMethod = (strtolower($fMethod)=="get") ? "0" : "1" ;
    $sParam = (strtolower($fMethod)=="get") ? "" : $fParam ;

    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL,"$sUrl"); //접속할 URL 주소
    curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // 인증서 체크같은데 true 시 안되는 경우가 많다.
    curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    // default 값이 true 이기때문에 이부분을 조심 (https 접속시에 필요)
    curl_setopt ($ch, CURLOPT_SSLVERSION,4); // SSL 버젼 (https 접속시에 필요)
    curl_setopt ($ch, CURLOPT_HEADER, 0); // 헤더 출력 여부
    curl_setopt ($ch, CURLOPT_POST, $sMethod); // Post Get 접속 여부
    curl_setopt ($ch, CURLOPT_POSTFIELDS, "$fParam"); // Post 값  Get 방식처럼적는다.
    curl_setopt ($ch, CURLOPT_TIMEOUT, 30); // TimeOut 값
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); // 결과값을 받을것인지
    $result = curl_exec ($ch);
    curl_close ($ch);
    return $result;
  }

  function encrypt($key, $text, $iv) {

      $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
      $padding = $block - (strlen($text) % $block);
      $text .= str_repeat(chr($padding), $padding);
      $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $text, MCRYPT_MODE_CBC, $iv);

      return base64_encode($crypttext);
  }

  function openRedis() {
      $redis = new Redis();
      try {
          $redis->connect('127.0.0.1', 6379, 2.5);//2.5 sec timeout
          // rank server because here is replication server
          return $redis;
      } catch (Exception $e) {
       //   exit( "Cannot connect to redis server : ".$e->getMessage() );
          return 0;
      }
  }

  $id = $idToken;

  $resultIP = "http://47.90.93.231/hrd_server/UserDBServer_China/";

  $dataDb = new mysqli("127.0.0.1", "root", "eoqkrskwk12", "ddookdak", 3306);

  if (mysqli_connect_errno()) {
      echo mysqli_error($dataDb);
      return;
  }
  $dataDb->query("set autocommit=0");

  $query = sprintf("select name from frdUserData where privateId = %d", $id);
  $res = $dataDb->query($query);
  if ($res == false) {
    echo 0;
    $dataDb->close();
    return;
  }

  if ($res->num_rows > 0) {
    $row = $res->fetch_assoc();
    $newName = $row["name"];


    $data["ip"] = $resultIP;
    $data["id"] = $id;

  }
  else {

    $time = time();
    $query = sprintf("insert into frdUserData values (%d, ' ', 0, 1, 0, 4000, 0, 0, 0, 0, 0, 0, 30, 0, 8, %d, 0, 0, 0, %d, 0, %d, 0)", 
      $id, $time - 86400, $time, $session);
    $isGood = $dataDb->query($query);
    if ($isGood == false) {
      echo $query.", ".mysqli_error($dataDb);
      $dataDb->query("rollback");
      $dataDb->close();
      return;
    }
    $newId = $id;

    $query = sprintf("insert into frdSkillPoints values (%d, 0)", $newId);
    $isGood = $dataDb->query($query);
    if ($isGood == false) {
      echo 0;
      $dataDb->query("rollback");
      $dataDb->close();
      return;
    }


    for ( $j=0; $j<9; $j++ ) {
      $temp = 0;
      for ( $idx=0; $idx<8; $idx++) {
        $temp <<= 8;
        $temp |= 10;
      }
      $query = sprintf("insert into frdCharacEvolveExps values (%d, %d, '%s')", $newId, $j, $temp);
      $isGood = $dataDb->query($query);
      if ($isGood == false) {
        echo $query.", ".mysqli_error($dataDb);
        $dataDb->query("rollback");
        $dataDb->close();
        return;
      }
    }
    
    $temp = 0;
    for ( $idx=0; $idx<3; $idx++) {
      $temp <<= 8;
      $temp |= 10;
    }
    $query = sprintf("insert into frdCharacEvolveExps values (%d, 9, '%s')", $newId, $temp);
    $isGood = $dataDb->query($query);
    if ($isGood == false) {
      echo $query.", ".mysqli_error($dataDb);
      $dataDb->query("rollback");
      $db->query("rollback");
      $dataDb->close();
      $db->close();
      return;
    }

    $dataDb->query("commit");
    $dataDb->close();

    $data["id"] = $newId;
    $data["ip"] = $resultIP;

  }

  $data = encrypt( "1351252013512520", json_encode($data), "1351252013512520" );
  echo $data;
?>
