<?php
// 주문 시작시 트랜잭션 유지를 위한 키를 발급

include_once("../myAes.php");
$userId = $_REQUEST["userId"];
$session = $_REQUEST["session"];

$data = array();
$data["error"] = 0;
$db = getDB();
if (mysqli_connect_errno()) {
	echo 0;
	return;
}
$db->query("set autocommit=0");
$redis = openRedis();
if ( $redis == false ) {
	echo 0;
	$db->close();
	return;
}

$query = sprintf("INSERT INTO frdPayLoad (userId, RegDate ) VALUES (%d, now())", $userId);
$res = $db->query($query);
if ($res == false) {
	echo 0;
	$db->query("rollback");
	$db->close();
	return;
}

$PayID = $db->insert_id;

$query = "SELECT PayLoadID, userId, DATE_FORMAT(RegDate,'%m%d%k%i%s') AS regdate FROM frdPayLoad WHERE PayLoadID = $PayID AND userId = $userId";
$res = $db->query($query);
if ($res == false) {
	echo 0;
	$db->query("rollback");
	$db->close();
	return;
}

if ($res->num_rows > 0) {
	$row = $res->fetch_assoc();
	$PID = $row["PayLoadID"];
	$userId = $row["userId"];
	$regdate = $row["regdate"];

	$PayLoadID = $PID.$userId.$regdate;
} else {
	echo 0;
	$db->query("rollback");
	$db->close();
	return;
}

$res->close();
$db->query("commit");
$db->close();
$redis->close();

$data = null;
$data['PayLoadID'] = $PayLoadID;
$keyAndIv = formatTo16String($session);
echo encrypt( $keyAndIv, json_encode($data), $keyAndIv);
?>
